/* vim: set filetype=mysql: */
DROP TABLE `auditlog`;
CREATE TABLE `auditlog` (
	`id` INTEGER AUTO_INCREMENT NOT NULL COMMENT 'Primary Key for the log table.',
	`table` VARCHAR(32) NOT NULL COMMENT 'The table that has been changed',
	`row` VARCHAR(32) NOT NULL COMMENT 'The primary key row of the table that has been changed',
	`column` VARCHAR(32) NOT NULL COMMENT 'The column that has been changed',
	`action` enum('INSERT','UPDATE','DELETE') NOT NULL COMMENT 'The type of action on the column',
	`timestamp` TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL COMMENT 'The timestamp for the action',
	`old` VARCHAR(1024) COMMENT 'The old value for updates and deletes',
	`new` VARCHAR(1024) COMMENT 'The new value for updates and inserts',
	PRIMARY KEY `auditlog_pk` (`id`),
	KEY `timestamp_k` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;


-- COMMENT ON TABLE `auditlog` IS 'Logging table for the ULTRA database.';

SELECT TRIGGER_NAME, EVENT_MANIPULATION, EVENT_OBJECT_TABLE, ACTION_STATEMENT
    FROM INFORMATION_SCHEMA.TRIGGERS;

SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='profiles' AND TABLE_SCHEMA='seas';

SELECT * FROM information_schema.routines;
DROP TABLE `auditlog`;
-- May need to change group_concat_max_len to get the full trigger.
-- also check max_allowed_packet		
DROP PROCEDURE log_delete_pr;
DROP PROCEDURE log_insert_pr;
DROP PROCEDURE log_update_pr;
SET SESSION group_concat_max_len = 2048;

CREATE PROCEDURE log_delete_pr(IN tableName VARCHAR(255), IN pkcol VARCHAR(255), OUT tr VARCHAR(4096))
BEGIN
SELECT
	CONCAT('CREATE TRIGGER ', tableName,'_delete_tr AFTER DELETE ON ', tableName,'
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		',
	GROUP_CONCAT(
		CONCAT(
			'INSERT INTO `auditlog` VALUES(null,''',tableName,''',OLD.',pkcol,',''',column_name,''',''DELETE'',curtime,OLD.',column_name,',null);'
		)
		SEPARATOR ' 
		'
	), ' 
	END;'
	) INTO tr
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = database() AND TABLE_NAME = tableName;
END;

CREATE PROCEDURE log_insert_pr(IN tableName VARCHAR(255), IN pkcol VARCHAR(255), OUT tr VARCHAR(4096))
BEGIN
SELECT
	CONCAT('CREATE TRIGGER ', tableName,'_insert_tr AFTER INSERT ON ', tableName,'
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		',
	GROUP_CONCAT(
		CONCAT(
			'IF NEW.',column_name,' IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,''',tableName,''',NEW.',pkcol,',''',column_name,''',''INSERT'',curtime,null,NEW.',column_name,');
			END IF;'
			
		)
		SEPARATOR ' 
		'
	), ' 
	END;'
	) INTO tr
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = database() AND TABLE_NAME = tableName;
END;

CREATE PROCEDURE log_update_pr(IN tableName VARCHAR(255), IN pkcol VARCHAR(255), OUT tr VARCHAR(4096))
BEGIN
SELECT
	CONCAT('CREATE TRIGGER ', tableName,'_update_tr AFTER UPDATE ON ', tableName,'
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		',
	GROUP_CONCAT(
		CONCAT(
			'IF OLD.',column_name,' != NEW.',column_name,' THEN
				INSERT INTO `auditlog` VALUES(null,''',tableName,''',OLD.',pkcol,',''',column_name,''',''UPDATE'',curtime,OLD.',column_name,',NEW.',column_name,');
			END IF;'
		)
		SEPARATOR ' 
		'
	), ' 
	END;'
	) INTO tr
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = database() AND TABLE_NAME = tableName;
END;


CALL log_insert_pr('profiles','pid',@a);
CALL log_update_pr('profiles','pid',@b);
CALL log_delete_pr('profiles','pid',@c);
SELECT @a,@b,@c;


/*
DROP TRIGGER `profiles_insert_tr`;
DROP TRIGGER `profiles_update_tr`;
DROP TRIGGER `profiles_delete_tr`;
*/
CALL log_insert_pr('applications','pid',@a);
CALL log_update_pr('applications','pid',@b);
CALL log_delete_pr('applications','pid',@c);
SELECT @a,@b,@c;

/*
DROP TRIGGER `applications_insert_tr`;
DROP TRIGGER `applications_update_tr`;
DROP TRIGGER `applications_delete_tr`;
*/
CALL log_insert_pr('positions','id',@a);
CALL log_update_pr('positions','id',@b);
CALL log_delete_pr('positions','id',@c);
SELECT @a,@b,@c;

INSERT INTO `profiles` (pid,firstname,middlename,lastname,vtid,birthday,major,class,gender,ethnicity) VALUES ('wonder','Diana','','Prince','651432435','-886273200','106','GRADUATE','FEMALE','23');
DELETE FROM `profiles` WHERE `pid`='wonder' LIMIT 1;

SELECT * FROM `auditlog`;

CREATE VIEW `profiles_insert_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='pid',`new`,null)) AS "pid",
	MAX(IF(`column`='firstname',`new`,null)) AS "firstname",
	MAX(IF(`column`='middlename',`new`,null)) AS "middlename",
	MAX(IF(`column`='lastname',`new`,null)) AS "lastname",
	MAX(IF(`column`='birthday',`new`,null)) AS "birthday",
	MAX(IF(`column`='class',`new`,null)) AS "class",
	MAX(IF(`column`='major',`new`,null)) AS "major",
	MAX(IF(`column`='type',`new`,null)) AS "type",
	MAX(IF(`column`='gender',`new`,null)) AS "gender",
	MAX(IF(`column`='ethnicity',`new`,null)) AS "ethnicity",
	MAX(IF(`column`='trash',`new`,null)) AS "trash"
FROM `auditlog` WHERE `action`='INSERT' AND `table`='profiles' GROUP BY `timestamp`;

CREATE VIEW `profiles_delete_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='pid',`old`,null)) AS "pid",
	MAX(IF(`column`='firstname',`old`,null)) AS "firstname",
	MAX(IF(`column`='middlename',`old`,null)) AS "middlename",
	MAX(IF(`column`='lastname',`old`,null)) AS "lastname",
	MAX(IF(`column`='birthday',`old`,null)) AS "birthday",
	MAX(IF(`column`='class',`old`,null)) AS "class",
	MAX(IF(`column`='major',`old`,null)) AS "major",
	MAX(IF(`column`='type',`old`,null)) AS "type",
	MAX(IF(`column`='gender',`old`,null)) AS "gender",
	MAX(IF(`column`='ethnicity',`old`,null)) AS "ethnicity",
	MAX(IF(`column`='trash',`old`,null)) AS "trash"
FROM `auditlog` WHERE `action`='DELETE' AND `table`='profiles' GROUP BY `timestamp`;

CREATE VIEW `applications_insert_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='docid',`new`,null)) AS "docid",
	MAX(IF(`column`='id',`new`,null)) AS "id",
	MAX(IF(`column`='pid',`new`,null)) AS "pid",
	MAX(IF(`column`='modified',`new`,null)) AS "modified",
	MAX(IF(`column`='expire',`new`,null)) AS "expire",
	MAX(IF(`column`='trash',`new`,null)) AS "trash"
FROM `auditlog` WHERE `action`='INSERT' AND `table`='applications' GROUP BY `timestamp`;

CREATE VIEW `applications_delete_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='docid',`old`,null)) AS "docid",
	MAX(IF(`column`='id',`old`,null)) AS "id",
	MAX(IF(`column`='pid',`old`,null)) AS "pid",
	MAX(IF(`column`='modified',`old`,null)) AS "modified",
	MAX(IF(`column`='expire',`old`,null)) AS "expire",
	MAX(IF(`column`='trash',`old`,null)) AS "trash"
FROM `auditlog` WHERE `action`='DELETE' AND `table`='applications' GROUP BY `timestamp`;

CREATE VIEW `positions_delete_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='id',`old`,null)) AS "id",
	MAX(IF(`column`='job',`old`,null)) AS "job",
	MAX(IF(`column`='employee',`old`,null)) AS "employee",
	MAX(IF(`column`='paycode',`old`,null)) AS "paycode"
FROM `auditlog` WHERE `action`='DELETE' AND `table`='positions' GROUP BY `timestamp`;

CREATE VIEW `positions_insert_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='id',`new`,null)) AS "id",
	MAX(IF(`column`='job',`new`,null)) AS "job",
	MAX(IF(`column`='employee',`new`,null)) AS "employee",
	MAX(IF(`column`='paycode',`new`,null)) AS "paycode"
FROM `auditlog` WHERE `action`='INSERT' AND `table`='positions' GROUP BY `timestamp`;

CREATE VIEW `profiles_update_type_log` AS
SELECT
	`timestamp`,
	`row`,
	MAX(IF(`column`='type',`old`,null)) AS "type",
	MAX(IF(`column`='type',`new`,null)) AS "totype"
FROM `auditlog` WHERE `action`='UPDATE' AND `table`='profiles' AND `column`='type' GROUP BY `timestamp`;

