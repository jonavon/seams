/*DELIMITER ;;
CREATE TRIGGER positions_insert_tr AFTER INSERT ON positions
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF NEW.id IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'id','INSERT',curtime,null,NEW.id);
			END IF; 
		IF NEW.job IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'job','INSERT',curtime,null,NEW.job);
			END IF; 
		IF NEW.employee IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'employee','INSERT',curtime,null,NEW.employee);
			END IF; 
		IF NEW.paycode IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'paycode','INSERT',curtime,null,NEW.paycode);
			END IF; 
	END ;;
CREATE TRIGGER positions_update_tr AFTER UPDATE ON positions
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF OLD.id != NEW.id THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'id','UPDATE',curtime,OLD.id,NEW.id);
			END IF; 
		IF OLD.job != NEW.job THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'job','UPDATE',curtime,OLD.job,NEW.job);
			END IF; 
		IF OLD.employee != NEW.employee THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'employee','UPDATE',curtime,OLD.employee,NEW.employee);
			END IF; 
		IF OLD.paycode != NEW.paycode THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'paycode','UPDATE',curtime,OLD.paycode,NEW.paycode);
			END IF; 
	END ;;
CREATE TRIGGER positions_delete_tr AFTER DELETE ON positions
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'id','DELETE',curtime,OLD.id,null); 
		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'job','DELETE',curtime,OLD.job,null); 
		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'employee','DELETE',curtime,OLD.employee,null); 
		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'paycode','DELETE',curtime,OLD.paycode,null); 
	END ;;

DELIMITER ;


CREATE VIEW `profiles_delete_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='pid',`old`,null)) AS "pid",
	MAX(IF(`column`='firstname',`old`,null)) AS "firstname",
	MAX(IF(`column`='middlename',`old`,null)) AS "middlename",
	MAX(IF(`column`='lastname',`old`,null)) AS "lastname",
	MAX(IF(`column`='birthday',`old`,null)) AS "birthday",
	MAX(IF(`column`='class',`old`,null)) AS "class",
	MAX(IF(`column`='major',`old`,null)) AS "major",
	MAX(IF(`column`='type',`old`,null)) AS "type",
	MAX(IF(`column`='gender',`old`,null)) AS "gender",
	MAX(IF(`column`='ethnicity',`old`,null)) AS "ethnicity",
	MAX(IF(`column`='trash',`old`,null)) AS "trash"
FROM `auditlog` WHERE `action`='DELETE' AND `table`='profiles' GROUP BY `timestamp`;

CREATE VIEW `applications_insert_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='docid',`new`,null)) AS "docid",
	MAX(IF(`column`='id',`new`,null)) AS "id",
	MAX(IF(`column`='pid',`new`,null)) AS "pid",
	MAX(IF(`column`='modified',`new`,null)) AS "modified",
	MAX(IF(`column`='expire',`new`,null)) AS "expire",
	MAX(IF(`column`='trash',`new`,null)) AS "trash"
FROM `auditlog` WHERE `action`='INSERT' AND `table`='applications' GROUP BY `timestamp`;

CREATE VIEW `applications_delete_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='docid',`old`,null)) AS "docid",
	MAX(IF(`column`='id',`old`,null)) AS "id",
	MAX(IF(`column`='pid',`old`,null)) AS "pid",
	MAX(IF(`column`='modified',`old`,null)) AS "modified",
	MAX(IF(`column`='expire',`old`,null)) AS "expire",
	MAX(IF(`column`='trash',`old`,null)) AS "trash"
FROM `auditlog` WHERE `action`='DELETE' AND `table`='applications' GROUP BY `timestamp`;

CREATE VIEW `positions_delete_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='id',`old`,null)) AS "id",
	MAX(IF(`column`='job',`old`,null)) AS "job",
	MAX(IF(`column`='employee',`old`,null)) AS "employee",
	MAX(IF(`column`='paycode',`old`,null)) AS "paycode"
FROM `auditlog` WHERE `action`='DELETE' AND `table`='positions' GROUP BY `timestamp`;

CREATE VIEW `positions_insert_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='id',`new`,null)) AS "id",
	MAX(IF(`column`='job',`new`,null)) AS "job",
	MAX(IF(`column`='employee',`new`,null)) AS "employee",
	MAX(IF(`column`='paycode',`new`,null)) AS "paycode"
FROM `auditlog` WHERE `action`='INSERT' AND `table`='positions' GROUP BY `timestamp`;
*/

DROP VIEW `profiles_update_type_log`;

CREATE VIEW `profiles_update_type_log` AS
SELECT
	`timestamp`,
	`row`,
	MAX(IF(`column`='type',`old`,null)) AS "type",
	MAX(IF(`column`='type',`new`,null)) AS "totype"
FROM `auditlog` WHERE `action`='UPDATE' AND `table`='profiles' AND `column`='type' GROUP BY `timestamp`;

