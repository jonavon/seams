/* vi: set filetype=mysql : */

/*
 * VIEW: UNITS
 */
CREATE OR REPLACE VIEW `units` AS SELECT `id`,`label`,`description`,`sort` FROM enumtable WHERE `type`= 'UNIT' AND `trash` = 0;

/*
 * VIEW: MAJORS
 */
CREATE OR REPLACE VIEW `majors` AS SELECT `id`,`label`,`description`,`sort` FROM enumtable WHERE `type`= 'MAJOR' AND `trash` = 0;

/*
 * VIEW: ETHNICITIES
 */
CREATE OR REPLACE VIEW `ethnicities` AS SELECT `id`,`label`,`description`,`sort` FROM enumtable WHERE `type`= 'ETHNICITY' AND `trash` = 0;

/*
 * VIEW: PAYCODES
 */
CREATE OR REPLACE VIEW `paycodes` AS SELECT `id`,`label`,`description`,`sort` FROM enumtable WHERE `type`= 'PAYCODE' AND `trash` = 0;

/*
 * VIEW: PREFERENCES 
 */
CREATE OR REPLACE VIEW `preferences` AS SELECT `id`,`label`,`description`,`sort` FROM enumtable WHERE `type`= 'PREFERENCE' AND `trash` = 0;

/*
 * VIEW: SETTINGS_V
 */
CREATE OR REPLACE VIEW `settings_v` AS SELECT `settings`.*,`enumtable`.`label` AS `label` FROM `settings` JOIN `enumtable` ON `settings`.`type` = `enumtable`.`id` WHERE `enumtable`.`trash` = 0 ORDER BY `pid`,`label`,`sort`;

/*
 * VIEW: APPLICATIONS_V
 */
CREATE OR REPLACE VIEW `applications_v` AS
SELECT 
	`applications`.`docid` AS 'docid',
	`applications`.`id` AS 'id',
	`applications`.`pid` AS 'pid',
	CONCAT_WS(', ',`profiles`.`lastname`,`profiles`.`firstname`) AS 'name',
	UNIX_TIMESTAMP(`applications`.`modified`) AS 'modified',
	UNIX_TIMESTAMP(`applications`.`expire`) AS 'expire',
	COUNT(`pools`.`application`) AS 'pooled'
FROM `profiles`,`applications` LEFT OUTER JOIN `pools` ON `applications`.`id` = `pools`.`application` 
WHERE `applications`.`trash` = 0 AND `applications`.`pid` = `profiles`.`pid` GROUP BY `applications`.`pid`;

/*
 * VIEW: POOLS_V
 */
CREATE OR REPLACE VIEW `pools_v` AS SELECT `pools`.`id` AS `id`, `pools`.`job` AS `job`, `pools`.`application` AS `application`, `applications_v`.`pid` AS `pid`, `applications_v`.`name` AS `name`, `applications_v`.`modified` AS `modified`, `applications_v`.`expire` AS `expire`, `applications_v`.`pooled` AS `pooled` FROM `pools` JOIN `applications_v` ON `pools`.`application` = `applications_v`.`id`;

/*
 * VIEW: JOBS_V
 */
CREATE OR REPLACE VIEW `jobs_v` AS SELECT `jobs`.`id` AS `id`,`jobs`.`label` AS `label`,`jobs`.`description` AS `description`,`jobs`.`supervisor` AS `supervisor`,`jobs`.`trash` AS `trash`,COUNT(DISTINCT `positions`.`id`) AS `positions`,COUNT(DISTINCT `pools`.`id`) AS `candidates` FROM ((`jobs` LEFT JOIN `positions` ON((`jobs`.`id` = `positions`.`job`))) LEFT JOIN `pools` ON((`pools`.`job` = `positions`.`job`))) GROUP BY `jobs`.`id` ORDER BY `jobs`.`trash`,`jobs`.`id`;

/*
 * VIEW: PROFILE_V
 */
CREATE OR REPLACE VIEW `profiles_v` AS
SELECT
	`profiles`.`pid` AS `pid`, 
	`profiles`.`vtid` AS `vtid`,
	`profiles`.`firstname` AS `firstname`,
	`profiles`.`middlename` AS `middlename`,
	`profiles`.`lastname` AS `lastname`,
	FROM_UNIXTIME(`profiles`.`birthday`,'%Y-%m-%d') AS `birthday`,
	`profiles`.`class` AS `class`,
	`majors`.`label` AS `major`,
	`units`.`label` AS `unit`,
	`profiles`.`type` AS `type`,
	`profiles`.`gender` AS `gender`,
	`ethnicities`.`label` AS `ethnicity`
FROM `profiles`
	LEFT OUTER JOIN `majors` ON `profiles`.`major` = `majors`.`id`
	LEFT OUTER JOIN `units` ON `profiles`.`unit` = `units`.`id`
	LEFT OUTER JOIN `ethnicities` ON `profiles`.`ethnicity` = `ethnicities`.`id`;

/*
 * VIEW: ANSWERS_V
 */

/*
CREATE OR REPLACE VIEW `answers_v` AS
SELECT `applications`.`docid`,`answers`.`application`,
GROUP_CONCAT(if(`answers`.`question`='another_department',`answers`.`answer`,null)) AS 'another_department',
GROUP_CONCAT(if(`answers`.`question`='break_worker',`answers`.`answer`,null)) AS 'break_worker',
GROUP_CONCAT(if(`answers`.`question`='citizen',`answers`.`answer`,null)) AS 'citizen',
GROUP_CONCAT(if(`answers`.`question`='hours',`answers`.`answer`,null)) AS 'hours',
GROUP_CONCAT(if(`answers`.`question`='library_experience',`answers`.`answer`,null)) AS 'library_experience',
GROUP_CONCAT(if(`answers`.`question`='lift50',`answers`.`answer`,null)) AS 'lift50',
GROUP_CONCAT(if(`answers`.`question`='other_experience',`answers`.`answer`,null)) AS 'other_experience',
GROUP_CONCAT(if(`answers`.`question`='transportation',`answers`.`answer`,null)) AS 'transportation',
GROUP_CONCAT(if(`answers`.`question`='workstudy',`answers`.`answer`,null)) AS 'workstudy',
GROUP_CONCAT(if(`answers`.`question`='work_reference_0_name',`answers`.`answer`,null)) AS 'work_reference_0_name',
GROUP_CONCAT(if(`answers`.`question`='work_reference_0_phone',`answers`.`answer`,null)) AS 'work_reference_0_phone',
GROUP_CONCAT(if(`answers`.`question`='work_reference_0_notes',`answers`.`answer`,null)) AS 'work_reference_0_notes',
GROUP_CONCAT(if(`answers`.`question`='work_reference_1_name',`answers`.`answer`,null)) AS 'work_reference_1_name',
GROUP_CONCAT(if(`answers`.`question`='work_reference_1_phone',`answers`.`answer`,null)) AS 'work_reference_1_phone',
GROUP_CONCAT(if(`answers`.`question`='work_reference_1_notes',`answers`.`answer`,null)) AS 'work_reference_1_notes',
GROUP_CONCAT(if(`answers`.`question`='work_reference_2_name',`answers`.`answer`,null)) AS 'work_reference_2_name',
GROUP_CONCAT(if(`answers`.`question`='work_reference_2_phone',`answers`.`answer`,null)) AS 'work_reference_2_phone',
GROUP_CONCAT(if(`answers`.`question`='work_reference_2_notes',`answers`.`answer`,null)) AS 'work_reference_2_notes'
FROM `answers`,`applications` WHERE `answers`.`application` = `applications`.`id` AND `applications`.`expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP) GROUP BY `answers`.`application`;
*/

CREATE OR REPLACE VIEW `answers_v` AS
SELECT `applications`.`docid`,`applicationindex`.`application`,
GROUP_CONCAT(if(`applicationindex`.`question`='another_department',`applicationindex`.`answer`,null)) AS 'another_department',
GROUP_CONCAT(if(`applicationindex`.`question`='break_worker',`applicationindex`.`answer`,null)) AS 'break_worker',
GROUP_CONCAT(if(`applicationindex`.`question`='citizen',`applicationindex`.`answer`,null)) AS 'citizen',
GROUP_CONCAT(if(`applicationindex`.`question`='hours',`applicationindex`.`answer`,null)) AS 'hours',
GROUP_CONCAT(if(`applicationindex`.`question`='library_experience',`applicationindex`.`answer`,null)) AS 'library_experience',
GROUP_CONCAT(if(`applicationindex`.`question`='lift50',`applicationindex`.`answer`,null)) AS 'lift50',
GROUP_CONCAT(if(`applicationindex`.`question`='other_experience',`applicationindex`.`answer`,null)) AS 'other_experience',
GROUP_CONCAT(if(`applicationindex`.`question`='transportation',`applicationindex`.`answer`,null)) AS 'transportation',
GROUP_CONCAT(if(`applicationindex`.`question`='workstudy',`applicationindex`.`answer`,null)) AS 'workstudy',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_name',`applicationindex`.`answer`,null)) AS 'work_reference_0_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_phone',`applicationindex`.`answer`,null)) AS 'work_reference_0_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_notes',`applicationindex`.`answer`,null)) AS 'work_reference_0_notes',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_name',`applicationindex`.`answer`,null)) AS 'work_reference_1_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_phone',`applicationindex`.`answer`,null)) AS 'work_reference_1_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_notes',`applicationindex`.`answer`,null)) AS 'work_reference_1_notes',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_name',`applicationindex`.`answer`,null)) AS 'work_reference_2_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_phone',`applicationindex`.`answer`,null)) AS 'work_reference_2_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_notes',`applicationindex`.`answer`,null)) AS 'work_reference_2_notes'
FROM `applicationindex`,`applications` WHERE `applicationindex`.`application` = `applications`.`id` AND `applications`.`expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP) GROUP BY `applicationindex`.`application`;

/*
 * VIEW: ACCESS_V
 */
CREATE OR REPLACE VIEW `access_v` AS
SELECT `ip`,INET_NTOA(`ip`) AS `ipv4`, INET_NTOA(`ip` + (pow(2, (32-`cidr`)) - 1)) AS `ending`, `cidr`, `level`, `allow` FROM `access`;

CREATE OR REPLACE VIEW `profiles_insert_log` AS
SELECT
	`timestamp`,
	MAX(IF(`column`='pid',`new`,null)) AS "pid",
	MAX(IF(`column`='firstname',`new`,null)) AS "firstname",
	MAX(IF(`column`='middlename',`new`,null)) AS "middlename",
	MAX(IF(`column`='lastname',`new`,null)) AS "lastname",
	MAX(IF(`column`='birthday',`new`,null)) AS "birthday",
	MAX(IF(`column`='class',`new`,null)) AS "class",
	MAX(IF(`column`='major',`new`,null)) AS "major",
	MAX(IF(`column`='type',`new`,null)) AS "type",
	MAX(IF(`column`='gender',`new`,null)) AS "gender",
	MAX(IF(`column`='ethnicity',`new`,null)) AS "ethnicity",
	MAX(IF(`column`='trash',`new`,null)) AS "trash"
FROM `auditlog` WHERE `action`='INSERT' AND `table`='profiles' GROUP BY `timestamp`;

/*
 * VIEW: POSITIONS_V
 */

CREATE OR REPLACE VIEW `positions_v` AS
SELECT
	`positions`.`id` AS `id`,
	`jobs`.`id` AS `job`,
	`jobs`.`label` AS `job_title`,
	`profiles`.`pid` AS `supervisor`,
	CONCAT(`profiles`.`lastname`, ', ',`profiles`.`firstname`) AS `supervisor_name`,
	`profiles_v`.`pid` AS `employee`,
	CONCAT(`profiles_v`.`lastname`, ', ',`profiles_v`.`firstname`) AS `employee_name`,
	`profiles_v`.`type` AS `employee_type`,
	`positions`.`paycode` AS `paycode`,
	CONCAT(`paycodes`.`label`, ' ', `paycodes`.`description`) AS `paycode_label`
FROM 
`jobs` JOIN `positions` ON `jobs`.`id` = `positions`.`job` 
	LEFT JOIN `profiles` ON `jobs`.`supervisor` = `profiles`.`pid` 
	LEFT JOIN `profiles_v` ON `positions`.`employee` = `profiles_v`.`pid`
	LEFT JOIN `paycodes` ON `positions`.`paycode` = `paycodes`.`id`;

