DELIMITER ;;

CREATE FUNCTION overlap_interval(x INT,y INT,a INT,b INT)
RETURNS INTEGER DETERMINISTIC
BEGIN
DECLARE
	overlap_amount INTEGER;
	IF (((x <= a) AND (a < y)) OR ((x < b) AND (b <= y)) OR (a < x AND y < b)) THEN
		IF (x < a) THEN
			IF (y < b) THEN
				SET overlap_amount = y - a;
			ELSE
				SET overlap_amount = b - a;
			END IF;
		ELSE
			IF (y < b) THEN
				SET overlap_amount = y - x;
			ELSE
				SET overlap_amount = b - x;
			END IF;
		END IF;
	ELSE
		SET overlap_amount = 0;
	END IF;
	RETURN overlap_amount;
END ;;

CREATE PROCEDURE log_delete_pr(IN tableName VARCHAR(255), IN pkcol VARCHAR(255), OUT tr VARCHAR(4096))
BEGIN
SELECT
	CONCAT('CREATE TRIGGER ', tableName,'_delete_tr AFTER DELETE ON ', tableName,'
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		',
	GROUP_CONCAT(
		CONCAT(
			'INSERT INTO `auditlog` VALUES(null,''',tableName,''',OLD.',pkcol,',''',column_name,''',''DELETE'',curtime,OLD.',column_name,',null);'
		)
		SEPARATOR ' 
		'
	), ' 
	END;'
	) INTO tr
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = database() AND TABLE_NAME = tableName;
END ;;

CREATE PROCEDURE log_insert_pr(IN tableName VARCHAR(255), IN pkcol VARCHAR(255), OUT tr VARCHAR(4096))
BEGIN
SELECT
	CONCAT('CREATE TRIGGER ', tableName,'_insert_tr AFTER INSERT ON ', tableName,'
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		',
	GROUP_CONCAT(
		CONCAT(
			'IF NEW.',column_name,' IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,''',tableName,''',NEW.',pkcol,',''',column_name,''',''INSERT'',curtime,null,NEW.',column_name,');
			END IF;'
			
		)
		SEPARATOR ' 
		'
	), ' 
	END;'
	) INTO tr
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = database() AND TABLE_NAME = tableName;
END ;;

CREATE PROCEDURE log_update_pr(IN tableName VARCHAR(255), IN pkcol VARCHAR(255), OUT tr VARCHAR(4096))
BEGIN
SELECT
	CONCAT('CREATE TRIGGER ', tableName,'_update_tr AFTER UPDATE ON ', tableName,'
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		',
	GROUP_CONCAT(
		CONCAT(
			'IF OLD.',column_name,' != NEW.',column_name,' THEN
				INSERT INTO `auditlog` VALUES(null,''',tableName,''',OLD.',pkcol,',''',column_name,''',''UPDATE'',curtime,OLD.',column_name,',NEW.',column_name,');
			END IF;'
		)
		SEPARATOR ' 
		'
	), ' 
	END;'
	) INTO tr
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = database() AND TABLE_NAME = tableName;
END ;;

DELIMITER ;

/*
SELECT id,application,beginning,ending,SUM(overlap) AS overlap FROM (
SELECT `id`,`application`,`beginning`,`ending`, overlap_interval(`beginning`,`ending`,:strt,:endng) AS 'overlap' FROM `availability`
UNION
SELECT `id`,`application`,`beginning`,`ending`, overlap_interval(`beginning`,`ending`,:strt1,:endng1) AS 'overlap' FROM `availability`
UNION
SELECT `id`,`application`,`beginning`,`ending`, overlap_interval(`beginning`,`ending`,:strt2,:endng2) AS 'overlap' FROM `availability`
) AS avl GROUP BY `application` ORDER BY overlap DESC;
*/
