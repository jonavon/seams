INSERT INTO `personnel`.`tags`
-- Skills z_tags       
SELECT * FROM (
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',1) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',2) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',3) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',4) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',5) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',6) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',7) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',8) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',9) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',10) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',11) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',12) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',13) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(SPLIT_STR(`skills`,',',14),';',2) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(SPLIT_STR(`skills`,',',15),';',2) AS `answer` FROM `abilities`) AS u WHERE `answer` <> '' AND `application` IN (SELECT `id` FROM `personnel`.`applications`) ORDER BY `application`;

INSERT INTO `personnel`.`availability`
-- Availability z_availability
-- All Day
SELECT * FROM (
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',1)='7-5','00700',SPLIT_STR(`sunday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',1)='7-5','01700',SPLIT_STR(`sunday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',4)='5-12','01700',SPLIT_STR(`sunday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',4)='5-12','01159',SPLIT_STR(`sunday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',2)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',2)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',2),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',3)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',3)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',3),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',5)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',5)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',5),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',6)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',6)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',6),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION


-- Mondays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',1)='7-5','10700',SPLIT_STR(`monday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',1)='7-5','11700',SPLIT_STR(`monday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',4)='5-12','11700',SPLIT_STR(`monday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',4)='5-12','11159',SPLIT_STR(`monday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field Monday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',2)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',2)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',2),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field Monday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',3)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',3)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',3),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field Monday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',5)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',5)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',5),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field Monday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',6)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',6)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',6),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- Tuesdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',1)='7-5','20700',SPLIT_STR(`tuesday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',1)='7-5','21700',SPLIT_STR(`tuesday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',4)='5-12','21700',SPLIT_STR(`tuesday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',4)='5-12','21159',SPLIT_STR(`tuesday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field Tuesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',2)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',2)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',2),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field Tuesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',3)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',3)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',3),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field Tuesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',5)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',5)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',5),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field Tuesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',6)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',6)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',6),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- wednesdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',1)='7-5','30700',SPLIT_STR(`wednesday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',1)='7-5','31700',SPLIT_STR(`wednesday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',4)='5-12','31700',SPLIT_STR(`wednesday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',4)='5-12','31159',SPLIT_STR(`wednesday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field wednesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',2)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',2)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',2),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field wednesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',3)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',3)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',3),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field wednesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',5)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',5)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',5),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field wednesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',6)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',6)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',6),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- thursdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',1)='7-5','40700',SPLIT_STR(`thursday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',1)='7-5','41700',SPLIT_STR(`thursday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',4)='5-12','41700',SPLIT_STR(`thursday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',4)='5-12','41159',SPLIT_STR(`thursday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field thursday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',2)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',2)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',2),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field thursday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',3)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',3)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',3),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field thursday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',5)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',5)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',5),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field thursday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',6)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',6)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',6),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- fridays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',1)='7-5','50700',SPLIT_STR(`friday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',1)='7-5','51700',SPLIT_STR(`friday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',4)='5-12','51700',SPLIT_STR(`friday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',4)='5-12','51159',SPLIT_STR(`friday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field friday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',2)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',2)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',2),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field friday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',3)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',3)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',3),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field friday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',5)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',5)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',5),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field friday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',6)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',6)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',6),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- saturdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',1)='7-5','60700',SPLIT_STR(`saturday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',1)='7-5','61700',SPLIT_STR(`saturday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',4)='5-12','61700',SPLIT_STR(`saturday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',4)='5-12','61159',SPLIT_STR(`saturday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field saturday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',2)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',2)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',2),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field saturday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',3)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',3)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',3),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field saturday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',5)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',5)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',5),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field saturday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',6)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',6)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',6),'-',2)='0','1159',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`) AS t WHERE `application` IN (SELECT `id` FROM `personnel`.`applications`) AND (`beginning`<> '' OR `beginning`<>`ending`) ORDER BY `application`;
