/* vim: set filetype=mysql: */
DROP TABLE IF EXISTS `session`;
DROP TABLE IF EXISTS `auditlog`;
DROP TABLE IF EXISTS `availability`;
-- DROP TABLE IF EXISTS `schedules`;
DROP TABLE IF EXISTS `positions`;
DROP TABLE IF EXISTS `pools`;
DROP TABLE IF EXISTS `jobs`;
-- DROP TABLE IF EXISTS `setoptions`;
DROP TABLE IF EXISTS `tags`;
DROP TABLE IF EXISTS `answers`;
DROP TABLE IF EXISTS `application_form`;
DROP TABLE IF EXISTS `applications`;
DROP TABLE IF EXISTS `applicationindex`;
DROP TABLE IF EXISTS `to`;
DROP TABLE IF EXISTS `messages`;
DROP TABLE IF EXISTS `contact`;
DROP TABLE IF EXISTS `settings`;
DROP TABLE IF EXISTS `profiles`;
DROP TABLE IF EXISTS `enumtable`;
DROP TABLE IF EXISTS `access`;

--
-- Table structure for table `enumtable`
--

CREATE TABLE `enumtable` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Enum ID.',
  `type` enum('UNIT','MAJOR','ETHNICITY','PAYCODE','PREFERENCE') NOT NULL COMMENT 'Enum of type of list.',
  `label` VARCHAR(128) NOT NULL COMMENT 'Label for the list',
  `description` VARCHAR(255) DEFAULT NULL COMMENT 'Description',
  `sort` INTEGER NOT NULL DEFAULT '0' COMMENT 'sort a list by this number.',
  `trash` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'True if deleted or trashed.'
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `pid` VARCHAR(8) NOT NULL PRIMARY KEY COMMENT 'Personal Identification. It must be 3-8 characters or numbers. It must start with a letter.',
  `vtid` int(10) NOT NULL UNIQUE DEFAULT '0' COMMENT '10 digit University Issued ID number.',
  `firstname` VARCHAR(32) NOT NULL COMMENT 'Given Name',
  `middlename` VARCHAR(32) NULL COMMENT 'Middle Name',
  `lastname` VARCHAR(32) NOT NULL COMMENT 'Family Name',
  `birthday` INTEGER(11) NULL COMMENT 'Date of Birth',
  `class` enum('FRESHMAN','SOPHOMORE','JUNIOR','SENIOR','GRADUATE') DEFAULT NULL COMMENT 'Enum list of class standings.',
  `major` INTEGER DEFAULT NULL COMMENT 'From enum table ethnicity type.',
  `unit` INTEGER DEFAULT NULL COMMENT 'From enum table ethnicity type.',
  `type` enum('APPLICANT','PENDING','EMPLOYEE','SUPERVISOR','PLUS','ADMIN') NOT NULL DEFAULT 'APPLICANT' COMMENT 'Profile type.',
  `gender` enum('MALE','FEMALE') DEFAULT NULL COMMENT 'Male or Female.',
  `ethnicity` INTEGER DEFAULT NULL COMMENT 'From enum table ethnicity type.',
  `trash` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'True if deleted or trashed.',
  KEY `profiles_ethnicity_enum_fk` (`ethnicity`),
  KEY `profiles_unit_enum_fk` (`unit`),
  KEY `profiles_major_enum_fk` (`major`),
  CONSTRAINT `profiles_ethnicity_enum_fk` FOREIGN KEY (`ethnicity`) REFERENCES `enumtable` (`id`) ON DELETE SET NULL,
  CONSTRAINT `profiles_major_enum_fk` FOREIGN KEY (`major`) REFERENCES `enumtable` (`id`) ON DELETE SET NULL,
  CONSTRAINT `profiles_unit_enum_fk` FOREIGN KEY (`unit`) REFERENCES `enumtable` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Setting ID.',
  `pid` VARCHAR(8) NOT NULL COMMENT 'Reference to Profiles Table, Multiple Primary Key',
  `type` INTEGER NOT NULL COMMENT 'From enum table preference type.',
  `value` VARCHAR(128) NOT NULL COMMENT 'Profile entered value',
  `sort` INTEGER NOT NULL DEFAULT '0' COMMENT 'sort a list by this number.',
	CONSTRAINT `settings_uq` UNIQUE (`pid`,`type`,`value`),
  CONSTRAINT `settings_pid_fk` FOREIGN KEY (`pid`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE,
  CONSTRAINT `settings_type_fk` FOREIGN KEY (`type`) REFERENCES `enumtable` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `pid` VARCHAR(8) NOT NULL COMMENT 'Reference to Profiles Table, Multiple Primary Key',
  `type` enum('HOME','WORK','SCHOOL','MOBILE') NOT NULL DEFAULT 'SCHOOL' COMMENT 'Multiple Primary Key',
  `key` enum('STREET','STREET2','LOCALITY','REGION','POSTAL-CODE','COUNTRY','PHONE','FAX','EMAIL','IM') NOT NULL COMMENT 'Type of contact information',
  `value` VARCHAR(128) NOT NULL COMMENT 'Profile entered value',
  PRIMARY KEY  (`pid`,`type`,`key`),
  CONSTRAINT `contact_pid_fk` FOREIGN KEY (`pid`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `messages`
--

SET FOREIGN_KEY_CHECKS = 0;
CREATE TABLE `messages` (
  `id` VARCHAR(9) NOT NULL PRIMARY KEY COMMENT 'Message ID.',
  `from` VARCHAR(8) NOT NULL COMMENT 'Reference to Profiles Table.',
  `body` VARCHAR(2048) NOT NULL COMMENT 'email body',
  `subject` VARCHAR(128) NOT NULL COMMENT 'comparable to email subject.',
  `creation` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'When the message was created.',
  `reply` VARCHAR(9) DEFAULT NULL COMMENT 'Reference to message table. Track replies.',
  KEY `messages_reply_messages_fk` (`reply`),
  KEY `messages_from_profiles_fk` (`from`),
  CONSTRAINT `messages_from_profiles_fk` FOREIGN KEY (`from`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE,
	CONSTRAINT `messages_reply_messages_fk` FOREIGN KEY (`reply`) REFERENCES `messages` (`id`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;
SET FOREIGN_KEY_CHECKS = 1;

--
-- Table structure for table `to`
--

CREATE TABLE `to` (
  `message` VARCHAR(9) NOT NULL COMMENT 'REFERENCE to message table.',
  `to` VARCHAR(8) NOT NULL COMMENT 'REFERENCE to PROFILES table.',
  `read` TIMESTAMP NULL DEFAULT NULL COMMENT 'When the message was read. Null if not read.',
  KEY `to_message_messages_fk` (`message`),
  KEY `to_to_profiles_fk` (`to`),
  CONSTRAINT `to_message_messages_fk` FOREIGN KEY (`message`) REFERENCES `messages` (`id`) ON DELETE CASCADE,
  CONSTRAINT `to_to_profiles_fk` FOREIGN KEY (`to`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `applications`
--

CREATE TABLE `applications` (
  `docid` INTEGER NOT NULL AUTO_INCREMENT COMMENT 'Position ID.',
  `id` VARCHAR(8) NOT NULL COMMENT 'Application ID.',
  `pid` VARCHAR(8) NOT NULL COMMENT 'Reference to Profiles Table.',
  `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date last ehanged.',
  `expire` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date this application will expire.',
  `trash` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'True if deleted or trashed.',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `docid` (`docid`),
  KEY `applications_pid_profiles_fk` (`pid`),
  CONSTRAINT `applications_pid_profiles_fk` FOREIGN KEY (`pid`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `application_form`
--

CREATE TABLE `application_form` (
  `name` VARCHAR(32) NOT NULL COMMENT 'form name',
  `type` enum('button','checkbox','file','hidden','password','radio','select','text','textarea','tags','checkset','selectset') NOT NULL COMMENT 'Type of form element.',
  `label` VARCHAR(128) DEFAULT NULL COMMENT 'label for this form member.',
  `description` VARCHAR(256) DEFAULT NULL COMMENT 'Description or help for this form member.',
  `required` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'If marked true then the element will be required.',
  `default` VARCHAR(256) DEFAULT NULL COMMENT 'Default value for this form member.',
  `fieldset` VARCHAR(128) DEFAULT NULL COMMENT 'Grouping of questions. Content should appear in legend tag of fieldset.',
  `sort` INTEGER DEFAULT '0',
  PRIMARY KEY  (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `answers`
--

CREATE TABLE `answers` (
  `application` VARCHAR(8) NOT NULL COMMENT 'REFERENCE to applications.',
  `question` VARCHAR(32) NOT NULL COMMENT 'Reference to Application_Form.',
  `answer` MEDIUMTEXT COMMENT 'User submitted content.',
  PRIMARY KEY  (`application`,`question`),
  KEY `answers_question_application_form_fk` (`question`),
  CONSTRAINT `answers_application_applications_fk` FOREIGN KEY (`application`) REFERENCES `applications` (`id`) ON DELETE CASCADE,
  CONSTRAINT `answers_question_application_form_fk` FOREIGN KEY (`question`) REFERENCES `application_form` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

CREATE TABLE `applicationindex` (
  `application` VARCHAR(8) NOT NULL COMMENT 'REFERENCE to applications.',
  `question` VARCHAR(32) NOT NULL COMMENT 'Reference to Application_Form.',
  `answer` mediumtext COMMENT 'User submitted content.',
  PRIMARY KEY (`application`,`question`),
  FULLTEXT(`answer`)
 ) ENGINE=MyISAM DEFAULT CHARACTER SET=utf8;
--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `application` VARCHAR(8) NOT NULL COMMENT 'REFERENCE to applications.',
  `question` VARCHAR(32) NOT NULL COMMENT 'Reference to Application_Form.',
  `tag` VARCHAR(32) NOT NULL COMMENT 'User submitted tag.',
  PRIMARY KEY  (`tag`,`question`,`application`),
  KEY `tags_application_applications_fk` (`application`),
  KEY `tags_question_application_form_fk` (`question`),
  CONSTRAINT `tags_application_applications_fk` FOREIGN KEY (`application`) REFERENCES `applications` (`id`) ON DELETE CASCADE,
  CONSTRAINT `tags_question_application_form_fk` FOREIGN KEY (`question`) REFERENCES `application_form` (`name`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `setoptions`
--

CREATE TABLE `setoptions` (
  `set` VARCHAR(32) NOT NULL COMMENT 'Reference to application form.',
  `option` VARCHAR(32) NOT NULL COMMENT 'Option value or checkbox value',
  `label` VARCHAR(128) DEFAULT NULL COMMENT 'Label for the option.',
  `group` VARCHAR(128) DEFAULT NULL COMMENT 'Option group or nested fieldset legend.',
  `sort` INTEGER DEFAULT '0',
  KEY `setoptions_set_application_form_fk` (`set`),
  CONSTRAINT `setoptions_set_application_form_fk` FOREIGN KEY (`set`) REFERENCES `application_form` (`name`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` INTEGER NOT NULL AUTO_INCREMENT COMMENT 'Job ID.',
  `label` VARCHAR(128) NOT NULL COMMENT 'Job title.',
  `description` text COMMENT 'Job description.',
  `supervisor` VARCHAR(8) NOT NULL COMMENT 'REFERENCE to profiles table. Supervisor that created that manages the job.',
  `trash` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'True if deleted or trashed.',
  PRIMARY KEY  (`id`),
  KEY `jobs_supervisor_profiles_fk` (`supervisor`),
  CONSTRAINT `jobs_supervisor_profiles_fk` FOREIGN KEY (`supervisor`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `pools`
--

CREATE TABLE `pools` (
  `id` INTEGER NOT NULL AUTO_INCREMENT COMMENT 'Pool ID.',
  `job` INTEGER NOT NULL COMMENT 'Reference to jobs table.',
  `application` VARCHAR(8) NOT NULL COMMENT 'Reference to applications table.',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `pools_uq` (`job`,`application`),
  KEY `application` (`application`),
  CONSTRAINT `pools_job_fk` FOREIGN KEY (`job`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `pools_application_fk` FOREIGN KEY (`application`) REFERENCES `applications` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `positions`
--

CREATE TABLE `positions` (
  `id` INTEGER NOT NULL AUTO_INCREMENT COMMENT 'Position ID.',
  `job` INTEGER NOT NULL COMMENT 'Reference to jobs table.',
  `employee` VARCHAR(8) DEFAULT NULL COMMENT 'Reference to Profiles Table.',
  `paycode` INTEGER DEFAULT NULL COMMENT 'Reference to enumtable Table for paycodes.',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `positions_uq` (`job`,`employee`),
  KEY `employee` (`employee`),
  CONSTRAINT `positions_job_fk` FOREIGN KEY (`job`) REFERENCES `jobs` (`id`) ON DELETE CASCADE,
  CONSTRAINT `positions_ibfk_1` FOREIGN KEY (`employee`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE,
  CONSTRAINT `positions_paycode_fk` FOREIGN KEY (`paycode`) REFERENCES `enumtable` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `schedules`
--

/*
-- CREATE TABLE `schedules` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Schedule ID.',
  `pid` VARCHAR(8) NOT NULL COMMENT 'Reference to Profiles Table.',
  `type` enum('FREEBUSY','WORK') NOT NULL COMMENT 'type of schedule',
  `file` text COMMENT 'Calendar file blob',
  `modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'la',
  `size` INTEGER DEFAULT '0' COMMENT 'Size in bytes.',
  KEY `pid` (`pid`),
  CONSTRAINT `schedules_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `profiles` (`pid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;
*/

CREATE TABLE `availability` (
  `id` INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key ID.',
  `application` VARCHAR(8) NOT NULL COMMENT 'Reference to Applications Table.',
	`beginning` INTEGER(5) UNSIGNED ZEROFILL NOT NULL COMMENT 'UNIX TIMESTAMP representing the start of a period of time for availability.',
	`ending` INTEGER(5) UNSIGNED ZEROFILL NOT NULL COMMENT 'UNIX TIMESTAMP representing the start of a period of time for availability.',
	KEY (`beginning`),
	KEY (`ending`),
	UNIQUE KEY `availability_uq` (`application`,`beginning`,`ending`),
  CONSTRAINT `availability_application_fk` FOREIGN KEY (`application`) REFERENCES `applications` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;
--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` VARCHAR(32) NOT NULL PRIMARY KEY COMMENT 'SESSION ID',
  `hash` VARCHAR(128) DEFAULT NULL COMMENT 'Unique hash for session.',
  `expire` INTEGER(11) NOT NULL DEFAULT '0' COMMENT 'UNIX TIMESTAMP for when this session will expire.',
  `data` text COMMENT 'Session Data'
) ENGINE=MyISAM DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
	`ip` INT UNSIGNED NOT NULL PRIMARY KEY COMMENT 'Start IP Address',
	`cidr` TINYINT(2) UNSIGNED NOT NULL DEFAULT 32 COMMENT 'CIDR number',
	`level` INTEGER NOT NULL DEFAULT 0 COMMENT 'Permission this is based on bits that are on in the integer.',
	`allow` TINYINT(1) NOT NULL DEFAULT 0 COMMENT 'Allow if 1 or true, Disallow if 0 or false',
	KEY (`ip`),
	CONSTRAINT `access_uq` UNIQUE (`ip`,`allow`)
) ENGINE=MyISAM DEFAULT CHARACTER SET=utf8;

--
-- Table structure for table `auditlog`
--

CREATE TABLE `auditlog` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key for the log table.',
  `table` varchar(32) NOT NULL COMMENT 'The table that has been changed',
  `row` varchar(32) NOT NULL COMMENT 'The primary key row of the table that has been changed',
  `column` varchar(32) NOT NULL COMMENT 'The column that has been changed',
  `action` enum('INSERT','UPDATE','DELETE') NOT NULL COMMENT 'The type of action on the column',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'The timestamp for the action',
  `old` varchar(1024) DEFAULT NULL COMMENT 'The old value for updates and deletes',
  `new` varchar(1024) DEFAULT NULL COMMENT 'The new value for updates and inserts',
  KEY `timestamp_k` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;

--
-- Triggers
--

DELIMITER ;;

CREATE TRIGGER `answers_insert_tr` AFTER INSERT ON `answers`
FOR EACH ROW BEGIN
	REPLACE INTO `applicationindex` (`application`,`question`,`answer`) VALUES (NEW.`application`,NEW.`question`,NEW.`answer`);
END ;;

CREATE TRIGGER `answers_update_tr` AFTER UPDATE ON `answers`
FOR EACH ROW BEGIN
	UPDATE `applicationindex` SET `application` = NEW.`application`, `question` = NEW.`question`, `answer` = NEW.`answer` WHERE `application` = OLD.`application` AND `question` = OLD.`question`;
END ;;

CREATE TRIGGER `answers_delete_tr` AFTER DELETE ON `answers`
FOR EACH ROW BEGIN
	DELETE FROM `applicationindex` WHERE `application` = OLD.`application` AND `question` = OLD.`question`;
END ;;

CREATE TRIGGER `applications_insert_tr` AFTER INSERT ON `applications`
FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF NEW.docid IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'applications',NEW.pid,'docid','INSERT',curtime,null,NEW.docid);
			END IF; 
		IF NEW.id IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'applications',NEW.pid,'id','INSERT',curtime,null,NEW.id);
			END IF; 
		IF NEW.pid IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'applications',NEW.pid,'pid','INSERT',curtime,null,NEW.pid);
			END IF; 
		IF NEW.modified IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'applications',NEW.pid,'modified','INSERT',curtime,null,NEW.modified);
			END IF; 
		IF NEW.expire IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'applications',NEW.pid,'expire','INSERT',curtime,null,NEW.expire);
			END IF; 
		IF NEW.trash IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'applications',NEW.pid,'trash','INSERT',curtime,null,NEW.trash);
			END IF; 
END ;;

CREATE TRIGGER `applications_update_tr` AFTER UPDATE ON `applications`
FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF OLD.docid != NEW.docid THEN
				INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'docid','UPDATE',curtime,OLD.docid,NEW.docid);
			END IF; 
		IF OLD.id != NEW.id THEN
				INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'id','UPDATE',curtime,OLD.id,NEW.id);
			END IF; 
		IF OLD.pid != NEW.pid THEN
				INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'pid','UPDATE',curtime,OLD.pid,NEW.pid);
			END IF; 
		IF OLD.modified != NEW.modified THEN
				INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'modified','UPDATE',curtime,OLD.modified,NEW.modified);
			END IF; 
		IF OLD.expire != NEW.expire THEN
				INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'expire','UPDATE',curtime,OLD.expire,NEW.expire);
			END IF; 
		IF OLD.trash != NEW.trash THEN
				INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'trash','UPDATE',curtime,OLD.trash,NEW.trash);
			END IF; 
END ;;

CREATE TRIGGER `applications_delete_tr` AFTER DELETE ON `applications`
FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'docid','DELETE',curtime,OLD.docid,null); 
		INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'id','DELETE',curtime,OLD.id,null); 
		INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'pid','DELETE',curtime,OLD.pid,null); 
		INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'modified','DELETE',curtime,OLD.modified,null); 
		INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'expire','DELETE',curtime,OLD.expire,null); 
		INSERT INTO `auditlog` VALUES(null,'applications',OLD.pid,'trash','DELETE',curtime,OLD.trash,null); 
END ;;

CREATE TRIGGER `profiles_insert_tr` AFTER INSERT ON `profiles`
FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF NEW.pid IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'pid','INSERT',curtime,null,NEW.pid);
			END IF; 
		IF NEW.vtid IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'vtid','INSERT',curtime,null,NEW.vtid);
			END IF; 
		IF NEW.firstname IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'firstname','INSERT',curtime,null,NEW.firstname);
			END IF; 
		IF NEW.middlename IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'middlename','INSERT',curtime,null,NEW.middlename);
			END IF; 
		IF NEW.lastname IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'lastname','INSERT',curtime,null,NEW.lastname);
			END IF; 
		IF NEW.birthday IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'birthday','INSERT',curtime,null,NEW.birthday);
			END IF; 
		IF NEW.class IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'class','INSERT',curtime,null,NEW.class);
			END IF; 
		IF NEW.major IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'major','INSERT',curtime,null,NEW.major);
			END IF; 
		IF NEW.unit IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'unit','INSERT',curtime,null,NEW.unit);
			END IF; 
		IF NEW.type IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'type','INSERT',curtime,null,NEW.type);
			END IF; 
		IF NEW.gender IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'gender','INSERT',curtime,null,NEW.gender);
			END IF; 
		IF NEW.ethnicity IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'ethnicity','INSERT',curtime,null,NEW.ethnicity);
			END IF; 
		IF NEW.trash IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',NEW.pid,'trash','INSERT',curtime,null,NEW.trash);
			END IF; 
END ;;

CREATE TRIGGER `profiles_update_tr` AFTER UPDATE ON `profiles`
FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF OLD.pid != NEW.pid THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'pid','UPDATE',curtime,OLD.pid,NEW.pid);
			END IF; 
		IF OLD.vtid != NEW.vtid THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'vtid','UPDATE',curtime,OLD.vtid,NEW.vtid);
			END IF; 
		IF OLD.firstname != NEW.firstname THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'firstname','UPDATE',curtime,OLD.firstname,NEW.firstname);
			END IF; 
		IF OLD.middlename != NEW.middlename THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'middlename','UPDATE',curtime,OLD.middlename,NEW.middlename);
			END IF; 
		IF OLD.lastname != NEW.lastname THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'lastname','UPDATE',curtime,OLD.lastname,NEW.lastname);
			END IF; 
		IF OLD.birthday != NEW.birthday THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'birthday','UPDATE',curtime,OLD.birthday,NEW.birthday);
			END IF; 
		IF OLD.class != NEW.class THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'class','UPDATE',curtime,OLD.class,NEW.class);
			END IF; 
		IF OLD.major != NEW.major THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'major','UPDATE',curtime,OLD.major,NEW.major);
			END IF; 
		IF OLD.unit != NEW.unit THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'unit','UPDATE',curtime,OLD.unit,NEW.unit);
			END IF; 
		IF OLD.type != NEW.type THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'type','UPDATE',curtime,OLD.type,NEW.type);
			END IF; 
		IF OLD.gender != NEW.gender THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'gender','UPDATE',curtime,OLD.gender,NEW.gender);
			END IF; 
		IF OLD.ethnicity != NEW.ethnicity THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'ethnicity','UPDATE',curtime,OLD.ethnicity,NEW.ethnicity);
			END IF; 
		IF OLD.trash != NEW.trash THEN
				INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'trash','UPDATE',curtime,OLD.trash,NEW.trash);
			END IF; 
END ;;

CREATE TRIGGER `profiles_delete_tr` AFTER DELETE ON `profiles`
FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'pid','DELETE',curtime,OLD.pid,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'vtid','DELETE',curtime,OLD.vtid,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'firstname','DELETE',curtime,OLD.firstname,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'middlename','DELETE',curtime,OLD.middlename,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'lastname','DELETE',curtime,OLD.lastname,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'birthday','DELETE',curtime,OLD.birthday,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'class','DELETE',curtime,OLD.class,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'major','DELETE',curtime,OLD.major,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'unit','DELETE',curtime,OLD.unit,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'type','DELETE',curtime,OLD.type,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'gender','DELETE',curtime,OLD.gender,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'ethnicity','DELETE',curtime,OLD.ethnicity,null); 
		INSERT INTO `auditlog` VALUES(null,'profiles',OLD.pid,'trash','DELETE',curtime,OLD.trash,null); 
END ;;


CREATE TRIGGER positions_insert_tr AFTER INSERT ON positions
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF NEW.id IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'id','INSERT',curtime,null,NEW.id);
			END IF; 
		IF NEW.job IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'job','INSERT',curtime,null,NEW.job);
			END IF; 
		IF NEW.employee IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'employee','INSERT',curtime,null,NEW.employee);
			END IF; 
		IF NEW.paycode IS NOT NULL THEN
				INSERT INTO `auditlog` VALUES(null,'positions',NEW.id,'paycode','INSERT',curtime,null,NEW.paycode);
			END IF; 
	END ;;
CREATE TRIGGER positions_update_tr AFTER UPDATE ON positions
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		IF OLD.id != NEW.id THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'id','UPDATE',curtime,OLD.id,NEW.id);
			END IF; 
		IF OLD.job != NEW.job THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'job','UPDATE',curtime,OLD.job,NEW.job);
			END IF; 
		IF OLD.employee != NEW.employee THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'employee','UPDATE',curtime,OLD.employee,NEW.employee);
			END IF; 
		IF OLD.paycode != NEW.paycode THEN
				INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'paycode','UPDATE',curtime,OLD.paycode,NEW.paycode);
			END IF; 
	END ;;
CREATE TRIGGER positions_delete_tr AFTER DELETE ON positions
	FOR EACH ROW BEGIN
		DECLARE curtime TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'id','DELETE',curtime,OLD.id,null); 
		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'job','DELETE',curtime,OLD.job,null); 
		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'employee','DELETE',curtime,OLD.employee,null); 
		INSERT INTO `auditlog` VALUES(null,'positions',OLD.id,'paycode','DELETE',curtime,OLD.paycode,null); 
	END ;;

CREATE TRIGGER `tags_insert_tr` AFTER INSERT ON `tags`
FOR EACH ROW BEGIN
	REPLACE INTO `applicationindex`
		SELECT `application`,`question`,GROUP_CONCAT(`tag` SEPARATOR ', ') AS "tags" FROM `tags` WHERE `application` IN (SELECT `id` FROM `applications_v`) GROUP BY `application`;
END ;;

CREATE TRIGGER `tags_delete_tr` AFTER DELETE ON `tags`
FOR EACH ROW BEGIN
	DELETE FROM `applicationindex` WHERE `application` = OLD.`application` AND `question` = OLD.`question`;
	REPLACE INTO `applicationindex`
		SELECT `application`,`question`,GROUP_CONCAT(`tag` SEPARATOR ', ') AS "tags" FROM `tags` WHERE `application` IN (SELECT `id` FROM `applications_v`) GROUP BY `application`;
END ;;

DELIMITER ;
