SELECT *,MATCH(`answer`) AGAINST('volunteer -boss' IN BOOLEAN MODE) AS score FROM `applicationindex` WHERE 
MATCH(`answer`) AGAINST('volunteer -boss' IN BOOLEAN MODE) GROUP BY `application`;

CREATE OR REPLACE VIEW `answers_v` AS
SELECT `applications`.`docid`,`applicationindex`.`application`,
GROUP_CONCAT(if(`applicationindex`.`question`='another_department',`applicationindex`.`answer`,null)) AS 'another_department',
GROUP_CONCAT(if(`applicationindex`.`question`='break_worker',`applicationindex`.`answer`,null)) AS 'break_worker',
GROUP_CONCAT(if(`applicationindex`.`question`='citizen',`applicationindex`.`answer`,null)) AS 'citizen',
GROUP_CONCAT(if(`applicationindex`.`question`='hours',`applicationindex`.`answer`,null)) AS 'hours',
GROUP_CONCAT(if(`applicationindex`.`question`='library_experience',`applicationindex`.`answer`,null)) AS 'library_experience',
GROUP_CONCAT(if(`applicationindex`.`question`='lift50',`applicationindex`.`answer`,null)) AS 'lift50',
GROUP_CONCAT(if(`applicationindex`.`question`='other_experience',`applicationindex`.`answer`,null)) AS 'other_experience',
GROUP_CONCAT(if(`applicationindex`.`question`='transportation',`applicationindex`.`answer`,null)) AS 'transportation',
GROUP_CONCAT(if(`applicationindex`.`question`='workstudy',`applicationindex`.`answer`,null)) AS 'workstudy',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_name',`applicationindex`.`answer`,null)) AS 'work_reference_0_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_phone',`applicationindex`.`answer`,null)) AS 'work_reference_0_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_notes',`applicationindex`.`answer`,null)) AS 'work_reference_0_notes',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_name',`applicationindex`.`answer`,null)) AS 'work_reference_1_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_phone',`applicationindex`.`answer`,null)) AS 'work_reference_1_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_notes',`applicationindex`.`answer`,null)) AS 'work_reference_1_notes',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_name',`applicationindex`.`answer`,null)) AS 'work_reference_2_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_phone',`applicationindex`.`answer`,null)) AS 'work_reference_2_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_notes',`applicationindex`.`answer`,null)) AS 'work_reference_2_notes'
FROM `applicationindex`,`applications` WHERE `applicationindex`.`application` = `applications`.`id` AND `applications`.`expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP) GROUP BY `applicationindex`.`application`;

SELECT `applicationindex`.`application`,
GROUP_CONCAT(if(`applicationindex`.`question`='another_department',`applicationindex`.`answer`,null)) AS 'another_department',
GROUP_CONCAT(if(`applicationindex`.`question`='break_worker',`applicationindex`.`answer`,null)) AS 'break_worker',
GROUP_CONCAT(if(`applicationindex`.`question`='citizen',`applicationindex`.`answer`,null)) AS 'citizen',
GROUP_CONCAT(if(`applicationindex`.`question`='hours',`applicationindex`.`answer`,null)) AS 'hours',
GROUP_CONCAT(if(`applicationindex`.`question`='library_experience',`applicationindex`.`answer`,null)) AS 'library_experience',
GROUP_CONCAT(if(`applicationindex`.`question`='lift50',`applicationindex`.`answer`,null)) AS 'lift50',
GROUP_CONCAT(if(`applicationindex`.`question`='other_experience',`applicationindex`.`answer`,null)) AS 'other_experience',
GROUP_CONCAT(if(`applicationindex`.`question`='transportation',`applicationindex`.`answer`,null)) AS 'transportation',
GROUP_CONCAT(if(`applicationindex`.`question`='workstudy',`applicationindex`.`answer`,null)) AS 'workstudy',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_name',`applicationindex`.`answer`,null)) AS 'work_reference_0_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_phone',`applicationindex`.`answer`,null)) AS 'work_reference_0_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_0_notes',`applicationindex`.`answer`,null)) AS 'work_reference_0_notes',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_name',`applicationindex`.`answer`,null)) AS 'work_reference_1_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_phone',`applicationindex`.`answer`,null)) AS 'work_reference_1_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_1_notes',`applicationindex`.`answer`,null)) AS 'work_reference_1_notes',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_name',`applicationindex`.`answer`,null)) AS 'work_reference_2_name',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_phone',`applicationindex`.`answer`,null)) AS 'work_reference_2_phone',
GROUP_CONCAT(if(`applicationindex`.`question`='work_reference_2_notes',`applicationindex`.`answer`,null)) AS 'work_reference_2_notes',
SUM((`applicationindex`.`question` = 'library_experience' AND MATCH(`answer`) AGAINST('volunteer' IN BOOLEAN MODE) )) AS score
FROM `applicationindex` WHERE (MATCH(`answer`) AGAINST('volunteer' IN BOOLEAN MODE) ) 
AND `applicationindex`.`application` IN (SELECT `applicationindex`.`application` FROM `applicationindex` WHERE (`applicationindex`.`question`='workstudy' AND `applicationindex`.`answer` IS NULL)) GROUP BY `applicationindex`.`application`;

SELECT * FROM `applicationindex` WHERE (`question` = 'library_experience' AND MATCH(`answer`) AGAINST('volunteer' IN BOOLEAN MODE)) AND (`application` IN (SELECT `application` FROM `applicationindex` WHERE `question`='workstudy' AND `answer` > 0));
SELECT * FROM `applicationindex` WHERE (`question` = 'library_experience' AND MATCH(`answer`) AGAINST('volunteer' IN BOOLEAN MODE)) AND `application` NOT IN (SELECT `application` FROM `applicationindex` WHERE `question`='workstudy' AND `answer` > 0);
SELECT * FROM `applicationindex` WHERE `question`='citizen' AND (`answer` = '' OR answer IS NULL OR `answer` = '0');

SELECT `applications_v`.*,snippits.* FROM `applications_v` JOIN (
SELECT `application`,GROUP_CONCAT(CONCAT(`question`, ':', `answer`) SEPARATOR '\n') AS snippit, SUM(score) AS score FROM (
SELECT `application`,`question`,`answer`,(MATCH(`answer`) AGAINST('work' IN BOOLEAN MODE)) AS score FROM `applicationindex` WHERE MATCH(`answer`) AGAINST('work' IN BOOLEAN MODE)
UNION
SELECT `application`,`question`,`answer`,(`question`='library_experience' AND MATCH(`answer`) AGAINST('University' IN BOOLEAN MODE)) AS score FROM `applicationindex` WHERE (`question`='library_experience' AND MATCH(`answer`) AGAINST('University' IN BOOLEAN MODE))
) AS tbl
WHERE 
`application` IN (SELECT `application` FROM `applicationindex` WHERE `question`='workstudy' AND `answer` > 0)
GROUP BY `application` 
) AS snippits ON `applications_v`.`id` = snippits.`application` ORDER BY score DESC;

SELECT `applications_v`.*,snippits.snippit AS snippit, snippits.score AS score FROM `applications_v` JOIN ( SELECT `application`,GROUP_CONCAT(CONCAT(`question`, ':', `answer`) SEPARATOR '\n') AS snippit, SUM(score) AS score FROM ( SELECT `application`,`question`,`answer`,(MATCH(`answer`) AGAINST(' ' IN BOOLEAN MODE)) AS score FROM `applicationindex` WHERE MATCH(`answer`) AGAINST(' ' IN BOOLEAN MODE) UNION SELECT `application`,`question`,`answer`,(`question` = 'library_experience' AND MATCH(`answer`) AGAINST('library_experience' )) AS score FROM `applicationindex` WHERE `question` = 'library_experience' AND MATCH(`answer`) AGAINST('Volunteer' )) AS tbl `application` IN (SELECT `application` FROM `applicationindex` WHERE `question`='workstudy' AND `answer` > 0)GROUP BY `application` ) AS snippits ON `applications_v`.`id` = snippits.application ORDER BY score DESC
;
SELECT `applications_v`.*,snippits.snippit AS snippit, snippits.score AS score FROM `applications_v` JOIN ( SELECT `application`,GROUP_CONCAT(CONCAT(`question`, ':', `answer`) SEPARATOR '\n') AS snippit, SUM(score) AS score FROM ( SELECT `application`,`question`,`answer`,(`question` = 'library_experience' AND MATCH(`answer`) AGAINST('library_experience' )) AS score FROM `applicationindex` WHERE `question` = 'library_experience' AND MATCH(`answer`) AGAINST('Volunteer' )) AS tbl WHERE `application` IN (SELECT `application` FROM `applicationindex` WHERE `question`='workstudy' AND `answer` > 0)GROUP BY `application` ) AS snippits ON `applications_v`.`id` = snippits.application ORDER BY score DESC;


SELECT `applications_v`.*,snippits.snippit AS snippit, snippits.score AS score FROM `applications_v` JOIN ( SELECT `application`,GROUP_CONCAT(CONCAT(`question`, ':', `answer`) SEPARATOR '\n') AS snippit, SUM(score) AS score FROM ( SELECT *,0 AS score FROM `applicationindex` WHERE `question`='workstudy') AS tbl WHERE `application` IN (SELECT `application` FROM `applicationindex` WHERE `question`='workstudy' AND `answer` > 0)GROUP BY `application` ) AS snippits ON `applications_v`.`id` = snippits.application ORDER BY score DESC;

UPDATE `applications` SET `expire` = FROM_UNIXTIME(1233688548 + 5256000);