INSERT INTO `personnel`.`profiles`
-- Profiles z_profiles
-- Some major ID's wer  not automatically transferable.
SELECT `pid`,`vtid`,`firstname`,`middlename`,`lastname`,`birthday`,`class`,`id` AS `major`,`unit`,`type`,`gender`,`ethnicity`,`trash` FROM
(SELECT 
	LOWER(SUBSTRING_INDEX(`email`,'@',1)) AS `pid`,
	`vtid` AS `vtid`,
	`first_name` AS `firstname`,
	null AS `middlename`,
	`last_name` AS `lastname`,
	UNIX_TIMESTAMP(STR_TO_DATE(`dob`,'%c/%d/%y')) AS `birthday`,
	CASE `class`
		WHEN 1 THEN 'FRESHMAN'
		WHEN 2 THEN 'SOPHMOMORE'
		WHEN 3 THEN 'JUNIOR'
		WHEN 4 THEN 'SENIOR'
		WHEN 5 THEN 'GRADUATE'
		ELSE NULL
	END
	AS `class`,
	`major` AS `major`,
	null AS `unit`,
	'APPLICANT' AS `type`,
	CASE `availability`.`sex`
		WHEN 'M' THEN 'MALE'
		WHEN 'F' THEN 'FEMALE'
	END
	AS `gender`,
	CASE `availability`.`race`
		WHEN 1 THEN 21
		WHEN 2 THEN 20
		WHEN 3 THEN 19
		WHEN 4 THEN 22
		WHEN 5 THEN 18
		ELSE 17
	END
	AS `ethnicity`,
	0 AS `trash`
FROM `applicants` JOIN `availability` ON `applicants`.`id`=`availability`.`id`) `t` LEFT OUTER JOIN `major_reference` ON `t`.`major`=`major_reference`.`maj_id` WHERE `vtid`<>0 GROUP BY `pid` ORDER BY `pid`,FIELD(`class`,'GRADUATE','SENIOR','JUNIOR','SOPHOMORE','FRESHMAN') ASC;

DELETE FROM `applicants` WHERE `id`= 'zCwctz8g9s2205lXXYvT';
DELETE FROM `abilities` WHERE `id`= 'zCwctz8g9s2205lXXYvT';
DELETE FROM `admin` WHERE `id`= 'zCwctz8g9s2205lXXYvT';
DELETE FROM `e_admin` WHERE `id`= 'zCwctz8g9s2205lXXYvT';

SELECT * FROM `applicants` WHERE `id`= '6SNrGeuzbCfpdbUNzrf3';
SELECT * FROM `abilities` WHERE `id`= '6SNrGeuzbCfpdbUNzrf3';
SELECT * FROM `admin` WHERE `id`= '6SNrGeuzbCfpdbUNzrf3';
SELECT * FROM `e_admin` WHERE `id`= '6SNrGeuzbCfpdbUNzrf3';

INSERT INTO `personnel`.`applications`
-- Applications z_applications
SELECT
	null AS `docid`,
	SUBSTRING(`admin`.`id`,1,8) AS `id`,
	LOWER(SUBSTRING_INDEX(`applicants`.`email`,'@',1)) AS `pid`,
	STR_TO_DATE(`admin`.`stamp`,'%m/%d/%y %H:%i:%s') AS `modified`,
	STR_TO_DATE(`admin`.`stamp`,'%m/%d/%y %H:%i:%s') + INTERVAL 120 DAY AS `expired`,
	0 AS `trash`
FROM `admin` JOIN `applicants` ON `admin`.`id`=`applicants`.`id` WHERE `admin`.`id` IN (SELECT `id` FROM `applicants` WHERE `vtid`<>0)  ORDER BY STR_TO_DATE(`admin`.`stamp`,'%m/%d/%y %H:%i:%s') DESC;

INSERT INTO `personnel`.`answers`
-- Answers z_answers
SELECT * FROM (
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'another_department' AS `question`, `location` AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'break_worker' AS `question`, 
CASE `break`
	WHEN 2 THEN 0
	WHEN 1 THEN 1
END AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'transportation' AS `question`, 
CASE `trans`
	WHEN 2 THEN 0
	WHEN 1 THEN 1
END AS `answer` FROM `abilities`
UNION
-- SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'credits' AS `question`, `credits` AS `answer` FROM `abilities`
-- UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'hours' AS `question`, `total` AS `answer` FROM `availability`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'lift50' AS `question`, 
CASE `lift`
	WHEN 2 THEN 0
	WHEN 1 THEN 1
END AS `answer` FROM `abilities`
UNION
-- SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, `skills` AS `answer` FROM `abilities`
-- UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'work_reference_2_notes' AS `question`, `ref3` AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'work_reference_1_notes' AS `question`, `ref2` AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'work_reference_0_notes' AS `question`, `ref1` AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'other_experience' AS `question`, `other` AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'library_experience' AS `question`, `prev` AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'citizen' AS `question`, 
CASE `citizen`
	WHEN 2 THEN 0
	WHEN 1 THEN 1
END AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'workstudy' AS `question`, `amount` AS `answer` FROM `abilities`) AS t WHERE `answer`<>'' AND `application` IN (SELECT LOWER(SUBSTRING(`id`,1,8)) FROM `applicants` WHERE `vtid`<>0) ORDER BY `application`;

SET GLOBAL log_bin_trust_function_creators = 1;
/*
CREATE FUNCTION SPLIT_STR(
  x VARCHAR(255),
  delim VARCHAR(12),
  pos INT
)
RETURNS VARCHAR(255)
RETURN REPLACE(SUBSTRING(SUBSTRING_INDEX(x, delim, pos),
       LENGTH(SUBSTRING_INDEX(x, delim, pos -1)) + 1),
       delim, '');
*/      
INSERT INTO `personnel`.`tags`
-- Skills z_tags       
SELECT * FROM (
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',1) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',2) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',3) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',4) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',5) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',6) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',7) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',8) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',9) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',10) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',11) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',12) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(`skills`,',',13) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(SPLIT_STR(`skills`,',',14),';',2) AS `answer` FROM `abilities`
UNION
SELECT LOWER(SUBSTRING(`id`,1,8)) AS `application`, 'skills' AS `question`, SPLIT_STR(SPLIT_STR(`skills`,',',15),';',2) AS `answer` FROM `abilities`) AS u WHERE `answer` <> '' AND `application` IN (SELECT `id` FROM `personnel`.`applications`) ORDER BY `application`;

INSERT INTO `personnel`.`availability`
-- Availability z_availability
-- All Day
SELECT * FROM (
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',1)='7-5','00700',SPLIT_STR(`sunday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',1)='7-5','01700',SPLIT_STR(`sunday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',4)='5-12','01700',SPLIT_STR(`sunday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',4)='5-12','02359',SPLIT_STR(`sunday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',2)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',2)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',2),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',3)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',3)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',3),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',5)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',5)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',5),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field Sunday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`sunday`,',',6)='0-0',null,CONCAT('0',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`sunday`,',',6)='0-0',null,CONCAT('0',IF(SPLIT_STR(SPLIT_STR(`sunday`,',',6),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`sunday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION


-- Mondays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',1)='7-5','10700',SPLIT_STR(`monday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',1)='7-5','11700',SPLIT_STR(`monday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',4)='5-12','11700',SPLIT_STR(`monday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',4)='5-12','12359',SPLIT_STR(`monday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field Monday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',2)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',2)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',2),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field Monday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',3)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',3)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',3),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field Monday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',5)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',5)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',5),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field Monday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`monday`,',',6)='0-0',null,CONCAT('1',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`monday`,',',6)='0-0',null,CONCAT('1',IF(SPLIT_STR(SPLIT_STR(`monday`,',',6),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`monday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- Tuesdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',1)='7-5','20700',SPLIT_STR(`tuesday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',1)='7-5','21700',SPLIT_STR(`tuesday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',4)='5-12','21700',SPLIT_STR(`tuesday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',4)='5-12','22359',SPLIT_STR(`tuesday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field Tuesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',2)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',2)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',2),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field Tuesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',3)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',3)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',3),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field Tuesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',5)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',5)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',5),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field Tuesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`tuesday`,',',6)='0-0',null,CONCAT('2',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`tuesday`,',',6)='0-0',null,CONCAT('2',IF(SPLIT_STR(SPLIT_STR(`tuesday`,',',6),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`tuesday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- wednesdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',1)='7-5','30700',SPLIT_STR(`wednesday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',1)='7-5','31700',SPLIT_STR(`wednesday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',4)='5-12','31700',SPLIT_STR(`wednesday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',4)='5-12','32359',SPLIT_STR(`wednesday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field wednesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',2)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',2)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',2),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field wednesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',3)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',3)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',3),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field wednesday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',5)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',5)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',5),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field wednesday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`wednesday`,',',6)='0-0',null,CONCAT('3',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`wednesday`,',',6)='0-0',null,CONCAT('3',IF(SPLIT_STR(SPLIT_STR(`wednesday`,',',6),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`wednesday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- thursdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',1)='7-5','40700',SPLIT_STR(`thursday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',1)='7-5','41700',SPLIT_STR(`thursday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',4)='5-12','41700',SPLIT_STR(`thursday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',4)='5-12','42359',SPLIT_STR(`thursday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field thursday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',2)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',2)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',2),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field thursday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',3)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',3)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',3),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field thursday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',5)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',5)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',5),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field thursday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`thursday`,',',6)='0-0',null,CONCAT('4',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`thursday`,',',6)='0-0',null,CONCAT('4',IF(SPLIT_STR(SPLIT_STR(`thursday`,',',6),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`thursday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- fridays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',1)='7-5','50700',SPLIT_STR(`friday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',1)='7-5','51700',SPLIT_STR(`friday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',4)='5-12','51700',SPLIT_STR(`friday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',4)='5-12','52359',SPLIT_STR(`friday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field friday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',2)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',2)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',2),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field friday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',3)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',3)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',3),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field friday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',5)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',5)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',5),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field friday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`friday`,',',6)='0-0',null,CONCAT('5',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`friday`,',',6)='0-0',null,CONCAT('5',IF(SPLIT_STR(SPLIT_STR(`friday`,',',6),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`friday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION



-- saturdays
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',1)='7-5','60700',SPLIT_STR(`saturday`,',',1)) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',1)='7-5','61700',SPLIT_STR(`saturday`,',',1)) AS `ending`
FROM `availability`
UNION

-- All Evening
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',4)='5-12','61700',SPLIT_STR(`saturday`,',',4)) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',4)='5-12','62359',SPLIT_STR(`saturday`,',',4)) AS `ending`
FROM `availability`
UNION

-- 2nd field saturday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',2)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',2),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',2)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',2),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',2),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 3rd field saturday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',3)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',3),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',3)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',3),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',3),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 5th field saturday 
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',5)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',5),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',5)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',5),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',5),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`
UNION

-- 6th field saturday
SELECT 
	null AS `id`,
	LOWER(SUBSTRING(`id`,1,8)) AS `application`,
	IF(SPLIT_STR(`saturday`,',',6)='0-0',null,CONCAT('6',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',6),'-',1),':',''),4,'0')) ) AS `beginning`,
	IF(SPLIT_STR(`saturday`,',',6)='0-0',null,CONCAT('6',IF(SPLIT_STR(SPLIT_STR(`saturday`,',',6),'-',2)='0','2359',LPAD(REPLACE(SPLIT_STR(SPLIT_STR(`saturday`,',',6),'-',2),':',''),4,'0'))) ) AS `ending`
FROM `availability`) AS t WHERE `application` IN (SELECT `id` FROM `personnel`.`applications`) AND (`beginning`<> '' OR `beginning`<>`ending`) ORDER BY `application`;

--
-- Add Supervisors
--

INSERT INTO `personnel`.`profiles`
SELECT 
	LOWER(`username`) AS `pid`,
	FLOOR(200  + RAND() * (400-200)) AS `vtid`,
	`username` AS `firstname`,
	null AS `middlename`,
	`username` AS `lastname`,
	null AS `birthday`,
	null AS `class`,
	null AS `major`,
	null AS `unit`,
	CASE `class`
		WHEN 'A' THEN 'ADMIN'
		WHEN 'S' THEN 'SUPERVISOR'
		
	END
	AS `type`,
	'MALE' AS `gender`,
	18 AS `ethnicity`,
	0 AS trash 
FROM `security` WHERE LOWER(`username`) NOT IN (SELECT `pid` FROM `personnel`.`profiles`)
;


--SELECT * FROM `security`;

INSERT INTO `personnel`.`jobs`
SELECT `id`, `label`, `description`, `supervisor`,`trash` FROM
(SELECT
	null AS 'id',
	SPLIT_STR(`dept_code`,',',1) AS 'label',
	'imported' AS 'description',
	`username` AS 'supervisor',
	`dept`,
	0 AS 'trash'
FROM `security`
UNION
SELECT
	null AS 'id',
	SPLIT_STR(`dept_code`,',',2) AS 'label',
	'imported' AS 'description',
	`username` AS 'supervisor',
	`dept`,
	0 AS 'trash'
FROM `security`
UNION
SELECT
	null AS 'id',
	SPLIT_STR(`dept_code`,',',3) AS 'label',
	'imported' AS 'description',
	`username` AS 'supervisor',
	`dept`,
	0 AS 'trash'
FROM `security`
UNION
SELECT
	null AS 'id',
	SPLIT_STR(`dept_code`,',',4) AS 'label',
	'imported' AS 'description',
	`username` AS 'supervisor',
	`dept`,
	0 AS 'trash'
FROM `security`
UNION
SELECT
	null AS 'id',
	SPLIT_STR(`dept_code`,',',5) AS 'label',
	'imported' AS 'description',
	`username` AS 'supervisor',
	`dept`,
	0 AS 'trash'
FROM `security`
UNION
SELECT
	null AS 'id',
	SPLIT_STR(`dept_code`,',',6) AS 'label',
	'imported' AS 'description',
	`username` AS 'supervisor',
	`dept`,
	0 AS 'trash'
FROM `security`
UNION
SELECT
	null AS 'id',
	SPLIT_STR(`dept_code`,',',7) AS 'label',
	'imported' AS 'description',
	`username` AS 'supervisor',
	`dept`,
	0 AS 'trash'
FROM `security`
) AS t WHERE label <> '' GROUP BY supervisor, label ORDER BY `dept` DESC, supervisor;

DELETE FROM `profiles` WHERE `pid` IN (SELECT `pid` FROM `applications` WHERE UNIX_TIMESTAMP(`expire`) < (UNIX_TIMESTAMP(CURDATE()) - 10000000));
-- Correct money format
UPDATE `answers` SET `answer`=FORMAT(REPLACE(REPLACE(`answer`,'$',''),',',''),2) WHERE `question`='workstudy';
/*
SELECT * FROM `personnel`.`jobs`;
SELECT * FROM `e_admin` JOIN `applicants` ON `e_admin`.`id`=`applicants`.`id` WHERE `status`='E';

SELECT * FROM `e_admin` e LEFT OUTER JOIN `personnel`.`jobs` u ON e.`dept`=u.`label` WHERE `status`='E' GROUP BY e.`table_id`;


SELECT * FROM `personnel`.`jobs` u JOIN (

SELECT * FROM `e_admin` JOIN `applicants` ON `e_admin`.`id`=`applicants`.`id` WHERE `status`='E'

)  t ON u.`label`=t.`dept`;
*/
