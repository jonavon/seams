	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ACIS','Accounting & Information Systems',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AE','Aerospace Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AAEc','Agricultural & Applied Economics',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ALS','Agricultural and Life Sciences',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AnSc',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','APSc','Animal & Poultry Sciences',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AOE','Aerospace & Ocean Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Arch','Architecture',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Art','Art & Art History',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AS','ROTC - Air Force ',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','BC','Building Construction',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Bchm','Biochemistry',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Biol','Biological Sciences',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AfSt','Aficana/Black Studies (SOC)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Bot',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','BSE','Biological Systems Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','CEE','Civil & Environmental Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ChE','Chemical Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Chem','Chemistry',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Chn','Chinese (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Cla','Classics (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','apE',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Comm','Communication',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','CS','Computer Science',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','CSES','Crop & Soil Environmental Sciences',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','CT',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','DaSc','Dairy Science',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EAd',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Econ','Economics',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdAC',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdAd',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdAE',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdCC',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdCI','Education, Curriculum Instruction',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EDP','Environmental Design Planning ',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdHL',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdPE','Education, Physical Education',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdSE','Education, Special Education Administration',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdSP',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EdVT',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ECE','Electrical Computer Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EF',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Engl','English',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EnSc','Environmental Science',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Ent','Entomology',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ESEn','Enviromental Sci & Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ESM','Engineering Science Mechanics',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','FA','Fine Arts',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','HD','Human Development',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Fin','Finance, Insurance, Business',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','FiW','Fisheries Wildlife Science',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','For','Forestry',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','FL','Foreign Languages Literatures',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Fr','French (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','FST','Food Science Technology',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','GE','General Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Geog','Geography',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Geol','Geological Sciences',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Geop',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Gen',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Ger','German (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Gr','Greek (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','HIDM','Housing, Int Des & Res Mgt',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Hist','History',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','HNFE','Human Nutrition, Foods, Exercise',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Hort','Horticulture',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','HTM','Hospitality Tou Management',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Hum','Humanities (IDST)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','IDS','Industrial Design',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ISE','Industrial Systems Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','InfS','Information Systems',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','IdSt','Interdisciplinary Studies',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','IS','International Studies',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','LAr','Landscape Architecture',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Math','Mathematics',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ME','Mechanical Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','MESc','Materials Engineering Science',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Mgt','Management',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Micr',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','MINE','Mining Minerals Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Mktg','Marketing',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','MN','ROTC - Navy/Marines',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','MS','ROTC - Army',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Msci',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','MSE','Materials Science Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Mus','Music',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NE',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','OE','Ocean Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Phil','Philosophy',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Phys','Physics',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','PPWS','Plant Pathology, Physiology, Weed Science',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Psci','Political Science',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Psyc','Psychology',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Rel','Religious Studies (IDST)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Soc','Sociology',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Span','Spanish (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Stat','Statistics',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','TA','Theatre Arts',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','UA',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','UAAC',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','UAP','Urban Affairs Planning (SPIA)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','UD',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','URPl','Urban & Regional Planning',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','VMS',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','VM','Veterinary Medicine',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Wood','Wood Science Forest Products',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','WS','Womens Studies (IDST)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Zool',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','PIA','Public & International Affairs',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NEID',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','PAPA','Public Admin/Public Affairs (SPIA)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Other','Unknown',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Other','Unknown',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Other','Unknown',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ELPS','Educational Leadership Policy Studies',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','Teach',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AEE','Agricultural & Extension Education',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AGED','Secondary Education, Agricultu',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AHRM','Apparel, Housing, & Resource Mgt',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AINS','American Indian Studies',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ALHR','Adult & Continuing Education',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','AT','Agricultural Technology',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ATSC',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','BIT','Business Information Technology',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','CEP','Cooperative Education Program',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','CIS',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EDCO','Education, Counseling',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EDCT','Education, Career & Technical Education',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EDRE','Education, Research Evaluation',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','EDTE',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ENGR','Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','GBCB','Gen, Bioinformatics, Comp Biol',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','GRAD','Graduate School',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','HEB','Hebrew (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','HR','Human Resources',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','HST','Humanities, Science, Technology (STS)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ISEP',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ITAL','Italian (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','ITDS','Interior Design',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','JPN','Japanese (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','JUD','Judaic Studies (IDST)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','LAT','Latin (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','LDRS','Leadership Studies (IDST)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','MACR','Macromolecular Science Engineering',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','MASC','Mathematical Sciences',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NECT',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NEHS',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NERM',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NR','Natural Resources',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NSE',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','NSEP','National Student Exchange',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','PORT','Portuguese (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','POUL',NULL,0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','RUS','Russian (FL)',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','SPIA','School of Public International Affairs',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','STS','Science Technology Studies',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','UH','University Honors Program',0,0 );
	INSERT INTO `enumtable` VALUES ( null,'MAJOR','BmVS','Biomed & Veterinary Sciences',0,0 );
