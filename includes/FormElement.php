<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File includes/FormElement.php
 *
 * Long description for file (if any)...
 *
 * @copyright Copyright (c) 2008 University Libraries, Virginia Tech
 * @license http://license.url
 * @version $Id:$
 * @link http://project.url
 * @since File available since the 0.0.0 Wed Oct 8 2008 10:56:12
 */

/**
 * class FormElement
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */
abstract class FormElement {
	/*** Constants: ***/
	const BUTTON_TYPE = 'button';
	const CHECKBOX_TYPE = 'checkbox';
	const FILE_TYPE = 'file';
	const HIDDEN_TYPE = 'hidden';
	const PASSWORD_TYPE = 'password';
	const RADIO_TYPE = 'radio';
	const TEXT_TYPE = 'text';
	const TEXTAREA_TYPE = 'textarea';
	const TAGS_TYPE = 'tags';
	const CHECKSET_TYPE = 'checkset';
	const SELECTSET_TYPE = 'selectset';
	const SELECT_TYPE = 'select';

	/*** Attributes: ***/

	/**
	 * Required element.
	 */
	protected $required;

	/**
	 * label for element.
	 */
	protected $label;

	/**
	 * A unique name for an input element.
	 */
	protected $name;

	/**
	 * Element that cannot be modified.
	 */
	protected $readonly;

	/**
	 * Type of form element. 
	 */
	protected $type;

	/**
	 * Value of the form element.
	 */
	protected $value;

	/**
	 * Extra class attributes.
	 */
	protected $classes;

	/**
	 * Extra attributes. Containes an array of attributes and its values.  
	 */
	protected $attributes;

	/**
	 * addToClasses 
	 * 
	 * @param mixed $string 
	 * @access public
	 * @return void
	 */
	public function addToClasses($string) {
		$this->classes[] = $string;
	} // end of method addToClasses

	/**
	 * toHTML 
	 * 
	 * @abstract
	 * @access public
	 * @return void
	 */
	abstract public function toHTML();

	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 */
	public function __get($field) {
		return $this->$field;
	} // end of method __get

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		$this->$field = $value;
	} // end of method __set
	
	/**
	 * Method formatAttributes .
	 * Takes the attribute array and formats it for html.
	 * 
	 * @access protected
	 * @return void
	 */
	protected function formatAttributes() {
		$attr;
		foreach($this->attributes as $key=>$value) {
			$v = htmlspecialchars($value, ENT_QUOTES, null, false);
			$attr .= "$key='$v' ";
		}
		return $attr;
	} // end of method importAttributes
	/**
	 * selfAttr 
	 * 
	 * @param mixed $string 
	 * @access protected
	 * @return void
	 */
	protected function selfAttr($string) {
		return "$string='$string'";
	}

} // end of FormElement
