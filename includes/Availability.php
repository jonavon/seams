<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
require_once 'Vader.php';

class Availability implements Vader {
	private $db;
	private $id;
	private $application;
	private $beginning;
	private $ending;
	private $dayoftheweek;
	private $starttime;
	private $endtime;

	public function __construct(PDO $database) {
		$this->db = $database;
		$this->id = null;
		$this->application = null;
		$this->beginning = null;
		$this->ending = null;
	}
	public function __get($field) {
		return $this->$field;
	}
	public function __set($field, $value) {
		switch($field){
			case 'application':
				if($value instanceof Application){
					$this->setApplication($value);
				}
				else {
					$this->setApplicationFromId($value);
				}
				break;
			case 'beginning':
			case 'ending':
				$value = sprintf('%05d', $value);
				$dotw = substr($value, 0, 1);
				$hotd = substr($value, 1, 2);
				$motd = substr($value, 3, 2);
				$this->dayoftheweek = date('l', strtotime("+$dotw day", strtotime('Last Sunday')));
				if($field == 'beginning'){
					$this->starttime = date("g:i a", mktime((int)$hotd, (int)$motd));
				}
				else {
					$this->endtime = date("g:i a", mktime((int)$hotd, (int)$motd));
				}
				$this->$field = $value;
				break;
			case 'dayoftheweek':
			case 'starttime':
			case 'endtime':
				throw new Exception("Field $field is immutable.");
			default:
				$this->$field = $value;
		}
	}

	public function view($id = null) {
		$result = null;
		if($id == null) {
			$id = $this->id;
		}
		$sql = "SELECT `id`, `application`, LPAD(`beginning`, 5, 0) AS 'beginning', LPAD(`ending`, 5, 0) AS 'ending' FROM `availability` WHERE `id` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_INT);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$result) {
			throw new Exception("Availability with $id does not exist within the database.");
		}
		foreach($result as $key=>$value){
			if($value !== null)	{
				$this->__set($key, $value);
			}
		}
		return $result;
	}

	public function add() {
		$this->edit();
	}
	public function delete() {
		$id = $this->id;
		$sql = "DELETE FROM `availability` WHERE id=:id";
		try {
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':id', $id, PDO::PARAM_STR);
			$statement->execute();
		}
		catch (Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	}

	public function edit() {
		if(isset($this->application)){
			$sql = "REPLACE INTO `availability` VALUES (:id, :application, :beginning, :ending)";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $this->id, PDO::PARAM_INT);
				$statement->bindParam(':application', $this->application, PDO::PARAM_STR);
				$statement->bindParam('beginning', $this->beginning, PDO::PARAM_INT);
				$statement->bindParam('ending', $this->ending, PDO::PARAM_INT);
				if($statement->execute()){
					$this->id = $this->db->lastInsertID();
					$this->view();
				}
				else {
					throw new Exception("There was an error with this statement:" . $statement->errorInfo() . " \n $sql");
				}
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot add availability without an Application ID.");
		}
	}

	public function restore() {
		throw new Exception("The function " . __FUNCTION__ . "is not implemented.");
	}

	public function purge() { 
		$this->delete();
	}

	public function read($field, $reread = false) {
		throw new Exception("The function " . __FUNCTION__ . "is not implemented.");
	}

	public function browse($filter = null, Array $suppress = null) {
		$unit = array();
		$sql = "SELECT `applications_v`.*, avl.`id` AS availid, avl.`application`, avl.`beginning`, avl.`ending`, SUM(avl.overlap) AS overlap FROM (";
		 foreach($filter as $avail) {
		 	$unit[] = "SELECT `id`, `application`, `beginning`, `ending`, overlap_interval(`beginning`, `ending`, {$avail['beginning']}, {$avail['ending']}) AS 'overlap' FROM `availability`";
		 }
		$sql .= implode("\nUNION\n", $unit);
		$sql .= ") AS avl JOIN `applications_v` ON `applications_v`.`id` = avl.`application` WHERE avl.overlap > 0 GROUP BY avl.`application` ORDER BY overlap DESC;";
		$statement = $this->db->prepare($sql);
		if(!$statement->execute()){
			throw new Exception("Unable to get applications.");
		}
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	/**
	 * setApplication 
	 * 
	 * @param Application $application 
	 * @access private
	 * @return void
	 */
	private function setApplication(Application $application) {
		$this->application = $application->__get('id');
	}

	/**
	 * setApplicationFromId 
	 * 
	 * @param mixed $id 
	 * @access private
	 * @return void
	 */
	private function setApplicationFromId($id) {
		if(isset($id)){
			$this->application = $id;
		}
		else {
			throw new Exception("Parameter cannot be null");
		}
	}
} // end of Availability
