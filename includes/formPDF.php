<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

require 'MyPDF.php';

class formPDF extends MyPDF {
	const CM = 28.3464566929; //{72/2.54};
	const MM = 2.83464566929; //{72/2.54};
	const IN = 72;
	public $docheader = 'University Libraries';
	public function __construct() {
		parent::__construct('P', 'pt', 'A4');
		$this->AliasNbPages();
		$this->setHeaderMargin(PDF_MARGIN_TOP);
		$this->setFooterMargin(PDF_MARGIN_BOTTOM);
		$this->setAutoPageBreak(true, PDF_MARGIN_BOTTOM);
	}
	public function Header() {
		$this->SetFont('helvetica', null, 8);
		$this->SetFont('helvetica', 'B', 16);
		$this->Cell(0, 16, $this->docheader, 0, 1, 'C');
		$this->SetFont('helvetica', 'B', 14);
		$this->Cell(0, 14, 'Wage Employee Appointment Record', 0, 1, 'C');
	}

	public function drawSectionHeader($text) {
		$this->Ln(16);
		$this->SetFont('helvetica', 'B', 12);
		$this->Cell(0, 12, $text . ':', 0, 1, 'L');
	}

	public function drawQA($question, $answer, $link=null) {
		$this->SetFont('helvetica', 'B', 12);
		$this->Cell(140, 12, strtoupper($question) . ':', 0, 0, 'L');
		$this->Cell(8);
		$this->SetFont('helvetica', '', 12);
		$this->SetTextColor(0);
		$this->Cell(390, 12, '    '.$answer.'    ', 'B', 1, 'L', false, $link);
		$this->Ln(12);
	}

	public function drawIndentedQA($question, $answer, $link=null) {
		$this->Cell(140);
		$this->SetFont('helvetica', 'B', 12);
		$this->Cell(200, 12, $question . ':', 0, 0, 'L');
		$this->Cell(8);
		$this->SetFont('helvetica', '', 12);
		$this->SetTextColor(0);
		$this->Cell(190, 12, '    '.$answer.'    ', 'B', 1, 'L', false, $link);
		$this->Ln(12);
	}

	public function drawBoxedInQA($question, $answer, $link=null) {
		$this->Cell(40);
		$this->SetFont('helvetica', 'B', 12);
		$this->Cell(140, 12, strtoupper($question) . ':', 0, 0, 'L');
		$this->Cell(8);
		$this->SetFont('helvetica', '', 12);
		$this->SetTextColor(0);
		$this->Cell(310, 12, '    '.$answer.'    ', 'B', 1, 'L', false, $link);
		$this->Ln(12);
	}

	public function Footer() {
		$this->SetY(-15 - self::CM);
		$this->SetFont('helvetica', 'BI', 12);
		$this->Cell(350, 12, '  Supervisor Signature', 'T', 0, 'L');
		$this->Cell(40);
		$this->Cell(140, 12, '  Date', 'T', 0, 'L');
	}



}
