<?php

/**
 * File ./includes/global.inc.php
 *
 * Global include File.
 *
 * @package SEAS
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @copyright Copyright (c) 2008 University Libraries, Virginia Tech
 */
error_reporting(E_ALL | E_STRICT);
ini_set("memory_limit", "128M");
$messages = array();
define('ROOT', dirname(dirname(__FILE__)));
set_include_path(dirname(__FILE__) . PATH_SEPARATOR . get_include_path());
// Read configuration file
$_seas_config = parse_ini_file(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'seas.ini', 1);

date_default_timezone_set($_seas_config["SEAS"]["timezone"]);

/*
 * These Constants come from the ini file within the root directory.
 */
define('SEAS_ROOT', $_seas_config["SEAS"]["root"]);
define('SEAS_WEB_ROOT', $_seas_config["SEAS"]["base"]);
define('SEAS_ADMIN_EMAIL', $_seas_config["SEAS"]["admin"]);
define('SEAS_DB', $_seas_config["DATABASE"]["dsn"]);
define('SEAS_DB_USER', $_seas_config["DATABASE"]["user"]);
define('SEAS_DB_PASS', $_seas_config["DATABASE"]["password"]);
define('TEMPLATE_DIR', $_seas_config["TEMPLATE"]["template_dir"]);
define('SESSION_LIMIT', $_seas_config["SESSION"]["timelimit"]);
define('SEAS_CALENDAR', $_seas_config["CALENDAR"]["filename"]);
// End Read configuration File
define('ACTION_DIR', SEAS_ROOT . DIRECTORY_SEPARATOR . 'public/actions');

define('APP_NOTICE', 'notice');
define('APP_WARNING', 'alert');
define('APP_ERROR', 'error');
define('APP_QUESTION', 'question');
////////////////////////////////////
// Persistant Database Connection //
////////////////////////////////////
try {
	$db = new PDO(SEAS_DB, SEAS_DB_USER, SEAS_DB_PASS, array(PDO::ATTR_PERSISTENT => true,PDO::MYSQL_ATTR_USE_BUFFERED_QUERY=> true));
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
	/*
		 $session = new Session($db);
		 $session->__set('expire', SESSION_LIMIT);
	 */
	// problem with sessions in safari made me not use the Session class.
	session_set_cookie_params(SESSION_LIMIT * 60);
	session_start();
} catch (Exception $e) { $messages[] = array(
		'type' => APP_ERROR,
		'content' => 'System Down: Unable to connect to database.'
		);
$template_vars['sys_error'] = true;
}


///////////////////////
// Utility Functions //
///////////////////////

/**
 *
 * Function __autoload.
 *
 * Loads the class automagically.
 *
 * @return void
 * @access public
 */
function __autoload($c) {
	require_once $c.'.php';
}

/**
 * Function seas_error_handler.
 * A user error handler for this application.
 *
 * @return void
 */
function seas_error_handler($e_number, $e_string,$e_file = null,$e_line = null,$e_context = null) {
	global $messages,$debug;
	$nomessage = false;
	switch($e_number) {
		case E_USER_NOTICE :
			$type = APP_NOTICE;
			if(isset($debug)){
				$debug->info($e_string);
			}
			break;
		case E_USER_WARNING:
			$type = APP_WARNING;
			if(isset($debug)){
				$debug->warn($e_string);
			}
			break;
		case E_USER_ERROR :
			$type = APP_ERROR;
			if(isset($debug)){
				$debug->error($e_string);
			}
			break;
		default :
			$type = APP_ERROR;
			$headers = "";
			$headers .= "Date: " . date('r') . "\n";
			$headers .= "From: SEAS System <no-reply@{$_SERVER['HTTP_HOST']}>\n";
			$headers .= "Reply-To: " . SEAS_ADMIN_EMAIL . "\n";
			$headers .= "X-Mailer: SEAS Error Log\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
			$headers .= "\n";
			$b = "<html><body>UNKNOWN ERROR [$e_number]: $e_string <a href='vim://$e_file@$e_line'>$e_file line:$e_line</a></body></html>\n";
			mail(SEAS_ADMIN_EMAIL, '[SEAS]: System Error', $b,$headers);
			$nomessage = true;
			if(isset($debug)){
				$debug->error($e_string);
				$nomessage = false;
			}
	}
	if(!$nomessage){
	$messages[] = array(
			'type' => $type,
			'content'=> $e_string
			);
	}
	if(isset($debug)) {
		$debug->trace("Trace: $e_string");
	}
}
set_error_handler('seas_error_handler');
/**
 *
 * This is the short Description for the Class
 *
 * This is the long description for the Class
 *
 * @author		Karl Heinz Marbaise <khmarbaise@gmx.de>
 * @copyright	(c) 2003 by Karl Heinz Marbaise
 * @version		$Id$
 * @package		Package
 * @subpackage	SubPackage
 * @see			??
 */
class SEAMS {
	/**
	 * Function logout_session.
	 * Closes a user session. Creates a message for the messages[] array.
	 */
	public static function logout_session($message = null) {
		global $messages;
		session_unset();
		session_destroy();
		if(empty($_SESSION['pid'])) {
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => ($message == null)?"You have successfully logged out.":$message
					);
		}
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); 
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Expires: Thu, 1 Jan 1970 00:00:00 GMT');
		header('Cache-Control: post-check=0, pre-check=0', false);
		header('Pragma: no-cache');
	}


	/**
	 * function apply_template 
	 * 
	 * Execute a PHP template file and return the result as a string.
	 *
	 * @param mixed $file 
	 * @param array $vars 
	 * @param mixed $include_globals 
	 * @access public
	 * @return String applied template file.
	 * @see http://www.bigsmoke.us/php-templates/functions
	 */
	public static function apply_template($file,$vars=array(),$include_globals=false) {
		extract($vars);
		if($include_globals){
			extract($GLOBALS, EXTR_SKIP);
		}
		ob_start();
		require($file);
		$template_html = ob_get_contents();
		header('Content-Length: ' . ob_get_length());
		ob_end_clean();
		unset($vars);
		return $template_html;
	}

	/**
	 * isPermitted 
	 * 
	 * @param Array $types 
	 * @param mixed $userPermission 
	 * @access public
	 * @return void
	 */
	public static function isPermitted(Array $types, $userPermission) {
		$resourcePermission = array_reduce($types, create_function('$n,$m', '$n += (int)$m; return $n;'), 0);
		return $resourcePermission & (int)$userPermission;
	}
}

