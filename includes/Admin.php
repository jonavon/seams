<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Profile.php. 
 * Contains the class definition of Admin.
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @copyright Copyright (c) University Libraries, Vriginia Tech.
 */

require_once 'global.inc.php';


/**
 * class Admin.
 * An instance of Admin denotes a user who has Administrative permission.
 */
class Admin extends Supervisor {
	/*** Attributes: ***/
	protected $permission = Profile::ADMIN_LEVEL;
	 
	/**
	 * pay()
	 * Pay applicant to a job.
	 * Pending candidate is changed to Employee type. Additionally he or she is
	 * given a paycode to associate position with funding.
	 *
	 * @param job
	 * @return 
	 * @access public
	 */
	public function pay(Position $position, $paycode) {
		try {
			$candidate = new Pending($position->employee, $this->db);
			$candidate->view();
			// Make an employee.
			$candidate->employ();
			// Add paydcode
			$position->__set('paycode', $paycode);
			$position->edit();
		}
		catch (Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	} // end of member function hire
} // end of Admin
