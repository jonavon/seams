<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Profile.php 
 * 
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @copyright Copyright (c) University Libraries, Vriginia Tech.
 */
/**
 * class Profile
 */
abstract class Profile implements Vader {

	const	APPLICANT = 'APPLICANT';
	const	PENDING = 'PENDING';
	const	EMPLOYEE = 'EMPLOYEE';
	const	SUPERVISOR = 'SUPERVISOR';
	const PLUS = 'PLUS';
	const	ADMIN = 'ADMIN';
	/*** Error ***/
	const E_NO_USER = 19456; // 19 * 1024
	/*** Permission Constants ***/
	const APPLICANT_LEVEL = 1;
	const EMPLOYEE_LEVEL = 2;
	const SUPERVISOR_LEVEL = 4;
	const ADMIN_LEVEL = 8;
	const PENDING_LEVEL = 16;
	const PLUS_LEVEL= 32;
	const NEW_LEVEL= 64; // Not used in subclasses.

	/*** Attributes: ***/
	/**
	 * db 
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $db;

	/**
	 * modifiedArray[] Stores the columns that have changed for edit
	 * operation.
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $modifiedArray;

	/**
	 * fieldValues 
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $fieldValues;

	/**
	 * __CLASS__ 
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $__CLASS__;

	/**
	 * pid 
	 * 8 Character university Identification.
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $pid;

	/**
	 * vtid 
	 * Virginia Tech ID.
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $vtid;

	/**
	 * firstname 
	 * Given Name of the Profile.
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $firstname;

	protected $middlename;

	/**
	 * Family name of the Profile.
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $lastname;

	protected $birthday;

	/**
	 * Primary Email Address.
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $email;

	/**
	 * gender 
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $gender;

	/**
	 * Ethnicity or profile
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $ethnicity;

	/**
	 * contact 
	 * 
	 * @var mixed
	 * @access protected
	 */
	protected $contact;

	protected $permission;
	protected $preferences;
	protected $settings;
	/**
	 * __construct 
	 * 
	 * @param mixed $pid 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function __construct($pid=null, PDO $database=null) {
		$this->pid=$pid;
		$this->__CLASS__ = get_class($this);
		if($database != null) {
			$this->setDatabase($database);
		}
		if($pid != null) {
			$this->view();
		}
		else {
			$this->vtid = 000000000;
			$this->firstname = null;
			$this->middlename = null;
			$this->lastname = null;
			$this->birthday = null;
			$this->email = null;
			$this->gender=null;
			$this->ethnicity=null;
			$this->contact=array();
			$this->preferences=array();
		}
		$this->modifiedArray = array();
		$this->fieldValues = array();
		$this->settings = array();
	}

	/**
	 * View Messages to or from this profile.
	 *
	 * @return Array
	 * @access protected
	 */
	/*
	protected function viewMessages( ) {
		trigger_error("Implement " . __FUNCTION__);
	} // end of member function viewMessages
	*/

	/**
	 *
	 * @param Array to Array of profiles that the message will be sent. Comparable to email field.
	 * @param String text Message text.
	 * @param String subject Subject field.
	 * @return 
	 * @access protected
	 */
	/*
	protected function sendMessage( $to,  $text,  $subject = NULL ) {
		trigger_error("Implement " . __FUNCTION__);
	} // end of member function sendMessage
	*/


	/**
	 * setDatabase 
	 * 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function setDatabase(PDO $database) {
		$this->db = $database;
	}

	/**
	 *
	 * @param String id Id for object
	 * @return Array
	 * @access public
	 */
	public function view( $pid = null ) {
		$result = null;
		if($pid == null) {
			$pid = $this->pid;
			$this->__set('type', strtoupper(get_class($this)));
		}
		$sql = "SELECT * FROM `profiles_v` WHERE `pid` = :pid";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':pid', $pid, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$result) {
			throw new Exception("Profile $pid does not exist within the database.");
		}
		foreach($result as $key=>$value){
			if($value !== NULL)	{
				$this->$key = $value;
			}
		}
		$result['contact'] = $this->loadContactInfo();
		$this->modifiedArray = array();
		return $result;
	} // end of member function view

	/**
	 * Add Object.
	 *
	 * @return 
	 * @access public
	 */
	public function add( ) {
		if($this->modifiedArray != null) {
			$sql = "INSERT INTO `profiles` (";
			$sql .= implode(", ", array_keys($this->modifiedArray));
			$sql .= ") VALUES ('";
			$sql .= implode("', '", array_values($this->modifiedArray));
			$sql .= "')";
			try {
				$this->db->beginTransaction();
				$statement = $this->db->prepare($sql);
				if($statement->execute()) {
					$this->db->commit();
				}
				else {
					throw new Exception("There was a problem inserting profile: $sql " . $statement->errorInfo());
				}
			}
			catch (Exception $e) {
				$this->db->rollback();
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("No attributes set.");
		}
	} // end of member function add

	/**
	 * Throws object into the trash without permenantly deleting.
	 *
	 * @return 
	 * @access public
	 */
	public function delete( ) {
		$pid=$this->pid;
		$sql = "UPDATE `profiles` SET `trash`=1 WHERE `pid`=:pid";
		try {
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':pid', $pid, PDO::PARAM_STR);
			$statement->execute();
		}
		catch (Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	} // end of member function delete

	/**
	 * Edit object.
	 *
	 * @return 
	 * @access public
	 */
	public function edit( ) {
		if($this->modifiedArray != null) {
			$pid = $this->pid;
			$set = array();
			$sql = "UPDATE `profiles` SET ";
			foreach($this->modifiedArray as $column => $value) {
				$value = (empty($value))?"null":"'" . addslashes($value). "'";
				$set[] = "`$column` = $value";
			}
			$sql .= implode(", ", $set);
			$sql .= " WHERE pid = '$pid'";
			try {
				$statement = $this->db->prepare($sql);
				$statement->execute();
				$this->modifiedArray = array();
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		$this->populateFields();
	} // end of member function edit

	/**
	 * Restore an object from trash.
	 *
	 * @param String id Restore from trash.
	 * @return void 
	 * @access public
	 */
	public function restore() {
		$pid =  $this->pid;
		$sql = "UPDATE `profiles` SET `trash`=0 WHERE `pid`=:pid";
		try {
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':pid', $pid, PDO::PARAM_STR);
			$statement->execute();
		}
		catch (Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	} // end of member function restore

	/**
	 * Method browse().
	 *
	 * Browse a list of a resources.        
	 *
	 * @param String filter Filter used to search results.
	 * @return Array
	 * @access public
	 */
	public function browse( $filter = null ) {
		$results = array();
		if(empty($filter)) {
			$sql = "SELECT * FROM `profiles_v`";
		}
		else {
			$sql = "SELECT * FROM `profiles_v` WHERE CONCAT_WS(' ' , LOWER(`pid`), LOWER(`vtid`), LOWER(`firstname`), LOWER(`middlename`), LOWER(`lastname`), LOWER(`birthday`), LOWER(`class`), LOWER(`major`), LOWER(`unit`), LOWER(`type`), LOWER(`gender`), LOWER(`ethnicity`)) REGEXP LOWER('$filter')";
		}
		$statement = $this->db->prepare($sql);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	} // end of member function browse

	/**
	 * query.
	 * Gets the search list using the documents returned by the index.
	 * 
	 * @param Array $id 
	 * @access protected
	 * @return void
	 * @deprecated
	 */
	protected function query(Array $documents) {
		$idlist = implode(", ", $documents);
		$sql = "SELECT * FROM `profiles_v` WHERE `vtid` IN ($idlist)";
		$statement = $this->db->query($sql);
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Permanantly delete from database.
	 *
	 * @param String id Identifier for the object
	 * @return 
	 * @access public
	 */
	public function purge() {
		$pid = $this->pid;
		$sql = "DELETE FROM `profiles` WHERE pid=:pid";
		try {
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':pid', $pid, PDO::PARAM_STR);
			$statement->execute();
		}
		catch (Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	} // end of member function purge

	/**
	 * Get actual value from database which may be a reference id number.
	 *
	 * @param String field Database field
	 * @param bool reread If set to true operation will query database again.
	 * @return Array
	 * @access public
	 */
	public function read( $field,  $reread = false ) {
		if(count($this->fieldValues) == 0 || $reread) {
			$this->populateFields();
		}
		return $this->fieldValues[$field];
	} // end of member function read
	/**
	 * loadProfile 
	 * Look at database entry and create an appropriate profile type based on the
	 * type field of the table.
	 * 
	 * @param mixed $pid 
	 * @param mixed $database 
	 * @static
	 * @access public
	 * @return void
	 */
	public static function loadProfile($pid, $database) {
		$sql = "SELECT * FROM `profiles_v` WHERE `pid` = :pid";
		$statement = $database->prepare($sql);
		$statement->bindParam(':pid', $pid, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		$p;
		if(!$result) {
			throw new Exception("Profile $pid does not exist within the database.", self::E_NO_USER);
		}
		else {
			switch($result['type']) {
				case Profile::EMPLOYEE :
					$p = new Employee($pid, $database);
					break;
				case Profile::SUPERVISOR:
					$p = new Supervisor($pid, $database);
					break;
				case Profile::ADMIN :
					$p = new Admin($pid, $database);
					break;
				case Profile::APPLICANT :
				default:
					$p = new Applicant($pid, $database);
			}
			foreach($result as $key=>$value){
				if($value !== NULL)	{
					$p->__set($key, $value);
					$p->__set('modifiedArray', null);
				}
			}
		}
		return $p;	
	}
	/**
	 * Method populateFields.
	 * Fill the fieldValues array with actual values from the database. 
	 *
	 * @access protected
	 * @return void
	 */
	protected function populateFields() {
		$pid = $this->pid;
		$sql = "SELECT * FROM `profiles` WHERE `pid` = :pid";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':pid', $pid, PDO::PARAM_STR);
		$statement->execute();
		$this->fieldValues = $statement->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * contactInfo 
	 * 
	 * @param mixed $type 
	 * @param mixed $key 
	 * @param mixed $value 
	 * @access protected
	 * @return void
	 */
	protected function contactInfo($type, $key, $value) {
		if(isset($this->pid)){
			$type = strtoupper($type);
			$key = strtoupper($key);
			$pid = $this->pid;
			$sql = "INSERT INTO `contact` VALUES (:pid, :type, :key, :value) ON DUPLICATE KEY UPDATE `value`=:value2";
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':pid', $pid, PDO::PARAM_STR);
			$statement->bindParam(':type', $type, PDO::PARAM_STR);
			$statement->bindParam(':key', $key, PDO::PARAM_STR);
			$statement->bindParam(':value', $value, PDO::PARAM_STR);
			$statement->bindParam(':value2', $value, PDO::PARAM_STR);
			if($statement->execute()) {
				$this->contact[$type][$key] = $value;
			}
			else {
				throw new Exception("There is a problem inserting the contact information: {$statement->errorInfo()}");
			}
		}
		else {
			throw new Exception("PID must be set before contact info can be created.");
		}
	}

	public function insertContactInfo(Array $contactinfo) {
		if(isset($this->pid)){
			foreach($contactinfo as $type => $info){
				foreach($info as $key => $value) {
					$this->contactInfo($type, $key, $value);
				}
			}
		}
		else {
			throw new Exception("PID must be set before contact info can be created.");
		}
	}

	protected function loadContactInfo() {
		$pid = $this->pid;
		$sql = "SELECT * FROM `contact` WHERE `pid` = '$pid'";
		$statement = $this->db->query($sql);
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		$data = array();
		foreach($results as $info) {
			$data[$info['type']][$info['key']] = $info['value'];
		}
		return $data;
	}
	/**
	 * Overloaded function that gets a specified field.
	 *
	 * @param String field Object field to get
	 * @return String
	 * @access public
	 */
	abstract public function __get( $field );
	 // end of member function __get

	/**
	 * Overloaded function that sets a specified field.
	 *
	 * @param String field Field to change
	 * @param String value Value that the field should be changed to.
	 * @return 
	 * @access public
	 */
	abstract public function __set( $field,  $value );
	 // end of member function __set

	/**
	 * loadSettings 
	 * 
	 * @access public
	 * @return void
	 */
	public function loadSettings() {
		if(isset($this->pid)){
			$sql = "SELECT `label`, `id`, `value` FROM `settings_v` WHERE `pid` = :pid";
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':pid', $this->pid, PDO::PARAM_STR);
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			if(!$result) {
				$err = $statement->errorInfo();
				if(isset($err[1])) {
					throw new Exception("There was an error with this statement: {$err[1]} {$err[2]}");
				}
			}
			else {
				foreach($result AS $set) {
					$rs[$set['label']][$set['id']] = array(
						'id' => (int)$set['id'],
						'value' => $set['value']
					);
				}
				$this->settings = $rs;
			}
		}
		else {
			throw new Exception("Supervisors pid must be set before personal settiings can be loaded.");
		}
	}

	/**
	 * changeSetting 
	 * 
	 * @param int $id 
	 * @param string $type 
	 * @param mixed $value 
	 * @param int $sort 
	 * @access public
	 * @return bool 
	 */
	public function changeSetting($id, $type, $value, $sort = null) {
		if(isset($this->pid)){
			$sort = (int)$sort;
			$sql = "INSERT INTO `settings` SELECT :id, :pid, `preferences`.`id`, :value, :sort FROM `preferences` WHERE `label` = :type ON DUPLICATE KEY UPDATE `type` = `preferences`.`id`, `value` = :value2, `sort` = :sort2";
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':id', $id, PDO::PARAM_INT);
			$statement->bindParam(':pid', $this->pid, PDO::PARAM_STR);
			$statement->bindParam(':type', $type, PDO::PARAM_INT);
			$statement->bindParam(':value', $value, PDO::PARAM_STR);
			$statement->bindParam(':sort', $sort, PDO::PARAM_INT);
			$statement->bindParam(':value2', $value, PDO::PARAM_STR);
			$statement->bindParam(':sort2', $sort, PDO::PARAM_INT);
			if(!$statement->execute()) {
				$err = $statement->errorInfo();
				throw new Exception("There was an error with this statement: {$err[1]} {$err[2]}");
			}
			else {
				return true;
			}
		}
		else {
			throw new Exception("Supervisors pid must be set before personal settiings can be changed.");
		}
	}

	/**
	 * deleteSetting 
	 * 
	 * @param mixed $id 
	 * @access public
	 * @return void
	 */
	public function deleteSetting($id) {
		$settingIDs = array();
		foreach($this->__get('settings')AS $type) {
			foreach($type AS $eyedee => $preference) {
				$settingIDs[] = $eyedee;
			}
		}
		if(!in_array($id, $settingIDs)) {
			throw new Exception("This setting does not belong to this Profile.");
		}
		$sql = "DELETE FROM `settings` WHERE `id` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_INT);
		if(!$statement->execute()) {
			$err = $statement->errorInfo();
			throw new Exception("There was an error with this statement: {$err[1]} {$err[2]}");
		}
		else {
			return true;
		}
	}
} // end of Profile
