<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

require('lib/tcpdf/tcpdf.php');

class MyPDF extends TCPDF {
	const CM = 28.3464566929; //{72/2.54};
	const MM = 2.83464566929; //{72/2.54};
	const IN = 72;
	public $docheader = 'Application for Student Employment';

	public function Header() {
	/*
		$this->SetFont('helvetica', null, 8);
		$this->SetXY((-150 - self::CM), self::CM);
		$this->Cell(150, 8, 'Application ID: {appid}', 0, 2);
		$this->Cell(150, 8, 'Expiration Date: {Month D, YYYY}', 0, 0);
*/
		$this->SetX(0);
		$this->SetY(self::CM);
		$this->SetFont('helvetica', 'B', 14);
		$this->Cell(0, 14, $this->docheader, 0, 1, 'C');
	}

	public function drawSectionHeader($text) {
		$this->SetFont('helvetica', 'BU', 12);
		$this->Cell(0, 12, $text, 0, 1, 'L');
	}

	public function drawQA($question, $answer, $link=null) {
		$this->SetFont('helvetica', 'B', 10);
		$this->SetFillColor(204);
		$this->SetTextColor(56);
		$this->Cell(200, 12, strtoupper($question), 0, 0, 'L', true);
		$this->Cell(10);
		if((bool)$link){
			$this->SetTextColor(0, 0, 255);
		}
		else {
			$this->SetTextColor(0);
		}
		$this->Cell(330, 12, '    '.$answer.'    ', 'B', 1, 'L', false, $link);
		$this->Ln(1);
	}

	public function drawCheckBox($question, $boolean, $last=false) {
		$this->SetFont('helvetica', 'B', 10);
		$this->SetFillColor(204);
		$this->SetTextColor(56);
		$this->Cell(255, 12, strtoupper($question), 0, 0, 'L', true);
		if((bool)$last){
			$this->SetTextColor(0, 0, 255);
		}
		else {
			$this->SetTextColor(0);
		}
		$answer = ($boolean)?'X':null;
		$this->Cell(12, 12, $answer, 1, 0, 'L', false, $last);
		if($last) {
			$this->Ln(15);
		}
		else {
			$this->Cell(10);
		}
	}
	public function drawTextArea($question, $answer, $link=null) {
		$this->Ln(1);
		$this->SetFont('helvetica', 'B', 10);
		$this->SetFillColor(204);
		$this->SetTextColor(56);
		$this->Cell(200, 12, strtoupper($question), 0, 1, 'L', true);
		$this->MultiCell(0, 30, $answer, 1, 'L');
		$this->Ln(3);
	}

	public function drawSet(Array $set) {
		$this->Ln(1);
		$this->SetFont('helvetica', 'I', 10);
		$linelength = 0;
		foreach($set as $value){
			$characters = (4 + (6 * strlen($value)));
			$linelength += $characters;
			if($linelength > 540){
				$linelength = 0;
				$this->Ln(15);
			}
			$this->Cell($characters, 12, $value, 'B', 0, 'L', false);
			$this->Cell(3);
		}
		$this->Ln(3);
	}

	private function signage() {
		$this->SetY(-30 - self::CM);
		$this->SetFont('helvetica', 'B', 8);
		$this->Cell(40, 10, 'Signature', 0, 0);
		$this->Cell(356, 10, ' ', 'B', 0, 'L', false);
		$this->Cell(8);
		$this->Cell(20, 10, 'Date', 0, 0);
		$this->Cell(108, 10, ' ', 'B', 0, 'L', false);
	}

	public function Footer() {
		if($this->PageNo() === 1){
			$this->signage();
		}
		$this->SetY(-15 - self::CM);
		$this->SetFont('helvetica', 'I', 6);
		$this->SetTextColor(128);
		$this->Cell(0, 10, $this->footertext, 0, 0, 'L');
		$this->Cell(0, 10, $this->PageNo() . '/{nb}', 0, 0, 'R');

	}

	public function setFooterText($string) {
		$this->footertext = $string;
	}
}
