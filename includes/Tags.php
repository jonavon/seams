<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
class Tags {
	
	/**
	 * cloud 
	 * 
	 * @param PDO $database 
	 * @param mixed $question 
	 * @static
	 * @access public
	 * @return array
	 */
	public static function cloud(PDO $database, $question) {
		$sql = "SELECT `tags`.`tag`, COUNT(`tags`.`tag`) AS \"count\" FROM `tags` JOIN `applications_v` ON `tags`.`application` = `applications_v`.`id` WHERE `tags`.`question` = :question GROUP BY `tags`.`tag`";
		$statement = $database->prepare($sql);
		$statement->bindParam(':question', $question, PDO::PARAM_STR);
		if(!$statement->execute()) {
			throw new Exception("Unable to get tag cloud.");
		}
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}

	/**
	 * browse 
	 * 
	 * @param PDO $database 
	 * @param mixed $question 
	 * @param mixed $tagslist 
	 * @static
	 * @access public
	 * @return void
	 */
	public static function browse(PDO $database, $question, $tagslist) {
		if(empty($tagslist)){
			$sql = "SELECT * FROM `applications_v` WHERE `expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)";
		}
		else {
			if(!is_array($tagslist)){
				$tagslist = explode(',', $tagslist);
			}
			array_walk($tagslist, create_function('&$v', '$v = "`tag`=\'" . trim($v) ."\'";'));
			$sql = "SELECT `applications_v`.* FROM `tags` JOIN `applications_v` ON `tags`.`application`=`applications_v`.`id`";
			$sql .= " WHERE (";
			$sql .= implode(' OR ', $tagslist);
			$sql .= " ) AND `applications_v`.`expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP) ";
			$sql .= " GROUP BY `tags`.`application` ORDER BY COUNT(`tags`.`application`) DESC";
		}
		$statement = $database->prepare($sql);
		if(!$statement->execute()) {
			throw new Exception("Unable to get tag cloud.");
		}
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $result;
	}
}
