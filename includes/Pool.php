<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
require_once 'Job.php';

/**
 * Pool 
 * 
 * @package 
 * @version $id$
 * @copyright University Libraries, Virginia Tech
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */
class Pool {

	/*** Attributes: ***/
	private $db;

	/**
	 * @access private
	 */
	private $id;

	/**
	 * @access private
	 */
	private $job;

	/**
	 * @access private
	 */
	private $application;

	/**
	 * __construct 
	 * 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function __construct(PDO $database) {
		$this->db = $database;
		$this->id = null;
		$this->job = null;
		$this->application = null;
	}
	
	public function view($id = null) {
		$result = null;
		if($id == null) {
			$id = $this->id;
		}
		$sql = "SELECT * FROM `pools` WHERE `id` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_INT);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$result) {
			throw new Exception("Pool $id does not exist within the database.");
		}
		foreach($result as $key=>$value){
			if($value !== null)	{
				$this->__set($key, $value);
			}
		}
		return $result;
	}

	public function add() {
		return $this->edit();
	}

	public function edit() {
		if($this->job != null){
			$sql = "REPLACE INTO `pools` VALUES (:id, :job, :application)";
			$application = (isset($this->application))?$this->application->__get('id'):null;
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $this->id, PDO::PARAM_INT);
				$statement->bindParam(':job', $this->job, PDO::PARAM_INT);
				$statement->bindParam(':application', $application, PDO::PARAM_STR);
				if($statement->execute()){
					$this->id = $this->db->lastInsertID();
					$this->view();
				}
				else {
					throw new Exception("There was an error with this statement:" . $statement->errorInfo() . " \n $sql");
				}
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot add pool without a Job ID.");
		}
	}

	public function removeByApp($job,$app) {
		if($job != null){
			$sql = "DELETE FROM `pools` WHERE `job`=:job AND `application`=:application";
			$application = (isset($this->application))?$this->application->__get('id'):null;
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':job', $job, PDO::PARAM_INT);
				$statement->bindParam(':application', $app, PDO::PARAM_STR);
				if(!$statement->execute()){
					throw new Exception("There was an error with this statement:" . $statement->errorInfo() . " \n $sql");
				}
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot add pool without a Job ID.");
		}
	}
	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 */
	public function __get($field) {
		return $this->$field;
	}

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		if(isset($value)){
			switch($field){
				case 'application' :
					if($value instanceof Application){
						$this->setApplication($value);
					}
					else {
						$this->setApplicationFromId($value);
					}
					break;
				default :
					$this->$field = $value;
			}
		}
		else {
			throw new Exception("Parameters cannot be null.");
		}
	}

	/**
	 * setApplication 
	 * 
	 * @param Application $application 
	 * @access private
	 * @return void
	 */
	private function setApplication(Application $application) {
		$this->application = $application;
	}

	/**
	 * setApplicationFromId 
	 * 
	 * @param mixed $id 
	 * @access private
	 * @return void
	 */
	private function setApplicationFromId($id) {
		if(isset($id)){
		$a = new Application($this->db);
		$a->view($id);
		$this->application = $a;
		}
		else {
			throw new Exception("Parameter cannot be null");
		}
	}
} // end of Pool
