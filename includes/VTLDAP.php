<?php
/**
 * File VTLDAP.php.
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version 2008-08-18
 */
include_once 'global.inc.php';
/**
 * Class VTLDAP.
 *
 * Contains Methods for managing user access to resources.
 *
 * @package		outreach	
 */
class VTLDAP {
	const LDAP_HOST = 'ldap://authn.directory.vt.edu';
	const LDAP_PORT = '389';
	const LDAP_BASE = 'ou=People, dc=vt, dc=edu';

	/**
	 * Method ldap.
	 * 
	 * Uses ldap connect function from VT Middleware. Caveats to it use includes
	 * using this function on a secure server for VT. Additionally the $filter
	 * local variable includes a filter for Library users only.
	 *
	 * @param String $pid 
	 * @param String $credential 
	 * @static
	 * @access public
	 * @return boolean 
	 * @see http://www.middleware.vt.edu/doku.php?id=middleware:ed:edauth:usage#php_applications
	 */
	public static function ldap($pid, $credential) {
		$authenticated = false;
		$host = self::LDAP_HOST;
		$port = self::LDAP_PORT;
		$base = self::LDAP_BASE;
		/*
		 * I wanted to make sure ldap authentication was library specific
		 * probably not necessary.
		 */
		$filter = "(&(uupid=$pid))";

		if(isset($pid) && $pid != '' && isset($credential)) {
			$ldap = ldap_connect($host);
			ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			if(!ldap_start_tls($ldap)) {
				trigger_error('Could Not Connect to TLS.', E_USER_WARNING);
			}
			else if(isset($ldap) && $ldap != '') {
				$result = ldap_search($ldap, $base, $filter/*, array('dn')*/);
				if($result != 0) {
					$entries = ldap_get_entries($ldap, $result);
					$principal = (isset($entries[0]['dn']))?$entries[0]['dn']:null;
					if(isset($principal)) {
						if(@ldap_bind($ldap, $principal, $credential)) {
							$authenticated = $entries[0];
						}
						else {
							trigger_error('Incorrect Password', E_USER_ERROR);
							return false;
						}
					}
					else {
						trigger_error("User $pid Not Found in LDAP", E_USER_ERROR);
						return false;
					}
					ldap_free_result($result);
				}
				else {
					trigger_error('Error occured searching the LDAP', E_USER_ERROR);
					return false;
				}
				ldap_close($ldap);
			}
			else {
				throw new Exception("Could not connect to LDAP at $host");
			}
		}
		return $authenticated;
	}

	public static function entryValue(Array $entry, $key) {
		return (isset($entry[$key]))?str_replace('$', "\n", $entry[$key][0]):null;
	}
}
