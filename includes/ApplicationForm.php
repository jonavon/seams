<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File includes/ApplicationForm.php
 *
 * Long description for file (if any)...
 *
 * @copyright Copyright (c) 2008 University Libraries, Virginia Tech
 * @license http://license.url
 * @version $Id:$
 * @link http://project.url
 * @since File available since Wed Oct 8 2008 10:56:12
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */

require_once 'global.inc.php';

/**
 * class ApplicationForm 
 */
class ApplicationForm {

	/*** Attributes: ***/

	/**
	 * db 
	 * 
	 * @var mixed
	 * @access private
	 */
	private $db;

	/**
	 * questions 
	 * 
	 * @var mixed
	 * @access private
	 */
	private $questions;

	/**
	 * __construct 
	 * 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function __construct(PDO $database) {
		$this->db = $database;
		$this->loadQuestions();
	} // end method __construct
	
	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 */
	public function __get($field) {
		return $this->$field;
	}

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		$this->$field = $value;
	}

	/**
	 * getFormQuestion 
	 * 
	 * @param mixed $name 
	 * @access public
	 * @return void
	 */
	public function getFormQuestion($name) {
		return $this->questions[$name];
	}

	/**
	 * Method loadQuestions.
	 * Get questions from the database and put them in to the questions array
	 * stack as a formelements.
	 *
	 * @param Array $question
	 * @access private
	 * @return void
	 * @todo FINISH THIS METHOD
	 */
	private function loadQuestions() {
		$sql = "SELECT * FROM `application_form` ORDER BY `fieldset`, `sort`";
		$statement = $this->db->query($sql);
		$questionsArray = $statement->fetchAll(PDO::FETCH_ASSOC);
		$statement->closeCursor();
		foreach($questionsArray as $appquestion) {
			$formelem = null;
			switch ($appquestion['type']) {
				case FormElement::BUTTON_TYPE :
				case FormElement::CHECKBOX_TYPE :
				case FormElement::FILE_TYPE :
				case FormElement::HIDDEN_TYPE :
				case FormElement::PASSWORD_TYPE :
				case FormElement::TEXT_TYPE :
					$formelem = new InputFormElement();
					break;
				case FormElement::SELECT_TYPE :
					$formelem = new SelectFormElement();
					break;
				case FormElement::TEXTAREA_TYPE :
					$formelem = new TextareaFormElement();
					break;
				case FormElement::TAGS_TYPE :
					$formelem = new TagsFormElement();
					break;
				case FormElement::CHECKSET_TYPE :
				case FormElement::SELECTSET_TYPE :
				default :
			}
			if ($formelem != null) {
				$formelem->__set('name', $appquestion['name']);
				$formelem->__set('type', $appquestion['type']);
				$formelem->__set('label', $appquestion['label']);
				$formelem->__set('value', $appquestion['default']);
				$formelem->__set('required', $appquestion['required']);
				if ($appquestion['type'] == FormElement::SELECT_TYPE) {
					$this->attachOptions($formelem);
				}
				$this->questions[$formelem->__get('name')] = $formelem;
			}
		}
	}

	/**
	 * attachOptions 
	 * 
	 * @param SelectFormElement $select 
	 * @access private
	 * @return void
	 */
	private function attachOptions(SelectFormElement $select) {
		$name=$select->__get('name');
		if($name != null) {
			$sql = "SELECT * FROM `setoptions` WHERE `set` = :set ORDER BY `group`, `sort`";
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':set', $name, PDO::PARAM_STR);
			$statement->execute();
			$opts = $statement->fetchAll(PDO::FETCH_ASSOC);
			$options = array();
			foreach($opts as $o) {
				$options[] = array (
					'option' => $o['option'],
					'value' => $o['label'],
					'group' => (($o['group'] != '')?$o['group']:null)
				);
			}
			$select->__set('options', $options);
		}
		else {
			throw new Exception("Name is not set for this element.");
		}
	}

	/**
	 * addAnswer 
	 * 
	 * @param Application $application 
	 * @param FormElement $element 
	 * @param mixed $answer 
	 * @access public
	 * @return void
	 */
	public function addAnswer(Application $application, FormElement $element, $answer) {
		$app = $application->__get('id');
		$question = $element->__get('name');
		if($element->__get('type') === FormElement::TAGS_TYPE) {
			$answer = $this->editTags($application, $element, $answer);
		}
		else {
			$sql = "REPLACE INTO `answers` VALUES (:application, :question, :answer)";
			$statement = $this->db->prepare($sql);
			$statement->bindValue(':application', $app, PDO::PARAM_INT);
			$statement->bindValue(':question', $question, PDO::PARAM_STR);
			$statement->bindValue(':answer', "$answer");
			if($statement->execute()) {
				$element->__set('value', "$answer");
				$statement->closeCursor();
			}
			else {
				throw new Exception("Unable to insert this $answer to this $question");
			}
		}
		return $element->__get('value');
	}

	/**
	 * editTags 
	 * 
	 * @param Application $application 
	 * @param TagsFormElement $tagelement 
	 * @param mixed $csv 
	 * @access private
	 * @return void
	 */
	private function editTags(Application $application, TagsFormElement $tagelement, $csv) {
		$tagelement->__set('value', $csv);
		$app = $application->__get('id');
		$question =  $tagelement->__get('name');
		$deletesql = "DELETE FROM `tags` WHERE `application` = :application AND `question` = :question";
		$deletestmt = $this->db->prepare($deletesql);
		$deletestmt->bindParam(':application', $app, PDO::PARAM_INT);
		$deletestmt->bindParam(':question', $question, PDO::PARAM_STR);

		$insertsql = "INSERT INTO `tags` VALUES (:application, :question, :tag)";
		$insertstmt = $this->db->prepare($insertsql);
		$insertstmt->bindParam(':application', $app, PDO::PARAM_INT);
		$insertstmt->bindParam(':question', $question, PDO::PARAM_STR);
		$insertstmt->bindParam(':tag', $tag, PDO::PARAM_STR);

		if($deletestmt->execute()){
			$deletestmt->closeCursor();
		}
		else {
			throw new Exception("Unable to delete tags '$deletelist'");
		}
		$additions = $tagelement->__get('tags');
		if(!empty($additions)) {
			foreach($additions as $tag) {
				if($insertstmt->execute()) {
					$insertstmt->closeCursor();
				}
				else {
					throw new Exception("Unable to add $tag");
				}
			}
		}
		return $tagelement->__get('value');
	}
} // end of ApplicationForm
