<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Pending.php
 *
 * Class file for the an Applicant that has been mark for hire.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version  Thu May 21 10:33:41 EDT 2009
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package seas
 */
/**
 *
 * class Pending.
 *
 * An instance of Pending represents a profile that has been marked for hire
 * and has not yet been hired by the Admin.
 */
class Pending extends Applicant {
	/*** Attributes: ***/
	protected $permission = Profile::PENDING_LEVEL;

	private $position = NULL;

	/**
	 * __construct
	 */
	public function __construct($pid=null, $database=null) {
		parent::__construct($pid, $database);
	}

	/**
	 * Method employ.
	 * Turn this candidate into an Employee.
	 */
	public function employ() {
		$this->__set('type', 'EMPLOYEE');
		$this->edit();
	}
}
