<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
require_once 'global.inc.php';

/**
 * Application 
 * 
 * @uses Vader
 * @package 
 * @version $id$
 * @copyright University Libraries, Virginia Tech
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */
class Application implements Vader {

	/*** Attributes: ***/

	private $db;

	/**
	 * Application identifier
	 * @access private
	 */
	private $id;
	private $docid;
	private $pid;
	private $name;
	private $pooled;

	/**
	 * Date this application is valid.
	 * @access private
	 */
	private $expire;

	/**
	 * Date the application was last modified.
	 * @access private
	 */
	private $modified;

	/**
	 * Associated Array containing questions and answers for this application.
	 * @access private
	 */
	private $content;

	private $availability;
	private $ranges;
	/**
	 * __construct 
	 * 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function __construct(PDO $database) {
		$this->db = $database;
		$this->docid = null;
		$this->id = null;
		$this->pid = null;
		$this->expire = null;
		$this->modified = null;
		$this->content = array();
		$this->availability = array();
		$this->ranges = array();
		$this->getQuestions();
	}
	/**
	 *
	 * @param String id Id for object

	 * @return Array
	 * @access public
	 */
	public function view( $id = null ) {
		$result = null;
		if($id == null) {
			$id = $this->id;
		}

		$sql = "SELECT `applications`.`docid` AS `docid`, `applications`.`id` AS `id`, `applications`.`pid` AS `pid`, CONCAT_WS(_utf8', ', `profiles`.`lastname`, `profiles`.`firstname`) AS `name`, unix_timestamp(`applications`.`modified`) AS `modified`, unix_timestamp(`applications`.`expire`) AS `expire`, count(`pools`.`application`) AS `pooled` from (`profiles` join (`applications` left join `pools` on((`applications`.`id` = `pools`.`application`)))) WHERE ((`applications`.`id` = :id) AND (`applications`.`pid` = `profiles`.`pid`)) GROUP BY `applications`.`pid`";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$result) {
			throw new Exception("Application $id does not exist within the database.");
		}
		foreach($result as $key=>$value){
			if($value !== NULL)	{
				$this->$key = stripslashes($value);
			}
		}
		return $result;
	} // end of member function view

	/**
	 * getContent 
	 * Get answers for this application.
	 * 
	 * @access private
	 * @return void
	 */
	private function getContent() {
		$id = $this->id;
		$this->getQuestions();
		$sql = "SELECT * FROM `answers` WHERE `application` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		foreach($result as $answer){
			$this->answerQuestion($answer['question'], stripslashes($answer['answer']));
		}
		$this->getAvailability();
		$this->getTags();
	}

	/**
	 * getAvailability 
	 * 
	 * @access private
	 * @return void
	 */
	private function getAvailability() {
		$id = $this->id;
		$sql = "SELECT `id`, `beginning`, `ending` FROM `availability` WHERE `application` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		foreach($result as $period){
			$avail = new Availability($this->db);
			$avail->__set('id', $period['id']);
			$avail->view();
			$this->availability[] = $avail;
			if(count($this->ranges) == 0){
				array_push(
						$this->ranges, array(
							'beginning' => $period['beginning'],
							'ending' => $period['ending']
							)
				);
			}
			else {
				$this->callback_ranges($this->ranges, $period);
			}
		}
	}

	/**
	 * getTags 
	 * 
	 * @access private
	 * @return void
	 */
	private function getTags() {
		$sql = "SELECT * FROM `tags` WHERE `application` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $this->id, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetchAll(PDO::FETCH_ASSOC);
		$fields = array();
		foreach($result as $tag){
			$fields[$tag['question']][] = stripslashes($tag['tag']);
		}
		foreach($fields as $field => $value){
			$tags = implode(', ', $value);
			$this->answerQuestion($field, $tags);
		}
	}

	/**
	 * callback_ranges 
	 * 
	 * @param Array $rangelist 
	 * @param Array $new 
	 * @access private
	 * @return void
	 */
	private function callback_ranges(Array &$rangelist, Array $new) {
		$nothingfits = true;
		foreach($rangelist as &$old){
			$oldbegin = (int)$old['beginning'];
			$oldend = (int)$old['ending'];
			$newbegin = (int)$new['beginning'];
			$newend = (int)$new['ending'];

			if(!(($this->isWithin($newbegin, $oldbegin, $oldend)) && ($this->isWithin($newend, $oldbegin, $oldend)))) {
				if( $this->isWithin($newbegin, $oldbegin, $oldend) && ($oldend < $newend)) {
					$old['ending'] = $new['ending'];
					$nothingfits = false;
					break;
				}
				elseif(($newbegin < $oldbegin) && ($this->isWithin($newend, $oldbegin, $oldend))) {
					$old['beginning'] = $new['beginning'];
					$nothingfits = false;
					break;
				}
				elseif(($newbegin < $oldbegin) && ($oldend < $newend)) {
					$old['beginning'] = $new['beginning'];
					$old['ending'] = $new['ending'];
					$nothingfits = false;
					break;
				}
			}
			else{
				$nothingfits = false;
			}
		}
		if($nothingfits) {
			array_push($rangelist, array( 'beginning' => $new['beginning'], 'ending' => $new['ending']));
		}
	}

	/**
	 * isWithin 
	 * 
	 * @param mixed $exp 
	 * @param mixed $min 
	 * @param mixed $max 
	 * @access private
	 * @return void
	 */
	private function isWithin($exp, $min, $max) {
		$exp = (int)$exp;
		$min= (int)$min;
		$max= (int)$max;
		return (($min <= $exp) && ($exp <= $max))?true:false;
	}

	/**
	 * Add Object
	 *
	 * @return 
	 * @access public
	 */
	public function add( ) {
		$this->edit();
	} // end of member function add

	/**
	 * Throws object into the trash without permenantly deleting.
	 *
	 * @return 
	 * @access public
	 */
	public function delete( ) {
		if((bool)$this->id){
			$id=$this->id;
			$sql = "UPDATE `applications` SET `trash`=1 WHERE `id`=:id";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $id, PDO::PARAM_INT);
				$statement->execute();
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot delete a job without an ID.");
		}
	} // end of member function delete

	/**
	 * Edit object
	 *
	 * @return 
	 * @access public
	 */
	public function edit( ) {
		$now = time();
		$expire = ($this->expire == null)?date('U', $now + (60 * 60 * 24 * 30)):$this->expire;
		if($this->pid == null) {
			throw new Exception("PID must be set before an application can be created.");
		}
		$sql = "INSERT INTO `applications` VALUES (:docid, :id, :pid, FROM_UNIXTIME(:modified), FROM_UNIXTIME(:expire), 0) ON DUPLICATE KEY UPDATE `modified`=FROM_UNIXTIME(:modified2), `expire`=FROM_UNIXTIME(:expire2)";
		try {
			$this->db->beginTransaction();
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':docid', $this->docid, PDO::PARAM_INT);
			$statement->bindParam(':id', $this->id, PDO::PARAM_INT);
			$statement->bindParam(':pid', $this->pid, PDO::PARAM_STR, 8);
			$statement->bindParam(':modified', $now, PDO::PARAM_INT, 11);
			$statement->bindParam(':expire', $expire, PDO::PARAM_INT, 11);
			$statement->bindParam(':modified2', $now, PDO::PARAM_INT, 11);
			$statement->bindParam(':expire2', $expire, PDO::PARAM_INT, 11);
			if(!$statement->execute()) {
				$err = $statement->errorInfo();
				throw new Exception("There was an error with this statement: {$err[0]} ({$err[1]}) {$err[2]}");
			}
			foreach($this->content as $question => &$property) {
				$answer = $property['answer'];
				$form = new ApplicationForm($this->db);
				$q = $property['question'];
				$property['answer'] = $form->addAnswer($this, $q, $answer);
			}
			$this->db->commit();
		}
		catch (Exception $e) {
			$this->db->rollback();
			throw new Exception($e->getMessage(),$e->getCode());
		}
	} // end of member function edit

	/**
	 * Restore an object from trash.
	 *
	 * @param String id Restore from trash.

	 * @return 
	 * @access public
	 */
	public function restore() {
		if((bool)$this->id){
			$id=$this->id;
			$sql = "UPDATE `applications` SET `trash`=0 WHERE `id`=:id";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $id, PDO::PARAM_INT);
				$statement->execute();
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot delete a job without an ID.");
		}
	} // end of member function restore

	/**
	 * Browse a list of a resource.
	 *
	 * @param String filter Filter used to search results.

	 * @return Array
	 * @access public
	 */
	public function browse( $filter = null, Array $in = null, $includeExpired=false ) {
		$results = array();
		$filter = trim($filter);
		if(!empty($filter)) {
			$where = array();
			$union = array();
			$search = $this->parseFilter($filter);
			if(!empty($search['text'])){
				$inbool = ((bool)preg_match('/[^\w]/', $search['text']))?' IN BOOLEAN MODE':null;
				$union[] = "SELECT `application`, `question`, `answer`, (MATCH(`answer`) AGAINST('{$search['text']}' $inbool)) AS score FROM `applicationindex` WHERE MATCH(`answer`) AGAINST('{$search['text']}' $inbool)";
			}
			foreach($search['fields'] as $query){
				$searchtext = trim($query[2]);
				if(preg_match('/[!]?[><=]\s*\d+/',$searchtext)) {
					if(substr($searchtext,0,1)==='!') {
						$subject = substr($searchtext,1);
						$where[] = "`application` NOT IN (SELECT `application` FROM `applicationindex` WHERE `question`='{$query[1]}' AND `answer` {$subject})";
					}
					else {
						$where[] = "`application` IN (SELECT `application` FROM `applicationindex` WHERE `question`='{$query[1]}' AND `answer` {$searchtext})";
					}
					$alt[] = "SELECT *, 0 AS score FROM `applicationindex` WHERE `question`='{$query[1]}'";
				}
				else {
					$inbool = ((bool)preg_match('/[^\w]/', $searchtext))?' IN BOOLEAN MODE':null;
					$union[] = "SELECT `application`, `question`, `answer`, (`question` = '{$query[1]}' AND MATCH(`answer`) AGAINST('{$query[1]}' $inbool)) AS score FROM `applicationindex` WHERE `question` = '{$query[1]}' AND MATCH(`answer`) AGAINST('{$searchtext}' $inbool)";
				}
			}
			if(empty($union)) {
				$union =& $alt;
			}
			$sql = "SELECT `applications_v`.*, snippits.snippit AS snippit, snippits.score AS score FROM `applications_v` JOIN (\n";
			$sql .= "SELECT `application`, GROUP_CONCAT(CONCAT(`question`, ':', `answer`) SEPARATOR '\\n') AS snippit, SUM(score) AS score FROM (\n";
			$sql .= implode("\nUNION\n", $union);
			$sql .= ") AS tbl\n";
			if(!empty($where)){
				$sql .= "WHERE (\n";
				$sql .= implode("\nAND\n", $where);
				$sql .= " ) ";
			}
			$sql .= "GROUP BY `application`\n";
			$sql .= ") AS snippits ON `applications_v`.`id` = snippits.application WHERE `applications_v`.`expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)";
			if(!empty($in)){
				$n = implode("', '", $in);
				$sql .= " AND `application` IN ('$n')";
			}
			$sql .= " ORDER BY score DESC";
		}
		else {
			$sql = "SELECT * FROM `applications_v`";
			if(!$includeExpired) {
				$sql .= " WHERE `expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP)";
			}
		}
		$statement = $this->db->query($sql);
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	} // end of member function browse

	private function parseFilter($filter) {
		$regex = '/([\w_]+):\[([\w\s\+\.~="!-<>\(\)\*]+)\]/';
		$result['text'] = trim(preg_replace($regex, '', $filter));
		preg_match_all($regex, $filter, $result['fields'], PREG_SET_ORDER);
		return $result;
	}
	/**
	 * query 
	 * 
	 * @param Array $documents 
	 * @access private
	 * @return void
	 */
	private function query(Array $documents) {
		$idlist = implode(", ", $documents);
		$sql = "SELECT * FROM `applications_v` WHERE `docid` IN ($idlist) AND `expire` > UNIX_TIMESTAMP(CURRENT_TIMESTAMP) ORDER BY FIELD(`docid`, $idlist)";
		$statement = $this->db->query($sql);
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}

	/**
	 * Permanantly delete from database.
	 *
	 * @param String id Identifier for the object

	 * @return 
	 * @access public
	 */
	public function purge() {
		trigger_error("Implement " . __FUNCTION__);
	} // end of member function purge

	/**
	 * Get actual value from database which may be a reference id number.
	 *
	 * @param String field Database field

	 * @param bool reread If set to true operation will query database again.

	 * @return Array
	 * @access public
	 */
	public function read( $field,  $reread = false ) {
		trigger_error("Implement " . __FUNCTION__);
	} // end of member function read

	/**
	 * Overloaded function that gets a specified field.
	 *
	 * @param String field Object field to get

	 * @return String
	 * @access public
	 */
	public function __get( $field ) {
		if($field == 'content') {
			$this->getContent();
		}
		return $this->$field;
	} // end of member function __get

	/**
	 * Overloaded function that sets a specified field.
	 *
	 * @param String field Field to change

	 * @param String value Value that the field should be changed to.

	 * @return 
	 * @access public
	 */
	public function __set( $field,  $value ) {
		$value = stripslashes($value);
		if(isset($this->id)){
			$this->getContent();
		}
		switch($field) {
			case 'id' :
				$this->id = (is_null($value))?$this->id:$value;
				break;
			default :
				$this->$field = $value;
		}
	} // end of member function __set

	/**
	 * answerQuestion 
	 * 
	 * @param mixed $name 
	 * @param mixed $answer 
	 * @access public
	 * @return void
	 */
	public function answerQuestion($name, $answer) {
		if(array_key_exists($name, $this->content)) {
			$this->content[$name]['answer'] = $answer;
			$q = &$this->content[$name]['question'];
			$q->__set('value', $answer);
		}
		else {
			throw new Exception("Question $name does not exist in this application.");
		}
	}

	/**
	 * getQuestions 
	 * 
	 * @access private
	 * @return void
	 */
	private function getQuestions() {
		$q = new ApplicationForm($this->db);
		$formquestions = $q->__get('questions');
		foreach($formquestions as $field) {
			$this->content[$field->__get('name')] = array(
					"question" => $field,
					"answer" => null
					);
		}
	}
} // end of Application
