<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * EnumList.php
 *
 * Query the enum table.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

/**
 *
 * This is the short Description for the Class
 *
 * This is the long description for the Class
 *
 * @author		Karl Heinz Marbaise <khmarbaise@gmx.de>
 * @copyright	(c) 2003 by Karl Heinz Marbaise
 * @version		$Id$
 * @package		Package
 * @subpackage	SubPackage
 * @see			??
 */
class EnumList {
	private $db;
	private $type;
	public function __construct(PDO $resource) {
		$this->db = $resource;
		$this->type = null;
	}

	public function view($enum) {
		$this->type = $enum;
		$sql = "SELECT * FROM `$enum` ORDER BY `sort`";
		try{
			$result = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
			array_walk(&$result, create_function('&$a, $k', '$a["label"] = stripslashes($a["label"]);$a["description"] = stripslashes($a["description"]);'));
		}
		catch (PDOException $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
		return $result;
	}

	public function modify(Array $list, $type) {
		$type = strtoupper($type);
		$sql = "INSERT INTO `enumtable` VALUES (:id, :type, :label, :description, :sort, 0) ON DUPLICATE KEY UPDATE `label`=:label2, `description`=:description2, `sort`=:sort2, `trash`=0";
		try {
			$this->db->beginTransaction();
			$statement = $this->db->prepare($sql);
			foreach($list as $item) {
				$statement->bindParam(':id', $item['id'], PDO::PARAM_INT);
				$statement->bindParam(':type', $type, PDO::PARAM_STR);
				$statement->bindParam(':label', $item['label'], PDO::PARAM_STR);
				$statement->bindParam(':description', $item['description'], PDO::PARAM_STR);
				$statement->bindParam(':sort', $item['sort'], PDO::PARAM_INT);
				$statement->bindParam(':label2', $item['label'], PDO::PARAM_STR);
				$statement->bindParam(':description2', $item['description'], PDO::PARAM_STR);
				$statement->bindParam(':sort2', $item['sort'], PDO::PARAM_INT);
				if(!$statement->execute()){
					throw new Exception("Cannot execute statement.");
				}
			}
			$this->db->commit();
		}
		catch (Exception $e) {
			$this->db->rollBack();
			throw new Exception($e->getMessage(),$e->getCode());
		}
	}

	public function delete($id) {
		$sql = "UPDATE `enumtable` SET `trash`=1 WHERE id=:id";
		try {
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':id', $id, PDO::PARAM_INT);
			if(!$statement->execute()){
				$err = $statement->errorInfo();
				throw new Exception("Unable to delete from the list #id:$id. Error Code:{$err[1]} {$err[2]}");
			}
		}
		catch (Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	}
}
