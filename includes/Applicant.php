<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Applicant.php.
 * Contains the class definition of Applicant.
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @copyright Copyright (c) University Libraries, Virginia Tech
 */

require_once 'global.inc.php';


/**
 * class Applicant.
 * An instance of Applicant denotes a user who has Applicant permission. This
 * typically is a new user. It can also be a student who is not employed.
 */
class Applicant extends Profile {

	/*** Attributes: ***/
	protected $permission = Profile::APPLICANT_LEVEL;
	/**
	 * Applicant Application Information. List of applications.
	 * @access protected
	 */
	protected $applications = null;

	/**
	 * University Major.
	 * @access protected
	 */
	protected $major = null;

	/**
	 * Academic standing. Freshman, Junior, Gradruate, etc.
	 * @access protected
	 */
	protected $class = null;
	protected $hireLog = null;

	/**
	 * __construct 
	 * 
	 * @param mixed $pid 
	 * @param mixed $database 
	 * @access public
	 * @return void
	 */
	public function __construct($pid=null, $database=null) {
		parent::__construct($pid, $database);
		if($pid == null) {
			$this->applications = null;
			$this->major = null;
			$this->class = null;
			$this->hireLog = null;
		}
		else {
			$this->loadApplication();
		}
	}

	/**
	 * Hire applicant to a job.
	 * Applicant is changed to Employee type. Additionally he or she is removed
	 * from all pools.
	 *
	 * @param job
	 * @return 
	 * @access public
	 */
	public function hire(Job $job) {
		// Make an employee.
		$this->__set('type', 'PENDING');
		$this->edit();
		// Fill a position
		$pos = $job->fillPosition($this);
		// Expire all applications.
		foreach($this->applications as $application){
			$this->hireLog['expiredApplications'][] = $application->__get('id');
			$application->__set('expire', time());
			$application->edit();
		}
		// Remove from pools too.
		$this->clearFromPools(); 
		return $pos;
	} // end of member function hire

	/**
	 * @override
	 */
	public function __get($field) {
		if($field == 'applications' && $this->applications == null) {
			$this->loadApplication();
		}
		return $this->$field;	
	} // end of member function __set

	/**
	 * @override
	 */
	public function __set($field, $value) {
		if(!is_array($value)){
			$value = stripslashes($value);
		}

		switch($field) {
			case 'db':
				throw new Exception($this->__CLASS__ . " field '$field' is not mutable by this method.");
			case 'birthday':
				$value = strtotime($value);
		}
		if(!is_array($value)){
			$this->modifiedArray[$field] = addslashes($value);
		}
		$this->$field = $value;
	} // end of member function __set

	/**
	 * Method loadApplication.
	 * Loads all user application objects into the applications property.
	 * 
	 * @access protected
	 * @return void
	 */
	protected function loadApplication() {
		$pid = $this->pid;
		$sql = "SELECT * FROM `applications` WHERE `trash`= 0 AND `pid` = '$pid' ORDER BY `modified`";
		$statement = $this->db->query($sql);
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		foreach($results as $application) {
			$a = new Application($this->db);
			$a->view($application['id']);
			$this->applications[$application['id']] = $a;
		}
	}

	/**
	 * Method clearFromPools.
	 * Will remove all of this users applications from any job pool.
	 * 
	 * @access protected
	 * @return void
	 */
	protected function clearFromPools() {
		$apps = "'" .  implode("', '", $this->hireLog['expiredApplications']) . "'";
		$getpools = "SELECT `job` FROM `pools` WHERE `application` IN ($apps)";
		$statement = $this->db->query($getpools);
		$this->hireLog['removedPools'] = $statement->fetchAll(PDO::FETCH_COLUMN, 0);
		$sql = "DELETE FROM `pools` WHERE `application` IN ($apps)";
		return $this->db->exec($sql);
	}
} // end of Applicant
