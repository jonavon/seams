<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 *
 * This is the short Description for the Class
 *
 * This is the long description for the Class
 *
 * @author		Karl Heinz Marbaise <khmarbaise@gmx.de>
 * @copyright	(c) 2003 by Karl Heinz Marbaise
 * @version		$Id$
 * @package		Package
 * @subpackage	SubPackage
 * @see			??
 */
class Stats {
	private $db;
	
	public function __construct(PDO $database) {
		$this->db = $database;
	}

	public function viewLog($table,$action,$since=null,$until=null) {
		$sql = "SELECT * FROM `$table" . "_$action" . "_log`";
		if(!is_null($since)) {
			$since = date('Y-m-d H:i:s',$since);
		}
		if(!is_null($until)) {
			$until = date('Y-m-d H:i:s',$until);
		}
		if(!(is_null($since) && is_null($until))) {
			$sql .= " WHERE ";
			$sql .=(!is_null($since))?"TIMEDIFF('$since',`timestamp`) < 0":null;
			$sql .=(!(is_null($since) || is_null($until)))?" AND ":null;
			$sql .=(!is_null($until))?"TIMEDIFF(`timestamp`,'$until') < 0":null;
		}
		return $this->q($sql);
	}

	public function q($text) {
		try{
			$result = $this->db->query($text)->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (PDOException $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
		return $result;
	}
}
