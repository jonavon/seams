<?php
/**
 * File Vader.php
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @copyright Copyright (c) University Libraries, Virginia Tech.
 */

/**
 * class Vader
 */
interface Vader
{

	/**
	 *
	 * @param String id Id for object
	 * @return Array
	 * @access public
	 */
	public function view( $id = null );

	/**
	 * Add Object
	 *
	 * @return 
	 * @access public
	 */
	public function add( );

	/**
	 * Throws object into the trash without permenantly deleting.
	 *
	 * @return 
	 * @access public
	 */
	public function delete( );

	/**
	 * Edit object
	 *
	 * @return 
	 * @access public
	 */
	public function edit( );

	/**
	 * Restore an object from trash.
	 *
	 * @param String id Restore from trash.
	 * @return 
	 * @access public
	 */
	public function restore();

	/**
	 * Browse a list of a resource.
	 *
	 * @param String filter Filter used to search results.
	 * @return Array
	 * @access public
	 */
	public function browse( $filter = null );

	/**
	 * Permanantly delete from database.
	 *
	 * @return 
	 * @access public
	 */
	public function purge();

	/**
	 * Get actual value from database which may be a reference id number.
	 *
	 * @param String field Database field
	 * @param bool reread If set to true operation will query database again.
	 * @return Array
	 * @access public
	 */
	public function read( $field,  $reread = false );
} // end of Vader
