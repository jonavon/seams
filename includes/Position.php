<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * Position 
 * 
 * @package 
 * @version $id$
 * @copyright Copyright (c) University Libraries, Virginia Tech
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */
class Position {

	/*** Attributes: ***/
	private $db;

	/**
	 * @access private
	 */
	private $id;

	/**
	 * @access private
	 */
	private $job;

	/**
	 * @access private
	 */
	private $paycode;

	/**
	 * @access private
	 */
	private $employee;

	/**
	 * __construct 
	 * 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function __construct(PDO $database) {
		$this->db = $database;
		$this->id = null;
		$this->job = null;
		$this->employee = null;
		$this->paycode= null;
		$this->applications = null;
		
		$this->job_title = null;
		$this->supervisor_name = null;
		$this->employee_name = null;
		$this->employee_type = null;
		$this->paycode_label = null;
	}
	
	public function view($id = null) {
		$result = null;
		if($id == null) {
			$id = $this->id;
		}
		$sql = "SELECT * FROM `positions_v` WHERE `id` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_INT);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$result) {
			throw new Exception("Position $id does not exist within the database.");
		}
		foreach($result as $key=>$value){
			if(!empty($value))	{
				$this->__set($key, $value);
			}
		}
		$a = new Applicant($this->employee,$this->db);
		$this->applications = array_keys($a->applications);
		return $result;
	}

	public function add() {
		return $this->edit();
	}

	public function delete() {
		return $this->purge();
	}

	public function edit() {
		if($this->job != null){
			$sql = "INSERT INTO `positions` VALUES (:id, :job, :employee, :paycode) ON DUPLICATE KEY UPDATE `job`=:job2, `employee`=:employee2, `paycode`=:paycode2";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $this->id, PDO::PARAM_INT);
				$statement->bindParam(':job', $this->job, PDO::PARAM_INT);
				$statement->bindParam(':job2', $this->job, PDO::PARAM_INT);
				$statement->bindParam(':employee', $this->employee, PDO::PARAM_STR);
				$statement->bindParam(':employee2', $this->employee, PDO::PARAM_STR);
				$statement->bindParam(':paycode', $this->paycode, PDO::PARAM_INT);
				$statement->bindParam(':paycode2', $this->paycode, PDO::PARAM_INT);
				if($statement->execute()){
					$this->id = $this->db->lastInsertID();
				}
				else {
					throw new Exception("There was an error with this statement:" . $statement->errorInfo() . " \n $sql");
				}
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
	}

	public function browse($filter=null) {
		$results = array();
		if(empty($filter)) {
			$sql = "SELECT * FROM `positions_v`";
		}
		else {
			$filter = strtolower($filter);
			$sql = "SELECT * FROM `positions_v` WHERE CONCAT_WS(' ' , CONCAT('id:', `id`), CONCAT('job:', `job`), CONCAT('supervisor:', LOWER(`supervisor`)), LOWER(`supervisor_name`), CONCAT('pid:', LOWER(`employee`)), LOWER(`employee_name`), CONCAT('type:', LOWER(`employee_type`)), CONCAT('paycode:', LOWER(`paycode`)), LOWER(`paycode_label`)) REGEXP LOWER('$filter');";
		}
		$statement = $this->db->prepare($sql);
		$statement->execute();
		$results = $statement->fetchAll(PDO::FETCH_ASSOC);
		return $results;
	}

	public function purge() {
		if((bool)$this->id){
			$id=$this->id;
			$sql = "DELETE FROM `positions` WHERE id=:id";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $id, PDO::PARAM_INT);
				$statement->execute();
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot delete a position without an ID.");
		}
	}

	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 */
	public function __get($field) {
		return $this->$field;
	}

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		switch($field){
			case 'employee' :
				if(!empty($value)){
					if($value instanceof Employee){
						$this->setEmployee($value);
					}
					else {
						$this->setEmployeeFromPid($value);
					}
				}
				break;
			default :
				$this->$field = $value;
		}
	}

	/**
	 * setEmployee 
	 * 
	 * @param Employee $employee 
	 * @access private
	 * @return void
	 */
	private function setEmployee(Employee $employee) {
		$this->employee = $employee->__get('pid');
	}

	/**
	 * setEmployeeFromPid 
	 * 
	 * @param mixed $pid 
	 * @access private
	 * @return void
	 */
	private function setEmployeeFromPid($pid) {
		$mployee = new Employee($pid, $this->db);
		$this->setEmployee($mployee);
	}

} // end of Position
