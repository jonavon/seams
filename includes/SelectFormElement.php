<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File includes/FormElement.php
 *
 * Long description for file (if any)...
 *
 * @copyright Copyright (c) 2008 University Libraries, Virginia Tech
 * @license http://license.url
 * @version $Id:$
 * @link http://project.url
 * @since File available since the 0.0.0 Wed Oct 8 2008 10:56:12
 */
/**
 * class FormElement
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */
class SelectFormElement extends FormElement {
	/*** Attributes ***/
	private $multiple;
	private $options;

	/**
	 * __construct 
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this->required = false;
		$this->multiple = false;
		$this->name = null;
		$this->readonly = false;
		$this->type = null;
		$this->value = null;
		$this->classes = array();
		$this->attributes = array();
		$this->options = array();
	} // end of __construct

	/**
	 * toHTML 
	 * 
	 * @access public
	 * @return void
	 */
	public function toHTML() {
		$required = ($this->required)? $this->addToClasses('required') : null;
		$readonly = ($this->readonly)? $this->selfAttr('readonly') : null;
		$attributes = $this->formatAttributes();
		$labelText = $this->__get('label');
		$name = $this->__get('name');
		// $value = $this->value;
		$classes = implode(' ', $this->classes);
		$label = "<label for='$name'><span>$labelText</span></label>";
		$input = "<select name='$name' class='$classes' $attributes $readonly>\n";
		$input .= $this->optionHTML();
		$input .= "</select>";
		$formelement = "<p>\n$label\n$input\n</p>\n";

		return $formelement;
	}

	/**
	 * addOptions 
	 * 
	 * @param Array $options an array with three associative values option, value, group.
	 * @access public
	 * @return void
	 * @exception Thrown if array is not properly formed.
	 */
	private function addOptions(Array $options) {
		if (array_key_exists('option', $options) || array_key_exists('value', $options)) {
			$this->options[] = $options;
		}
		else {
			throw new Exception("Array not formed correctly. Must be an associative array with three values: option, value, group.");
		}
	}

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		switch ($field) {
			case 'options' :
				foreach($value as $options) {
					$this->addOptions($options);
				}
				break;
			case 'type' :
				$this->multiple = ($value == FormElement::SELECTSET_TYPE)? true : false;
			default :
				parent::__set($field, $value);
				break;
		}
	}

	/**
	 * Method processOptions.
	 * Creates an array that will be grouped together by the group key. Formated
	 * for toHTML method.
	 *
	 * @access private
	 * @return Array
	 */
	private function processOptions() {
		$o = array();
		foreach($this->options as $option) {
			if (array_key_exists('group', $option) && ($option['group'] != null)) {
				$o[$option['group']][] = array(
					"option" => $option['option'],
					"value" => $option['value']
				);
			}
			else {
				$o[] = array(
					"option" => $option['option'],
					"value" => $option['value']
				);
			}
		}
		return $o;
	}

	/**
	 * optionHTML 
	 * Creates the internal html for a select element.
	 * 
	 * @access private
	 * @return String
	 */
	private function optionHTML() {
		$opts = $this->processOptions();
		$html;
		foreach($opts as $key => $option) {
			if(is_int($key)) {
				$html .= $this->singleOption($option);
			}
			else {
				$html .= "\t<optgroup label='$key'>\n";
				foreach($option as $v) {
					$html .= "\t" . $this->singleOption($v);
				}
				$html .= "\t</optgroup>\n";
			}
		}
		return $html;
	}

	/**
	 * singleOption 
	 * Creates a single option line.
	 * 
	 * @param Array $option 
	 * @access private
	 * @return void
	 */
	private function singleOption(Array $option) {
		$value = $option['value'];
		$text = htmlspecialchars($option['option'], ENT_QUOTES, null, false);
		$html = "\t<option value='$value'>$text</option>\n";
		return $html;
	}
}
