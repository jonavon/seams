<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Preferences.php
 *
 * Prefernces class creates a preference object for a particular profile.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package seas
 */
class Preference implements Vader {
	private $db;
	private $id;
	private $pid;
	private $label;
	private $value;
	private $sort;
	private $trash;

	/**
	 * __construct 
	 * Instantiate a preference object.
	 * 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function __construct(PDO $database) {
		$this->db = $database;
		$this->id = null;
		$this->pid = null;
		$this->label = null;
		$this->value = null;
		$this->sort = 0;
		$this->trash = 0;
	}

	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 * @override
	 */
	public function __get($field) {
		return $this->$field;
	}

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field,$value) {
		$this->$field = $value;
	}

	/**
	 * view
	 * View the preference
	 * 
	 * @param mixed $id 
	 * @access public
	 * @return void
	 */
	public function view($id = null) {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
	public function add() {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
	public function delete() {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
	public function edit() {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
	public function restore() {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
	public function browse($filter = null) {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
	public function purge() {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
	public function read( $field,  $reread = false ) {
		throw new Exception("The function " . __FUNCTION__ . " is not implemented.");
	}
}
