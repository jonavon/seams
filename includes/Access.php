<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * Access.php
 *
 * Contains the class that handles IP address access.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version Mon Jun  8 10:11:18 EDT 2009
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package seas
 */

/**
 *
 * Class Access.
 * Handles Access to system.
 */
class Access {
	private $db;
	private $access;
	private $message;

	public function __construct(PDO $database) {
	 $this->db = $database;
	 $this->view();
	 $this->message = null;
	}
	
	/**
	 * Method view.
	 * Obtain the list from database.
	 * @access public
	 * @return Array Array of ip addresses and access level.
	 */
	public function view() {
		$set = null;
		$sql = 'SELECT * FROM `access_v`';
		try {
			$statement = $this->db->prepare($sql);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$results = $statement->fetchAll();
			foreach($results AS $access) {
				$set[$access['ip']] = $access;
			}
		}
		catch (PDOException $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
		$this->access = $set;
		return $set;
	}
	
	public function getAccess() {
		return $this->access;
	}

	public function getMessage() {
		return $this->message;
	}
	/**
	 * Method add.
	 * Add or replace list in the database.
	 * @param $iparray Array with required signature
	 * array(
	 *   [ip] => "String IP Address",
	 *   [cidr] => integer < 32,
	 *   [level] => integer,
	 *   [allow] => boolean
	 * )
	 * @access public
	 * @return void
	 */
	public function add(Array $iparray) {
		$sql = 'INSERT INTO `access` VALUES(INET_ATON(:ip), :cidr, :level, :allow) ON DUPLICATE KEY UPDATE `cidr`=:cidr2, `level`=:level2, `allow`=:allow2';
		try {
			$this->db->beginTransaction();
			$statement = $this->db->prepare($sql);
			foreach($iparray AS $ip){
				$statement->bindParam(':ip', $ip['ip'], PDO::PARAM_STR);
				$statement->bindParam(':cidr', $ip['cidr'], PDO::PARAM_INT);
				$statement->bindParam(':level', $ip['level'], PDO::PARAM_INT);
				$statement->bindParam(':allow', $ip['allow'], PDO::PARAM_INT);
				$statement->bindParam(':cidr2', $ip['cidr'], PDO::PARAM_INT);
				$statement->bindParam(':level2', $ip['level'], PDO::PARAM_INT);
				$statement->bindParam(':allow2', $ip['allow'], PDO::PARAM_INT);
				if(!$statement->execute()) {
					$err = $statement->errorInfo();
					throw new Exception("{$err[0]} {$err[1]} {$err[2]}");
				}
			}
			$this->db->commit();
		}
		catch(Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	}

	/**
	 * Method delete.
	 * Delete 1p ranges from the database based on start ip address.
	 * @param $iparray Array of ip addresses.
	 * @access public
	 * @return void
	 */
	public function delete(Array $iparray) {
		$sql = 'DELETE FROM `access` WHERE `ip`=INET_ATON(:ip)';
		try {
			$this->db->beginTransaction();
			$statement = $this->db->prepare($sql);
			foreach($iparray AS $ip){
				$statement->bindParam(':ip', $ip, PDO::PARAM_STR);
				if(!$statement->execute()) {
					$err = $statement->errorInfo();
					throw new Exception("{$err[0]} {$err[1]} {$err[2]}");
				}
			}
			$this->db->commit();
		}
		catch(Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
	}

	/**
	 * Method allow();
	 */
	public function allowed($ip,$permission) {
		$sql = "SELECT `allow` FROM `access` WHERE INET_ATON(:ip) >= `ip` AND INET_ATON(:ip2) <= (`ip` + (POW(2,(32 - `cidr`)) - 1)) AND `level` & :permission ORDER BY `allow` ASC LIMIT 1";
		try {
			$this->db->beginTransaction();
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':ip', $ip, PDO::PARAM_STR);
			$statement->bindParam(':ip2', $ip, PDO::PARAM_STR);
			$statement->bindParam(':permission', $permission, PDO::PARAM_INT);
			if(!$statement->execute()) {
				$err = $statement->errorInfo();
				throw new Exception("{$err[0]} {$err[1]} {$err[2]}");
			}
			$results = $statement->fetchAll();
			if(count($results) > 0) {
				return (int)$results[0]['allow'];
			}
			else {
				$this->message = "Not permitted";
			}

		}
		catch(Exception $e) {
			throw new Exception($e->getMessage(),$e->getCode());
		}
		return false;
	}
}
