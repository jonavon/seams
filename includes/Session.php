<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Session.php
 *
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 */
/**
 * Personalized Session handler for this application.
 * CREATE TABLE session (
 * 	id CHAR(32) PRIMARY KEY,
 * 	hash VARCHAR NOT NULL,
 * 	expire INT NOT NULL,
 * 	data
 * )
 * @see http://www.nateklaiber.com/blog/2006/05/10/custom-php-session-handler
 */
class Session {

	/**
	 * field db. 
	 * Reference to the database resource.
	 * 
	 * @var mixed
	 * @access private
	 */
	private $db;

	/**
	 * field expire. 
	 * Expiration  
	 *
	 * @var mixed
	 * @access private
	 */
	private $expire;

	/**
	 * field hash. 
	 *
	 * @var mixed
	 * @access private
	 */
	private $hash;

	/**
	 * field expired.
	 * state variable if session has expired.
	 *
	 * @var boolean
	 * @access private
	 */
	private $expired;

	/**
	 * __construct 
	 * 
	 * @param PDO $resource 
	 * @access public 
	 * @return void
	 */
	public function __construct(PDO $resource) {
		$this->db = $resource;
		$this->hash = $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'];
		session_set_save_handler(
				array(&$this, 'open'),
				array(&$this, 'close'),
				array(&$this, 'read'),
				array(&$this, 'write'),
				array(&$this, 'destroy'),
				array(&$this, 'clean')
		);
		register_shutdown_function('session_write_close');
		if(!isset($_SESSION)){
			session_start();
		}
	}

	public function __destruct() {
		session_write_close();
	}

	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 */
	public function __get($field) {
		return $this->$field;
	}

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		switch($field) {
			case 'db':
				throw new Exception('Database resource cannot be changed. Please use the constructor.');
				break;
			case 'expire':
				$this->$field = $value;
				break;
			default:
				throw new Exception("The field $field is not Mutable by this method.");
				break;
		}
	}

	//////////////////////
	// Callback Methods //
	//////////////////////
	/**
	 * open 
	 * 
	 * @access private
	 * @return void
	 */
	public function open() {
		$this->clean();
		$this->expire = ($this->expire === null)?(ini_get('session.gc_maxlifetime')/60):$this->expire;
		return true;
	}

	/**
	 * close 
	 * 
	 * @access private
	 * @return void
	 */
	public function close() {
		return true;
	}

	/**
	 * read 
	 * 
	 * @param mixed $id 
	 * @access private
	 * @return void
	 */
	public function read($id) {
		$sql = "SELECT data AS data FROM session WHERE id = :id AND hash = :hash";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(":id", $id, PDO::PARAM_INT);
		$statement->bindParam(":hash", $this->hash, PDO::PARAM_STR);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		$containsData = (bool)(count($result) > 0);
		return ($containsData)?$result['data']:'';
	}

	/**
	 * write 
	 * 
	 * @param mixed $id 
	 * @param mixed $data 
	 * @access private
	 * @return boolean
	 */
	public function write($id, $data) {
		$write = false;
		// Time to live
		$ttl = (time() + ($this->expire * 60));
		// SQLITE
		//$sql = "INSERT OR REPLACE INTO session VALUES(:id, :hash, :expire, :data)";
		// MYSQL
		$sql = "INSERT INTO `session` VALUES(:id, :hash, :expire, :data) ON DUPLICATE KEY UPDATE `expire`=:expire2, `data`=:data2";
		// STANDARD
		//$sql = "MERGE INTO session VALUES(:id, :hash, :expire, :data)";
		$this->db->beginTransaction();
		$statement = $this->db->prepare($sql);
		$statement->bindParam(":id", $id, PDO::PARAM_INT);
		$statement->bindParam(":hash", $this->hash, PDO::PARAM_STR);
		$statement->bindParam(":expire", $ttl, PDO::PARAM_INT);
		$statement->bindParam(":data", $data, PDO::PARAM_STR);
		$statement->bindParam(":expire2", $ttl, PDO::PARAM_INT);
		$statement->bindParam(":data2", $data, PDO::PARAM_STR);
		if( $statement->execute()) {
			$this->db->commit();
			$write = true;
		}
		else {
			throw new Exception($statement->errorInfo());
			$this->db->rollBack();
		}
		return $write;
	}

	/**
	 * destroy 
	 * 
	 * @param mixed $id 
	 * @access private
	 * @return boolean
	 */
	public function destroy($id) {
		$sql="DELETE FROM session WHERE id=:id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(":id", $id, PDO::PARAM_INT);
		return $statement->execute();
	}

	/**
	 * clean 
	 * 
	 * @access private
	 * @return boolean
	 */
	public function clean() {
		$now = time();
		$sql = "DELETE FROM session WHERE expire < :now";
		$clean = false;
		$this->db->beginTransaction();
		$statement = $this->db->prepare($sql);
		$statement->bindParam(":now", $now); 
		if($statement->execute()) {
			$this->db->commit();
			$clean = true;
		}
		else {
			throw new Exception($statement->errorInfo());
			$this->db->rollBack();
		}
		return $clean;
	}
}
