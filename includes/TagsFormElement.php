<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
/**
 * TagsFormElement 
 * 
 * @uses FormElement
 * @package 
 * @version $id$
 * @copyright 1997-2005 The PHP Group
 * @author Tobias Schlitt <toby@php.net> 
 * @license PHP Version 3.0 {@link http://www.php.net/license/3_0.txt}
 */
class TagsFormElement extends TextareaFormElement {
	private $tags;
	/**
	 * __construct 
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this->required = false;
		$this->name = null;
		$this->readonly = false;
		$this->type = FormElement::TAGS_TYPE;
		$this->value = null;
		$this->classes = array();
		$this->attributes = array();
		$this->tags = array();
	} // end of __construct

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		if($field === 'value') {
			$this->setTags($value);
		}
		else {
			$this->$field = $value;
		}
	}
	
	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 */
	public function __get($field) {
		return $this->$field;
	}
	
	/**
	 * setTags 
	 * 
	 * @param mixed $csv 
	 * @access private
	 * @return void
	 */
	private function setTags($csv) {
		$subject = explode(', ', $csv);
		array_walk($subject, create_function('&$v', '$v = trim($v);'));
		$this->tags = $subject;
		$this->value = implode(', ', $this->tags);
	}
}
