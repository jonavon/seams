<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Employee.php
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @copyright Copyright (c) University Libraries, Virginia Tech
 */
require_once 'global.inc.php';

/**
 * class Employee
 */
class Employee extends Applicant {

	/*** Attributes: ***/
	protected $permission = Profile::EMPLOYEE_LEVEL;

	/**
	 * One or more positions that this Employee is actively doing.
	 * @access private
	 */
	private $positions = NULL;

	/**
	 * __construct 
	 * 
	 * @param mixed $pid 
	 * @param mixed $database 
	 * @access public
	 * @return void
	 */
	public function __construct($pid=null, $database=null) {
		parent::__construct($pid, $database);
	}

	/**
	 * Remove employee from Job.
	 *
	 * @param includes::Job job Job.

	 * @return 
	 * @access public
	 */
	public function fire( $job ) {
		trigger_error("Implement" . __FUNCTION__);	
	} // end of member function fire

} // end of Employee
