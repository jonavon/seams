<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * Job 
 * 
 * @uses Vader
 * @package 
 * @version $id$
 * @copyright University Libraries, Virginia Tech
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */
class Job implements Vader {

	/*** Attributes: ***/
	private $db;
	/**
	 * Job identifier.
	 * @access private
	 */
	private $id;

	/**
	 * Supervisor that created this Job.
	 * @access private
	 */
	//	private $supervisor;

	/**
	 * Description text for the position.
	 * @access private
	 */
	private $description;

	/**
	 * Array of positions.
	 * @access private
	 */
	private $positions;
	private $pools;


	private $filled;
	private $modifiedArray;
	private $trash;

	public function __construct(PDO $database) {
		$this->db = $database;
		$this->id = null;
		$this->label = null;
		$this->description = null;
		$this->positions = array();
		$this->pools = array();
		$this->filled = 0;
		$this->modifiedArray = array();
		$this->trash = 0;
	}

	/**
	 *
	 * @param String id Id for object
	 * @return Array
	 * @access public
	 */
	public function view( $id = null ) {
		$result = null;
		if($id == null) {
			$id = $this->id;
		}
		$sql = "SELECT * FROM `jobs` WHERE `id` = :id";
		$statement = $this->db->prepare($sql);
		$statement->bindParam(':id', $id, PDO::PARAM_INT);
		$statement->execute();
		$result = $statement->fetch(PDO::FETCH_ASSOC);
		if(!$result) {
			throw new Exception("Job $id does not exist within the database.");
		}
		foreach($result as $key=>$value){
			if($value !== null)	{
				$this->__set($key, $value);
			}
		}
		return $result;
	} // end of member function view

	/**
	 * Add Object
	 *
	 * @return 
	 * @access public
	 */
	public function add( ) {
		if(!empty($this->supervisor)){
			if(empty($this->label)) {
				$this->label = 'STUDENT WORKER';
			}
			$sql = "INSERT INTO `jobs` VALUES (:id, :label, :description, :supervisor, 0)";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $this->id, PDO::PARAM_INT);
				$statement->bindParam(':label', $this->label, PDO::PARAM_STR);
				$statement->bindParam(':description', $this->description, PDO::PARAM_STR);
				$statement->bindParam(':supervisor', $this->supervisor, PDO::PARAM_STR);
				if($statement->execute()){
					if(empty($this->id)){;
						$this->id = $this->db->lastInsertID();
					}
				}
				else {
					$err = $statement->errorInfo();
					throw new Exception("There was an error with this statement: {$err[2]}");
				}
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Supervisor property is required for this method.");
		}
	} // end of member function add

	/**
	 * Throws object into the trash without permenantly deleting.
	 *
	 * @return 
	 * @access public
	 */
	public function delete( ) {
		if((bool)$this->id){
			$id=$this->id;
			$sql = "UPDATE `jobs` SET `trash`=1 WHERE `id`=:id";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $id, PDO::PARAM_INT);
				$statement->execute();
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot delete a job without an ID.");
		}
	} // end of member function delete

	/**
	 * Edit object
	 *
	 * @return 
	 * @access public
	 */
	public function edit( ) {
		if($this->supervisor != null){
			if($this->label == null) {
				$this->label = 'STUDENT WORKER';
			}
			$this->checkRequiredFields();
			if(count($this->modifiedArray) > 0){
				$id = $this->id;
				$sql = "UPDATE `jobs` SET ";
				foreach($this->modifiedArray as $column => $value) {
					$set[] = "$column=:$column";
				}
				$sql .= implode(", ", $set);
				$sql .= " WHERE id = '$id'";
				try {
					$statement = $this->db->prepare($sql);
					foreach($this->modifiedArray as $column => $value){
						$statement->bindValue(":$column", $this->$column);
					}
					if($statement->execute()){
						$this->modifiedArray = array();
					}
					else {
						throw new Exception("There was an error with this statement:" . $statement->errorInfo());
					}
				}
				catch (Exception $e) {
					throw new Exception($e->getMessage(),$e->getCode());
				}
			}
		}
	} // end of member function edit

	/**
	 * Restore an object from trash.
	 *
	 * @param String id Restore from trash.
	 * @return 
	 * @access public
	 */
	public function restore() {
		if((bool)$this->id){
			$id=$this->id;
			$sql = "UPDATE `jobs` SET `trash`=0 WHERE `id`=:id";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $id, PDO::PARAM_INT);
				$statement->execute();
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Cannot delete a job without an ID.");
		}
	} // end of member function restore

	/**
	 * Browse a list of a resource.
	 *
	 * @param String filter Filter used to search results.

	 * @return Array
	 * @access public
	 */
	public function browse( $filter = null ) {
		$sql = "SELECT * FROM `jobs_v` WHERE `trash`=0";
		$statement = $this->db->query($sql);
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	} // end of member function browse

	/**
	 * Permanantly delete from database.
	 *
	 * @param String id Identifier for the object
	 * @return 
	 * @access public
	 */
	public function purge() {
		if(isset($this->id)) {
			$id = $this->id;
			$sql = "DELETE FROM `jobs` WHERE id=:id";
			try {
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $id, PDO::PARAM_STR);
				$statement->execute();
			}
			catch (Exception $e) {
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
		else {
			throw new Exception("Unable to purge a Job without a job id set.");
		}
	} // end of member function purge

	/**
	 * Get actual value from database which may be a reference id number.
	 *
	 * @param String field Database field
	 * @param bool reread If set to true operation will query database again.
	 * @return Array
	 * @access public
	 */
	public function read( $field,  $reread = false ) {
		throw new Exception("Implement " . __FUNCTION__);
	} // end of member function read

	/**
	 * Overloaded function that gets a specified field.
	 *
	 * @param String field Object field to get
	 * @return String
	 * @access public
	 */
	public function __get( $field ) {
		switch($field) {
			case 'positions' :
				$this->loadPositions();
				break;
			case 'pools' :
				$this->loadPools();
				break;
			default:
		}
		return $this->$field;
	} // end of member function __get

	/**
	 * Overloaded function that sets a specified field.
	 *
	 * @param String field Field to change
	 * @param String value Value that the field should be changed to.
	 * @return 
	 * @access public
	 */
	public function __set( $field,  $value ) {
		$value = (!empty($value))?stripslashes($value):null;
		switch($field){
			default :
				$this->$field = $value;
		}
		$this->modifiedArray[$field] = $value;
	} // end of member function __set

	/**
	 * addOpenPositions 
	 * 
	 * @param mixed $number 
	 * @access public
	 * @return void
	 */
	public function addOpenPositions($number) {
		for($i=0;$i < $number;$i++){
			$p = new Position($this->db);
			$p->__set('job', $this->__get('id'));
			$p->add();
			//$this->addPosition($p);
		}
	}

	/**
	 * loadPositions 
	 * 
	 * @access private
	 * @return void
	 */
	private function loadPositions() {
		if(isset($this->id)){
			$sql = "SELECT * FROM `positions` WHERE `job` = :job";
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':job', $this->id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			if($result) {
				foreach($result as $value) {
					$p = new Position($this->db);
					$p->__set('id', $value['id']);
					$p->__set('job', $value['job']);
					$p->__set('employee', $value['employee']);
					$p->view();
					if($value['employee'] != null){
						$this->filled++;
					}
					$this->positions[] = $p;
				}
			}
		}
		else {
			throw new Exception("Job id must be set before postitions can be loaded.");
		}
	}

	public function poolApplication(Application $application) {
		$p = new Pool($this->db);
		$p->__set('job', $this->id);
		$p->__set('application', $application);
		try {
			$p->add();
		}
		catch (Exception $e) {
			throw new Exception("Unable to pool this application.");
		}
	}
	/**
	 * loadPools 
	 * 
	 * @access private
	 * @return void
	 */
	private function loadPools() {
		if(isset($this->id)){
			$sql = "SELECT * FROM `pools_v` WHERE `job` = :job";
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':job', $this->id, PDO::PARAM_INT);
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			if($result) {
				foreach($result as $value) {
					$p = new Pool($this->db);
					$p->__set('id', $value['id']);
					$p->__set('job', $value['job']);
					$p->__set('application', $value['application']);
					$this->pools[] = $p->view();
				}
			}
		}
		else {
			throw new Exception("Job id must be set before pools can be loaded.");
		}
	}

	private function checkRequiredFields() {
		if(!isset($this->id)){
			throw new Exception("ID field required.");
		}
		elseif(!isset($this->label)){
			throw new Exception("Label field required.");
		}
		elseif(!isset($this->supervisor)){
			throw new Exception("Supervisor field required.");
		}
	}

	public function fillPosition(Applicant $applicant) {
		if(count($this->positions)) {
			$openings = array_filter(
					$this->positions,
					create_function('$position', '$test = $position->__get("employee");return empty($test);') 
			);
			$p =& $openings[0];
		}
		if(empty($p)) {
			$p = new Position($this->db);
			$p->__set('job', $this->id);
		}
		$p->__set('employee', $applicant->__get('pid'));
		$p->add();
		return $p->__get('id');
	}
} // end of Job
