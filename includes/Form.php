<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File includes/Form.php
 *
 * Long description for file (if any)...
 *
 * @copyright Copyright (c) 2008 University Libraries, Virginia Tech
 * @license http://license.url
 * @version $Id:$
 * @link http://project.url
 * @since File available since the 0.0.0 Wed Oct 8 2008 10:56:12
 */

/**
 * class Form
 *
 * Represents a form object that includes questions and labels.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 */
class Form {
	/*** Constants ***/
	const POST = "post";
	const GET = "get";
	/*** Attributes: ***/
	private $action;
	private $method;
	private $name;
	private $classes;
	private $elements;

	/**
	 * __construct 
	 * 
	 * @param mixed $action 
	 * @access public
	 * @return void
	 */
	public function __construct($action=null) {
		$this->action = $action;
		$this->method = self::POST;
		$this->name = null;
		$this->classes = array();
		$this->elements = array();
	} // end of __construct

	/**
	 * __get 
	 * 
	 * @param mixed $field 
	 * @access public
	 * @return void
	 */
	public function __get($field) {
		return $this->$field;
	}

	/**
	 * __set 
	 * 
	 * @param mixed $field 
	 * @param mixed $value 
	 * @access public
	 * @return void
	 */
	public function __set($field, $value) {
		$this->$field = $value;
	}

	/**
	 * addElement 
	 * 
	 * @param FormElement $element 
	 * @access public
	 * @return void
	 */
	public function addElement(FormElement $element) {
		$this->elements[] = $element;
	}

	/**
	 * toHTML 
	 * 
	 * @access public
	 * @return void
	 */
	public function toHTML() {
		$action = $this->__get('action');
		$method = $this->__get('method');
		$class = implode(' ', $this->classes);
		$html = "<form method='$method' action='$action'>";
		foreach($this->elements as $element) {
			$html .= $element->toHTML();
		}
		$html .= "</form>\n";
		return $html;
	}
} // end of Form
