<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
/**
 *
 * This is the short Description for the Class
 *
 * This is the long description for the Class
 *
 * @author		Karl Heinz Marbaise <khmarbaise@gmx.de>
 * @copyright	(c) 2003 by Karl Heinz Marbaise
 * @version		$Id$
 * @package		Package
 * @subpackage	SubPackage
 * @see			??
 */
class TextareaFormElement extends FormElement {
	/**
	 * __construct 
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		$this->required = false;
		$this->name = null;
		$this->readonly = false;
		$this->type = FormElement::TEXTAREA_TYPE;
		$this->value = null;
		$this->classes = array();
		$this->attributes = array();
	} // end of __construct

	/**
	 * toHTML 
	 * 
	 * @access public
	 * @return void
	 */
	public function toHTML() {
		$required = ($this->required)? $this->addToClasses('required') : null;
		$readonly = ($this->readonly)? $this->selfAttr('readonly') : null;
		$labelText = $this->label;
		$name = $this->name;
		$type = $this->type;
		$value = $this->value;
		$classes = implode(' ', $this->classes);
		$label = "<label for='$name'></span>$labelText</label>";
		$input = "<textarea name='$name' class='$classes' $readonly>\n\t$value\n</textarea>";
		$formelement = "<p>\n$label\n$input\n</p>\n";

		return $formelement;
	}
}
