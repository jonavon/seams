<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/** dbext:profile=seas
 * File Supervisor.php
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @copyright Copyright (c) University Libraries, Virginia Tech
 */
/**
 * class Supervisor
 */
class Supervisor extends Employee {

	/*** Attributes: ***/
	protected $permission = Profile::SUPERVISOR_LEVEL;

	/**
	 * List of employees for this supervisor
	 * @access private
	 */
	protected $employees = null;

	/**
	 * Internal department or sub-department of this supervisor.
	 * @access private
	 */
	protected $unit;
	protected $jobs;
	
	/**
	 * __construct 
	 * 
	 * @param mixed $pid 
	 * @param mixed $database 
	 * @access public
	 * @return void
	 */
	public function __construct($pid=null, $database=null) {
		parent::__construct($pid, $database);
		$this->jobs = null;
		if(isset($pid)) {
			$this->view();
		}
	}

	/**
	 * loadJobs 
	 * 
	 * @access protected
	 * @return void
	 */
	protected function loadJobs() {
		if(isset($this->pid)){
			$sql = "SELECT * FROM `jobs` WHERE `supervisor` = :supervisor AND `trash`=0";
			$statement = $this->db->prepare($sql);
			$statement->bindParam(':supervisor', $this->pid, PDO::PARAM_STR);
			$statement->execute();
			$result = $statement->fetchAll(PDO::FETCH_ASSOC);
			if(!$result) {
				$err =  $statement->errorInfo();
				if(isset($err[1])){
					throw new Exception("There was an error with this statement: {$err[1]} {$err[2]}");
				}
			}
			foreach($result as $value) {
				$j = new Job($this->db);
				$j->__set('id', $value['id']);
				$j->__set('label', $value['label']);
				$j->__set('description', $value['description']);
				$j->view();
				$this->jobs[] = $j;
			}
		}
		else {
			throw new Exception("Supervisors pid must be set before jobs can be loaded.");
		}
	}


	/**
	 * employ 
	 * A supervisor or admin cannot be hired that is just silly. 
	 * @access public
	 * @return void
	 */
	public function employ(Job $job, Applicant $applicant) {
		if(isset($job->id)) {
			try {
				$applicant->hire($job);
			}
			catch (Exception $e) {
				throw new Exception("There is a problem with this hire: {$e->getMessage()}.");
			}
		}
		else {
			throw new Exception("Job must have an id to use hire method.");
		}
	}

	/**
	 * @override
	 */
	public function __get($field) {
		switch($field){
			case 'jobs' :
				$this->loadJobs();
				break;
			case 'settings' :
				$this->loadSettings();
				break;
		}
		return parent::__get($field);
	} // end of member function __get

} // end of Supervisor
