<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File Message.php
 *
 * Contains the class for messages.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version  Fri May 22 11:12:26 EDT 2009
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package seas
 */
include_once 'markdown.php';

/**
 * class Message
 *
 * 
 *
 * @author		Karl Heinz Marbaise <khmarbaise@gmx.de>
 * @copyright	(c) 2003 by Karl Heinz Marbaise
 * @version		$Id$
 * @package		Package
 * @subpackage	SubPackage
 * @see			??
 */
class Message implements Vader {
	const FROM = 'SEAS System NO-REPLY';
	private $pid;
	private $db;
	private $to;
	private $subject;
	private $message;
	private $uid;
	private $mailer;
	private $headers;
	private $multipart;


	/**
	 * __construct
	 * @see RFC 23822 http://www.faqs.org/rfcs/rfc2822
	 */
	public function __construct($pid = null, PDO $database = null, $to = null, $subject = null, $message = null) {
		if($database != null) {
			$this->setDatabase($database);
		}
		$this->pid = $pid;
		if(is_array($to)){
			$this->to = $to;
		}
		else {
			$this->to = array($to);
		}
		$this->subject = $subject;
		$this->message = $message;
		$this->id = substr(sha1(time()), -9); 
		$this->uid = "==Multipart_Boundary_({$this->id})==--";
		$this->mailer = 'SEAS Message System';
	}
	/**
	 * method send.
	 * Post message to database and send email to user.
	 */
	public function send() {
		try{
			$this->add();
			$this->email();
		}
		catch(Exception $e) {
			 throw new Exception($e->getMessage(),$e->getCode());
		}
	}

	
	/**
	 * email 
	 * 
	 * @access public
	 * @return String
	 */
	public function email() {
		foreach($this->to AS $m_to){
			if(empty($this->headers)) {
				$this->buildHeaders();
			}
			if(empty($this->multipart)){
				$this->buildHtmlMessage();
			}
			if(!mail("$m_to@vt.edu", $this->subject, $this->multipart, $this->headers)){
				throw new Exception("Unable to send email to $m_to@vt.edu");
			}
		}
	}

	/**
	 * buildHeaders 
	 * 
	 * @access private
	 * @return String 
	 */
	private function buildHeaders() {
		$headers = "";
		$headers .= "Date: " . date('r') . "\n";
		$headers .= 'From: ' . self::FROM . " <no-reply@{$_SERVER['HTTP_HOST']}>\n";
		$headers .= "Reply-To: {$this->pid}@vt.edu\n";
		$headers .= "Subject: {$this->subject}\n";
		$headers .= "X-Mailer: {$this->mailer}\n";
		$headers .= "X-SEAS-MessageID: {$this->id}\n";
		$headers .= "MIME-Version: 1.0\n";
		// If we want attachment in the future we would need to set the Content-Type
		// to multipart/mixed
		$headers .= "Content-Type: multipart/alternative;\n boundary=\"{$this->uid}\"\n";
		$headers .= "X-Mailer: {$this->mailer}";
		$this->headers = $headers;
	}


	private function buildHtmlMessage() {
		$text = wordwrap($this->message, 78, "\n", false);
		$html = Markdown($this->message);
		$msg = "This is a multi-part message in MIME format...\n\n";
		$msg .= "--{$this->uid}\n";
		$msg .= "Content-Type: text/plain; charset=\"iso-8859-1\"\n";
		$msg .= "Content-Transfer-Encoding: 7bit\n";
		$msg .= "\n";
		$msg .= "$text\n";
		$msg .= "\n";
		$msg .= "--{$this->uid}\n";
		$msg .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
		$msg .= "Content-Transfer-Encoding: 7bit\n";
		$msg .= "\r\n";
		$msg .= "$html\n";
		$msg .= "\r\n";
		$this->multipart = $msg;
	}

	/**
	 * setDatabase 
	 * 
	 * @param PDO $database 
	 * @access public
	 * @return void
	 */
	public function setDatabase(PDO $database) {
		$this->db = $database;
	}

	/**
	 *
	 * @param String id Id for object

	 * @return Array
	 * @access public
	 */
	public function view( $id = null ) {
		throw new Exception("Implement " . __FUNCTION__);
	} // end of member function view

	/**
	 * Add Object
	 *
	 * @return 
	 * @access public
	 */
	public function add($reply = null) {
		if(empty($this->subject) || empty($this->message)){
			throw new Exception("Subject and Message must be set in order to send a message.");
		}
		else {
			$sql = "INSERT INTO `messages` VALUES(:id, :from, :body, :subject, null, :reply)";
			$sql2 = "INSERT INTO `to` VALUES(:message, :to, null)";
			try {
				$this->db->beginTransaction();
				$statement = $this->db->prepare($sql);
				$statement->bindParam(':id', $this->id, PDO::PARAM_STR, 9);
				$statement->bindParam(':from', $this->pid, PDO::PARAM_STR, 8);
				$statement->bindParam(':body', $this->message, PDO::PARAM_STR);
				$statement->bindParam(':subject', $this->subject, PDO::PARAM_STR);
				$statement->bindParam(':reply', $reply, PDO::PARAM_STR, 9);
				if(!$statement->execute()) {
					$err = $statement->errorInfo();
					throw new Exception("Unable to add message.");
				}
				$statement2 = $this->db->prepare($sql2);
				$statement2->bindParam(':message', $this->id, PDO::PARAM_STR, 9);
				foreach($this->to AS $recipient) {
					$statement2->bindParam(':to', $recipient, PDO::PARAM_STR, 8);
					if(!$statement2->execute()) {
						$err = $statement2->errorInfo();
						throw new Exception("Unable to set message for $recipient. {$err[0]} {$err[1]} {$err[2]} ");
					}
				}
				$this->db->commit();
			}
			catch (Exception $e) {
				$this->db->rollback();
				throw new Exception($e->getMessage(),$e->getCode());
			}
		}
	} // end of member function add

	/**
	 * Throws object into the trash without permenantly deleting.
	 *
	 * @return 
	 * @access public
	 */
	public function delete( ) {
		throw new Exception("Implement " . __FUNCTION__);
	} // end of member function delete

	/**
	 * Edit object
	 *
	 * @return 
	 * @access public
	 */
	public function edit( ) {
		throw new Exception("Cannot edit sent Messages");
	} // end of member function edit

	/**
	 * Restore an object from trash.
	 *
	 * @param String id Restore from trash.
	 * @return 
	 * @access public
	 */
	public function restore() {
		throw new Exception("Cannot restore deleted Messages");
	} // end of member function restore

	/**
	 * Browse a list of a resource.
	 *
	 * @param String filter Filter used to search results.
	 * @return Array
	 * @access public
	 */
	public function browse( $filter = null ) {
		throw new Exception("Implement " . __FUNCTION__);
	} // end of member function browse

	/**
	 * Permanantly delete from database.
	 *
	 * @param String id Identifier for the object

	 * @return 
	 * @access public
	 */
	public function purge() {
		$this->delete();
	} // end of member function purge

	/**
	 * Get actual value from database which may be a reference id number.
	 *
	 * @param String field Database field

	 * @param bool reread If set to true operation will query database again.

	 * @return Array
	 * @access public
	 */
	public function read( $field,  $reread = false ) {
		throw new Exception("Implement " . __FUNCTION__);
	} // end of member function read

	/**
	 * Overloaded function that gets a specified field.
	 *
	 * @param String field Object field to get

	 * @return String
	 * @access public
	 */
	public function __get( $field ) {
		return $this->$field;
	} // end of member function __get

	/**
	 * Overloaded function that sets a specified field.
	 *
	 * @param String field Field to change

	 * @param String value Value that the field should be changed to.

	 * @return 
	 * @access public
	 */
	public function __set( $field,  $value ) {
		$this->$field = $value;
	} // end of member function __set
} // End Messssage class
