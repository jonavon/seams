<?php
/**
 * File index.php
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2008 University Libraries, Virginia Tech
 */

require_once '../includes/global.inc.php';

// View Calendar
require_once 'lib/iCalcreator.class.php';

date_default_timezone_set($_seas_config['SEAS']['timezone']);
$now = time();
$today = strtotime(date('Y-m-d') . " 00:00:00" . date('T'));
$plusmore = $now + (60 * 60 * 24 * 365);
$y = (int)date('Y');
$m = (int)date('m');
$d = (int)date('d');
$ny = (int)date('Y', $plusmore);
$nm = (int)date('m', $plusmore);
$nd = (int)date('d', $plusmore);
$ical = new vcalendar();
$ical->setConfig('unique_id', 'seas.lib.vt.edu');
$ical->parse(SEAS_CALENDAR);
$ical->sort();
$details = $ical->selectComponents($y-1, $m, $d-1, $ny, $nm, $nd, 'vevent');
if((bool)$details){
	foreach($details as $year=>$yr) {
		foreach($yr as $month=>$mth) {
			foreach($mth as $day=>$dy) {
				foreach($dy as $vevent) {
					$dtiso8601 = $year . '-' .str_pad($month,2,'0',STR_PAD_LEFT). '-' . str_pad($day,2,'0',STR_PAD_LEFT);
					$vev = strtotime("$dtiso8601 00:00:00" . date('T'));
					if($vev < $today){
						if($vevent->summary['value'] === 'Hiring Period Begins') {
							$hirebegin[0] = $vev;
						}
					}
					if($vev >= $today) {
						if($vevent->summary['value'] === 'Application Day') {
							$appday[] =  $vev;
						}
						if($vevent->summary['value'] === 'Hiring Period Begins') {
							$hirebegin[] = $vev;
						}
						if($vevent->summary['value'] === 'Last Day to Drop Classes') {
							$dropday[] = $vev;
						}
					}
				}
			}
		}
	}
	unset($y, $m, $d, $ny, $nm, $nd);
	if((bool)$appday) {
		@$template_vars['application_day']['next']['dtstart'] = $appday[0];

		if(count($appday) > 1){
			@$template_vars['application_day']['after']['dtstart'] = $appday[1];
			$template_vars['after_application_drive_date'] = date('l, F jS', $template_vars['application_day']['after']['dtstart']);
		}
		$template_vars['next_application_drive_date'] = date('l, F jS', $template_vars['application_day']['next']['dtstart']);

		$test[0] = date('Y-m-d', time());
		$test[1] = date('Y-m-d', $template_vars['application_day']['next']['dtstart']);

		$template_vars['appdayistoday'] = (date('Y-m-d', time()) === date('Y-m-d', $template_vars['application_day']['next']['dtstart']));
	}
}
// Cleanup
unset($evt0s, $evt1s, $evt0e, $evt1e, $ical, $details,$year, $month,$day);
// End View Calendar

$user = null;

$template_vars['title'] = null;
// Logout of the session.
if(isset($_SERVER['PATH_INFO'])){
	if($_SERVER['PATH_INFO'] == '/logout'){
		SEAMS::logout_session();
	}
	/*
	else if($_SERVER['PATH_INFO'] == '/privacy'){ 
		$object = 'privacy';
		$action = null;
		$objid = null;
		$template_vars['object'] = $object;
		include_once ACTION_DIR . DIRECTORY_SEPARATOR . "$object.inc.php";
	}
	*/
	else {
		if(isset($_SESSION['pid'])){
			$path = explode('/', $_SERVER['PATH_INFO']);
			$object = (isset($path[1]))?$path[1]:null;
			$action = (isset($path[2]))?$path[2]:null;
			$objid = (isset($path[3]))?$path[3]:null;
		}
		else {
			$object = 'login';
			$action = null;
			$objid = null;
		}

		if(isset($_POST['login'])) {
			if(empty($_POST['username'])){
				trigger_error("You must use a PID/Password combination to log in.", E_USER_ERROR);
				$object = 'login';
				$action = null;
				$objid = null;
			}
			else {
				// Get ldap information will only work with proper username and password.

				$ldapinfo = VTLDAP::ldap($_POST['username'], $_POST['password']);
				if($ldapinfo) {
					session_regenerate_id(true);
					$_SESSION['pid'] = strtolower($_POST['username']);

					try {
						$user = Profile::loadProfile($_SESSION['pid'], $db);
						$_SESSION['permission'] = $user->permission;
						$_SESSION['settings'] = $user->settings;
						unset($ldapinfo);
					}
					catch (Exception $e) {
						if(!$template_vars['appdayistoday']) {
							trigger_error("We are sorry new applications will not be taken at this time. Please try again {$template_vars['next_application_drive_date']}", E_USER_WARNING);
						}
						else {
							if($e->getCode() == 19456) {
								$object = 'profile';
								$action = 'add';
								$objid = null;
								$_SESSION['permission'] = Profile::NEW_LEVEL;
								$_SESSION['newapplicant'] = array(
										'profile' => false,
										'application' => false,
										'schedule' => false
										);
							}
							else {
								trigger_error($e->getMessage(), E_USER_ERROR);
							}
						}
					}
					
					$admittance = new Access($db);
					if(!($admittance->allowed($_SERVER['REMOTE_ADDR'],$_SESSION['permission']))){
						unset($_SESSION['pid']);
						trigger_error("You are not permitted to access this resource from this computer. IP Address: {$_SERVER['REMOTE_ADDR']}", E_USER_WARNING);
					}

				}
				else {
					$message[] = array(
							'type' => APP_ERROR,
							'content' => "We are unable to authenticate you. Please check your username and password."
							);
				}
			}
		}
		if(isset($object)) {
			include ACTION_DIR . DIRECTORY_SEPARATOR . "$object.inc.php";
		}
		$template_vars['object'] = $object;
		$template_vars['action'] = $action;
		$template_vars['objid'] = $objid;
	}
}

$template_vars['permission'] = (isset($_SESSION['permission']))?$_SESSION['permission']:null;
$template_vars['template_dir'] = TEMPLATE_DIR;
$template_vars['base_url'] = SEAS_WEB_ROOT .'/';
$template_vars['messages'] = $messages;
$template_vars['user'] = $user;
$template_vars['lastperiod'] = $hirebegin[0];
$html = SEAMS::apply_template(dirname(__FILE__) . DIRECTORY_SEPARATOR . TEMPLATE_DIR . DIRECTORY_SEPARATOR . "_main.tpl.php", &$template_vars);
echo $html;
