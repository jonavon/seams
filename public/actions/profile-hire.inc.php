<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * profile-hire.inc.php
 *
 * Hire an applicant to certain job.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 */

if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL,Profile::SUPERVISOR_LEVEL,Profile::PLUS_LEVEL),$_SESSION['permission'])){
	if(isset($path[4])){
		$j = new Job($db);
		$j->__set('id',$path[4]);
		try{
			$j->view();
			$a = new Applicant($objid,$db);
			$pos = $a->hire($j);
			$messages[] = array(
				'type' => APP_NOTICE,
				'content' => "Yay! You have a new employee! $objid@vt.edu"
			);
			$template_vars['hireid'] = $path[4];
			$placeholder = $objid;
			$objid = $_SESSION['pid'];
			include_once 'profile-settings.inc.php';
			$objid = $placeholder;
			unset($placeholder);
		}
		catch (Exception $e) {
			trigger_error("There appears to be a problem with hiring: {$e->getMessage()}", E_USER_ERROR);
		}
		//
		// Send Email
		//
		$sender = new Supervisor($_SESSION['pid'],$db);
		$mbody = "The supervisor {$sender->firstname} {$sender->lastname} has just completed appointment forms for the following hire:\n\n* [{$a->firstname} {$a->lastname}({$a->vtid})]({$_seas_config['SEAS']['base']}/admin/positions/pending/#position$pos)";
		if(!(isset($template_vars['settings']['PAYCODE']))) {
			$mbody .= "\n\n**THIS SUPERVISOR DOES NOT HAVE A PAYCODE**";
		}
		unset($sender);
		unset($a);
		$p = new Admin(null,$db);
		$list = $p->browse();
		$adm_list = array_filter($list,create_function('$item','return (strtoupper($item["type"])==="ADMIN");'));
		$admins = null;
		foreach($adm_list AS $adm) {
			$admins[] = $adm['pid'];
		}
		$mail = new Message($_SESSION['pid'],$db,$admins,'[SEAS] New Hire',$mbody);
		try{
			$mail->send();
		}
		catch(Exception $e) {
			trigger_error("System Message: Unable to send message", E_USER_ERROR);
		}

	}
	else {
		trigger_error("Malformed URL: Need a job id to hire this applicant.", E_USER_WARNING);
	}
}
else {
	trigger_error("You are not permitted to use this resource.", E_USER_ERROR);
}
$template_vars['title'] = "New Hire";
