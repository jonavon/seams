<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$objid = (isset($objid))?$objid:$_SESSION['pid'];
try {
	$p = Profile::loadProfile($objid,$db);
	$settings = $p->__get('settings');
	$template_vars['settings'] = $settings;
	$appforeference = new Application($db);
	foreach($appforeference->browse() AS $app){
		$referencelist['SUPPRESS'][$app['id']] = $app; 
	}
	$enumforreference = new EnumList($db);
	foreach($enumforreference->view('paycodes') AS $pcode){
		$referencelist['PAYCODE'][$pcode['id']] = $pcode;
	}
	$template_vars['referencelist'] = $referencelist;
	if(empty($settings)) {
		$messages[] = array(
			'type' => APP_WARNING,
			'content' => "There are no settings."
		);
	}
}
catch (Exception $e) {
	trigger_error($e->getMessage(), E_USER_ERROR);
}
$template_vars['title'] = "View Personal Settings";
