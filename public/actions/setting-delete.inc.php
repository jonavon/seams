<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

if(isset($objid)) {
	$p = Profile::loadProfile($_SESSION['pid'],$db)	;
	try {
		$p->deleteSetting($objid);
		$messages[] = array(
			'type' => APP_NOTICE,
			'content' => "Setting $objid has been deleted."
		);
	}
	catch(Exception $e){
		trigger_error($e->getMessage(), E_USER_ERROR);
	}
}
else {
	trigger_error("You need an id to complete this operation.", E_USER_ERROR);
}
$template_vars['title'] = "Delete Setting $objid";
