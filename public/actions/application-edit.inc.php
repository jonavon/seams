<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$enum = new EnumList($db);
$template_vars['title'] = "$objid - $action Application";
$template_vars['majors'] = $enum->view('majors');
$appform = new ApplicationForm($db);
$questions = $appform->__get('questions');
$template_vars['questions'] = $questions;

if($action == 'edit') {
	if(isset($objid)) {
		include 'application-view.inc.php';
	}
	else {
		$p = Profile::loadProfile($_SESSION['pid'],$db);
		$apps = $p->__get('applications');
		$a = reset($apps);
		$a->__get('questions');
		$template_vars['application'] = $a->view();
	}
}
else {
	foreach($questions as $key=>$question) {
		$template_vars['application']['content'][$key]['question'] = $question;
	}
	$template_vars['application']['content']['skills']['answer'] = array();
	$template_vars['application']['pid'] = $_SESSION['pid'];
	$template_vars['application']['id'] = null;
	$template_vars['application']['expire'] = null;
	$template_vars['application']['content']['workstudy']['answer'] = null;
	$template_vars['application']['content']['citizen']['answer'] = false;
	$template_vars['application']['content']['library_experience']['answer'] = null;
	$template_vars['application']['content']['other_experience']['answer'] = null;
	$template_vars['application']['content']['work_reference_0_name']['answer'] = null;
	$template_vars['application']['content']['work_reference_0_phone']['answer'] = null;
	$template_vars['application']['content']['work_reference_0_notes']['answer'] = null;
	$template_vars['application']['content']['work_reference_1_name']['answer'] = null;
	$template_vars['application']['content']['work_reference_1_phone']['answer'] = null;
	$template_vars['application']['content']['work_reference_1_notes']['answer'] = null;
	$template_vars['application']['content']['work_reference_2_name']['answer'] = null;
	$template_vars['application']['content']['work_reference_2_phone']['answer'] = null;
	$template_vars['application']['content']['work_reference_2_notes']['answer'] = null;
	$template_vars['application']['content']['another_department']['answer'] = null;
	$template_vars['application']['content']['citizen']['answer'] = false;
	$template_vars['application']['content']['break_worker']['answer'] = false;
	$template_vars['application']['content']['lift50']['answer'] = false;
	$template_vars['application']['content']['transportation']['answer'] = false;
}
