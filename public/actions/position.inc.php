<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
$template_vars['title'] = "Positions";
if(isset($action)) {
	include "position-$action.inc.php";
}
