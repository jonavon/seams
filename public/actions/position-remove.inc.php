<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['ok'] = false;
if($action === 'remove') {
	if(isset($objid)) {
		$p = new Position($db);
		$p->__set('id', $objid);
		try {
			$p->delete();
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => "Position id #$objid has been deleted."
					);
			$template_vars['ok'] = true;
		}
		catch (Exception $e) {
			trigger_error('Unable to delete this position.', E_USER_ERROR);
		}
	}
}
$template_vars['title'] = "$objid - Delete Position";
