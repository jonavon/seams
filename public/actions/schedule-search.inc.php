<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
if(!((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL,Profile::SUPERVISOR_LEVEL,Profile::PLUS_LEVEL),$_SESSION['permission'])))){
	trigger_error("You are not permitted to use this resource.", E_USER_ERROR);
	$action = "error";
	$objid = "unauthorized";
}
else {
	$template_vars['title'] = "Search by Schedule";
	if(isset($_POST['submit'])) {
		$start = date('Hi',strtotime($_POST['beginning']));
		$end = date('Hi',strtotime($_POST['ending']));
		foreach($_POST as $key=>$value){
			if(substr($key,-3) == "DAY"){
				$strt = (int) $value . $start;
				$endng = (int) $value . $end;
				$avail = new Availability($db);
				$avail->__set('beginning',$strt);
				$avail->__set('ending',$endng);
				$_SESSION['schedulesearch'][] = array(
						'beginning' => $avail->__get('beginning'),
						'ending' => $avail->__get('ending')
						);
			}
		}
	}
	if(isset($_POST['clear'])){
		unset($_SESSION['schedulesearch']);
		unset($_SESSION['set']);
	}
	if(isset($_SESSION['schedulesearch'])){
		$temp = array();
		foreach($_SESSION['schedulesearch'] as $data){
			$temp[md5(serialize($data))] = $data;
		}
		$_SESSION['schedulesearch'] = $temp;
		unset($temp);

		usort($_SESSION['schedulesearch'], create_function('$a,$b','return strcasecmp(serialize($a),serialize($b));'));
		//$_SESSION['schedulesearch'] = (array_unique($_SESSION['schedulesearch']));
		$template_vars['ranges'] = $_SESSION['schedulesearch'];
		foreach($_SESSION['schedulesearch'] AS $range) {
			$value = sprintf('%05d',$range['beginning']);
			$evalue = sprintf('%05d',$range['ending']);
			$dotw = substr($value,0,1);
			$bhotd = substr($value,1,2);
			$bmotd = substr($value,3,2);
			$ehotd = substr($evalue,1,2);
			$emotd = substr($evalue,3,2);
			$arrRange = array(
					'dayoftheweek' => date('l',strtotime("+$dotw day",strtotime('Last Sunday'))),
					'starttime' => date("g:i a",mktime((int)$bhotd,(int)$bmotd)),
					'endtime' =>  date("g:i a",mktime((int)$ehotd,(int)$emotd))
					);
			$template_vars['availabilitytext'][] = (object) $arrRange;
		}
	}
	$objid = "schedule";
	$action = "search";
	/*
		 $template_vars['availabilitytext'] = $a->__get('availability');
	 */
}
