<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['ok'] = false;
if($action === 'delete'){
	if(isset($objid)) {
		$job = new Job($db);
		$job->__set('id', $objid);
		try {
			$job->delete();
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => "Pool id #$objid has been deleted."
					);
			$template_vars['ok'] = true;
		}
		catch(Exception $e) {
			trigger_error('Unable to delete this pool.', E_USER_ERROR);
		}
	}
	else {
		trigger_error('The resource id must be set.', E_USER_ERROR);
	}
}
$template_vars['title'] = "Delete Pool";
