<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$_SESSION['permission'])) {
	switch($objid) {
		case 'ethnicities':
			$enumtype = 'ETHNICITY';
			break;
		case 'majors':
			$enumtype = 'MAJOR';
			break;
		case 'units':
			$enumtype = 'UNIT';
			break;
		case 'paycodes':
		 $enumtype = 'PAYCODE';
		 break;
		case 'preferences':
		 $enumtype = 'PREFERENCE';
		 break;
	}
	$enum = new EnumList($db);
	if(isset($_POST['submit'])) {
		$trashbin = array_filter($_POST['enum'], create_function('$iter','return isset($iter["trash"]);'));
		$enumtable = array_filter($_POST['enum'], create_function('$iter','return !isset($iter["trash"]);'));
		$newenum = $_POST['new'];
		$changelist = array();

		if(!empty($newenum['label'])){
			$changelist[] = array(
				'id' => null,
				'label' => $newenum['label'],
				'description' => $newenum['description'],
				'sort' => (int)$newenum['sort']
			);
			unset($newenum);
		}
		foreach($enumtable as $enumid => $fields) {
			$changelist[] = array(
				'id' => (int)$enumid,
				'label' => $fields['label'],
				'description' => $fields['description'],
				'sort' => (int)$fields['sort']
			);
		}
		try {
			foreach(array_keys($trashbin) as $item) {
				$enum->delete($item);
				$messages[] = array(
					'type' => APP_NOTICE,
					'content' => "Deleted enum with id #$item."
				);
			}
			$enum->modify($changelist,$enumtype);
			$messages[] = array(
				'type' => APP_NOTICE,
				'content' => "List changed."
			);
		}
		catch (Exception $e) {
			trigger_error($e->getMessage(), E_USER_ERROR);
		}
	}
	$template_vars['enums'] = $enum->view($objid);
}
else {
	trigger_error("You do not have permission to use this resource", E_USER_ERROR);
	$object = "";
}
