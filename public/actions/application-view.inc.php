<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

if(isset($objid)) {
	$a = new Application($db);
	$template_vars['application'] = $a->view($objid);
	$template_vars['application']['content'] = $a->__get('content');
	$template_vars['ranges'] = $a->__get('ranges');
	$template_vars['availabilitytext'] = $a->__get('availability');
	$old = $objid;
	$objid = $a->__get('pid');
	include 'profile-view.inc.php';
	$objid = $old;
	unset($old);
}
if(isset($_SESSION['permission'])){
	if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL,Profile::SUPERVISOR_LEVEL),$_SESSION['permission'])) {
		$old = $objid;
		$objid = $_SESSION['pid'];
		include 'job-list.inc.php';
		$objid = $old;
		unset($old);
	}
}
$template_vars['title'] = "{$p['lastname']}, {$p['firstname']} - View Application";

