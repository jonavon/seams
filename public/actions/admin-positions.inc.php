<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */
if(isset($_POST['submit'])) {
	$admin = new Admin($_SESSION['pid'],$db);
	foreach($_POST['position'] as $pos_id=>$data) {
		if(!empty($data['paycode'])) {
			$pos = new Position($db);
			$pos->view($pos_id);
			$admin->pay($pos,$data['paycode']);
		}
	}
}

$position_admin = new Position($db);
if(isset($objid)){
	$template_vars['positions'] = $position_admin->browse('type:[[:<:]]'.$objid.'[[:>:]]');
	foreach($template_vars['positions'] as $peep){
		$template_vars['peeps'][$peep['employee']] = Profile::loadProfile($peep['employee'],$db);
	}
}
else {
	$template_vars['positions'] = $position_admin->browse();
}

$enum = new EnumList($db);
$template_vars['paycodes'] = $enum->view('paycodes');
