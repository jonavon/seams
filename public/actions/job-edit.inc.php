<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['title'] = "$action Pool Information";
if(isset($_POST['submit'])) {
	$j = new Job($db);
	if(!empty($_POST['jobid'])) {
		$j->__set('id',$_POST['jobid']);
		$j->view();
	}
	$j->__set('label',$_POST['label']);
	$j->__set('description',$_POST['description']);
	$j->__set('supervisor',$_POST['supervisor']);
	try {
		if(empty($_POST['jobid'])){
			$j->add();
			$objid = $j->__get('id');
			$content = "Pool Information $objid added. You may view it <a href='./job/view/$objid' title='View this job'>View Pool $objid</a>";
		}
		else {
			$j->edit();
			$content = "Pool Information {$_POST['jobid']} successfully edited.";
		}
		$messages[] = array(
			"type" => APP_NOTICE,
			"content" => $content
		);
	}
	catch (Exception $e) {
		trigger_error("Unable to post this pool information. JOB ID: {$_POST['jobid']}", E_USER_WARNING);
	}
}
if($action === 'add'){
	$template_vars['job'] = array(
		'id' => null,
		'supervisor' => $_SESSION['pid'],
		'label' => 'STUDENT WORKER',
		'description' => 'Descriptive Job Description'
	);
}
if(isset($objid)) {
	include 'job-view.inc.php';
}
