<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}

if(isset($path[4])) {
	$pool = new Pool($db);
	$pool->removeByApp($objid,$path[4]);
}
else {
	trigger_error('The resource id must be set.', E_USER_ERROR);
}
include_once "job-view.inc.php";
$template_vars['title'] = "Remove Applicant from Pool";
