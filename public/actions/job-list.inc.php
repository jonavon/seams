<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
if($_SESSION['pid']){
	if(!isset($objid)) {
		$objid = $_SESSION['pid'];
	}
	$listjobs = array();
	$o = Profile::loadProfile($objid,$db);
	if($o->__get('permission') & 12){
		$jbs = $o->__get('jobs');
		if((bool)$jbs){
			foreach($jbs as $jb) {
				$jobinfo =  $jb->view();
				$jobinfo['positions'] = $jb->__get('positions');
				$jobinfo['pool'] = $jb->__get('pools');
				$listjobs[] = $jobinfo;
			}
		}
	}
}
$template_vars['jobs'] = $listjobs;
$template_vars['title'] = "$objid - Pool Information";
