<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$p = Profile::loadProfile($_SESSION['pid'],$db);
if($p->__get('permission') & 12){
	try {
		$p->changeSetting(null,'SUPPRESS',$objid);
		$messages[] = array(
			'type' => APP_NOTICE,
			'content' => "This application has been suppressed"
		);
		$_SESSION['settings'] = $p->__get('settings');
	}
	catch (Exception $e) {
		trigger_error("Cannot suppress this application." . $e->getMessage(), E_USER_WARNING);
	}
}
else {
	trigger_error("You do not have permission to access this resource.", E_USER_ERROR);
}
$template_vars['title'] = "$objid - Suppress Application";
