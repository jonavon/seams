<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$messages[] = array(
	'type'=>APP_NOTICE,
	'content'=>'This is a notice.'
);
$messages[] = array(
	'type'=>APP_WARNING,
	'content'=>'This is a warning.'
);
$messages[] = array(
	'type'=>APP_ERROR,
	'content'=>'This is an error.'
);
$messages[] = array(
	'type'=>APP_QUESTION,
	'content'=>'This is a question.'
);
$template_vars['title'] = "Sample Page";
