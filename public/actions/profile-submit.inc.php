<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['title']="Personal Information Submission";
if(isset($_POST['submit'])) {
	$_POST['pid'] = strtolower($_POST['pid']);
	try {
		$profile = Profile::loadProfile($_POST['pid'],$db);
	}
	catch(Exception $e) {
		$objid = 'add';
		$profile = new Applicant(null,$db);
		$profile->__set('pid',$_POST['pid']);
	}
	$properties['vtid'] = null;
	$properties['firstname'] = null;
	$properties['middlename'] = null;
	$properties['lastname'] = null;
	$properties['birthday'] = null;
	$properties['unit'] = null;
	$properties['type'] = null;
	$properties['major'] = null;
	$properties['class'] = null;
	$properties['gender'] = null;
	$properties['ethnicity'] = null;

	$settings = array_intersect_key($_POST, $properties);
	foreach($settings as $field => $value) {
		$value = strip_tags($value);
		$profile->__set($field, $value);
	}
	if(isset($_POST['paycodes'])){
		foreach($_POST['paycodes'] AS $value){
			$profile->changeSetting(null,'PAYCODE',$value);
		}
	}
	try {
		if($objid == 'add') {
			$profile->add();
			$content = 'Your profile was added.';
			if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$_SESSION['permission'])){
				$objid = $_POST['type'];
				include 'profile-list.inc.php';
			}
		}
		else {
			$profile->edit();
			$content = "Your profile was updated.";
			if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$_SESSION['permission'])){
				$objid = $_POST['type'];
				include 'profile-list.inc.php';
			}
		}
		$profile->insertContactInfo($_POST['contact']);
		$messages[] = array(
				'type' => APP_NOTICE,
				'content' => $content
				);
		if(isset($_SESSION['newapplicant'])) {
			include 'application-add.inc.php';
			$_SESSION['newapplicant']['profile'] = true;
		}
	}
	catch (Exception $e) {
		trigger_error("Profile could not be added. Please check your input. Fields like pid and vtid must be unique.", E_USER_ERROR);
		$enum = new EnumList($db);
		$template_vars['majors'] = $enum->view('majors');
		$template_vars['units'] = $enum->view('units');
		$template_vars['ethnicities'] = $enum->view('ethnicities');
		$template_vars['profile'] = $_POST;
	}
}
