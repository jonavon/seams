<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['title'] = "Pool Detail";
if(isset($objid)) {
	$job = new Job($db);
	$job->__set('id', $objid);
	$template_vars['job'] = $job->view();
	if($action != 'edit'){
		$candidates =  $job->__get('pools');
		$pos = $job->__get('positions');
		$poool = array();
		foreach($candidates as $candidate) {
			$app = new Application($db);
			$poool[] = $app->view($candidate['application']);
		}
		$template_vars['pool'] = $poool;
		$template_vars['positions'] = $pos;
	}
}
else {
	trigger_error('The resource id must be set.', E_USER_ERROR);
}
