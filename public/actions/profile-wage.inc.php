<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
include 'profile-view.inc.php';
$pdf = new formPDF();
$pdf->SetAuthor('SEAS');
$pdf->AddPage();
$pdf->Ln(50);
$pdf->drawQA('Employee Name',"{$template_vars['profile']['lastname']}, {$template_vars['profile']['firstname']}");
$pdf->drawQA('Employee ID',"{$template_vars['profile']['vtid']}");
$pdf->drawQA('Appointment',$_REQUEST['appointment']);

$date = (!empty($_REQUEST['appointment-date']))?date('F j, Y', strtotime($_REQUEST['appointment-date'])):'';
$paycode = (isset($_REQUEST['paycode']))?$_REQUEST['paycode']:'';
$pay = (isset($_REQUEST['pay']))?$_REQUEST['pay']:'';
$pdf->drawIndentedQA('Date Appointment will take effect', $date );
$pdf->drawIndentedQA('Employee Type',$_REQUEST['type']);
$pdf->drawSectionHeader('Employee Position');
$pdf->drawIndentedQA('Department Pay Code', $paycode);
$pdf->drawIndentedQA('Pay Rate per Hour','$' . number_format($pay,2));

$pdf->Ln(16);
$pdf->rect($pdf->GetX(), $pdf->GetY(),540,168);
$pdf->Ln(16);
$pdf->SetFont('helvetica', 'B', 12);
$pdf->Cell(0,12,'    LIBRARY PERSONNEL USE ONLY'. ':',0,1,'L');
$pdf->Ln(16);

$pdf->drawBoxedInQA('Banner Position','');
$pdf->drawBoxedInQA('Banner Transaction','');
$pdf->drawBoxedInQA('Timecard Entry','');
$pdf->drawBoxedInQA('SEAS Entry','');


$pdf->Output();
exit();
