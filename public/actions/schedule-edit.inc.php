<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
if(!((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$_SESSION['permission']))
|| ($objid === $_SESSION['appid']) 
)){
	trigger_error("You are not permitted to use this resource.", E_USER_ERROR);
	$action = "error";
	$objid = "unauthorized";
}
else {
$template_vars['title'] = "Edit Schedule";

if(isset($_SESSION['newapplicant'])) {
	$objid = $_SESSION['newapplicant']['application'];
}
if(isset($_POST['delete'])){
	foreach($_POST['deletion'] as $item) {
		$avail = new Availability($db);
		$avail->__set('id',$item);
		$avail->delete();
	}
}
if(isset($_POST['submit']) && isset($_POST['beginning'])) {
	$start = date('Hi',strtotime($_POST['beginning']));
	$end = date('Hi',strtotime($_POST['ending']));
	foreach($_POST as $key=>$value){
		if(substr($key,-3) == "DAY"){
			$strt = (int) $value . $start;
			$endng = (int) $value . $end;
			$avail = new Availability($db);
			$avail->__set('beginning',$strt);
			$avail->__set('ending',$endng);
			$avail->__set('application',$objid);
			$avail->add();
		}
	}
	if(isset($_SESSION['newapplicant'])) {
		$_SESSION['newapplicant']['schedule'] = true;
	}
}
if((isset($_POST['submit']) && isset($_POST['beginning'])) || isset($_POST['delete'])){
	$a = new Application($db);
	$a->view($objid);
	$expiration = (($hirebegin[0] < $dropday[0]) && ($dropday[0] < $hirebegin[1]))?$dropday[1]:$dropday[0];
	$a->__set('expire',$expiration);
	try {
		$a->edit();
		$messages[] = array(
			'type' => APP_NOTICE,
			'content' => "Your application has been edited and will expire on " . date('F j, Y', $expiration)
		);
	}
	catch (Exception $e) {
		trigger_error($e->getMessage(), E_USER_ERROR);
	}
}
include 'application-view.inc.php';
}
