<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

if(isset($_POST['submit'])) {
	$j = new Job($db);
	if(empty($_POST['job'])){
		$_POST['supervisor'] = $_SESSION['pid'];
		$_POST['label'] = null;
		$_POST['description'] = null;
		include 'job-add.inc.php';
	}
	else{
		$objid = $_POST['job'];
	}
	$j->view($_POST['job']);
	if((isset($_POST['applications'])) && count($_POST['applications']) > 0) {
		foreach($_POST['applications'] as $candidate){
			$app = new Application($db);
			$app->view($candidate);
			$j->poolApplication($app);
		}
	}
	include 'job-view.inc.php';
}
$template_vars['title'] = "Pool Information Submission";
