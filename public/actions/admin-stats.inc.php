<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File admin-stats.inc.php
 * 
 * Action file for the statistics needed for the seas system.
 */

$enum = new EnumList($db);
$ethnicities = array();
foreach($enum->view('ethnicities') as $item) {
	$ethnicities[$item['label']] = $item;
}
$template_vars['ethnicities'] = $ethnicities;
$p = new Admin(null,$db);
$template_vars['applicants'] = $p->browse('applicant');
$template_vars['employees'] = $p->browse('employee');

if(isset($_REQUEST['begin'])) {
}

$stat = new Stats($db);
$template_vars['log']['applications']['new'] = $stat->viewLog('applications','insert',$hirebegin[0]);
$template_vars['log']['applications']['deleted'] = $stat->viewLog('applications','delete',$hirebegin[0]);
$template_vars['log']['positions']['new'] = $stat->viewLog('positions','insert',$hirebegin[0]);
$template_vars['log']['positions']['deleted'] = $stat->viewLog('positions','delete',$hirebegin[0]);
$template_vars['log']['profiles']['new'] = $stat->viewLog('profiles','insert',$hirebegin[0]);
$template_vars['log']['profiles']['deleted'] = $stat->viewLog('profiles','delete',$hirebegin[0]);
