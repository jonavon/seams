<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['ok'] = false;
if($action === 'delete') {
	if(isset($objid)) {
		$app = new Application($db);
		$app->__set('id', $objid);
		try {
			$app->delete();
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => "Application id #$objid has been deleted."
					);
			unset($_SESSION['appid']);
			$template_vars['ok'] = true;
		}
		catch (Exception $e) {
			trigger_error('Unable to delete this application.', E_USER_ERROR);
		}
	}
}
$template_vars['title'] = "$objid - Delete Application";
