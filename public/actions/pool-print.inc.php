<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */

if(isset($objid)) {
	include_once 'job-view.inc.php';
	foreach($poool as $person){
		$c = new Applicant($person['pid'], $db);
		$contacts[] = $c->view();
	}
	$template_vars['contacts']=$contacts;
}

