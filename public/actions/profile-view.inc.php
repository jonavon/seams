<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
include_once 'job-list.inc.php';
$objid = (isset($objid))?$objid:$_SESSION['pid'];
try {
	$o = Profile::loadProfile($objid,$db);
	$p = $o->view();
	$p['permission'] = $o->__get('permission');
	$p['middlename'] =(isset($p['middlename']))?$p['middlename']:null;
if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$_SESSION['permission']) || ($_SESSION['pid'] === $p['pid'])
	|| SEAMS::isPermitted(array(Profile::PENDING_LEVEL,Profile::EMPLOYEE_LEVEL),$p['permission']) || $action === 'wage') {
	// TODO: Link Employee to supervisor in a more simple way.
	$p['vtid'] =(isset($p['vtid']))?$p['vtid']:null;
	$p['class'] =(isset($p['class']))?$p['class']:null;
}
else {
	$p['vtid'] = 'XXXXXXXXX';
	$p['class'] = 'XXXXXXXXX';
}
	$p['birthday'] = (isset($p['birthday'] ))?$p['birthday']:null;
	$p['contact']['SCHOOL']['STREET'] = (isset($p['contact']['SCHOOL']['STREET']))?$p['contact']['SCHOOL']['STREET']:null;
	$p['contact']['SCHOOL']['PHONE'] = (isset($p['contact']['SCHOOL']['PHONE']))?$p['contact']['SCHOOL']['PHONE']:null;
	$p['major'] = (isset($p['major']))?$p['major']:null;
	$p['applications'] = $o->__get('applications');
	$p['settings'] = $o->__get('settings');
	if(isset($p['settings']['PAYCODE'])) {
		foreach($p['settings']['PAYCODE'] as $paycode){
			$p['paycodes'][] = $paycode['value'];
		}
	}
}
catch(Exception $e) {
	trigger_error($e->getMessage(), E_USER_ERROR);
}
$template_vars['profile'] = $p;
if(!empty($p['applications'])){
	$placeholder = $objid;
	$objid = key($p['applications']);
	if(!empty($objid)){
		include_once 'application-view.inc.php';
	}
	if(isset($placeholder)){
		$objid = $placeholder;
		unset($placeholder);
	}
}
if(empty($_SESSION['appid']) && SEAMS::isPermitted(array(Profile::APPLICANT_LEVEL),$_SESSION['permission'])){
	$messages[] = array(
		'type' => APP_NOTICE,
		'content' => 'We do not have an application on record for you. Create an application to be considered for employment.'
	);
}
if(SEAMS::isPermitted(array(Profile::SUPERVISOR_LEVEL,Profile::ADMIN_LEVEL), $p['permission'])) {
	$template_vars['title'] = "Tools";
	$tempapp = new Application($db);
	$template_vars['application_count'] = count($tempapp->browse());
}
else {
	$template_vars['title'] = "View Profile";
}
