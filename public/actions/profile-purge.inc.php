<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */

$template_vars['ok'] = false;
if($action === 'purge'){
	if(isset($objid)) {
		$profile = Profile::loadProfile($objid,$db);
		try {
			$profile->purge();
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => "Profile pid $objid has been permanantly deleted."
					);
			$template_vars['ok'] = true;
		}
		catch(Exception $e) {
			trigger_error('Unable to purge this profile.', E_USER_ERROR);
		}
	}
	else {
		trigger_error('The resource id must be set.', E_USER_ERROR);
	}
}
$template_vars['title'] = "Permantly Delete Profile";

