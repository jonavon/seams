<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
if(!((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL,Profile::SUPERVISOR_LEVEL,Profile::PLUS_LEVEL),$_SESSION['permission']))
|| ($objid === $_SESSION['appid']) 
		)){
	trigger_error("You are not permitted to use this resource.", E_USER_ERROR);
	$action = "error";
	$objid = "unauthorized";
}
else {
	switch($action){
		case 'edit':
			include 'schedule-edit.inc.php';
			break;
		default :
			include "schedule-$action.inc.php";
	}
}
