<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
$template_vars['title'] = "Admin";
if(isset($action)) {
	include "pool-$action.inc.php";
}
