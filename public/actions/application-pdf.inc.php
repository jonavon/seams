<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
if(isset($_REQUEST['submit'])){
	$objid = implode(",",$_REQUEST['app']);
	header("Location:" . SEAS_WEB_ROOT . "/application/pdf/$objid");
	exit();
}
$apps[] = $objid;
if(strlen($objid) > 8) {
	$apps =	explode(',',$objid);
}
$pdf = new MyPDF(PDF_PAGE_ORIENTATION, 'pt', PDF_PAGE_FORMAT, true, 'UTF-8', false);
foreach($apps as $objid) {
	include 'application-view.inc.php';
	$t = &$template_vars;
	$modified = date('F j, Y',(int)$template_vars['application']['modified']); 
	$pdf->setFooterText("Modified: $modified \t\t ID: {$template_vars['application']['id']}");

	//set margins
	$pdf->setHeaderMargin(60);
	$pdf->setFooterMargin(PDF_MARGIN_FOOTER);

	//set auto page breaks

	$pdf->AliasNbPages();
	$pdf->SetAuthor('SEAS');
	$pdf->SetTopMargin(60);
	$pdf->SetAutoPageBreak(TRUE, 60);


	$pdf->AddPage();
	$pdf->drawSectionHeader('Personal Information');
	$pdf->drawQA('name',"{$t['profile']['lastname']}, {$t['profile']['firstname']}");
	$pdf->drawQA('Student ID Number',$t['profile']['vtid']);
	$pdf->drawQA('Email Address',"{$t['profile']['pid']}@vt.edu","mailto:{$t['profile']['pid']}@vt.edu");
	$pdf->drawQA('Phone Number',$t['profile']['contact']['SCHOOL']['PHONE']);
	$pdf->drawQA('Academic Class',$t['profile']['class']);
	$pdf->drawQA('Academic Major',$t['profile']['major']);
	$pdf->drawSectionHeader('Employment Information');
	$pdf->drawQA('Workstudy', $t['application']['content']['workstudy']['answer']);
	$pdf->drawQA('Other Department Employed',$t['application']['content']['another_department']['answer']);
	$pdf->drawQA('Hours Willing to Work',$t['application']['content']['hours']['answer']);
	$pdf->drawTextArea('Library Experience',$t['application']['content']['library_experience']['answer']);
	$pdf->drawTextArea('Other Experience',$t['application']['content']['other_experience']['answer']);
	$pdf->drawCheckBox('Transportation',$t['application']['content']['transportation']['answer']);
	$pdf->drawCheckBox('Able To Lift 50 Pounds',$t['application']['content']['lift50']['answer'],1);
	$pdf->drawCheckBox('Work during University Breaks',$t['application']['content']['break_worker']['answer']);
	//$pdf->drawCheckBox('Citizen',$t['application']['content']['citizen']['answer'],1);
	$pdf->Ln(15);
	$pdf->drawSectionHeader('References');
	$pdf->drawQA('Name',$t['application']['content']['work_reference_0_name']['answer']);
	$pdf->drawQA('Phone',$t['application']['content']['work_reference_0_phone']['answer']);
	$pdf->drawTextArea('Contact Information',$t['application']['content']['work_reference_0_notes']['answer']);
	$pdf->drawQA('Name',$t['application']['content']['work_reference_1_name']['answer']);
	$pdf->drawQA('Phone',$t['application']['content']['work_reference_1_phone']['answer']);
	$pdf->drawTextArea('Contact Information',$t['application']['content']['work_reference_1_notes']['answer']);
	$pdf->drawQA('Name',$t['application']['content']['work_reference_2_name']['answer']);
	$pdf->drawQA('Phone',$t['application']['content']['work_reference_2_phone']['answer']);
	$pdf->drawTextArea('Contact Information',$t['application']['content']['work_reference_2_notes']['answer']);
	$pdf->drawSectionHeader('Skill Set');
	$skills = explode(", ",$t['application']['content']['skills']['answer']);
	$pdf->drawSet($skills);

	$pdf->setPrintHeader(false);
	$pdf->AddPage();
	$pdf->drawSectionHeader('Availability');
	$tidy_config['hide-comments'] = true;
	$tidy_config['quote-nbsp'] = true;
	$tidy_config['preserve-entities'] = true;
	$sched = SEAMS::apply_template(dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . TEMPLATE_DIR . DIRECTORY_SEPARATOR . '_pdf-schedule.tpl.php', &$template_vars);
	$sched = tidy_repair_string($sched,$tidy_config);
	$pdf->SetFont('helvetica','B',8);
	$pdf->writeHTML($sched);
}

if(count($apps) > 1) {
	$pdf->Output('apps.pdf');
}
else {
	$pdf->Output($t['profile']['pid'] . '.pdf');
}
exit();
