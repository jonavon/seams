<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
include_once 'skills-search.inc.php';
include_once 'job-list.inc.php';


$_GET['skills'] = (isset($_GET['skills']))?$_GET['skills']:null;
$_POST['combined'] = (isset($_POST['combined']))?$_POST['combined']:false;
$template_vars['applications'] = Tags::browse($db,'skills',$_GET['skills']);

if(isset($_POST['search_schedule'])) {
	if(isset($_SESSION['schedulesearch'])){
		$schedsearch = new Availability($db);
		$applications = ($schedsearch->browse($_SESSION['schedulesearch']));
		$template_vars['totalselected'] = array_reduce($_SESSION['schedulesearch'], create_function('$i,$j','$i +=(isset($j["ending"]))?((int)$j["ending"] - (int)$j["beginning"]):0; return $i;'),0);
		$template_vars['durationInSeconds'] = array_reduce($_SESSION['schedulesearch'], create_function('$i,$j','$i +=(isset($j["ending"]))?(date("U",strtotime(substr($j["ending"],-4))) - date("U",strtotime(substr($j["beginning"],-4)))):0; return $i;'),0);
		// Filter 50% overlaps
		$template_vars['applications'] = array_filter($applications, create_function('$item','global $template_vars; return ((round(((int)$item["overlap"] / (int)$template_vars["totalselected"]))) > .50);')); 
		// Create Text Representation of Schedule Info
	}
}
if(isset($_POST['submit'])) {
	if(isset($_SESSION['schedulesearch'])){
		$forschedule = new Availability($db);
		$fromschedule = $forschedule->browse($_SESSION['schedulesearch']);
		$template_vars['totalselected'] = array_reduce($_SESSION['schedulesearch'], create_function('$i,$j','$i +=(isset($j["ending"]))?((int)$j["ending"] - (int)$j["beginning"]):0; return $i;'),0);
		$template_vars['durationInSeconds'] = array_reduce($_SESSION['schedulesearch'], create_function('$i,$j','$i +=(isset($j["ending"]))?(date("U",strtotime(substr($j["ending"],-4))) - date("U",strtotime(substr($j["beginning"],-4)))):0; return $i;'),0);
		foreach($fromschedule as $set) {
			$_SESSION['set'][$set['id']] = $set;
		}
	}
	$properties['workstudy'] = null;
	$properties['hours'] = null;
	$properties['library_experience'] = null;
	$properties['other_experience'] = null;
	$properties['citizen'] = null;
	$properties['lift50'] = null;
	$properties['break_worker'] = null;
	$properties['transportation'] = null;

	$settings = array_intersect_key($_POST, $properties);
	$search = "{$_POST['q']} "; 
	foreach($settings as $field => $value) {
		if($value != '') {
			if($field == 'hours'){
				$search .= "$field:[>$value] ";
			}
			else {
				$search .= "$field:[$value] ";
			}
		}
	}
	$combine_set = (isset($_SESSION['set']) && (bool)$_POST['combined'])?array_keys($_SESSION['set']):null;
	$sengine = new Application($db);
	$template_vars['applications'] = $sengine->browse($search,$combine_set);
	$template_vars['searchtext'] = $search;
	if(isset($_SESSION['settings']['SUPPRESS'])){
		$hiddenlist = array();
		foreach($_SESSION['settings']['SUPPRESS'] as $suppressed) {
			$hiddenlist[] = $suppressed['value'];
		}
		$template_vars['applications'] = array_filter($template_vars['applications'],'filterResults');

		unset($hiddenlist);
	}
	if((bool)($_POST['combined'])){
		foreach($template_vars['applications'] AS &$app){
			$app['overlap'] = $_SESSION['set'][$app['id']]['overlap'];
		}
		$template_vars['applications'] = array_filter($template_vars['applications'], create_function('$item','global $template_vars; return ((round(((int)$item["overlap"] / (int)$template_vars["totalselected"]))) > .50);')); 
	}
}
if(((bool)($_POST['combined'])) || (isset($_POST['search_schedule']))) {
	if(isset($_SESSION['schedulesearch'])){
		$dateToEnglish = array();
		foreach($_SESSION['schedulesearch'] AS $range) {
			$timeunit = date('g:i a',strtotime(substr(str_pad($range['beginning'], 5, "0", STR_PAD_LEFT),-4))) . " - " . date('g:i a',strtotime(substr(str_pad($range['ending'], 5, "0", STR_PAD_LEFT),-4)));
			$dotw = substr(str_pad($range['beginning'], 5, "0", STR_PAD_LEFT),0,1);
			$weekdayName = date('D',strtotime("+$dotw day",strtotime('Last Sunday')));
			$dateToEnglish[$timeunit][] = $weekdayName;
		} 

		$humantext = null;
		foreach($dateToEnglish AS $hours => $days) {
			$humantext .= $hours . ' On ' . implode(',',$days) . "\n";
		}
		$dateToEnglish['text'] = $humantext;
		$template_vars['scheduleToHuman'] = $dateToEnglish;
	}
}
if(empty($template_vars['applications'])) {
	$messages[]	= array (
			'type' => APP_WARNING,
			'content' => "Your search Gave 0 results. Please modify your search query."
			);
}

$template_vars['title'] = "Application Search Results";

/**
 *
 * This is the short Description for the Function
 *
 * This is the long description for the Class
 *
 * @return	mixed	 Description
 * @access	public
 * @see		??
 */
function filterResults($item) {
	$keep = true;
	global $hiddenlist;
	if(in_array($item['id'],$hiddenlist)) {
		$keep = false;
	}
	return $keep;
}
