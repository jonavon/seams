<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['ok'] = false;
if($action === 'restore'){
	if(isset($objid)) {
		$job = new Job($db);
		$job->__set('id', $objid);
		try {
			$job->restore();
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => "Pool information id #$objid has been restored."
					);
			$template_vars['ok'] = true;
		}
		catch(Exception $e) {
			trigger_error('Unable to restore this pool information.', E_USER_ERROR);
		}
		include_once 'job-view.inc.php';
	}
	else {
		trigger_error('The resource id must be set.', E_USER_ERROR);
	}
}
$template_vars['title'] = "$objid - Pool Restoration";
