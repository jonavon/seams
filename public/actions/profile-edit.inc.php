<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package [package name]
 */
$enum = new EnumList($db);
$template_vars['majors'] = $enum->view('majors');
$template_vars['units'] = $enum->view('units');
$template_vars['ethnicities'] = $enum->view('ethnicities');
$template_vars['paycodes'] = $enum->view('paycodes');
if(isset($_POST['username'])){
	$p['pid'] = $_SESSION['pid'];
}
else {
		$p['pid'] = (!empty($objid))?$objid:null;
}
if(!empty($ldapinfo)) {
	$p['vtid'] = null;
	$p['birthday'] = null;
	$p['class'] = null;
	$p['gender'] = null;
	$p['type'] = null;
	$p['unit'] = null;
	$p['ethnicity'] = null;
	$p['firstname'] = VTLDAP::entryValue($ldapinfo,'givenname');
	$p['middlename'] = VTLDAP::entryValue($ldapinfo,'middlename');
	$p['lastname'] = VTLDAP::entryValue($ldapinfo,'sn');
	$p['contact']['SCHOOL']['STREET'] = VTLDAP::entryValue($ldapinfo,'localpostaladdress');
	$p['contact']['SCHOOL']['PHONE'] = VTLDAP::entryValue($ldapinfo,'localphone');
	$p['major'] = VTLDAP::entryValue($ldapinfo,'majorcode');
	$p['permission'] = null;
}
else {
	if(($action == 'add')){
		$p['vtid'] = null;
		$p['birthday'] = null;
		$p['class'] = null;
		$p['gender'] = null;
		$p['type'] = null;
		$p['unit'] = null;
		$p['ethnicity'] = null;
		$p['firstname'] = null;
		$p['middlename'] = null;
		$p['lastname'] = null;
		$p['contact']['SCHOOL']['STREET'] = null;
		$p['contact']['SCHOOL']['PHONE'] = null;
		$p['major'] = null;
		$p['permission'] = null;
	}
	else{
		include 'profile-view.inc.php';
	}
}
$template_vars['profile'] = $p;
$template_vars['title'] = "Modify Personal Information";
