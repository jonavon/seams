<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */
$access = new Access($db);
if(isset($_POST['submit'])){
	if(isset($_POST['delete'])){
		$access->delete($_POST['delete']);
	}
	if(!((empty($_POST['start'])) || (ip2long(gethostbyname($_POST['start'])) === false))){
		$_POST['start'] = gethostbyname($_POST['start']);
		$_POST['end'] = (empty($_POST['end']))?$_POST['start']:$_POST['end'];
		$_POST['allow'] = (empty($_POST['allow']))?null:(int)$_POST['allow'];
		$blocks = CIDR::rangeToCIDRList($_POST['start'],$_POST['end']);
		$level = array_reduce($_POST['level'],create_function('$v,$w','return $v += $w;'),0);
		$ips = array();
		foreach($blocks AS $ipblock){
			$route = explode('/',$ipblock);
			$ips[] = array(
					'ip'=>$route[0],
					'cidr'=>(int)$route[1],
					'level'=> $level,
					'allow'=>(int)$_POST['allow']
					);
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => "{$route[0]}/{$route[1]} has been submitted."
					);

		}
		$access->add($ips);
	}
	else {
		trigger_error('Start field does not contain a valid ip address!?', E_USER_WARNING);
	}
}
$template_vars['access'] = array_reverse($access->view(),true);
