<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$p = new Admin(null,$db);
if(isset($objid)) {
	$list = $p->browse();
	$template_vars['profiles'] = array_filter($list,create_function('$item','global $objid; return (strtoupper($objid)===strtoupper($item["type"]));'));
}
else {
	$filter = (isset($_POST['q']))?$_POST['q']:null;
	$list = $p->browse($filter);
	$template_vars['profiles'] = $list;
}
$template_vars['title'] = "Profile List";
