<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}

if(!SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$_SESSION['permission'])){
	trigger_error("You are not permitted to use this resource.", E_USER_ERROR);
	$action = "error";
	$objid = "unauthorized";
}
else {
	$template_vars['title'] = "Admin";
	if(isset($action)) {
		include "admin-$action.inc.php";
	}
}
