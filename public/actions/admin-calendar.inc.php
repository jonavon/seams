<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * File admin-calendar.inc.php
 *
 * Include file for the /admin/calendar page. 
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package seas
 */
$date = time();
$today = date('Y-m-j',time());
$month = date('m',$date);
$year = date('Y',$date);
$firstday = mktime(0,0,0,$month,1,$year);
$caption = date('F Y', $firstday);
$numberDays = date('t',$date);
if(empty($objid) && isset($_POST['uid'])) {
	$objid = $_POST['uid'];
}
require_once 'lib/iCalcreator.class.php';
$ical = new vcalendar();
$ical->setConfig('unique_id','seas.lib.vt.edu');
$ical->setConfig('url',SEAS_CALENDAR);
$ical->parse();
$ical->sort();
if(isset($_POST['submit'])) {
	if(isset($_POST['all-day'])){
		$_POST['start-time'] = null;
		$_POST['end-time'] = null;
	}
	$_POST['end-date'] = (empty($_POST['end-date']))?$_POST['start-date']:$_POST['end-date'];

	if(empty($_POST['uid'])) {
		$postevent = new vevent();
		$_POST['uid'] = null;
	}
	else {
		$postevent = $ical->getComponent($objid);
	}
	$postevent->setProperty('summary',trim($_POST['summary']));
	$postevent->setProperty('dtstart',(("{$_POST['start-date']} {$_POST['start-time']}")),array('VALUE'=>(isset($_POST['all-day']))?'DATE':'DATE-TIME'));
	$postevent->setProperty('dtend',(("{$_POST['end-date']} {$_POST['end-time']}")),array('VALUE'=>(isset($_POST['all-day']))?'DATE':'DATE-TIME'));

	if(isset($_POST['recurrence'])) {
		$recur['FREQ'] = $_POST['freq'];
		$recur['COUNT'] = (!empty($_POST['count']))?$_POST['count']:null;
		$recur['UNTIL'] = (isset($_POST['until']))?$_POST['until']:null;
		$recur['INTERVAL'] = $_POST["recur-". strtolower($_POST['freq'])]['interval'];
		if(isset($_POST['recur-weekly'])) {
			$recur['BYDAY'] = $_POST["recur-". strtolower($_POST['freq'])]['byday'];
		}
		else {
			$recur['BYDAY'] = (!empty($_POST["recur-". strtolower($_POST['freq'])]['byday']))?array($_POST["recur-". strtolower($_POST['freq'])]['byday'], 'DAY' => $_POST["recur-". strtolower($_POST['freq'])]['byday-suffix']):null;
		}
		$recur['BYMONTH'] = (isset($_POST["recur-". strtolower($_POST['freq'])]['bymonth']))?$_POST["recur-". strtolower($_POST['freq'])]['bymonth']:null;
		$recur['BYMONTHDAY'] = (isset($_POST["recur-". strtolower($_POST['freq'])]['bymonthday']))?$_POST["recur-". strtolower($_POST['freq'])]['bymonthday']:null;
		$recur['BYYEARDAY'] = (isset($_POST["recur-". strtolower($_POST['freq'])]['byyearday']))?$_POST["recur-". strtolower($_POST['freq'])]['byyearday']:null;
		$recur = array_filter($recur);
		$postevent->deleteProperty('rrule',1);
		$postevent->setProperty('rrule',$recur);
	}
	$ical->setComponent($postevent, $_POST['uid']);
	$ical->saveCalendar();
}
$details = $ical->selectComponents($year,$month,1,$year,$month,$numberDays,'vevent',true);
$stuff = $ical->components;
$events = array();
foreach($stuff AS $evt) {
	$start = $evt->getProperty('dtstart');
	$end = $evt->getProperty('dtend');
	$events[] = array(
			'uid' => $evt->getProperty('uid'),
			'start'=> strtotime("{$start['year']}-{$start['month']}-{$start['day']} 00:00:00" . date('T')),
			'end'=> strtotime("{$end['year']}-{$end['month']}-{$end['day']} 00:00:00" . date('T')),
			'recurs' => $evt->getProperty('rrule',1,1),
			'summary' => $evt->getProperty('summary')
			);
	unset($start,$end);
}
if(!empty($objid)){
	$vevent = $ical->getComponent($objid);
	if((bool)$vevent){
		$start = $vevent->getProperty('dtstart');
		$end = $vevent->getProperty('dtend');
		$template_vars['vevent'] = array(
				'uid' => $vevent->getProperty('uid'),
				'summary' => $vevent->getProperty('summary'),
				'start'=> strtotime("{$start['year']}-{$start['month']}-{$start['day']} 00:00:00" . date('T')),
				'end'=> strtotime("{$end['year']}-{$end['month']}-{$end['day']} 00:00:00" . date('T')),
				'allday' => (isset($start['hour']))?false:true,
				'rrule' => $vevent->getProperty('rrule')
				);
		unset($start,$end);
	}
	else {
		trigger_error("Unable to get event with ID $objid", E_USER_ERROR);
	}
}
if(isset($_POST['delete'])) {
	if(!empty($_POST['uid'])){
		$ical->deleteComponent($_POST['uid']);
		$ical->saveCalendar();
		$messages[] = array(
				'type' => APP_NOTICE,
				'content' => "Event ID: {$_POST['uid']} has been permanantly deleted."
				);
		$objid = null;
	}
	else {
		$messages[] = array(
				'type' => APP_WARNING,
				'content' => 'An event must be chosen before it can be deleted. Please choose an event below.'
				);
	}
}
$template_vars['events'] = array_reverse($events,true);
$template_vars['title'] = "$objid - Calendar Administration";

