<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$template_vars['title'] = "Application Submitted";
if(isset($_POST['submit'])){
	$a = new Application($db);
	if(empty($_POST['id']) && empty($_SESSION['appid'])) {
		$objid = substr(sha1(md5(uniqid(rand()))),0,8);
		$a->__set('id',$objid);
	}
	else {
		$objid = (empty($_POST['id']))?$_SESSION['appid']:$_POST['id'];
		$a->view($objid);
	}
	$a->__set('pid',$_POST['pid']);
	$expiration = (($hirebegin[0] < $dropday[0]) && ($dropday[0] < $hirebegin[1]))?$dropday[1]:$dropday[0];
	$a->__set('expire',$expiration);


	$properties['workstudy'] = null;
	$properties['citizen'] = null;
	$properties['skills'] = null;
	$properties['hours'] = null;
	$properties['lift50'] = null;
	$properties['transportation'] = null;
	$properties['break_worker'] = null;
	$properties['library_experience'] = null;
	$properties['other_experience'] = null;
	$properties['another_department'] = null;
	$properties['work_reference_0_name'] = null;
	$properties['work_reference_0_phone'] = null;
	$properties['work_reference_0_notes'] = null;
	$properties['work_reference_1_name'] = null;
	$properties['work_reference_1_phone'] = null;
	$properties['work_reference_1_notes'] = null;
	$properties['work_reference_2_name'] = null;
	$properties['work_reference_2_phone'] = null;
	$properties['work_reference_2_notes'] = null;

	$settings = array_intersect_key($_POST, $properties);
	foreach($settings as $field => $value) {
		$value = strip_tags($value);
		try{
			$a->answerQuestion($field,$value);
		}
		catch(Exception $f){
			$messages[] = array(
					'type' => APP_NOTICE,
					'content' => $f->getMessage()
					);
		}
	}

	try {
		if(empty($_POST['id'])) {
			$a->add();
			$content = "Your Employment information was added. It set to expire on" . date('F j, Y', $expiration);
		}
		else {
			$a->edit();
			$content = "Your Employment information was edited. It set to expire on" . date('F j, Y', $expiration);;
		}
		$messages[] = array(
			'type' => APP_NOTICE,
			'content' => $content
		);
		$_SESSION['appid'] = $objid;
		if(isset($_SESSION['newapplicant'])) {
			unset($_SESSION['newapplicant']);
			$_SESSION['newapplicant']['application'] = $_SESSION['appid'];
			include 'schedule-edit.inc.php';
		}
	}
	catch(Exception $e){
		trigger_error($e->getMessage(), E_USER_ERROR);
	}
}
