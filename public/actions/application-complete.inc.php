<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
$messages[]= array(
		'type' => APP_NOTICE,
		'content' => 'You have completed the application process. Please review your information below. Remember to logout when you are done.'
		);
$objid = $_SESSION['appid'];
include 'application-view.inc.php';
$template_vars['title'] = "Finalize Application";

// Send Email to Applicant

$expdate = date('F j, Y',$template_vars['application']['expire']);
$mbody = <<<TEXT
You have completed the application process. You may print the application
{$_seas_config['SEAS']['base']}/application/pdf/{$_SESSION['appid']} . This
application's expiration date is $expdate.

****************************************************************************

**Applications can only be accessed at the locations and times listed in the blue box above.**

To be eligible for student employment in the library:

*   You must be a Virginia Tech student. 
    *   Students "affiliated with Virginia Tech" are not eligible.
*   Students must be enrolled for the following hours: 
    *   Undergraduate students: 6 hours
    *   Graduate students: 5 hours
    *   Summer School Students: 3 hours 
        *   Students not enrolled in Summer but enrolled the previous Spring and following Fall semesters are eligible. However, you will be required to pay social security taxes.
*   Resident Aliens/Visa F-1 holders are restricted to 20 hours a week during the academic year 
    *   Exceptions: official school breaks and summer (unless enrolled in classes, then student is restricted to 20 hours a week).

### Important Information - Please Read:

*   If you are applying for the first time, complete a new student application.
*   If you have already completed an application, and are still interested in library employment, you must update your application at the beginning of every semester.
*   Hiring is done on **as needed throughout the year.**
    *   If a supervisor is interested in hiring you, he or she will contact you directly.
    *   Library Personnel Services (<libwage@vt.edu>) does not have information on what jobs are available. 
*   References and recommendations are not required, but recommended. 
*   Students are hired in the following areas: Circulation, Collection Management, Dean's Office, Special Collections, Digital Library and Archives (DLA), Reference and Instructional Services (RIS), Library Systems, InterLibrary Loan (ILL), Mailroom, Photocopy, Shelving, Technical Services, and at our branches: Art and Architecture Library and Veterinary Medicine Library. 
    *   Pay rates vary with the level of responsibilities of each position.

**Failure to complete any of the required fields (marked with an asterisk *) will result in your application being placed on hold and unavailable to supervisors.**

If you encounter problems or have questions that were not answered above, please e-mail <libwage@vt.edu>
TEXT;

$mail = new Message($_SESSION['pid'],$db,$_SESSION['pid'],'[SEAS] Application Recieved',$mbody);
try{
	$mail->send();
}
catch(Exception $e) {
	trigger_error("System Message: Unable to send message", E_USER_ERROR);
}
