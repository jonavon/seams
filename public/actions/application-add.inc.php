<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
if(empty($_SESSION['appid']) && SEAMS::isPermitted(array(Profile::APPLICANT_LEVEL),$_SESSION['permission'])){
	$_SESSION['newapplicant'] = true;
}
include 'application-edit.inc.php';
$template_vars['title'] = "New Application";
