<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
?>
<?php if(!isset($_SESSION['pid'])): ?>
		<div id="login-form">
			<form method="post" action="<?php echo $_SERVER['REQUEST_URI'];?>">
				<fieldset>
					<legend>SEAS Login</legend>
					<p>
						<label for='username'>VT PID</label>
						<input name='username' type='text' id='username' size='25' maxlength='8' />
					</p>
					<p>
						<label for='password'>Password</label>
						<input name='password' type='password' id='password' size='25' maxlength='25' />
					</p>
					<input name='login' type='submit' value='login' <?php if(isset($sys_error)): if($sys_error):?>disabled="disabled"<?php endif;endif;?> />
				</fieldset>
			</form>
		</div>
<?php else:
if($_SERVER['PATH_INFO'] == '/login'){
	header("Location:" . SEAS_WEB_ROOT . '/profile/view');
}
else {
	header("Location:" . SEAS_WEB_ROOT . $_SERVER['PATH_INFO']);
}
exit;
?>
		<div>
			<p>You are logged in as <a href="./profile/view/<?php echo $USER['pid']; ?>" title="Go to your profile"><?php echo $_SESSION['pid']; ?></a>.</p>
		</div>
<?php endif; ?>
