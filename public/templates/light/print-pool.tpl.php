<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */

?>

		<div id="pool-phonebook">
			<h2>Pool <?php echo $objid; ?> Phonebook</h2>
			<table summary="Pool Phonebook">
				<thead>
					<tr>
						<th>Name</th>
						<th>E-mail</th>
						<th>Phone Numbers</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Name</th>
						<th>E-mail</th>
						<th>Phone Numbers</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($contacts as $contact): ?>
					<tr>
						<td><?php echo "{$contact['firstname']} {$contact['lastname']}"; ?></td>
						<td><a href="mailto:<?php echo $contact['pid']; ?>@vt.edu" title="E-mail"><?php echo $contact['pid']; ?>@vt.edu</a></td>
						<td><?php if(isset($contact['contact']['SCHOOL'])):?>SCHOOL: <?php echo "{$contact['contact']['SCHOOL']['PHONE']}"; ?><?php endif; ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
			<div id="pool-contact-form" class="hide">
				<form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
					<fieldset>
						<legend>E-mail Pool</legend>
						<p>
							<label for="subject">Subject</label>
							<input type="text" id="subject" name="subject" value="" />
						</p>
						<p>
							<label for="body">Message</label>
							<textarea id="body" name="body" rows="20" cols="60"></textarea>
						</p>
					</fieldset>
				</form>
			</div>
		</div>
