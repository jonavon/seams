		<div id="supervisor-page">

		<div class="section applicationS">
<?php include '_simple-search.tpl.php'; ?>
			<h4>All Applicants</h4>
			<ul>
				<li><a href="./application/results" title="Display a list of all applicants">All Applicants (<?php echo $application_count; ?>)</a></li>
			</ul>
		</div>
		<div class="section jobs">
<?php include 'list-job.tpl.php'; ?>
		</div>
		<div class="section forms">
			<h4>Forms</h4>
			<ul>
				<li><a href="./files/wage-form.pdf" title="Wage Appointment Form" rel="external">Wage Appointment Record</a></li>
				<li><a href="./files/emergency-hire.pdf" title="Emergency Hire Form" rel="external">Emergency Hire form</a></li>
				<li><a href="./files/emergency-hire-extension.pdf" title="Emergency Hire Form" rel="external">Emergency Hire Extension Form</a></li>
			</ul>
		</div>

		</div>
