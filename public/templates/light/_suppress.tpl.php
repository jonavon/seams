<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
<?php if(isset($settings['SUPPRESS'])): ?>
		<div id="suppression-table">
			<h4>Suppression List</h4>
			<table summary="List of applications that are suppressed.">
				<thead>
					<tr>
						<th>Application Id</th>
						<th>Name</th>
						<th>Delete</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Application Id</th>
						<th>Name</th>
						<th>Delete</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($settings['SUPPRESS'] as $suppressed): ?>
					<tr>
						<td><a href="./application/view/<?php echo $suppressed['value'];?>" title="Delete this setting"><?php echo $suppressed['value'];?></a></td>
						<td><?php echo $referencelist['SUPPRESS'][(string)$suppressed['value']]['name']; ?></td>
						<td><a href="./setting/delete/<?php echo $suppressed['id'];?>" title="Delete this setting">Unsuppress</a></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>
<?php endif; ?>
