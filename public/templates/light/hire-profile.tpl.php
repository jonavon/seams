<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
} if(isset($hireid)):?>
<a href="./job/view/<?php echo $hireid; ?>">Return to Pool <?php echo $hireid;?></a>

<div id="wage-form">
	<form action="./profile/wage/<?php echo $objid; ?>" method="post" rel="external">
		<fieldset>
			<legend>Wage appointment form</legend>
			<p>
				<label for="appointment">Appointment</label>
				<select id="appointment" name="appointment">
					<option value="">-- Please Select --</option>
					<option value="New" selected="selected">New</option>
					<option value="Additional">Additional</option>
				</select>
			</p>
			<p>
				<label for="appointment-date">Date Appointment will take effect</label>
				<input id="appointment-date" name="appointment-date" type="text" class="required date" />
			</p>
			<p>
				<label for="type">Employment Type</label>
				<select id="type" name="type" class="required">
					<option value="">-- Please Select --</option>
					<option value="Wage">Wage</option>
					<option value="Student Wage" selected="selected">Student Wage</option>
					<option value="Work Study">Work Study</option>
					<option value="Emergency Hire">Emergency Hire</option>
				</select>
			</p>
<?php if(isset($settings['PAYCODE'])): ?>
			<p>
				<label for="paycode">Pay Code</label>
				<select id="paycode" name="paycode" class="required">
					<option value="">-- Please Select --</option>
<?php foreach($settings['PAYCODE'] as $paycode): ?>
					<option value="<?php echo "{$referencelist['PAYCODE'][$paycode['value']]['label']} {$referencelist['PAYCODE'][$paycode['value']]['description']}"; ?>"><?php echo "{$referencelist['PAYCODE'][$paycode['value']]['label']} {$referencelist['PAYCODE'][$paycode['value']]['description']}"; ?></option>
<?php endforeach; ?>
				</select>
			</p>
<?php endif; ?>
			<p>
				<label for="pay">Pay Rate per Dollar</label>
				<input id="pay" name="pay" type="text" class="required currency_nosymbol" value="0.00" />
			</p>
			<input type="submit" value="submit" />
		</fieldset>
	</form>
</div>
<?php endif;?>
