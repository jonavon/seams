<h1 id="privacystatement">Privacy Statement</h1>

<p><strong>Site:</strong>   Student Employee Application System <br />
<strong>URL:</strong>    <a href="https://seas.lib.vt.edu/">https://seas.lib.vt.edu/</a></p>

<p>Virginia Tech has created this privacy statement in order demonstrate our
firm commitment to privacy. The following discloses the information gathering
and dissemination practices for this web site.</p>

<h2 id="informationautomaticallylogged">Information Automatically Logged</h2>

<p>This site uses your IP address to help diagnose problems with our server and
to administer our site.</p>

<p>This site records the browser type to monitor market penetration of various
web browsers so we can better determine the Internet technologies we may
utilize in the design of our pages.</p>

<p>This site uses page referrer data - that is, information about the page that
linked you to this page - to determine to what extent our page is referenced
by other resources on the Web.</p>

<p>This data may be used to preserve the integrity of our computing resources.</p>

<h2 id="personalinformation">Personal Information</h2>

<p>This site collects personnel and demographic information.</p>

<p>We use the personal information to provide service related to employment.
Additional demographic information is obtained for statistical purposes only.</p>

<p>Certain portions of you personal information is shared only with people who
are directly involved in the hiring process. Other information that will be
aggregated and compiled for statistics will be clearly marked. Statistical
information is only used for reporting and improving website services.</p>

<p>Certain portions of the information requested will be marked as required.
Otherwise, it is optional.</p>

<p>Please login to update your personal information.</p>

<h2 id="cookies">Cookies</h2>

<p>This site uses cookies for session management. See the Session Management
section for more information about how cookies are used.</p>

<h2 id="sessionmanagement">Session Management</h2>

<p>This site uses session management to improve the user experience.</p>

<p>Session management may use the IP address and browser USER AGENT string to
uniquely identify the session.</p>

<p>The session information used by this site is deleted when your browser is
closed.</p>

<h2 id="security">Security</h2>

<p>This site has security measures in place to protect the loss, misuse, and
alteration of the information under our control. Log file access is
restricted to system administrators while stored on the server. Log files are
rotated regularly and archived in a secure location.</p>

<p>User and password information is encrypted before it is transmitted across
the network.</p>

<p>Users should also consult Virginia Tech&#8217;s policy on Acceptable Use.
<a href="http://www.vt.edu/admin/policies/acceptuseguide.html">http://www.vt.edu/admin/policies/acceptuseguide.html</a></p>

<p>Virginia Tech complies with all statutory and legal requirements with respect
to access to information.
Contact Information</p>

<p>If you have any questions about this privacy statement, the practices of this
site, or your dealings with this site, you can contact <a href="&#x6D;&#x61;&#x69;&#x6C;t&#111;:&#108;&#x69;&#98;&#x77;&#x61;&#x67;&#101;&#64;&#118;&#x74;&#x2E;&#101;du">&#108;&#x69;&#98;&#x77;&#x61;&#x67;&#101;&#64;&#118;&#x74;&#x2E;&#101;du</a></p>
