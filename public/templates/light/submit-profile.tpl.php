<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
<div id="profile-submission">
</div>

<?php
if(($_SESSION['newapplicant']['profile'])) {
	include 'add-application.tpl.php';
}
else {
	if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$_SESSION['permission'])){
		include 'list-profile.tpl.php';
	}
}
?>
