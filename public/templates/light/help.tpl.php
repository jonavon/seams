<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
		<div id="help-doc">
			<h1>Help</h1>
			<div id="help-general">
				<h2>General</h2>
				<p>SEAS, Student Employee Application System, is a special purpose application. Built internally for the personnel department of the Virginia Tech Library System.</p>
				<p>The SEAS system is meant to be a simple method for applicants to provide the best and most complete information about themselves while allowing supervisors to select the best candidates for positions. This is meant to improve on already established resources like the Virginia Tech Job Application System and is a great improvement over hard copy files and paper.</p>
			</div>
			<div id="help-start">
				<h2>Getting Started</h2>
				<p>In order to use this system you will have to meet very simple conditions. You must be affiliated with Virginia Tech. You also must prove your affiliation by providing Your PID/Password combination at the <a href="./login" title="Login to the seas system">login</a> screen.</p>
				<div id="help-login">
					<h3>Logging In</h3>
					<p>Any person affiliated with Virginia Tech May be able to log into the system. If you are not already assigned a role within SEAS then it will be assumed that you are attempting to fill out an application and will be directed appropriately. If you believe that you were directed to an incorrect area please contact the <a href="mailto:<?php echo SEAS_ADMIN_EMAIL; ?>" title="SEAS Admin">SEAS Administrator</a></p>
				</div>
				<div id="help-dashboard">
					<h3>Dashboard</h3>
					<p>Once a user has logged in the user will be presented with a start page that will offer some of the most used tools for their role.</p>
				</div>
			</div>
			<div id="help-supervisor">
				<h2>Getting Started as a Supervisor</h2>
				<p>When a user is given the role of supervisor that user may view and search for Applicant information. A supervisor has the ability to do several functions.</p>
				<ul>
					<li>Search for applications</li>
					<li>Create and Edit Jobs</li>
					<li>Pool Applicants</li>
					<li>Suppress Applications</li>
					<li>Hire (Tentatively) Applicants</li>
					<li>Send Messages to Applicants</li>
				</ul>
				<div id="help-search-applications">
					<h3>Search for applications</h3>
					<p>This system offers powerful methods in which to find the best candidate for student positions. Though, there are limitations built into the system to help alleviate bias. With these tools the supervisor will be able hire with SEAS being transparent in the process. There are four related methods to search application content.</p>
					<dl>
						<dt>Simple Search</dt>
						<dd>Takes any alphanumeric input and searches all text within the application.</dd>

						<dt>Schedule Search</dt>
						<dd>Using this system tool a schedule may be created and searched against.</dd>

						<dt>Advanced Search</dt>
						<dd>Using this search form will allow for more filtering of applications.</dd>

						<dt>Skill Search</dt>
						<dd>Uses links or &quot;tags&quot; to list applications that have entered that particular tag.</dd>
					</dl>
					<div id="help-search-simple">
						<h4>Simple Search</h4>
						<p>Using Simple Search type a word or words to see if they exist in the applications.</p>
					</div>
					<div id="help-search-schedule">
						<h4>Schedule Search</h4>
						<p>The schedule search is a new and revolutionary method with which to find applicants that fit your schedule needs. It should be noted that the way you input schedule search is very similar to how the applicant has input their schedule.</p>
						<dl>
							<dt>First Look</dt>
							<dd>There are nine inputs with which to build your schedule. The first input, labeled &quot;start&quot;, should be filled with the nearest hour to be used as a guide. Likewise the second, labeled &quot;ending&quot;, is treated in the same manner as start. Additionally there are seven checkboxes one for each day of the week.</dd>

							<dt>Enter Shift</dt>
							<dd>The start input should be given a start time that you would like to search against. Likewise for end time. Note that end time should not be less than the start time. After that you must select each day that the shift fits into. For example if you would like to enter this shift: 11am to 3pm Monday, Thursday; Then start = 11:00 am, ending = 3:00 pm, Mon = checked, Thu = checked. To submit the form click &quot;Enter Shift&quot; once. The form should return a little differently, but should allow you to enter multiple shifts.  By entering a new start and end time and selecting the day(s) you want that set of hours to be on you can choose different schedules for different days of the week.</dd>
							
							<dt>Search Application Stack</dt>
							<dd>You may examine the results of all your entries below the form. If it is complete you will have the option to &quot;Search Application Stack.&quot; By clicking this button you will be searching using the schedule you created and comparing it to the most compatible schedules in the system.</dd>
							<dd>The results themselves are context sensitive. The results should list applications that are sorted by the overlap column. You may examine each schedule and pick the application that best fits your needs. For more information see the <a href="./help#help-search-results" title="Search Result Section">results</a> section.</dd>

							<dt>Clear Times</dt>
							<dd>The schedule that was created will stay in the system until the session ends. In order to edit or clear it out you must click on the button that says, &quot;Clear Times.&quot;</dd>
						</dl>
					</div>
					<div id="help-search-advanced">
						<h4>Advanced Search</h4>
						<p>Advanced search allows you to provide more fine grained filtering of applications. Using the fields provided you combine searches to limit by the schedule, text of the application, or by yes/no questions.</p>
					</div>
					<div id="help-search-skills">
						<h4>Skill Search</h4>
						<p>This search is a special tag cloud for each skill listed on the student application. Here the largest quantity of applicants are larger in text size and decrease in text size proportionate to the number of applicants listing the skill.  This search is unique in that you can save the URL and return at your leisure.</p>
					</div>
				</div>
				<div id="help-jobs">
					<h3>Create and Edit Jobs</h3>
					<p>Users of the older system can think of Jobs as a named pool. By default a supervisor may have a pool that is labeled &quot;STUDENT WORKER.&quot; For a supervisor and for the purposes of this help a &quot;pool&quot; may be used interchangeably with &quot;job.&quot;</p>
				</div>
				<div id="help-pool-applicants">
					<h3>Pool Applicants</h3>
					<p>More on this later...</p>
					<div id="help-search-results">
						<h4>Search Results</h4>
						<p>More on this later...</p>
					</div>
					<div id="help-submit-pool">
						<h4>Pool</h4>
						<p>More on this later...</p>
					</div>
					<div id="help-view-applications">
						<h4>Viewing Applications</h4>
						<p>More on this later...</p>
						<div id="help-print">
							<h5>Print Applications</h5>
							<p>More on this later...</p>
						</div>
						<div id="help-suppress">
							<h5>Suppress Applications</h5>
							<p>More on this later...</p>
						</div>
				</div>
				<div id="help-hire">
					<h3>Hire Applicants</h3>
					<p>More on this later...</p>
				</div>
				<div id="help-messages">
					<h3>Send Messages</h3>
					<p>More on this later...</p>
				</div>
			</div>
			<div id="help-appendix">
				<h2>Appendix</h2>
				<div id="help-markdown">
					<h3>Markdown Syntax</h3>
					<p>Many of the text fields including messages allow for markdown syntax. A more complete <a href="http://daringfireball.net/projects/markdown/syntax" title="Markdown Syntax Documentation" rel="external">overview</a> can be found at the style developer&apos;s website, <a href="http://daringfireball.net/projects/markdown/" title="Markdown Homepage" rel="external">Daring Fireball</a>. A cheatsheet should be available on the pages that are able to use the syntax.</p>
				</div>
			</div>
		</div>
