<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
date_default_timezone_set('America/New_York');
header("Expires: " . gmdate("D, d M Y H:i:s", time() + (SESSION_LIMIT * 60)) . " GMT");
header('Pragma: SEAS');
header("Cache-Control:max-age=" . (SESSION_LIMIT * 60));

$object = (isset($object))?$object:"index";
$action = (isset($action))?$action:null;
$tpl = (isset($action))?"$action-":null;
$tpl .= $object;
$tpl = (substr($tpl,0,1) != "_")?$tpl:null;

$template_header = null;
$template_content = null;
$template_script = null;
if(file_exists(dirname(__FILE__) . DIRECTORY_SEPARATOR . "$tpl.hdr.php")){
        $template_header = "$tpl.hdr.php";
}
if(file_exists(dirname(__FILE__) . DIRECTORY_SEPARATOR . "$tpl.script.php")){
        $template_script = "$tpl.script.php";
}
if(file_exists(dirname(__FILE__) . DIRECTORY_SEPARATOR . "$tpl.tpl.php")){
        $template_content = "$tpl.tpl.php";
}
else{
        header("HTTP/1.0 404 Not Found");
        $messages[] = array(
                'type' => APP_ERROR,
                'content'=> "Resource {$_SERVER['REQUEST_URI']} is not available."
        );
}

$_SESSION['appid'] = (isset($_SESSION['appid']))?$_SESSION['appid']:null;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<!-- BEGIN HEAD -->
<head>
	<title><?php echo $title; ?> | University Libraries | Virginia Tech</title>
	<base href="<?php echo $base_url; ?>" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<link rel="Shortcut Icon" href="./favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="./<?php echo $template_dir; ?>/assets/libs/css/jquery.ui.themes/ui.all.css" title="default" type="text/css" media="screen,handheld" />
	<link rel="stylesheet" href="./<?php echo $template_dir; ?>/assets/libs/js/jquery-tooltip/jquery.tooltip.css" title="default" type="text/css" media="screen,handheld" />
	<link rel="stylesheet" href="./<?php echo $template_dir; ?>/assets/css/style.css" title="default" type="text/css" media="screen,handheld" />
	<link rel="stylesheet" href="./<?php echo $template_dir; ?>/assets/libs/css/anytimec.css" type="text/css" media="screen,handheld" />
	<link rel="stylesheet" href="./<?php echo $template_dir; ?>/assets/css/print.css" type="text/css" media="print" />
<!--[if IE]><link rel="stylesheet" href="./<?php echo $template_dir; ?>/assets/css/ie.css" type="text/css" media="all" /><![endif]-->
<!--[if IE 6]><link rel="stylesheet" href="./<?php echo $template_dir; ?>/assets/css/ie6.css" type="text/css" media="all" /><![endif]-->

<?php if(isset($template_header)){include $template_header;} ?>
</head>
<body id="<?php echo $tpl; ?>">
<div id="container">
	<div id="header">
		<h1>Student Employee Application System</h1>
		<h2><?php echo $title; ?><?php if(SEAMS::isPermitted(array(Profile::SUPERVISOR_LEVEL),$permission)): ?> - Supervisor<?php endif;?><?php if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission)): ?> - Administrator<?php endif;?></h2>
		<div class="pipelist">
		<ul>
<?php if(isset($_SESSION['pid'])): ?>
			<li><a href="./profile/settings/<?php echo $_SESSION['pid']; ?>" title="Look at your settings.">Settings</a></li>
			<li><a href="./help" title="Help for this system">Help</a></li>
<?php if(isset($_SESSION['newapplicant'])):?>
			<li>Welcome <?php echo $_SESSION['pid']; ?></li>
<?php else: ?>
			<li>Welcome <a href="./profile/view/<?php echo $_SESSION['pid']; ?>" title="Go to your profile"><?php echo $_SESSION['pid']; ?></a>.</li>
<?php endif; ?>
<?php else: ?>
			<li><a href="./login" title="Login to your profile">Login</a></li>
<?php endif; ?>
		</ul>
		</div>
<?php if(isset($_SESSION['newapplicant']) && isset($_SESSION['pid'])):
	include '_newapplicant.tpl.php'; ?>
<?php endif; ?>
	</div>
<?php if(isset($_SESSION['pid'])): ?>
	<div id="navigation">
		<ul>
			<li>
				<a href="./" title=""<?php if($tpl === 'index'):?> class="active"<?php endif;?>>SEAS</a>
			</li>
<?php if(!isset($_SESSION['newapplicant'])):?>
			<li>
				<a href="./profile/view" title=""<?php if($object === 'profile' && $action !== 'list'):?> class="active"<?php endif;?>>Home</a>
			</li>
<?php endif; ?>
<?php  if((bool)$_SESSION['appid']):?>
			<li>
				<a href="./application/view/<?php echo $_SESSION['appid'];?>" title="View application"<?php if($object === 'application' && $action !== 'results'):?> class="active"<?php endif;?>>Employment Information</a>
			</li>
<?php else: ?>
<?php if(SEAMS::isPermitted(array(Profile::APPLICANT_LEVEL), $_SESSION['permission'])):?>
			<li>
				<a href="./application/add" title="Add an Application">Create An Application</a>
			</li>
<?php endif; ?>
<?php endif; ?>
<?php if(SEAMS::isPermitted(array(Profile::SUPERVISOR_LEVEL, Profile::ADMIN_LEVEL),$permission)): ?>
			<li>
				<a href="./search" title="Search for employees"<?php if($action === 'search' || $object === 'search' || $action === 'results'):?> class="active"<?php endif;?>>Search</a>
			</li>
			<li>
				<a href="./job/list" title="Setup pools"<?php if($object === 'job'):?> class="active"<?php endif;?>>Pools</a>
			</li>
<?php endif; ?>
			<li>
				<a href="./calendar" title=""<?php if($object === 'calendar'):?> class="active"<?php endif;?>>Calendar</a>
			</li>
<?php if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission)): ?>
			<li>
				<a href="./admin" title=""<?php if($object === 'enum' || $action === 'list'):?> class="active"<?php endif;?>>Admin</a>
			</li>
<?php endif; ?>
			<li id="logout">
				<a href="./logout" title=""<?php if($object === 'logout'):?> class="active"<?php endif;?>>Logout</a>
			</li>
		</ul>
	</div>
<?php endif; ?>
	<div id="main">
<?php if(isset($messages)): ?>								
		<div id="messages">
<?php foreach($messages as $message): ?>
			<p class="<?php echo $message['type']; ?>"><?php echo $message['content']; ?></p>
<?php endforeach; ?>
		</div>
<?php endif; ?>
		<div id="content">
<?php include $template_content; ?>
		</div>
	</div>
	<div id="footer">
		<p>Copyright &copy; 2001-2009 University Libraries, Virginia Tech</p>
		<div class="pipelist">
			<ul>
				<li>About</li>
				<li><a href="./privacy" title="about">Privacy</a></li>
				<li>Contact</li>
			</ul>
		</div>
	</div>
</div>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.searchable.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/anytimec.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/js/seas.js"></script>
	<script type="text/javascript">
		<!--<![CDATA[-->
		$(document).ready(function(){
			session_limit = <?php echo SESSION_LIMIT * 1000 * 60; ?>;
			setTimeout('location.reload(true)', session_limit);
		});
		<!--]]>-->
	</script>
<?php if(isset($template_script)){include $template_script;} ?>
</body>
</html>
