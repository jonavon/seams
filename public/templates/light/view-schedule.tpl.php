<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
/*
$ranges = array(
		array(
			'beginning' => '10700',
			'ending' => '11200'
			),
		array(
			'beginning' => '21200',
			'ending' => '22000'
			),
		array(
			'beginning' => '30830',
			'ending' => '30900'
			),
		array(
			'beginning' => '31900',
			'ending' => '32100'
			),
		array(
			'beginning' => '40855',
			'ending' => '41030'
			),
		array(
				'beginning' => '41500',
				'ending' => '41800'
				),
		array(
				'beginning' => '51500',
				'ending' => '51800'
				),
		array(
				'beginning' => '61100',
				'ending' => '62300'
				));
*/
if(isset($ranges)) {
	$availabilityTable = formatRanges($ranges);
}


///////////////
// Functions //
///////////////
/**
 * timeWithin 
 * 
 * @param mixed $rangeEvent 
 * @param Array $availbility 
 * @param mixed $incrementfifteen 
 * @access public
 * @return boolean
 */
function timeWithin($rangeEvent,Array $availbility, &$incrementfifteen = null) {
	if(preg_match('/^\d{3}(00|15|30|45)$/',$rangeEvent) == 1) {	
		$dayHour = substr($rangeEvent,0,3);
		$minuteofthehour = substr($rangeEvent,3,2);
		$plusfifteen = (int)($minuteofthehour) + 15;
		
		foreach($availbility as $rang){
			$sdh = substr($rang['beginning'],0,3); // Start Day Hour
			$sh = substr($rang['beginning'],1,2); // Start Hour
			$sdm = substr($rang['beginning'],3,2); // Start Minutes
			$edh = substr($rang['ending'],0,3); // End Day Hour
			$eh = substr($rang['ending'],1,2); // End Hour
			$edm = substr($rang['ending'],3,2); // End Minutes
			if((((int)($rangeEvent)) >= ((int)($rang['beginning']))) && (((int)($rangeEvent)) < ((int)($rang['ending'])))) {
				return true;
			}
		}
	}
	else {
		throw new Exception("String input $rangeEvent should be a 5 digit string only. With the last two character representing 15 minute increments of an hour. 00,15,30,45");
	}
	return false;
}

/**
 * timeStart 
 * 
 * @param mixed $rangeEvent 
 * @param Array $availbility 
 * @param mixed $incrementfifteen 
 * @access public
 * @return void
 */
function timeStart($rangeEvent,Array $availbility, &$incrementfifteen = null) {
	if(preg_match('/^\d{3}(00|15|30|45)$/',$rangeEvent) == 1) {	
		$dayHour = substr($rangeEvent,0,3);
		$minuteofthehour = substr($rangeEvent,3,2);
		$plusfifteen = (int)($minuteofthehour) + 15;
		
		foreach($availbility as $rang){
			$sdh = substr($rang['beginning'],0,3); // Start Day Hour
			$sh = substr($rang['beginning'],1,2); // Start Hour
			$sdm = substr($rang['beginning'],3,2); // Start Minutes
			$edh = substr($rang['ending'],0,3); // End Day Hour
			$eh = substr($rang['ending'],1,2); // End Hour
			$edm = substr($rang['ending'],3,2); // End Minutes
			// Check start time of each to see if it is within fifteen of start or end 
			if($dayHour == $sdh){
				if( ( ((int)($sdm)) >= ((int)($minuteofthehour)) ) && ( ((int)($sdm)) < $plusfifteen )) {
					$incrementfifteen = ceil(((mktime((int)$eh,(int)$edm) - mktime((int)$sh,(int)$sdm))/60)/15);
					return true;
				}
			}		
		}
	}
	else {
		throw new Exception("String input $rangeEvent should be a 5 digit string only. With the last two character representing 15 minute increments of an hour. 00,15,30,45");
	}
	return false;
}

/**
 * extremeHours 
 * 
 * @param Array $rng 
 * @access public
 * @return void
 */
function extremeHours(Array $rng) {
	$top = null;
	$bottom = null;
	foreach($rng as $hour) {
		$top = (!isset($top))?(int)(substr(($hour['beginning']),1,2)):$top;
		$bottom = (!isset($bottom))?(int) (substr(($hour['ending']),1,2)):$bottom;

		$top = ((substr($hour['beginning'],1,2)) < $top)?(int) substr($hour['beginning'],1,2):$top;
		$bottom = ((int)(substr($hour['ending'],1,2)) > $bottom)?(int) substr($hour['ending'],1,2):$bottom;
	}
	$extremes = array(
		'top' => $top,
		'bottom' => $bottom
	);
	return $extremes;
}
function formatRanges(Array $timeranges) {
	$extremes = extremeHours($timeranges);
	$latest = $extremes['bottom'];
	$earliest = $extremes['top'];
	$info = array();
	for($i=0;$i < 24;$i++){
		if(($latest >= $i) && ($i >= $earliest)){
			for($j=0;$j < 4;$j++){
				$t = date('H:i',mktime($i,$j*15));
				$info[$t]['th'] = ($j==0)?true:false;
				for($k=0;$k < 7;$k++){
					$timeid = $k . sprintf('%02d',$i) . sprintf('%02d',($j * 15));	
					$timetitle = date('l',strtotime("+$k day",strtotime("last Sunday"))) ." ". date('g:i a', mktime($i,($j * 15)));
					$rowspan;
					if(timeStart($timeid,$timeranges,&$rowspan)){
						$info[$t]['cells'][] = array(
							'class' => 'available',
							'rowspan' => $rowspan,
							'id' => "a" . $timeid,
							'title' => $timetitle
						);
					}
					elseif(!timeWithin($timeid,$timeranges)){
						$info[$t]['cells'][] = array(
							'id' => "a" . $timeid,
							'title' => $timetitle
						);
					}
				}
			}
		}
	}
	return $info;
}
?>
<?php if(isset($ranges)): ?>
					<div id="schedule-display">
<?php if(isset($availabilitytext)): ?>
<?php if($action != 'view' && $action != 'complete'  ): ?>
					<form method="post" action="">
						<fieldset>
						<legend>Availability Entries</legend>
<?php endif; ?>
						<table summary="List of available times">
							<caption>Availability Times
<?php if(isset($_SESSION['schedulesearch']) && $action == 'search'): ?>
								<input type="submit" name="clear" value="Clear Times" />
<?php endif; ?>
							</caption>
							<thead>
								<tr>
<?php if($action != 'view' && $action != 'complete' && $action != 'search'):  ?>
									<th></th>
<?php endif; ?>
									<th>Day</th>
									<th>Start</th>
									<th>End</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
<?php if($action != 'view' && $action != 'complete' && $action != 'search'):  ?>
									<th></th>
<?php endif; ?>
									<th>Day</th>
									<th>Start</th>
									<th>End</th>
								</tr>
							</tfoot>
							<tbody>
<?php foreach($availabilitytext as $timeunit): ?>
								<tr>
<?php if($action != 'view' && $action != 'complete' && $action != 'search'):  ?>
									<td><input type="checkbox" name="deletion&#91;&#93;" value="<?php echo $timeunit->id; ?>" /></td>
<?php endif; ?>
									<td><?php echo $timeunit->dayoftheweek; ?></td>
									<td><?php echo $timeunit->starttime; ?></td>
									<td><?php echo $timeunit->endtime; ?></td>
								</tr>
<?php endforeach; ?>
							</tbody>
						</table>
<?php if($action != 'view' && $action != 'complete' && $action != 'search'):  ?>
						<input type="submit" name="delete" value="delete" />
						</fieldset>
					</form>
<?php endif; ?>
<?php endif; ?>
					<table class="week" summary="Week Display">
						<caption>Weekly Availability Display</caption>
						<colgroup>
							<col class="hour" />
							<col class="Sun" />
							<col class="Mon" />
							<col class="Tue" />
							<col class="Wed" />
							<col class="Thu" />
							<col class="Fri" />
							<col class="Sat" />
						</colgroup>
						<thead>
							<tr>
								<th>Time</th>
								<th>Sun</th>
								<th>Mon</th>
								<th>Tue</th>
								<th>Wed</th>
								<th>Thu</th>
								<th>Fri</th>
								<th>Sat</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>Time</th>
								<th>Sunday</th>
								<th>Monday</th>
								<th>Tuesday</th>
								<th>Wednesday</th>
								<th>Thursday</th>
								<th>Friday</th>
								<th>Saturday</th>
							</tr>
						</tfoot>
						<tbody>
<?php foreach($availabilityTable as $key=>$quarterhour): ?>
							<tr<?php if($quarterhour['th']): ?> class="hourincrement"<?php endif; ?>>
<?php if($quarterhour['th']): ?>
								<th rowspan="4"><?php echo date('g:i a',strtotime($key)); ?></th>
<?php endif; ?>
<?php if(isset($quarterhour['cells'])):?>
<?php foreach($quarterhour['cells'] as $cell): ?>
								<td id="<?php echo $cell['id']; ?>" title="<?php echo $cell['title']; ?>"<?php if(isset($cell['class'])): ?> class="<?php echo $cell['class']; ?>"<?php endif; ?><?php if(isset($cell['rowspan'])): ?> rowspan="<?php echo $cell['rowspan']; ?>"<?php endif; ?>><?php echo (isset($cell['content']))?$cell['content']:"&nbsp;"; ?></td>
<?php endforeach; ?>
<?php else: ?>
								<td></td>
<?php endif; ?>

							</tr>
<?php endforeach; ?>
						</tbody>
					</table>
					</div>
<?php endif; ?>
