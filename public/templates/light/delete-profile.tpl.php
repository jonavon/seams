<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
<div class="undo">
	<p>Would you like to <a href="./profile/restore/<?php echo $objid; ?>" title="Restore this Profile">restore</a> this profile?</p>
	<p><a class="button" href="./profile/purge/<?php echo $objid; ?>" title="Permanantly Delete this Profile"><span>Permanantly Delete</span></a><p>
</div>
