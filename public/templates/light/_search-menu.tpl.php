<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
			<ul class="nav">
				<li<?php if($object === 'search'):?> class="current"<?php endif;?>>
					<a href="./search" title="Quick Search of the Application Stack">Quick Search</a>
				</li>
				<li<?php if($object === 'schedule'):?> class="current"<?php endif;?>>
					<a href="./schedule/search" title="Search Schedules">Schedule Search</a>
				</li>
				<li<?php if(($object === 'application') && ($action != 'results')):?> class="current"<?php endif;?>>
					<a href="./application/search" title="Search for employees">Advanced Search</a>
				</li>
				<li<?php if($object === 'skills'):?> class="current"<?php endif;?>>
					<a href="./skills/search" title="Search for employees">Skill Set Search</a>
				</li>
<?php if($action === 'results'):?>
				<li class="current">
					<a href="./skills/search" title="Search for employees">Results</a>
				</li>
<?php endif; ?>
			</ul>
