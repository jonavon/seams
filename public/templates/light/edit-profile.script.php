<?php
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.metadata.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.validate.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/additional-methods.js"></script>
	<script type="text/javascript">
	//<![CDATA[
		$(document).ready(function(){
			$("form").validate();
			$("#firstname").focus();
			$("#type").blur(function(){
				if($(this).val()=='SUPERVISOR' || $(this).val()=='ADMIN'){
					$("#stat-info").remove();
				}
			});
			var atleast17 = new Date( Date.now() - (1000 * 60 * 60 * 24 * 365 * 17) ); 
			var dateConv = new AnyTime.Converter({format:"%M %e, %Z"});
			$("#birthday").AnyTime_noPicker();
			$("#birthday").AnyTime_picker({
				format: "%M %e, %Z",
				latest: dateConv.format(atleast17)
			});

		});
	//]]>
	</script>
