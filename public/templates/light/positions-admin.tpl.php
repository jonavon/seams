<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
		<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
		</div>
		<div id="<?php echo (isset($objid))?$objid:'positions'; ?>-list" class="concol">
			<form action=".<?php echo $_SERVER['PATH_INFO'];?>" method="post">
			<div>
			<table class="searchable" summary="List of profiles">	
				<caption><?php echo (isset($objid))?$objid:'positions'; ?> List (<?php echo count($positions); ?>)</caption>
				<thead>
					<tr>
						<th>Job</th>
						<th>Employee Name</th>
						<th>Supervisor</th>
						<th>Paycode</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Job</th>
						<th>Employee Name</th>
						<th>Supervisor</th>
						<th>Paycode</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($positions as $position): ?>				
					<tr id="position<?php echo $position['id'];?>">
						<td><a href="./job/view/<?php echo $position['job']; ?>"><?php echo $position['job_title']; ?></a></td>
						<td><a href="./application/view/<?php echo key($peeps[$position['employee']]->applications);?>" title="application"><?php echo $position['employee_name']; ?></a></td>
						<td><?php echo $position['supervisor_name']; ?></td>
						<td>
<?php if(empty($position['paycode'])): ?>
							<select name="position&#91;<?php echo $position['id'];?>&#93;&#91;paycode&#93;">
								<option value="">-- Empty --</option>
<?php foreach($paycodes as $paycode): ?>
								<option value="<?php echo $paycode['id']; ?>"><?php echo $paycode['label']; ?> <?php echo htmlspecialchars($paycode['description']); ?></option>

<?php endforeach; ?>
							</select>
<?php else: ?>
<?php echo "{$position['paycode_label']}"; ?><a href="./position/delete/<?php echo $position['id'];?>" title="Remove this position">[terminate]</a>
<?php endif; ?>
						</td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
			<input type="submit" id="submit" name="submit" value="submit" />
			</div>
			</form>
		</div>
