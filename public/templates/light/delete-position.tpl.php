<div class="delete">
	<p><a class="button" href="./admin/positions/employee" title="Cancel this action">No, No, No! I do not want.</a></p>
	<p>You would like to delete position <?php echo $objid; ?>. This action cannot be undone.</p>
	<p><a href="./position/remove/<?php echo $objid; ?>" title="purge this position">Yes, I want to remove this position</a></p>
</div>
