<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
		<div id="new-applicant">
			<ol>
				<li class="<?php if(($tpl==='add-profile') || ($tpl==='edit-profile')): ?>current<?php endif; ?> <?php if($_SESSION['newapplicant']['profile']): ?>done<?php endif; ?>"><a href="<?php if($_SESSION['newapplicant']['profile']): ?>./profile/edit/<?php echo $_SESSION['pid']; ?><?php else:?>./login#<?php endif;?>" title="Personal Information">Personal Information</a></li>
				<li class="<?php if(($tpl==='submit-profile') || ($tpl==='edit-application') || ($tpl==='add-application')): ?>current<?php endif; ?> <?php if($_SESSION['newapplicant']['application']): ?>done<?php endif; ?>"><?php if($_SESSION['newapplicant']['profile']): ?><a href="<?php if($_SESSION['newapplicant']['application']): ?>./application/edit/<?php echo $_SESSION['appid']; ?><?php else:?>./application/add<?php endif;?>" class="pointer" title="Employment Information">Employment Information</a><?php else: ?><a href="<?php echo $_SERVER['PATH_INFO'];?>">Employment Information</a><?php endif;?></li>
				<li class="<?php if(($tpl==='submit-application') || ($tpl==='edit-schedule')): ?>current<?php endif; ?> <?php if($_SESSION['newapplicant']['schedule']): ?>done<?php endif; ?>"><?php if($_SESSION['newapplicant']['application']): ?><a href="<?php if($_SESSION['newapplicant']['schedule']): ?>./schedule/edit/<?php echo $_SESSION['appid']; ?><?php else:?>#<?php endif;?>" title="Availability">Schedule Availability</a><?php else: ?><a href="<?php echo $_SERVER['PATH_INFO'];?>">Schedule Availability</a><?php endif;?></li>
				<li class="<?php if($tpl==='complete-application'): ?>current<?php endif; ?> <?php if($_SESSION['newapplicant']['schedule']): ?>done<?php endif; ?>"><?php if($_SESSION['newapplicant']['schedule']):?><a href="#" title="Complete Application">Finished</a><?php else: ?><a href="<?php echo $_SERVER['PATH_INFO'];?>">The End</a><?php endif;?></li>
			</ol>
		</div>
