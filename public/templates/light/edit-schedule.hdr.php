<?php
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

?>
	<link rel="stylesheet" media="screen" href="./<?php echo $template_dir; ?>/assets/libs/css/jquery.timepickr.css" type="text/css" />
	<link rel="stylesheet" media="screen" href="./<?php echo $template_dir; ?>/assets/libs/css/smoothness/jquery-ui-1.7.1.custom.css" type="text/css" />
