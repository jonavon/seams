<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */

?>
		<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
		</div>
		<div id="network-management" class="concol">
			<form method="post" action="./admin/network">
				<fieldset>
					<legend>Access Form</legend>
					<p>
						<label for="start" class="ipaddress">Start IP</label>
						<input type="text" name="start" id="start" value=".lib.vt.edu" />
					</p>
					<p>
						<label for="end" class="ipaddress">End IP</label>
						<input type="text" name="end" id="end" value="" />
					</p>
					<p>
						<label for="allow" class="inline">Allow Access</label>
						<input type="checkbox" name="allow" id="allow" class="inline" value="1" checked="checked" />
					</p>
					<fieldset>
						<legend>Profile Types</legend>
						<p>
							<input type="checkbox" id="admin" name="level&#91;&#93;" class="inline" value="<?php echo Profile::ADMIN_LEVEL;?>" />
							<label for="admin" class="inline">Administrators</label>
						</p>
						<p>
							<input type="checkbox" id="plus" name="level&#91;&#93;" class="inline" value="<?php echo Profile::PLUS_LEVEL;?>" />
							<label for="plus" class="inline">Supervisor Plus</label>
						</p>
						<p>
							<input type="checkbox" id="supervisor" name="level&#91;&#93;" class="inline" value="<?php echo Profile::SUPERVISOR_LEVEL;?>" />
							<label for="supervisor" class="inline">Supervisor</label>
						</p>
						<p>
							<input type="checkbox" id="employee" name="level&#91;&#93;" class="inline" value="<?php echo Profile::EMPLOYEE_LEVEL;?>" />
							<label for="employee" class="inline">Employee</label>
						</p>
						<p>
							<input type="checkbox" id="pending" name="level&#91;&#93;" class="inline" value="<?php echo Profile::PENDING_LEVEL;?>" />
							<label for="pending" class="inline">Pending</label>
						</p>
						<p>
							<input type="checkbox" id="applicant" name="level&#91;&#93;" class="inline" value="<?php echo Profile::APPLICANT_LEVEL;?>" />
							<label for="applicant" class="inline">Applicant</label>
						</p>
						<p>
							<input type="checkbox" id="new" name="level&#91;&#93;" class="inline" value="<?php echo Profile::NEW_LEVEL;?>" />
							<label for="new" class="inline">New Logins</label>
						</p>
					</fieldset>
					<input type="submit" name="submit" value="submit" />
				</fieldset>
				<table summary="List of ip access ranges.">
					<thead>
						<tr>
							<th>Hostname</th>
							<th>Start IP</th>
							<th>End IP</th>
							<th>Permission</th>
							<th>Allowed</th>
							<th>DELETE</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Hostname</th>
							<th>Start IP</th>
							<th>End IP</th>
							<th>Permission</th>
							<th>Allowed</th>
							<th>DELETE</th>
						</tr>
					</tfoot>
					<tbody>
		<?php foreach($access AS $ippermit): ?>			
						<tr>
							<td><?php echo gethostbyaddr($ippermit['ipv4']); ?></td>
							<td><?php echo $ippermit['ipv4']; ?></td>
							<td><?php echo $ippermit['ending']; ?></td>
							<td>
								<ul>
<?php if(Profile::ADMIN_LEVEL & (int)$ippermit['level']):?>								
									<li>Administrators</li>
<?php endif;?>
<?php if(Profile::PLUS_LEVEL & (int)$ippermit['level']):?>								
									<li>Supervisor Plus</li>
<?php endif;?>
<?php if(Profile::SUPERVISOR_LEVEL & (int)$ippermit['level']):?>								
									<li>Supervisor</li>
<?php endif;?>
<?php if(Profile::EMPLOYEE_LEVEL & (int)$ippermit['level']):?>								
									<li>Employee</li>
<?php endif;?>
<?php if(Profile::PENDING_LEVEL & (int)$ippermit['level']):?>								
									<li>Pending</li>
<?php endif;?>
<?php if(Profile::APPLICANT_LEVEL & (int)$ippermit['level']):?>								
									<li>Applicant</li>
<?php endif;?>
<?php if(Profile::NEW_LEVEL & (int)$ippermit['level']):?>								
									<li>New Login</li>
<?php endif;?>
								</ul>
							</td>
							<td><?php echo ((int)$ippermit['allow'])?'yes':'no'; ?></td>
							<td><input type="checkbox" name="delete&#91;&#93;" value="<?php echo $ippermit['ipv4']; ?>" /></td>
						</tr>
		<?php endforeach; ?>
					</tbody>
				</table>
				<input type="submit" name="submit" value="submit" />
			</form>
		</div>
