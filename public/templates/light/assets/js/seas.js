// Open appropriate links in external windows.
$(function(){
	$('a[rel*=external]').click(function(){
		window.open(this.href);
		return false;
	});
});

$("input.date").AnyTime_picker({
	format: "%M %e, %Z"
});
$("input.time").AnyTime_picker({
	format: "%h:%i %p"
});
$("input.timeinput").AnyTime_picker({
	format: "%h:%i %p",
	earliest: "06:30 AM"
//	latest: "11:59 PM"
});
// Hack for forms also
$(function(){
	$('form[rel*=external]').attr('target','_blank');
});
// make tables searchable
$(function(){
	$('table.searchable').searchable();
});
/*
$(".tip").tooltip({
	bodyHandler: function(){
		return $(this).html();
	}
});
*/

// Disable submit button when clicked.
$('input[type=submit], input[type=button]').one("click",function() {
	$(this).clone().attr('type','hidden').insertBefore(this);
});

$('form').submit(function() {
	if($(this).valid()){
		if(typeof jQuery.data(this, "disabledOnSubmit") == 'undefined') {
			jQuery.data(this, "disabledOnSubmit", { submited: true });
			$('input[type=submit], input[type=button]', this).each(function() {
				$(this).fadeTo("slow", 0.33).attr('disabled','disabled');
			});
			return true;
		}
		else
		{
			return false;
		}
	}
});
// Custom jquery function
(function($){ 
 jQuery.fn.idle = function(time) { 
 var o = $(this); 
 o.queue(function() { 
	 setTimeout(function() { 
		 o.dequeue();
		}, time);
	 });
 };
})(jQuery);
//
// http://jacwright.com/projects/javascript/date_format
// Simulates PHP's date function
//
Date.prototype.format=function(format){var returnStr='';var replace=Date.replaceChars;for(var i=0;i<format.length;i++){var curChar=format.charAt(i);if(replace[curChar]){returnStr+=replace[curChar].call(this);}else{returnStr+=curChar;}}return returnStr;};Date.replaceChars={shortMonths:['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],longMonths:['January','February','March','April','May','June','July','August','September','October','November','December'],shortDays:['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],longDays:['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'],d:function(){return(this.getDate()<10?'0':'')+this.getDate();},D:function(){return Date.replaceChars.shortDays[this.getDay()];},j:function(){return this.getDate();},l:function(){return Date.replaceChars.longDays[this.getDay()];},N:function(){return this.getDay()+1;},S:function(){return(this.getDate()%10==1&&this.getDate()!=11?'st':(this.getDate()%10==2&&this.getDate()!=12?'nd':(this.getDate()%10==3&&this.getDate()!=13?'rd':'th')));},w:function(){return this.getDay();},z:function(){return"Not Yet Supported";},W:function(){return"Not Yet Supported";},F:function(){return Date.replaceChars.longMonths[this.getMonth()];},m:function(){return(this.getMonth()<11?'0':'')+(this.getMonth()+1);},M:function(){return Date.replaceChars.shortMonths[this.getMonth()];},n:function(){return this.getMonth()+1;},t:function(){return"Not Yet Supported";},L:function(){return"Not Yet Supported";},o:function(){return"Not Supported";},Y:function(){return this.getFullYear();},y:function(){return(''+this.getFullYear()).substr(2);},a:function(){return this.getHours()<12?'am':'pm';},A:function(){return this.getHours()<12?'AM':'PM';},B:function(){return"Not Yet Supported";},g:function(){return this.getHours()%12||12;},G:function(){return this.getHours();},h:function(){return((this.getHours()%12||12)<10?'0':'')+(this.getHours()%12||12);},H:function(){return(this.getHours()<10?'0':'')+this.getHours();},i:function(){return(this.getMinutes()<10?'0':'')+this.getMinutes();},s:function(){return(this.getSeconds()<10?'0':'')+this.getSeconds();},e:function(){return"Not Yet Supported";},I:function(){return"Not Supported";},O:function(){return(this.getTimezoneOffset()<0?'-':'+')+(this.getTimezoneOffset()/60<10?'0':'')+(this.getTimezoneOffset()/60)+'00';},T:function(){return"Not Yet Supported";},Z:function(){return this.getTimezoneOffset()*60;},c:function(){return"Not Yet Supported";},r:function(){return this.toString();},U:function(){return this.getTime()/1000;}};


