<div id="admin-page">
		<div class="section profile search">
			<form method="post" action="./profile/list">
				<fieldset>
					<legend>Search Profiles</legend>
					<p>
						<label for="q">Profile Search</label>
						<input id="q" name="q" type="text" />
					</p>
					<input name="submit" type="submit" value="submit" />
				</fieldset>
			</form>
		</div>
		<p>
			<a class="security actionbtn" href="./admin/network" title="Update IP Security"><span><strong>Network Security</strong> <em>Update system security</em></span></a>
			<a class="hire actionbtn" href="./admin/positions/pending" title="Hire a Pending Applicant"><span><strong>Hire</strong> <em>Hire employees from this list.</em></span></a>
		</p>
		<p>
			<a class="calendar actionbtn" href="./admin/calendar" title="Edit the Calendar Information"><span><strong>Calendar</strong> <em>Edit Application Days</em></span></a>
			<a class="stats actionbtn" href="./admin/stats" title="Edit the Calendar Information"><span><strong>Statistics</strong> <em>View System Statistics</em></span></a>
		</p>
		<p>
			<a class="terminate actionbtn" href="./admin/positions/employee" title="Remove a position"><span><strong>Terminate</strong> <em>Remove a position</em></span></a>
		</p>
</div>
