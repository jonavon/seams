<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

?>
				<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
				</div>
				<div id="admin-dashboard" class="concol">
					<p>Some pertinant admin stuff should go here!</p>
				</div>

