<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$cnt = count($enums);
?>
		<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
		</div>
		<div id="enum-form" class="concol">
			<form method="post" action="">
				<fieldset>
				<input type="hidden" value="<?php echo $objid; ?>" name="type" />
				<legend><?php echo $objid; ?> List</legend>
				<table summary="Enum List">
					<caption>Review <?php echo "($cnt) $objid"; ?> List</caption>
					<colgroup>
						<col class="enum id"/>
						<col class="enum label"/>
						<col class="enum description"/>
						<col class="enum sort"/>
						<col class="enum DELETE"/>
					</colgroup>
					<thead>
						<tr>
							<th>id</th>
<?php if($objid === 'paycodes'): ?>
							<th>Unit #</th>
							<th><?php echo $objid; ?></th>
<?php else: ?>
							<th><?php echo $objid; ?></th>
							<th>description</th>
<?php endif; ?>
							<th>sort</th>
							<th>DELETE</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>id</th>
							<th>label</th>
							<th>description</th>
							<th>sort</th>
							<th>DELETE</th>
						</tr>
					</tfoot>
					<tbody>
<?php foreach($enums as $item):?>
						<tr>
							<td class="id"><?php echo $item['id']; ?></td>
							<td class="label"><input value="<?php echo htmlspecialchars($item['label']); ?>" type="text" name="enum&#91;<?php echo $item['id']; ?>&#93;&#91;label&#93;" /></td>
							<td class="description"><input value="<?php echo htmlspecialchars($item['description']); ?>" type="text" name="enum&#91;<?php echo $item['id']; ?>&#93;&#91;description&#93;" /></td>
							<td class="sort"><input value="<?php echo $item['sort']; ?>" type="text" name="enum&#91;<?php echo $item['id']; ?>&#93;&#91;sort&#93;" /></td>
							<td class="DELETE"><input type="checkbox" name="enum&#91;<?php echo $item['id']; ?>&#93;&#91;trash&#93;" value="1" /></td>
						</tr>
<?php endforeach; ?>
						<tr>
							<th class="id">New</th>
							<th class="label"><input value="" type="text" name="new&#91;label&#93;" /></th>
							<th class="description"><input value="" type="text" name="new&#91;description&#93;" /></th>
							<th class="sort"><input value="" type="text" name="new&#91;sort&#93;" /></th>
							<th class="DELETE">New</th>
						</tr>
					</tbody>
				</table>
				<input type="submit" name="submit" value="Submit List" />
				</fieldset>
			</form>
		</div>
