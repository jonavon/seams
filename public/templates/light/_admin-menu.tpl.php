<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>

			<ul class="nav">
				<li<?php if($objid === 'admin'):?> class="current"<?php endif;?>>
					<a href="./profile/list/admin" title="List of Admins">Administrator List</a>
				</li>
				<li<?php if($objid === 'supervisor'):?> class="current"<?php endif;?>>
					<a href="./profile/list/supervisor" title="List of supervisors">Supervisor List</a>
				</li>
				<li<?php if($objid === 'employee'):?> class="current"<?php endif;?>>
					<a href="./admin/positions/employee" title="List of employees">Employee List</a>
				</li>
				<li<?php if($objid === 'pending'):?> class="current"<?php endif;?>>
					<a href="./admin/positions/pending" title="List of pending">Pending</a>
				</li>
				<li>
					<a href="./profile/add" title="Add new user">New User</a>
				</li>
				<li<?php if($action === 'applications'):?> class="current"<?php endif;?>>
					<a href="./admin/applications" title="List of applications">Application List</a>
				</li>
				<li<?php if($action === 'jobs'):?> class="current"<?php endif;?>>
					<a href="./admin/jobs" title="List of applications">Jobs List</a>
				</li>
			</ul>
			<h3>Drop Downs</h3>
			<ul class="nav">
				<li<?php if($objid === 'ethnicities'):?> class="current"<?php endif;?>>
					<a href="./enum/edit/ethnicities" title="List of Ethnicity">Ethnicity Options</a>
				</li>
				<li<?php if($objid === 'majors'):?> class="current"<?php endif;?>>
					<a href="./enum/edit/majors" title="List of Major">Major Options</a>
				</li>
				<li<?php if($objid === 'units'):?> class="current"<?php endif;?>>
					<a href="./enum/edit/units" title="List of units">Unit Options</a>
				</li>
				<li<?php if($objid === 'paycodes'):?> class="current"<?php endif;?>>
					<a href="./enum/edit/paycodes" title="List of Paycodes">Paycode Options</a>
				</li>
			</ul>
			<h3>Other Stuff</h3>
			<ul class="nav">
				<li<?php if($action === 'stats'):?> class="current"<?php endif;?>>
					<a href="./admin/stats" title="Statistics for this application">Statistics</a>
				</li>
				<li<?php if($action === 'calendar'):?> class="current"<?php endif;?>>
					<a href="./admin/calendar" title="Change the SEAS calendar">Modify Calendar</a>
				</li>
				<li>
					<a href="./admin/network" title="Manage Ip Address Access">IP Address Security</a>
				</li>
			</ul>
