<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * stats-admin.tpl.php
 *
 * Display for system statistics.
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package seas
 */
$period_applicants_new = count($log['applications']['new']);
$period_positions_new = count($log['positions']['new']);
$total_employees = count($employees);
$total_freshmen = count(array_filter($employees,create_function('$item','return ("FRESHMAN"==strtoupper($item["class"]));')));
$total_sophomores = count(array_filter($employees,create_function('$item','return ("SOPHOMORE"==strtoupper($item["class"]));')));
$total_juniors = count(array_filter($employees,create_function('$item','return ("JUNIOR"==strtoupper($item["class"]));')));
$total_seniors = count(array_filter($employees,create_function('$item','return ("SENIOR"==strtoupper($item["class"]));')));
$total_graduates = count(array_filter($employees,create_function('$item','return ("GRADUATE"==strtoupper($item["class"]));')));
$total_males = count(array_filter($employees,create_function('$item','return ("MALE"==strtoupper($item["gender"]));')));
$total_females = count(array_filter($employees,create_function('$item','return ("FEMALE"==strtoupper($item["gender"]));')));


$total_applicants = count($applicants);
$atotal_freshmen = count(array_filter($applicants,create_function('$item','return ("FRESHMAN"==strtoupper($item["class"]));')));
$atotal_sophomores = count(array_filter($applicants,create_function('$item','return ("SOPHOMORE"==strtoupper($item["class"]));')));
$atotal_juniors = count(array_filter($applicants,create_function('$item','return ("JUNIOR"==strtoupper($item["class"]));')));
$atotal_seniors = count(array_filter($applicants,create_function('$item','return ("SENIOR"==strtoupper($item["class"]));')));
$atotal_graduates = count(array_filter($applicants,create_function('$item','return ("GRADUATE"==strtoupper($item["class"]));')));
$atotal_males = count(array_filter($applicants,create_function('$item','return ("MALE"==strtoupper($item["gender"]));')));
$atotal_females = count(array_filter($applicants,create_function('$item','return ("FEMALE"==strtoupper($item["gender"]));')));
?>
		<div id="statistics">
			<h3>Applicants Statistics</h3>
			<div id="applicants-stats">
				<table summary="Statistics since a certain period">
					<caption>Since <?php echo date('F j, Y',$lastperiod); ?></caption>
					<thead>
						<th>Label</th>
						<th>#</th>
					</thead>
					<tfoot>
						<th>Label</th>
						<th>#</th>
					</tfoot>
					<tbody>
						<td>New Applications</td>
						<td><?php echo $period_applicants_new; ?></td>
					</tbody>
				</table>
			</div>
			<h4>Applicant Profile Statistics</h4>
			<div id="applicant-profile-stats">
				<table summary="Display of the statistics for applicants">
					<caption><?php echo $total_applicants; ?> Total Applicants</caption>
					<thead>
						<tr>
							<th>Label</th>
							<th>#</th>
							<th>Percentage</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Label</th>
							<th>#</th>
							<th>Percentage</th>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<th colspan="3">Class Status</th>
						</tr>
						<tr>
							<td>Freshman</td>
							<td><?php  echo $atotal_freshmen; ?></td>
							<td><?php echo (round((double)($atotal_freshmen/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Sophomore</td>
							<td><?php echo $atotal_sophomores; ?></td>
							<td><?php echo (round((double)($atotal_sophomores/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Junior</td>
							<td><?php  echo $atotal_juniors; ?></td>
							<td><?php echo (round((double)($atotal_juniors/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Senior</td>
							<td><?php  echo $atotal_seniors; ?></td>
							<td><?php echo (round((double)($atotal_seniors/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Graduate</td>
							<td><?php  echo $atotal_graduates; ?></td>
							<td><?php echo (round((double)($atotal_graduates/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
					</tbody>
					<tbody>
						<tr>
							<th colspan="3">Gender</th>
						</tr>
						<tr>
							<td>Males</td>
							<td><?php  echo $atotal_males; ?></td>
							<td><?php echo (round((double)($atotal_males/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Females</td>
							<td><?php echo $atotal_females; ?></td>
							<td><?php echo (round((double)($atotal_females/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
					</tbody>
					<tbody>
						<tr>
							<th colspan="3">Ethnicities</th>
						</tr>
<?php foreach($ethnicities as $ethnicity): ?>
						<tr>
							<td><?php echo $ethnicity['label']; ?></td>
							<td><?php $current_count = count(array_filter($applicants,create_function('$item','return ("' . $ethnicity['label'] . '"==($item["ethnicity"]));'))); echo $current_count; ?></td>
							<td><?php echo (round((double)($current_count/$total_applicants),4) * 100), '%'; ?></td>
						</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div>
			<h3>Employee Statistics</h3>
			<div id="employee-stats">
				<table summary="Statistics since a certain period">
					<caption>Since <?php echo date('F j, Y',$lastperiod); ?></caption>
					<thead>
						<th>Label</th>
						<th>#</th>
					</thead>
					<tfoot>
						<th>Label</th>
						<th>#</th>
					</tfoot>
					<tbody>
						<td>New Employees</td>
						<td><?php echo $period_positions_new; ?></td>
					</tbody>
				</table>
			</div>
			<h4>Employee Profile Statistics</h4>
			<div id="employee-profile-stats">
				<table summary="Display of the statistics for employees">
					<caption><?php echo $total_employees; ?> Total Employees</caption>
					<thead>
						<tr>
							<th>Label</th>
							<th>#</th>
							<th>Percentage</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th>Label</th>
							<th>#</th>
							<th>Percentage</th>
						</tr>
					</tfoot>
					<tbody>
						<tr>
							<th colspan="3">Class Status</th>
						</tr>
						<tr>
							<td>Freshman</td>
							<td><?php  echo $total_freshmen; ?></td>
							<td><?php echo (round((double)($total_freshmen/$total_employees),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Sophomore</td>
							<td><?php echo $total_sophomores; ?></td>
							<td><?php echo (round((double)($total_sophomores/$total_employees),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Junior</td>
							<td><?php  echo $total_juniors; ?></td>
							<td><?php echo (round((double)($total_juniors/$total_employees),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Senior</td>
							<td><?php  echo $total_seniors; ?></td>
							<td><?php echo (round((double)($total_seniors/$total_employees),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Graduate</td>
							<td><?php  echo $total_graduates; ?></td>
							<td><?php echo (round((double)($total_graduates/$total_employees),4) * 100), '%'; ?></td>
						</tr>
					</tbody>
					<tbody>
						<tr>
							<th colspan="3">Gender</th>
						</tr>
						<tr>
							<td>Males</td>
							<td><?php  echo $total_males; ?></td>
							<td><?php echo (round((double)($total_males/$total_employees),4) * 100), '%'; ?></td>
						</tr>
						<tr>
							<td>Females</td>
							<td><?php echo $total_females; ?></td>
							<td><?php echo (round((double)($total_females/$total_employees),4) * 100), '%'; ?></td>
						</tr>
					</tbody>
					<tbody>
						<tr>
							<th colspan="3">Ethnicities</th>
						</tr>
<?php foreach($ethnicities as $ethnicity): ?>
						<tr>
							<td><?php echo $ethnicity['label']; ?></td>
							<td><?php $current_count = count(array_filter($employees,create_function('$item','return ("' . $ethnicity['label'] . '"==($item["ethnicity"]));'))); echo $current_count; ?></td>
							<td><?php echo (round((double)($current_count/$total_employees),4) * 100), '%'; ?></td>
						</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div>
		</div>
