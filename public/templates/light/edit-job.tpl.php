<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
				<div id="job-form">
					<h3>Pool Information <?php echo $job['id']; ?> Edit</h3>
					<form method="post" action="">
						<fieldset>
							<legend>Pool Information</legend>
							<input type="hidden" value="<?php echo $job['id']; ?>" name="jobid"/>
							<input type="hidden" value="<?php echo $job['supervisor']; ?>" name="supervisor"/>
							<p>
								<label for="label">Title</label>
								<input type="text" value="<?php echo $job['label']; ?>" name="label" id="label"/>
							</p>
							<p>
								<label for="description">Description</label>
								<textarea id="description" name="description" rows="10" cols="80"><?php echo $job['description']; ?></textarea>
							</p>
							<input type="submit" name="submit" value="submit" />
						</fieldset>
					</form>
				</div>
