<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
/*
$next_application_drive_semester = '{next application drive semester}';
$fall_dates = array('{date}','{date}','{date}','{date}');
$spring_dates = array('{date}','{date}');
$summer_dates = array('{date}','{date}','{date}','{date}','{date}','{date}');
*/
$gateway_text = <<<TEXT
**Applications can only be accessed at the locations and times listed in the blue box above.**

To be eligible for student employment in the library:

*   You must be a Virginia Tech student. 
    *   Students "affiliated with Virginia Tech" are not eligible.
*   Students must be enrolled for the following hours: 
    *   Undergraduate students: 6 hours
    *   Graduate students: 5 hours
    *   Summer School Students: 3 hours 
        *   Students not enrolled in Summer but enrolled the previous Spring and following Fall semesters are eligible. However, you will be required to pay social security taxes.
*   Resident Aliens/Visa F-1 holders are restricted to 20 hours a week during the academic year 
    *   Exceptions: official school breaks and summer (unless enrolled in classes, then student is restricted to 20 hours a week).

### Important Information - Please Read:

*   If you are applying for the first time, complete a new student application.
*   If you have already completed an application, and are still interested in library employment, you must update your application at the beginning of every semester.
*   Hiring is done on **as needed throughout the year.**
    *   If a supervisor is interested in hiring you, he or she will contact you directly.
    *   Library Personnel Services (<libwage@vt.edu>) does not have information on what jobs are available. 
*   References and recommendations are not required, but recommended. 
*   Students are hired in the following areas: Circulation, Collection Management, Dean's Office, Special Collections, Digital Library and Archives (DLA), Reference and Instructional Services (RIS), Library Systems, InterLibrary Loan (ILL), Mailroom, Photocopy, Shelving, Technical Services, and at our branches: Art and Architecture Library and Veterinary Medicine Library. 
    *   Pay rates vary with the level of responsibilities of each position.

**Failure to complete any of the required fields (marked with an asterisk *) will result in your application being placed on hold and unavailable to supervisors.**

If you encounter problems or have questions that were not answered above, please e-mail <libwage@vt.edu>
TEXT;
include_once 'markdown.php';
?>
		<div id="gateway-page">
			<div  class="important update">
				<p>
					Applications will be taken <?php echo ($appdayistoday)?'today':'next on';?> <br />
					<?php echo $next_application_drive_date; ?> 
				</p>
<?php if($appdayistoday):?>
				<p>
					After today Applications will be taken on <?php echo $after_application_drive_date;?>.
				</p>
<?php endif; ?>
				<p>
					Newman Library Public Computers<br />
					7:30am to Midnight.
				</p>
			</div>
			<p>Library Personnel Sevices will be accepting student applications on the following dates for the 2009-2010 academic year.</p>
<?php include 'view-calendar.tpl.php'; ?>
			<div id="gateway-text">
<?php echo Markdown($gateway_text); ?>
			</div>
			<p>
<?php if($appdayistoday): ?>
				<a class="actionbtn" href="./login" title="New Application Link"><span><strong>Create A New Application</strong> <em>Create an opportunity for employment</em></span></a>
<?php endif; ?>
				<a class="actionbtn" href="./login" title="Update an Application"><span><strong>Update an Application</strong> <em>Update or Withdraw Your Application</em></span></a>
			</p>
		</div>
