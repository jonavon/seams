<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */

?>


		<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
		</div>
		<div id="admin-applications-list" class="concol">
			<form method="get" action="./application/pdf">
			<table class="searchable" summary="List of applications">
				<caption>List of all applications (<?php echo count($applications); ?>)</caption>
				<thead>
					<tr>
						<th>Print</th>
						<th>Application</th>
						<th>Name [PID]</th>
						<th>Modified</th>
						<th>Expiration</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Print</th>
						<th>Application</th>
						<th>Name [PID]</th>
						<th>Modified</th>
						<th>Expiration</th>
					</tr>
				</tfoot>
				<tbody>
<?php $now = time(); foreach($applications as $application): ?>
					<tr<?php if($application['expire'] < $now):?> class="alert"<?php endif; ?>>
						<td><input name="app&#91;&#93;" type="checkbox" id="app-<?php echo $application['id']; ?>" value="<?php echo $application['id']; ?>" /></td>
						<td><a href="./application/view/<?php echo $application['id']; ?>" title="<?php echo $application['name'];?> application"><?php echo $application['id']; ?></a></td>
						<td><?php echo "{$application['name']} [{$application['pid']}]"; ?></td>
						<td><?php echo date('M j, Y',$application['modified']); ?></td>
						<td><?php echo date('M j, Y',$application['expire']); ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			<input type="submit" name="submit" value="print" />
			</table>
			</form>
		</div>
