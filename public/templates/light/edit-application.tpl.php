<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

/*
$application['id'] = '{application id}';
$application['expire'] = '{application expiration date}';
$application['content']['workstudy']['answer'] = '{application workstudy}';
$application['content']['citizen']['answer'] = false;
$application['content']['library_experience']['answer'] = '{application answer library experience}';
$application['content']['other_experience']['answer'] = '{application answer other experience}';
$application['content']['work_reference_0_name']['answer'] = '{application answer work reference name}';
$application['content']['work_reference_0_phone']['answer'] = '{application answer work reference phone}';
$application['content']['work_reference_0_notes']['answer'] = '{application answer work reference notes}';
$application['content']['work_reference_1_name']['answer'] = '{application answer work reference name}';
$application['content']['work_reference_1_phone']['answer'] = '{application answer work reference phone}';
$application['content']['work_reference_1_notes']['answer'] = '{application answer work reference notes}';
$application['content']['work_reference_2_name']['answer'] = '{application answer work reference name}';
$application['content']['work_reference_2_phone']['answer'] = '{application answer work reference phone}';
$application['content']['work_reference_2_notes']['answer'] = '{application answer work reference notes}';
$application['content']['another_department']['answer'] = '{application answer other department}';
$application['content']['citizen']['answer'] = true;
$application['content']['break_worker']['answer'] = true;
$application['content']['lift50']['answer'] = false;
$application['content']['transportation']['answer'] = true;
*/
?>
 			<div id="application-form">
				<form method="post" action="./application/submit">
					<fieldset>
						<legend>Application <?php echo $application['id']; ?></legend>
						<input type="hidden" name="id" id="id" value="<?php echo $application['id']; ?>" />
						<p>
							<label for="pid">PID</label>
							<input type="text" name="pid" id="pid" value="<?php echo $application['pid']; ?>" class="required" readonly="readonly"/>
						</p>
						<p>
							<label for="workstudy" class="required"><?php echo $application['content']['workstudy']['question']->__get('label');?></label>
							<label for="yesihaveworkstudy" class="inline required">Yes</label>
							<input type="radio" name="doyouhaveworkstudy" id="yesihaveworkstudy" value="1" class="inline required" <?php if((bool)$application['content']['workstudy']['answer']): ?>checked="checked"<?php endif; ?>/>
							<label for="nopenoworkstudy" class="inline required">No</label>
							<input type="radio" name="doyouhaveworkstudy" id="nopenoworkstudy" value="0" class="inline required" <?php if(($action=='edit')&&(empty($application['content']['workstudy']['answer']))): ?>checked="checked"<?php endif; ?>/>
						</p>
						<p>
							<label for="workstudy">Amount $</label>
							<select name="workstudy" id="workstudy">
								<option value="">None</option>
								<option value="1,000.00" <?php if($application['content']['workstudy']['answer'] === '1,000.00'):?>selected="selected"<?php endif; ?>>1 Semester [$1,000 Fall or Spring]</option>
								<option value="2,000.00"<?php if($application['content']['workstudy']['answer'] === '2,000.00'):?>selected="selected"<?php endif; ?>>2 Semesters [$2,000 Fall and Spring]</option>
							</select>
						</p>
						<p>
							<label for="hours"><?php echo $application['content']['hours']['question']->__get('label');?></label>
							<input type="text" name="hours" class="number" id="hours" value="<?php echo $application['content']['hours']['answer']; ?>" maxlength="2" />
						</p>
						<h4>Work Experience</h4>
						<p>
							<label for="library_experience"><?php echo $application['content']['library_experience']['question']->__get('label');?></label>
							<textarea name="library_experience" id="library_experience" rows="10" cols="50"><?php echo $application['content']['library_experience']['answer']; ?></textarea>
						</p>
						<p>
							<label for="other_experience"><?php echo htmlentities($application['content']['other_experience']['question']->__get('label'),ENT_QUOTES);?></label>
							<textarea name="other_experience" id="other_experience" rows="10" cols="50"><?php echo $application['content']['other_experience']['answer']; ?></textarea>
						</p>
						<div id="skills-tags" class="clear">
							<div class="help">
								<h4>Suggested Skills</h4>
								<p>Click on the item from the list of skills to add to the input box below. You may also add additional skills, just separate them by commas.</p>
							</div>
							<span>Accounting</span>
							<span>Addison</span>
							<span>Archives Special Collections</span>
							<span>Bookkeeping</span>
							<span>Bus Driver</span>
							<span>Cashier</span>
							<span>Clerical</span>
							<span>Computer Maintenance</span>
							<span>Computer Programming</span>
							<span>Courier</span>
							<span>Customer Service</span>
							<span>Database</span>
							<span>Desktop Publishing</span>
							<span>General Internet Skills</span>
							<span>Photocopy</span>
							<span>Photoshop</span>
							<span>Receptionist</span>
							<span>Software Installation</span>
							<span>Spreadsheets</span>
							<span>Storage</span>
							<span>Warehouse</span>
							<span>Web Design</span>
						</div>
						<div class="clear">&nbsp;</div>
						<p>
							<label for="skills"><?php echo $application['content']['skills']['question']->__get('label');?></label>
							<textarea name="skills" id="skills" class="csv" rows="10" cols="50"><?php if((bool)$application['content']['skills']['answer']){ echo $application['content']['skills']['answer'];} ?></textarea>
						</p>
						<fieldset>

								<legend>Work References</legend>
								<p>
									<label for="work_reference_0_name" class="inline required"><?php echo $application['content']['work_reference_0_name']['question']->__get('label');?></label>
									<input class="required" name="work_reference_0_name" id="work_reference_0_name" type="text" value="<?php echo $application['content']['work_reference_0_name']['answer']; ?>" />
									<label for="work_reference_0_phone" class="inline required"><?php echo $application['content']['work_reference_0_phone']['question']->__get('label');?></label>
									<input class="required" name="work_reference_0_phone" id="work_reference_0_phone" type="text" value="<?php echo $application['content']['work_reference_0_phone']['answer']; ?>"/>
									<label for="work_reference_0_notes" class="clear required"><?php echo $application['content']['work_reference_0_notes']['question']->__get('label');?></label>
									<textarea class="required" name="work_reference_0_notes" id="work_reference_0_notes" rows="4" cols="50"><?php echo $application['content']['work_reference_0_notes']['answer']; ?></textarea>
								</p>
								<hr />
								<p>
									<label for="work_reference_1_name" class="inline"><?php echo $application['content']['work_reference_1_name']['question']->__get('label');?></label>
									<input name="work_reference_1_name" id="work_reference_1_name" type="text" value="<?php echo $application['content']['work_reference_1_name']['answer']; ?>" />
									<label for="work_reference_1_phone" class="inline"><?php echo $application['content']['work_reference_1_phone']['question']->__get('label');?></label>
									<input name="work_reference_1_phone" id="work_reference_1_phone" type="text" value="<?php echo $application['content']['work_reference_1_phone']['answer']; ?>"/>
									<label for="work_reference_1_notes" class="clear"><?php echo $application['content']['work_reference_1_notes']['question']->__get('label');?></label>
									<textarea name="work_reference_1_notes" id="work_reference_1_notes" rows="4" cols="50"><?php echo $application['content']['work_reference_1_notes']['answer']; ?></textarea>
								</p>
								<hr />
								<p>
									<label for="work_reference_2_name" class="inline"><?php echo $application['content']['work_reference_2_name']['question']->__get('label');?></label>
									<input name="work_reference_2_name" id="work_reference_2_name" type="text" value="<?php echo $application['content']['work_reference_2_name']['answer']; ?>" />
									<label for="work_reference_2_phone" class="inline"><?php echo $application['content']['work_reference_2_phone']['question']->__get('label');?></label>
									<input name="work_reference_2_phone" id="work_reference_2_phone" type="text" value="<?php echo $application['content']['work_reference_2_phone']['answer']; ?>"/>
									<label for="work_reference_2_notes" class="clear"><?php echo $application['content']['work_reference_2_notes']['question']->__get('label');?></label>
									<textarea name="work_reference_2_notes" id="work_reference_2_notes" rows="4" cols="50"><?php echo $application['content']['work_reference_2_notes']['answer']; ?></textarea>
								</p>
							</fieldset>
								<fieldset>
								<legend>Quick Questions</legend>
								<p>
									<label for="lift50"><?php echo $application['content']['lift50']['question']->__get('label');?></label>
									<label for="lift50" class="inline">yes</label>
									<input name="lift50" id="lift50" class="inline" value="1" type="radio" <?php if($application['content']['lift50']['answer']): ?>checked="checked"<?php endif; ?> />
									<label for="nolift50" class="inline">no</label>
									<input name="lift50" id="nolift50" class="inline" value="" type="radio" />
								</p>
								<hr />
								<p>
									<label for="break_worker"><?php echo $application['content']['break_worker']['question']->__get('label');?></label>
									<label for="break_worker" class="inline">yes</label>
									<input name="break_worker" class="inline" id="break_worker" value="1" type="radio" <?php if($application['content']['break_worker']['answer']): ?>checked="checked"<?php endif; ?>/>
									<label for="nobreak_worker" class="inline">no</label>
									<input name="break_worker" class="inline" id="nobreak_worker" value="" type="radio" />
								</p>
								<hr />
								<p>
									<label for="transportation"><?php echo $application['content']['transportation']['question']->__get('label');?></label>
									<label for="transportation" class="inline">yes</label>
									<input name="transportation" class="inline" id="transportation" value="1" type="radio" <?php if($application['content']['transportation']['answer']): ?>checked="checked"<?php endif; ?>/>
									<label for="notransportation" class="inline">no</label>
									<input name="transportation" class="inline" id="notransportation" value="" type="radio" />
								</p>
								<hr />
								<p>
									<label for="another_department"><?php echo $application['content']['another_department']['question']->__get('label'); ?></label>
									<input name="another_department" id="another_department" value="<?php echo $application['content']['another_department']['answer']; ?>" type="text" />
								</p>
								</fieldset>
						<fieldset class="important">
							<legend class="required"><?php echo $application['content']['citizen']['question']->__get('label');?></legend>
							<p>
							<strong>Employment eliibility must be verified at the time of employment</strong></p>
							<p>
							<label for="domestic" class="required">I am a citizen</label>
							<input class="required" type="radio" name="citizen" id="domestic" value="1" <?php if($application['content']['citizen']['answer']): ?>checked="checked"<?php endif; ?>/>
							</p>
							<p>
							<label class="required" for="foreign">No, but I am authorized to work</label>
							<input  class="required" type="radio" name="citizen" id="foreign" value="0" <?php if(!$application['content']['citizen']['answer']): ?>checked="checked"<?php endif; ?>/>
							</p>
							<p>If yes, you must present a <strong>state driver&#39;s license</strong> or a <strong>Virginia Tech ID card</strong>, plus a <strong>social security card</strong> or <strong>birth certificate</strong> when hired.</p>
							<p>If no, you must present an <strong>unexpired foreign passport</strong>, with attacted <strong>employment authorization</strong> at the time of employment.</p>
						</fieldset>
						<input type="submit" value="Submit  Employment Information" name="submit" />
					</fieldset>
				</form>
			</div>
