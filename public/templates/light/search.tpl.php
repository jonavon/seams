<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

?>
				<div id="aside" class="concol">
<?php include '_search-menu.tpl.php'; ?>
				</div>
				<div id="search-quick" class="concol">
<?php include '_simple-search.tpl.php'; ?>
				<div class="help hide">
			   <h2 id="searchoperators">

      Search Operators
    </h2>
    <dl>
      <dt>
        <strong>+</strong>
      </dt>
      <dd>
        <p>
          word <strong>must</strong> be present
        </p>

      </dd>
      <dt>
        <strong>-</strong>
      </dt>
      <dd>
        <p>
          word <strong>must not</strong> be present
        </p>

      </dd>
      <dd>
        <p>
          only returns rows that are otherwise matched by other search terms. It will return an empty result if it is the only term.
        </p>
      </dd>
      <dt>
        <strong>&gt;</strong>
      </dt>

      <dd>
        <p>
          <strong>increase</strong> word&rsquo;s relevance.
        </p>
      </dd>
      <dt>
        <strong>&lt;</strong>
      </dt>

      <dd>
        <p>
          <strong>decrease</strong> word&rsquo;s relevance.
        </p>
      </dd>
      <dt>
        <strong>( )</strong>

      </dt>
      <dd>
        <p>
          parentheses group words; can also be nested.
        </p>
      </dd>
      <dt>
        <strong>*</strong>
      </dt>

      <dd>
        <p>
          wildcard operator
        </p>
      </dd>
      <dt>
        <strong>&ldquo;</strong>
      </dt>
      <dd>

        <p>
          Literal phrases
        </p>
      </dd>
    </dl>
    <h3 id="examples">
      Examples
    </h3>
    <dl>
      <dt>

        <strong>library book</strong>
      </dt>
      <dd>
        <p>
          finds at least one of the two words
        </p>
      </dd>
      <dt>
        <strong>library +book</strong>

      </dt>
      <dd>
        <p>
          find records that have &lsquo;book&rsquo; but rank higher if it has &lsquo;library&rsquo;
        </p>
      </dd>

      <dt>
        <strong>library -book</strong>
      </dt>
      <dd>
        <p>
          find records that have &lsquo;library&rsquo; but not &lsquo;book&rsquo;

        </p>
      </dd>
      <dt>
        <strong>&ldquo;library book&rdquo;</strong>
      </dt>
      <dd>
        <p>
          find records with the exact phrase &lsquo;library book&rsquo;

        </p>
      </dd>
      <dt>
        <strong>library &lt;book</strong>
      </dt>
      <dd>
        <p>
          finds at least one of the two words, but rank &lsquo;book&rsquo; lower in relevance
        </p>

      </dd>
      <dt>
        <strong>library &gt;book</strong>
      </dt>
      <dd>
        <p>
          finds at least one of the two words, but rank &lsquo;book&rsquo; higher in relevance
        </p>

      </dd>
      <dt>
        <strong>lib* book</strong>
      </dt>
      <dd>
        <p>
          finds words begining with &lsquo;lib&rsquo; or &lsquo;book&rsquo;

        </p>
      </dd>
      <dt>
        <strong>government +( library &lt;book )</strong>
      </dt>
      <dd>
        <p>
          matches government, library, book, government library, government book, but records with book are lower in relevance.
        </p>

      </dd>
    </dl>
	
				</div>
				</div>
