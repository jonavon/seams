<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
switch($permission) {
	case Profile::ADMIN_LEVEL :
		$f = '_admin';
		break;
	case Profile::SUPERVISOR_LEVEL :
		$f = '_super';
		break;
	case Profile::EMPLOYEE_LEVEL :
	case Profile::APPLICANT_LEVEL :
	default :
		$f = '_employee';
}
?>
		<div id="profile-page">
<?php
include "$f.tpl.php";
if(isset($_SESSION['appid'])) {
	include 'view-application.tpl.php';
}
?>
		</div>
