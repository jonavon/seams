<?php
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>

		<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery-ui-1.7.1.custom.min.js"></script>
		<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.metadata.js"></script>
		<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.validate.js"></script>
		<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/additional-methods.js"></script>
		<script type="text/javascript">
			/*<![CDATA[*/
			$(document).ready(function(){
				d = new Date();
				$("#schedule-form > form").validate({
					rules: {
						ending: {
							endtime: "#beginning"  
						},
						SUNDAY:{
							required: function(){ return ($("input[type=checkbox]:checked").length == 0)?true:false; }
						}
					},
					messages: {
						SUNDAY: "At least one day must be selected."
					}
				});
				$("input[type=checkbox]").change(function(){
					$("#schedule-form > form").valid();
				});
				$("#ending").change(function(){
					$("#schedule-form > form").valid();
				});
			});
			/*]]>*/
		</script>
