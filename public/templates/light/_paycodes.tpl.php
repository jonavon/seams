<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
<?php if(isset($settings['PAYCODE'])): ?>
		<div id="paycode-table">
			<h4>Paycode List</h4>
			<table summary="List of applications that are suppressed.">
				<caption>Paycodes can only be changed by admin.</caption>
				<thead>
					<tr>
						<th>Paycode</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Paycode</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($settings['PAYCODE'] as $paycode): ?>
					<tr>
						<td><?php echo "{$referencelist['PAYCODE'][$paycode['value']]['label']} {$referencelist['PAYCODE'][$paycode['value']]['description']}"; ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>
<?php endif; ?>
