<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
<?php if(isset($_POST['submit']) && isset($ranges) && !isset($_SESSION['schedulesearch']) && ($_SESSION['newapplicant']['schedule'])): ?>
				<form method="post" action="./application/complete">
					<div>
						<button class="large awesome red">Complete and Verify this Application</button>
						<p class="help"> ...Or enter another shift</p>
					</div>
				</form>
<?php endif; ?>
				<div id="schedule-form">
					<form method="post" action="<?php if($object === 'application' && $action === 'submit'):?>./schedule/edit/<?php echo $objid; endif; ?>">
						<fieldset>
							<legend>Availability</legend>
							<p>
								<label for="beginning" class="inline required">Start</label>
								<input type="text" name="beginning" id="beginning" class="required inline timeinput" />

								<label for="ending" class="inline required">End</label>
								<input type="text" name="ending" id="ending" class="required inline timeinput" />
							</p>
							<p>
								<input id="MONDAY" class="inline" name="MONDAY" type="checkbox" value="1" />
								<label for="MONDAY" class="inline">Mon</label>

								<input id="TUESDAY" class="inline" name="TUESDAY" type="checkbox" value="2" />
								<label for="TUESDAY" class="inline">Tue</label>

								<input id="WEDNESDAY" class="inline" name="WEDNESDAY" type="checkbox" value="3" />
								<label for="WEDNESDAY" class="inline">Wed</label>

								<input id="THURSDAY" class="inline"name="THURSDAY" type="checkbox" value="4" />
								<label for="THURSDAY" class="inline">Thu</label>

								<input id="FRIDAY" class="inline" name="FRIDAY" type="checkbox" value="5" />
								<label for="FRIDAY" class="inline">Fri</label>

								<input id="SATURDAY" class="inline" name="SATURDAY" type="checkbox" value="6" />
								<label for="SATURDAY" class="inline">Sat</label>

								<input id="SUNDAY" class="inline" name="SUNDAY" type="checkbox" value="0" />
								<label for="SUNDAY" class="inline">Sun</label>
							</p>
							<input type="submit" name="submit" value="Enter Shift" />
						</fieldset>
					</form>
<?php include 'view-schedule.tpl.php'; ?>
				</div>
