<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
<?php if(!isset($_SESSION['newapplicant'])): ?>
		<div id="application-submission">
			<p> You may want to attach your <a href="./schedule/edit/<?php echo $objid;?>" title="Edit the schedule">availability</a> to this application.</p>
		</div>
<?php endif; ?>

<?php
if(isset($_SESSION['newapplicant'])) {
	include 'edit-schedule.tpl.php';
}
?>
