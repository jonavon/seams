<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$_SESSION['schedulesearch'] = (isset($_SESSION['schedulesearch']))?$_SESSION['schedulesearch']:null;
?>
		<div id="aside" class="concol">
<?php include '_search-menu.tpl.php'; ?>
		</div>
		<div id="application-search-form" class="concol">
			<form method="post" action="./application/results">
				<fieldset>
<?php if((bool)$_SESSION['schedulesearch']):?>
					<p>
						<input class="inline" id="combined" name="combined" type="checkbox" value="1" />
						<label class="inline" for="combined">Match against Schedule</label>
					</p>
<?php endif; ?>
					<p>
						<label for="q">Search</label>
						<input id="q" name="q" type="text" />
					</p>
					<p>
						<label for="library_experience">Library Experience</label>
						<input id="library_experience" name="library_experience" type="text" />
					</p>
					<p>
						<label for="other_experience">Other Experience</label>
						<input id="other_experience" name="other_experience" type="text" />
					</p>
					<p>
						<label for="hours">Hours (at least)</label>
						<input id="hours" name="hours" type="text" />
					</p>
					<p>
						<label for="lift50">Able to lift 50lbs?</label>
						<label for="yeslift50" class="inline">Yes</label>
						<input name="lift50" id="yeslift50" value="=1" type="radio" class="inline"/>
						<label for="nolift50" class="inline">No</label>
						<input name="lift50" id="nolift50" value="!=1" type="radio"   class="inline"/>
						<label for="lift50" class="inline">N/A</label>
						<input name="lift50" id="lift50" value="" type="radio"   class="inline"/>
					</p>
					<p>
						<label for="workstudy">Workstudy?</label>
						<label for="yesworkstudy" class="inline">Yes</label>
						<input name="workstudy" id="yesworkstudy" value=">1" type="radio"  class="inline" />
						<label for="workstudy" class="inline">No</label>
						<input name="workstudy" id="noworkstudy" value="!>1" type="radio"  class="inline" />
						<label for="workstudy" class="inline">N/A</label>
						<input name="workstudy" id="workstudy" value="" type="radio"  class="inline" />
					</p>
					<p>
						<label for="break_worker">Willing to work during break/vacations?</label>
						<label for="yesbreak_worker" class="inline">Yes</label>
						<input name="break_worker" id="yesbreak_worker" value="=1" type="radio"  class="inline"/>
						<label for="nobreak_worker" class="inline">No</label>
						<input name="break_worker" id="nobreak_worker" value="!=1" type="radio"  class="inline"/>
						<label for="break_worker" class="inline">N/A</label>
						<input name="break_worker" id="break_worker" value="" type="radio"  class="inline"/>
					</p>
					<p>
						<label for="transportation">Own transportation?</label>
						<label for="yestransportation" class="inline">Yes</label>
						<input name="transportation" id="yestransportation" value="=1" type="radio"  class="inline"/>
						<label for="notransportation" class="inline">No</label>
						<input name="transportation" id="notransportation" value="!=1" type="radio"  class="inline"/>
						<label for="transportation" class="inline">N/A</label>
						<input name="transportation" id="transportation" value="" type="radio"  class="inline"/>
					</p>
					<input name="submit" type="submit" value="submit" />
				</fieldset>
			</form>
		</div>
