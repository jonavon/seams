<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
		<div id="message-list">
		<ol class="messages">
			<li class="odd">
			<div class="message">
					<span class="timestamp" title="1970-01-01T00:00:00Z"><span class="month">Jan</span> <span class="day">1</span> <span class="time">12:00 am</span></span>
					<button class="delete-button">trash</button>
					<div class="text">
						<a href="@vt.edu" class="author url fn" title="From">Some Body</a>
						<p>Pellentesque nisl felis, egestas porttitor, ullamcorper ut, egestas eu, felis. Nullam semper, nibh vitae convallis dictum, est risus tempus dui, eget vulputate urna odio non augue. Integer sagittis dictum ante. Morbi at diam. Ut eget lacus et risus viverra suscipit. Donec massa ligula, laoreet eu, tincidunt egestas, consequat luctus, leo. Nam at erat vel ante venenatis bibendum. Aenean sollicitudin feugiat dui. Etiam sit amet erat. Vestibulum fermentum, odio at varius dignissim, nulla augue sodales nisi, et tempor nunc est at sem. Mauris odio sapien, venenatis ut, laoreet consectetur, porttitor ac, purus. Etiam porttitor diam a pede. Pellentesque eleifend magna nec erat. Nullam dapibus viverra turpis. Mauris non purus non mauris porta sodales. Vivamus mi nibh, vehicula vel, adipiscing eu, auctor eleifend, libero. Etiam sagittis, urna in semper elementum, odio est malesuada ligula, eu luctus quam risus vulputate sapien.</p> 
					</div>
			</div>
			</li>
			<li class="even">
			<div class="message">
					<span class="timestamp" title="1970-01-01T00:00:00Z"><span class="month">Jan</span> <span class="day">1</span> <span class="time">12:00 am</span></span>
					<button class="delete-button">trash</button>
					<div class="text">
						<a href="@vt.edu" class="author url fn" title="From">Some Body</a>
						<p>Pellentesque nisl felis, egestas porttitor, ullamcorper ut, egestas eu, felis. Nullam semper, nibh vitae convallis dictum, est risus tempus dui, eget vulputate urna odio non augue. Integer sagittis dictum ante. Morbi at diam. Ut eget lacus et risus viverra suscipit. Donec massa ligula, laoreet eu, tincidunt egestas, consequat luctus, leo. Nam at erat vel ante venenatis bibendum. Aenean sollicitudin feugiat dui. Etiam sit amet erat. Vestibulum fermentum, odio at varius dignissim, nulla augue sodales nisi, et tempor nunc est at sem. Mauris odio sapien, venenatis ut, laoreet consectetur, porttitor ac, purus. Etiam porttitor diam a pede. Pellentesque eleifend magna nec erat. Nullam dapibus viverra turpis. Mauris non purus non mauris porta sodales. Vivamus mi nibh, vehicula vel, adipiscing eu, auctor eleifend, libero. Etiam sagittis, urna in semper elementum, odio est malesuada ligula, eu luctus quam risus vulputate sapien. Pellentesque nisl felis, egestas porttitor, ullamcorper ut, egestas eu, felis. Nullam semper, nibh vitae convallis dictum, est risus tempus dui, eget vulputate urna odio non augue. Integer sagittis dictum ante. Morbi at diam. Ut eget lacus et risus viverra suscipit. Donec massa ligula, laoreet eu, tincidunt egestas, consequat luctus, leo. Nam at erat vel ante venenatis bibendum. Aenean sollicitudin feugiat dui. Etiam sit amet erat. Vestibulum fermentum, odio at varius dignissim, nulla augue sodales nisi, et tempor nunc est at sem. Mauris odio sapien, venenatis ut, laoreet consectetur, porttitor ac, purus. Etiam porttitor diam a pede. Pellentesque eleifend magna nec erat. Nullam dapibus viverra turpis. Mauris non purus non mauris porta sodales. Vivamus mi nibh, vehicula vel, adipiscing eu, auctor eleifend, libero. Etiam sagittis, urna in semper elementum, odio est malesuada ligula, eu luctus quam risus vulputate sapien.</p> 
					</div>
			</div>
			</li>
			<li class="odd">
			<div class="message">
					<span class="timestamp" title="1970-01-01T00:00:00Z"><span class="month">Jan</span> <span class="day">1</span> <span class="time">12:00 am</span></span>
					<button class="delete-button">trash</button>
					<div class="text">
						<a href="@vt.edu" class="author url fn" title="From">Some Body</a>
						<p>Pellentesque nisl felis, egestas porttitor, ullamcorper ut, egestas eu, felis. Nullam semper, nibh vitae convallis dictum, est risus tempus dui, eget vulputate urna odio non augue.</p> 
					</div>
			</div>
			</li>
		</ol>
		</div>
