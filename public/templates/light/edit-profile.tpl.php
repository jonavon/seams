<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
// $profile['pid'] = '{profile pid}';
// $profile['firstname'] = '{profile firstname}';
//$profile['middlename'] = '{profile middlename}';
// $profile['lastname'] = '{profile lastname}';
// $profile['vtid'] = '{profile virginia tech id}';
// $profile['contact']['local']['address'] = '{profile local address}';
// $profile['contact']['local']['phone'] = '{profile local phone}';
//$profile['birthday'] = '{profile day of birth}';
// $profile['major'] = '{profile academic major}';
// $profile['class'] = '{profile academic class}';
// $majors = array(); // Majors list
?>
 			<div id="profile-form">
<?php if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission)): ?>
				<a class="button" href="./profile/delete/<?php echo $objid; ?>" title="Delete this Profile"><span>Delete</span></a>
<?php endif; ?>
				<form id="pform" method="post" action="./profile/submit/<?php echo $action; ?>">
					<fieldset>
						<legend>Personal Information</legend>
						<p>
							<label for="pid">PID</label>
							<input type="text" name="pid" id="pid" value="<?php echo strtolower($profile['pid']); ?>" <?php if(!empty($profile['pid'])): ?>readonly="readonly"<?php else:?>class="required alphanumeric"<?php endif;?> maxlength="8" />
						</p>
						<p>
							<label for="firstname" class="required">First Name</label>
							<input type="text" name="firstname" id="firstname" class="required" value="<?php echo $profile['firstname']; ?>" maxlength="32" />
						</p>
						<p>
							<label for="middlename">Middle Name</label>
							<input type="text" name="middlename" id="middlename" value="<?php echo $profile['middlename']; ?>" maxlength="32" />
						</p>
						<p>
							<label for="lastname" class="required">Last Name</label>
							<input type="text" name="lastname" id="lastname" class="required" value="<?php echo $profile['lastname']; ?>" maxlength="32" />
						</p>
						<p>
							<label for="vtid" class="required">Virginia Tech ID Number</label>
							<input type="text" name="vtid" id="vtid" class="required number" value="<?php echo $profile['vtid']; ?>" maxlength="9" />
						</p>
						<fieldset>
						<legend>Contact Information</legend>
						<p>
							<label for="address" class="required">Local Address</label>
							<textarea name="contact&#91;SCHOOL&#93;&#91;STREET&#93;" id="address" class="required" rows="4" cols="40"><?php echo $profile['contact']['SCHOOL']['STREET']; ?></textarea>
						</p>
						<p>
							<label for="phone" class="required">Local Phone No</label>
							<input type="text" name="contact&#91;SCHOOL&#93;&#91;PHONE&#93;" id="phone" class="required phone" value="<?php echo $profile['contact']['SCHOOL']['PHONE']; ?>" />
						</p>
						</fieldset>
<?php if(!SEAMS::isPermitted(array(Profile::SUPERVISOR_LEVEL,Profile::ADMIN_LEVEL),$permission)): ?>
<?php if(!SEAMS::isPermitted(array(Profile::SUPERVISOR_LEVEL,Profile::ADMIN_LEVEL),$profile['permission'])): ?>
						<fieldset id="stat-info">
						<legend>Statistical Information</legend>
						<p>
							<label for="birthday" class="required">Date of Birth</label>
							<input type="text" name="birthday" id="birthday" class="required date" value="<?php echo (!empty($profile['birthday']))?date('F j, Y',strtotime($profile['birthday'])):date('F j, Y',( time() - (60 * 60 * 24 * 365 * 18) )); ?>" />
						</p>
						<p>
							<label for="major" class="required">Academic Major</label>
							<select name="major" id="major" class="required">
								<option value="">-- Please Select --</option>
<?php foreach($majors as $major): ?>
								<option value="<?php echo $major['id']; ?>"<?php if((strtolower($profile['major'])===strtolower($major['label'])) || ($profile['major'] === $major['id'])): ?> selected="selected"<?php endif; ?>><?php echo htmlentities($major['description'], ENT_QUOTES); ?> [<?php echo $major['label']; ?>]</option>
<?php endforeach; ?>
							</select>
						</p>
						<p>
							<label for="class" class="required">Academic Class</label>
							<select name="class" id="class" class="required">
								<option value="">-- Please Select --</option>
								<option value="FRESHMAN"<?php if($profile['class']==='FRESHMAN'):?> selected="selected"<?php endif; ?>>Freshman</option>
								<option value="SOPHOMORE"<?php if($profile['class']==='SOPHOMORE'):?> selected="selected"<?php endif; ?>>Sophomore</option>
								<option value="JUNIOR"<?php if($profile['class']==='JUNIOR'):?> selected="selected"<?php endif; ?>>Junior</option>
								<option value="SENIOR"<?php if($profile['class']==='SENIOR'):?> selected="selected"<?php endif; ?>>Senior</option>
								<option value="GRADUATE"<?php if($profile['class']==='GRADUATE'):?> selected="selected"<?php endif; ?>>Graduate</option>
							</select>
						</p>
							<fieldset id="private-info">
								<legend>Demographical Information</legend>
								<div class="disclaimer">
									<h5>Disclaimer</h5>
									<p>To meet the requirements of federal regulation, please answer the following questions for record keeping purposes.  This information will not be used for making employment decisions.</p>
								</div>
								<p>
									<label for="gender" class="required">Gender</label>
									<select name="gender" id="gender" class="required">
										<option value="">-- Please Select --</option>
										<option value="MALE"<?php if($profile['gender']==='MALE'):?> selected="selected"<?php endif; ?>>Male</option>
										<option value="FEMALE"<?php if($profile['gender']==='FEMALE'):?> selected="selected"<?php endif; ?>>Female</option>
									</select>
								</p>
								<p>
									<label for="ethnicity" class="required">Ethnicity</label>
									<select name="ethnicity" id="ethnicity" class="required">
										<option value="">-- Please Select --</option>
		<?php foreach($ethnicities as $ethnicity): ?>
										<option value="<?php echo $ethnicity['id']; ?>"<?php if((strtolower($profile['ethnicity'])===strtolower($ethnicity['label'])) || ($profile['ethnicity'] === $ethnicity['id'])): ?> selected="selected"<?php endif; ?>><?php echo $ethnicity['label']; ?></option>
		<?php endforeach; ?>
									</select>
								</p>
							</fieldset>
						</fieldset>
<?php endif; ?>
<?php endif; ?>
<?php if(SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission)):?>
						<fieldset>
							<legend>For Admin Only</legend>
							<p>
							<label for="type" class="required">Type</label>
							<select name="type" id="type" class="required">
								<option value="">-- Please Select --</option>
<?php if(($profile['type'])==='APPLICANT'): ?>
								<option value="APPLICANT"<?php if(($profile['type'])==='APPLICANT'): ?> selected="selected"<?php endif; ?>>Applicant</option>
<?php endif; ?>
								<option value="PENDING"<?php if(($profile['type'])==='PENDING'): ?> selected="selected"<?php endif; ?>>Pending Hire</option>
								<option value="EMPLOYEE"<?php if(($profile['type'])==='EMPLOYEE'): ?> selected="selected"<?php endif; ?>>Employee</option>
								<option value="SUPERVISOR"<?php if(($profile['type'])==='SUPERVISOR'): ?> selected="selected"<?php endif; ?>>Supervisor</option>
								<option value="PLUS"<?php if(($profile['type'])==='PLUS'): ?> selected="selected"<?php endif; ?>>Supervisor Plus</option>
								<option value="ADMIN"<?php if(($profile['type'])==='ADMIN'): ?> selected="selected"<?php endif; ?>>Admin</option>
							</select>
							</p>
							<p>
							<label for="unit">Unit</label>
							<select name="unit" id="unit">
								<option value="">-- Please Select --</option>
<?php foreach($units as $unit): ?>
								<option value="<?php echo $unit['id']; ?>"<?php if(strtolower($profile['unit'])===strtolower($unit['label'])): ?> selected="selected"<?php endif; ?>><?php echo htmlspecialchars($unit['label']); ?></option>
<?php endforeach; ?>
							</select>
							</p>
<?php if(SEAMS::isPermitted(array(Profile::PLUS_LEVEL,Profile::SUPERVISOR_LEVEL,Profile::ADMIN_LEVEL),$profile['permission'])): ?>
							<fieldset>
								<legend>Paycodes</legend>
								<ul>
<?php foreach($paycodes as $paycode): ?>
									<li><input class="inline" type="checkbox" id="paycode<?php echo $paycode['id']; ?>" name="paycodes&#91;&#93;" value="<?php echo $paycode['id']; ?>"<?php if(in_array($paycode['id'],$profile['paycodes'])): ?> checked="checked"<?php endif; ?> /><label for="paycode<?php echo $paycode['id']; ?>" class="inline"><?php echo $paycode['label']; ?> <?php echo htmlspecialchars($paycode['description']); ?></label></li>
<?php endforeach; ?>
								</ul>
							</fieldset>
<?php endif; ?>
						</fieldset>
<?php endif; ?>
						<input type="submit" name="submit" value="<?php echo $action; ?> profile" />
					</fieldset>
				</form>
			</div>
