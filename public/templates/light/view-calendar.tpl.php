<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$date = (isset($objid))?strtotime(urldecode($objid)):time();
$today = date('Y-m-j',time());
$month = date('m',$date);
$year = date('Y',$date);
$firstday = mktime(0,0,0,$month,1,$year);
$caption = date('F Y', $firstday);
$numberDays = date('t',$date);
require_once 'lib/iCalcreator.class.php';
$ical = new vcalendar();
$ical->setConfig('unique_id','seas.lib.vt.edu');
$ical->setConfig('url',SEAS_CALENDAR);
$ical->parse();
$ical->sort();
$details = $ical->selectComponents($year,$month,1,$year,$month,$numberDays,'vevent');
?>
		<div id="seas-calendar">
			<div>
				<a  href="./calendar/view/<?php echo date('Y-m',strtotime('-1 month',$date)); ?>-01" title="Previous Month">Previous</a>
				<a  href="./calendar/view/<?php echo date('Y-m',strtotime('+1 month',$date)); ?>-01" title="Next Month">Next</a>
			</div>
			<table summary="A view of the Application Dates for Seas">
				<caption><?php echo $caption; ?></caption>
				<colgroup>
					<col class="Sun"/>
					<col class="Mon"/>
					<col class="Tue"/>
					<col class="Wed"/>
					<col class="Thu"/>
					<col class="Fri"/>
					<col class="Sat"/>
				</colgroup>
				<thead>
					<tr>
						<th scope="col">Sunday</th>
						<th scope="col">Monday</th>
						<th scope="col">Tuesday</th>
						<th scope="col">Wednesday</th>
						<th scope="col">Thursday</th>
						<th scope="col">Friday</th>
						<th scope="col">Saturday</th>
					</tr>
				</thead>
				<tbody>
<?php for($day = -((int)date('w',$firstday));$day < $numberDays;): ?>
					<tr>
<?php for($i=0;$i<7;$day++,$i++):?>
						<td id="day<?php echo "$year-$month-" . ($day+1);?>" <?php if($today == "$year-$month-" . ($day+1)):?>class="today"<?php endif; ?>>
							<div class="day"><span><?php echo ($day < 0 || $day >= $numberDays)?'&nbsp;':$day + 1; ?></span></div>
<?php if(!empty($details[(int)$year][(int)$month][$day+1])): ?>
							<ul class="detail">
<?php
$events = $details[(int)$year][(int)$month][$day+1];
foreach($events as $vevent):
	$summary = $vevent->getProperty('summary');
	$dtstart = $vevent->getProperty('dtstart');
	$dtend = $vevent->getProperty('dtend');
	if(isset($dtstart['hour'])) {
		@$start = "{$dtstart['year']}-{$dtstart['month']}-{$dtstart['day']} {$dtstart['hour']}:{$dtstart['min']}:{$dtstart['sec']}{$dtstart['tz']}";
		@$end = "{$dtend['year']}-{$dtend['month']}-{$dtend['day']} {$dtend['hour']}:{$dtend['min']}:{$dtend['sec']}{$dtend['tz']}";
		$beginning = date('g:i A',strtotime($start));
		$ending = date('g:i A',strtotime($end));
		$timefields = "$beginning to $ending";
	}
	else {
		$timefields = "All Day";
	}
?>							
								<li><?php echo "$summary" ?> <span class="timefield">(<?php echo $timefields;?>)</span></li>
<?php endforeach; ?>
							</ul>
<?php endif; ?>
						</td>
<?php endfor; ?>
					</tr>
<?php endfor; ?>
				</tbody>
			</table>
		</div>
