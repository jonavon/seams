			<div id="aside" class="concol">
				<ul class="nav">
					<li><a href="#">Link 1</a></li>
					<li><a href="#">Link 2</a></li>
					<li class="current"><a href="#">Link 3</a></li>
				</ul>
				<ul class="nav">
					<h3>Header</h3>
					<li><a href="#">Link 4</a></li>
					<li><a href="#">Link 5</a></li>
					<li><a href="#">Link 6</a></li>
				</ul>
			</div>
			<div id="sample-page" class="concol">
				<div id="anchor-samples">
					<ul>
						<li><a href="./sample#headers-samples">Headers</a></li>
						<li><a href="./sample#text-samples">Text</a></li>
						<li><a href="./sample#special-objects">Special Objects</a></li>
						<li><a href="./sample#list-samples">List</a></li>
						<li><a href="./sample#table-samples">Table</a></li>
						<li><a href="./sample#seas-calendar">SEAS Calendar</a></li>
						<li><a href="./sample#availability">Availability</a></li>
						<li><a href="./sample#application-display">Application View</a></li>
						<li><a href="./sample#form-samples">Form</a></li>
					</ul>
				</div>
				<div id="headers-samples">
					<h1>Header 1</h1>
					<p>This is a paragraph after a Level 1 Header.</p>
					<h2>Header 2</h2>
					<p>This is a paragraph after a Level 2 Header.</p>
					<h3>Header 3</h3>
					<p>This is a paragraph after a Level 3 Header.</p>
					<h4>Header 4</h4>
					<p>This is a paragraph after a Level 4 Header.</p>
					<h5>Header 5</h5>
					<p>This is a paragraph after a Level 5 Header.</p>
					<h6>Header 6</h6>
					<p>This is a paragraph after a Level 6 Header.</p>
				</div>
				<hr />
				<div id="text-samples">
					<p>This is just really long paragraph with some Lorem Ipsum Text.Sed scelerisque sagittis lorem. Phasellus sodales. Nulla urna justo, vehicula in, suscipit nec, molestie sed, tellus. Quisque justo. Nam libero leo, elementum in, dapibus a, suscipit vitae, purus. Duis arcu. Integer dignissim fermentum enim. Morbi convallis felis vel nibh. Etiam viverra augue non enim.</p> 
					<blockquote><p>This is a paragraph inside of a blockquote</p></blockquote>
					<address>Address: 1 Simple Way</address>
					<p>This paragraph uses <span>valid</span> <abbr title="eXtensible Hypertext Markup Language">XHTML</abbr>. <strong>Strong markup</strong> is used along with <em>Emphasis on this text</em></p>
					<pre>Preformatted markup                can have a lot of spaces!</pre>
				</div>
				<hr />
				<div id="special-objects">
					<div class="clear tooltips">
						<div class="tip">
							<span>This is a helpful tip.</span>
						</div>
					<!--
						<div class="fg-tooltip ui-widget ui-widget-content ui-corner-all">
							Tooltip content goes here...
							<div class="fg-tooltip-pointer-down ui-widget-content">
								<div class="fg-tooltip-pointer-down-inner"></div>
							</div>
						</div>
					</div>
					<div class="clear tooltips">
						<div class="fg-tooltip fg-tooltip-left ui-widget ui-widget-content ui-corner-all">
							Tooltip content goes here...
							<div class="fg-tooltip-pointer-down ui-widget-content">
								<div class="fg-tooltip-pointer-down-inner"></div>
							</div>
						</div>
					</div>
					<div class="clear tooltips">
						<div class="fg-tooltip fg-tooltip-right ui-widget ui-widget-content ui-corner-all">
							Tooltip content goes here...
							<div class="fg-tooltip-pointer-down ui-widget-content">
								<div class="fg-tooltip-pointer-down-inner"></div>
							</div>
						</div>
						-->
					</div>
					<div class="clear">
						<a class="button" href="./pool/print/12" title="print the applications in this pool"><span>Print Pool</span></a>
					</div>
					<div id="new-applicant">
						<ol>
							<li class="done"><a href="#" title="">Personal Information </a></li>
							<li class="current done"><a href="#" title="">Pool Information </a></li>
							<li class=""><a href="#" title="">Schedule Availability </a></li>
						</ol>
					</div>
					<div  class="important update">
						<p>Paragraph inside an important update.</p>
					</div>


					<p>
						<a class="actionbtn" href="#" title=""><span><strong>Take Action</strong> <em>Use this action button</em></span></a>
						<a class="actionbtn" href="#" title=""><span><strong>Take Action</strong> <em>Use this action button</em></span></a>
					</p>
					<div id="skills-tags" class="clear">
							<span class="">Tags</span>
							<span class="">A lot of Tags</span>
							<span class="chosen">Tags</span>
							<span class="">Tags for you</span>
							<span class="">Tags for me</span>
							<span class="">Tags</span>
							<span class="chosen">Odd ball Tags</span>
							<span class="chosen">Maybe</span>
							<span class="chosen">This belongs</span>
							<span class="chosen">In the form</span>
							<span class="">Section</span>
							<span class="">Oh Well</span>
							<span class="chosen">Tags</span>
						</div>
						<div class="disclaimer">
							<h5>Disclaimer</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut lacinia cursus arcu. Etiam felis. Vestibulum ac libero. Fusce et nisl. Pellentesque metus mauris, tempus id.</p>
						</div>
				</div>
				<hr />
				<div id="list-samples">
					<ul>
						<li>Unordered list 01</li>
						<li>Unordered list 02</li>
						<li>Unordered list 03
							<ul>
								<li>Unordered list inside list level 2</li>
								<li>Unordered list inside list level 2
									<ul>
										<li>Unordered list inside list level 3</li>
										<li>Unordered list inside list level 3</li>
									</ul>
								</li>
							</ul>
						</li>
					</ul>
					<ol>
						<li>Ordered list 01</li>
						<li>Ordered list 02</li>
						<li>Ordered list 03
							<ol>
								<li>Ordered list inside list level 2</li>
								<li>Ordered list inside list level 2
									<ol>
										<li>Ordered list inside list level 3</li>
										<li>Ordered list inside list level 3</li>
									</ol>
								</li>
							</ol>
						</li>
					</ol>
					<dl>
						<dt>Description list title 01</dt>
						<dd>Description list description 01</dd>
						<dt>Description list title 02</dt>
						<dd>Description list description 02</dd>
						<dd>Description list description 03</dd>
					</dl>
				</div>
				<hr />
				<div id="table-samples">
					<table summary="This is a sample table">
						<caption>Sample Table</caption>
						<colgroup>
							<col class="this" />
							<col class="that" />
							<col class="this" />
							<col class="that" />
							<col class="this" />
							<col class="that" />
						</colgroup>
						<thead>
							<tr>
								<th>First</th>
								<th>Second</th>
								<th>Third</th>
								<th>Fourth</th>
								<th>Fifth</th>
								<th>Sixth</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>First</th>
								<th>Second</th>
								<th>Third</th>
								<th>Fourth</th>
								<th>Fifth</th>
								<th>Sixth</th>
							</tr>
						</tfoot>
						<tbody>
							<tr class="odd">
								<td>Row 1 Column 1</td>
								<td>Row 1 Column 2</td>
								<td>Row 1 Column 3</td>
								<td>Row 1 Column 4</td>
								<td>Row 1 Column 5</td>
								<td>Row 1 Column 6</td>
							</tr>
							<tr class="even">
								<td>Row 2 Column 1</td>
								<td>Row 2 Column 2</td>
								<td>Row 2 Column 3</td>
								<td>Row 2 Column 4</td>
								<td>Row 2 Column 5</td>
								<td>Row 2 Column 6</td>
							</tr>
							<tr class="odd">
								<td>Row 3 Column 1</td>
								<td>Row 3 Column 2</td>
								<td>Row 3 Column 3</td>
								<td>Row 3 Column 4</td>
								<td>Row 3 Column 5</td>
								<td>Row 3 Column 6</td>
							</tr>
							<tr class="even">
								<td>Row 4 Column 1</td>
								<td>Row 4 Column 2</td>
								<td>Row 4 Column 3</td>
								<td>Row 4 Column 4</td>
								<td>Row 4 Column 5</td>
								<td>Row 4 Column 6</td>
							</tr>
							<tr class="odd">
								<td>Row 5 Column 1</td>
								<td>Row 5 Column 2</td>
								<td>Row 5 Column 3</td>
								<td>Row 5 Column 4</td>
								<td>Row 5 Column 5</td>
								<td>Row 5 Column 6</td>
							</tr>
							<tr class="even">
								<td>Row 6 Column 1</td>
								<td>Row 6 Column 2</td>
								<td>Row 6 Column 3</td>
								<td>Row 6 Column 4</td>
								<td>Row 6 Column 5</td>
								<td>Row 6 Column 6</td>
							</tr>
						</tbody>
					</table>
				</div>
				<hr />
				<div id="seas-calendar">
					<div>
						<a  href="./calendar/view/2009-01" title="Previous Month">Previous</a>
						<a  href="./calendar/view/2009-03" title="Next Month">Next</a>
					</div>
					<table summary="A view of the Application Dates for Seas">
						<caption>February 2009</caption>

						<colgroup>
							<col class="Sun"/>
							<col class="Mon"/>
							<col class="Tue"/>
							<col class="Wed"/>
							<col class="Thu"/>
							<col class="Fri"/>
							<col class="Sat"/>
						</colgroup>

						<thead>
							<tr>
								<th scope="col">Sunday</th>
								<th scope="col">Monday</th>
								<th scope="col">Tuesday</th>
								<th scope="col">Wednesday</th>
								<th scope="col">Thursday</th>
								<th scope="col">Friday</th>
								<th scope="col">Saturday</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td id="day2009-02-1" >
									<div class="day"><span>1</span></div>

								</td>
								<td id="day2009-02-2" >
									<div class="day"><span>2</span></div>
									<ul class="detail">
									
										<li>Application Day (All Day)</li>
									</ul>
								</td>
								<td id="day2009-02-3" >

									<div class="day"><span>3</span></div>
								</td>
								<td id="day2009-02-4" >
									<div class="day"><span>4</span></div>
								</td>
								<td id="day2009-02-5" >
									<div class="day"><span>5</span></div>

								</td>
								<td id="day2009-02-6" >
									<div class="day"><span>6</span></div>
								</td>
								<td id="day2009-02-7" >
									<div class="day"><span>7</span></div>
								</td>
							</tr>

							<tr>
								<td id="day2009-02-8" >
									<div class="day"><span>8</span></div>
								</td>
								<td id="day2009-02-9" >
									<div class="day"><span>9</span></div>
								</td>
								<td id="day2009-02-10" >

									<div class="day"><span>10</span></div>
									<ul class="detail">
									
										<li>Application Day (All Day)</li>
									</ul>
								</td>
								<td id="day2009-02-11" >
									<div class="day"><span>11</span></div>

								</td>
								<td id="day2009-02-12" >
									<div class="day"><span>12</span></div>
								</td>
								<td id="day2009-02-13" >
									<div class="day"><span>13</span></div>
								</td>
								<td id="day2009-02-14" >

									<div class="day"><span>14</span></div>
								</td>
							</tr>
							<tr>
								<td id="day2009-02-15" >
									<div class="day"><span>15</span></div>
								</td>
								<td id="day2009-02-16" >

									<div class="day"><span>16</span></div>
									<ul class="detail">
									
										<li>Application Day (All Day)</li>
									</ul>
								</td>
								<td id="day2009-02-17" >
									<div class="day"><span>17</span></div>

									<ul class="detail">
									
										<li>Application Day (All Day)</li>
									</ul>
								</td>
								<td id="day2009-02-18" >
									<div class="day"><span>18</span></div>
									<ul class="detail">
									
										<li>Application Day (All Day)</li>

									</ul>
								</td>
								<td id="day2009-02-19" >
									<div class="day"><span>19</span></div>
									<ul class="detail">
									
										<li>Application Day (All Day)</li>
									</ul>
								</td>

								<td id="day2009-02-20" >
									<div class="day"><span>20</span></div>
									<ul class="detail">
									
										<li>Application Day (All Day)</li>
									</ul>
								</td>
								<td id="day2009-02-21" >
									<div class="day"><span>21</span></div>

								</td>
							</tr>
							<tr>
								<td id="day2009-02-22" >
									<div class="day"><span>22</span></div>
								</td>
								<td id="day2009-02-23" >
									<div class="day"><span>23</span></div>

									<ul class="detail">
									
										<li>Application Day (All Day)</li>
									</ul>
								</td>
								<td id="day2009-02-24" >
									<div class="day"><span>24</span></div>
									<ul class="detail">
									
										<li>Application Day (All Day)</li>

									</ul>
								</td>
								<td id="day2009-02-25" class="today">
									<div class="day"><span>25</span></div>
								</td>
								<td id="day2009-02-26" >
									<div class="day"><span>26</span></div>
								</td>

								<td id="day2009-02-27" >
									<div class="day"><span>27</span></div>
								</td>
								<td id="day2009-02-28" >
									<div class="day"><span>28</span></div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<hr />
				<div id="availability">
					<div id="schedule-display">
						<table class="week" summary="Week Display">
							<caption>Weekly Availability Display</caption>
							<colgroup>
								<col class="hour" />
								<col class="Sun" />
								<col class="Mon" />
								<col class="Tue" />
								<col class="Wed" />
								<col class="Thu" />
								<col class="Fri" />
								<col class="Sat" />
							</colgroup>
							<thead>
								<tr>
								<th>Time</th>
								<th>Sun</th>
								<th>Mon</th>
								<th>Tue</th>
								<th>Wed</th>
								<th>Thu</th>
								<th>Fri</th>
								<th>Sat</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
								<th>Time</th>
								<th>Sunday</th>
								<th>Monday</th>
								<th>Tuesday</th>
								<th>Wednesday</th>
								<th>Thursday</th>
								<th>Friday</th>
								<th>Saturday</th>
								</tr>
							</tfoot>
							<tbody>
								<tr class="hourincrement">
								<th rowspan="4">01:00</th>
								<td id="a00100" title="Sunday 1:00 am">&nbsp;</td>
								<td id="a10100" title="Monday 1:00 am" class="available" rowspan="26">&nbsp;</td>
								<td id="a20100" title="Tuesday 1:00 am">&nbsp;</td>
								<td id="a30100" title="Wednesday 1:00 am">&nbsp;</td>
								<td id="a40100" title="Thursday 1:00 am">&nbsp;</td>
								<td id="a50100" title="Friday 1:00 am" class="available" rowspan="26">&nbsp;</td>
								<td id="a60100" title="Saturday 1:00 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00115" title="Sunday 1:15 am">&nbsp;</td>
								<td id="a20115" title="Tuesday 1:15 am">&nbsp;</td>
								<td id="a30115" title="Wednesday 1:15 am">&nbsp;</td>
								<td id="a40115" title="Thursday 1:15 am">&nbsp;</td>
								<td id="a60115" title="Saturday 1:15 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00130" title="Sunday 1:30 am">&nbsp;</td>
								<td id="a20130" title="Tuesday 1:30 am">&nbsp;</td>
								<td id="a30130" title="Wednesday 1:30 am">&nbsp;</td>
								<td id="a40130" title="Thursday 1:30 am">&nbsp;</td>
								<td id="a60130" title="Saturday 1:30 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00145" title="Sunday 1:45 am">&nbsp;</td>
								<td id="a20145" title="Tuesday 1:45 am">&nbsp;</td>
								<td id="a30145" title="Wednesday 1:45 am">&nbsp;</td>
								<td id="a40145" title="Thursday 1:45 am">&nbsp;</td>
								<td id="a60145" title="Saturday 1:45 am">&nbsp;</td>
								</tr>
								<tr class="hourincrement">
								<th rowspan="4">02:00</th>
								<td id="a00200" title="Sunday 2:00 am">&nbsp;</td>
								<td id="a20200" title="Tuesday 2:00 am">&nbsp;</td>
								<td id="a30200" title="Wednesday 2:00 am">&nbsp;</td>
								<td id="a40200" title="Thursday 2:00 am">&nbsp;</td>
								<td id="a60200" title="Saturday 2:00 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00215" title="Sunday 2:15 am">&nbsp;</td>
								<td id="a20215" title="Tuesday 2:15 am">&nbsp;</td>
								<td id="a30215" title="Wednesday 2:15 am">&nbsp;</td>
								<td id="a40215" title="Thursday 2:15 am">&nbsp;</td>
								<td id="a60215" title="Saturday 2:15 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00230" title="Sunday 2:30 am">&nbsp;</td>
								<td id="a20230" title="Tuesday 2:30 am">&nbsp;</td>
								<td id="a30230" title="Wednesday 2:30 am">&nbsp;</td>
								<td id="a40230" title="Thursday 2:30 am">&nbsp;</td>
								<td id="a60230" title="Saturday 2:30 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00245" title="Sunday 2:45 am">&nbsp;</td>
								<td id="a20245" title="Tuesday 2:45 am">&nbsp;</td>
								<td id="a30245" title="Wednesday 2:45 am">&nbsp;</td>
								<td id="a40245" title="Thursday 2:45 am">&nbsp;</td>
								<td id="a60245" title="Saturday 2:45 am">&nbsp;</td>
								</tr>
								<tr class="hourincrement">
								<th rowspan="4">03:00</th>
								<td id="a00300" title="Sunday 3:00 am">&nbsp;</td>
								<td id="a20300" title="Tuesday 3:00 am">&nbsp;</td>
								<td id="a30300" title="Wednesday 3:00 am">&nbsp;</td>
								<td id="a40300" title="Thursday 3:00 am">&nbsp;</td>
								<td id="a60300" title="Saturday 3:00 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00315" title="Sunday 3:15 am">&nbsp;</td>
								<td id="a20315" title="Tuesday 3:15 am">&nbsp;</td>
								<td id="a30315" title="Wednesday 3:15 am">&nbsp;</td>
								<td id="a40315" title="Thursday 3:15 am">&nbsp;</td>
								<td id="a60315" title="Saturday 3:15 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00330" title="Sunday 3:30 am">&nbsp;</td>
								<td id="a20330" title="Tuesday 3:30 am">&nbsp;</td>
								<td id="a30330" title="Wednesday 3:30 am">&nbsp;</td>
								<td id="a40330" title="Thursday 3:30 am">&nbsp;</td>
								<td id="a60330" title="Saturday 3:30 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00345" title="Sunday 3:45 am">&nbsp;</td>
								<td id="a20345" title="Tuesday 3:45 am">&nbsp;</td>
								<td id="a30345" title="Wednesday 3:45 am">&nbsp;</td>
								<td id="a40345" title="Thursday 3:45 am">&nbsp;</td>
								<td id="a60345" title="Saturday 3:45 am">&nbsp;</td>
								</tr>
								<tr class="hourincrement">
								<th rowspan="4">04:00</th>
								<td id="a00400" title="Sunday 4:00 am">&nbsp;</td>
								<td id="a20400" title="Tuesday 4:00 am">&nbsp;</td>
								<td id="a30400" title="Wednesday 4:00 am">&nbsp;</td>
								<td id="a40400" title="Thursday 4:00 am">&nbsp;</td>
								<td id="a60400" title="Saturday 4:00 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00415" title="Sunday 4:15 am">&nbsp;</td>
								<td id="a20415" title="Tuesday 4:15 am">&nbsp;</td>
								<td id="a30415" title="Wednesday 4:15 am">&nbsp;</td>
								<td id="a40415" title="Thursday 4:15 am">&nbsp;</td>
								<td id="a60415" title="Saturday 4:15 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00430" title="Sunday 4:30 am">&nbsp;</td>
								<td id="a20430" title="Tuesday 4:30 am">&nbsp;</td>
								<td id="a30430" title="Wednesday 4:30 am">&nbsp;</td>
								<td id="a40430" title="Thursday 4:30 am">&nbsp;</td>
								<td id="a60430" title="Saturday 4:30 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00445" title="Sunday 4:45 am">&nbsp;</td>
								<td id="a20445" title="Tuesday 4:45 am">&nbsp;</td>
								<td id="a30445" title="Wednesday 4:45 am">&nbsp;</td>
								<td id="a40445" title="Thursday 4:45 am">&nbsp;</td>
								<td id="a60445" title="Saturday 4:45 am">&nbsp;</td>
								</tr>
								<tr class="hourincrement">
								<th rowspan="4">05:00</th>
								<td id="a00500" title="Sunday 5:00 am">&nbsp;</td>
								<td id="a20500" title="Tuesday 5:00 am">&nbsp;</td>
								<td id="a30500" title="Wednesday 5:00 am">&nbsp;</td>
								<td id="a40500" title="Thursday 5:00 am">&nbsp;</td>
								<td id="a60500" title="Saturday 5:00 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00515" title="Sunday 5:15 am">&nbsp;</td>
								<td id="a20515" title="Tuesday 5:15 am">&nbsp;</td>
								<td id="a30515" title="Wednesday 5:15 am">&nbsp;</td>
								<td id="a40515" title="Thursday 5:15 am">&nbsp;</td>
								<td id="a60515" title="Saturday 5:15 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00530" title="Sunday 5:30 am">&nbsp;</td>
								<td id="a20530" title="Tuesday 5:30 am">&nbsp;</td>
								<td id="a30530" title="Wednesday 5:30 am">&nbsp;</td>
								<td id="a40530" title="Thursday 5:30 am">&nbsp;</td>
								<td id="a60530" title="Saturday 5:30 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00545" title="Sunday 5:45 am">&nbsp;</td>
								<td id="a20545" title="Tuesday 5:45 am">&nbsp;</td>
								<td id="a30545" title="Wednesday 5:45 am">&nbsp;</td>
								<td id="a40545" title="Thursday 5:45 am">&nbsp;</td>
								<td id="a60545" title="Saturday 5:45 am">&nbsp;</td>
								</tr>
								<tr class="hourincrement">
								<th rowspan="4">06:00</th>
								<td id="a00600" title="Sunday 6:00 am">&nbsp;</td>
								<td id="a20600" title="Tuesday 6:00 am">&nbsp;</td>
								<td id="a30600" title="Wednesday 6:00 am">&nbsp;</td>
								<td id="a40600" title="Thursday 6:00 am">&nbsp;</td>
								<td id="a60600" title="Saturday 6:00 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00615" title="Sunday 6:15 am">&nbsp;</td>
								<td id="a20615" title="Tuesday 6:15 am">&nbsp;</td>
								<td id="a30615" title="Wednesday 6:15 am">&nbsp;</td>
								<td id="a40615" title="Thursday 6:15 am">&nbsp;</td>
								<td id="a60615" title="Saturday 6:15 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00630" title="Sunday 6:30 am">&nbsp;</td>
								<td id="a20630" title="Tuesday 6:30 am">&nbsp;</td>
								<td id="a30630" title="Wednesday 6:30 am">&nbsp;</td>
								<td id="a40630" title="Thursday 6:30 am">&nbsp;</td>
								<td id="a60630" title="Saturday 6:30 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00645" title="Sunday 6:45 am">&nbsp;</td>
								<td id="a20645" title="Tuesday 6:45 am">&nbsp;</td>
								<td id="a30645" title="Wednesday 6:45 am">&nbsp;</td>
								<td id="a40645" title="Thursday 6:45 am">&nbsp;</td>
								<td id="a60645" title="Saturday 6:45 am">&nbsp;</td>
								</tr>
								<tr class="hourincrement">
								<th rowspan="4">07:00</th>
								<td id="a00700" title="Sunday 7:00 am">&nbsp;</td>
								<td id="a20700" title="Tuesday 7:00 am">&nbsp;</td>
								<td id="a30700" title="Wednesday 7:00 am">&nbsp;</td>
								<td id="a40700" title="Thursday 7:00 am">&nbsp;</td>
								<td id="a60700" title="Saturday 7:00 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00715" title="Sunday 7:15 am">&nbsp;</td>
								<td id="a20715" title="Tuesday 7:15 am">&nbsp;</td>
								<td id="a30715" title="Wednesday 7:15 am">&nbsp;</td>
								<td id="a40715" title="Thursday 7:15 am">&nbsp;</td>
								<td id="a60715" title="Saturday 7:15 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00730" title="Sunday 7:30 am">&nbsp;</td>
								<td id="a10730" title="Monday 7:30 am">&nbsp;</td>
								<td id="a20730" title="Tuesday 7:30 am">&nbsp;</td>
								<td id="a30730" title="Wednesday 7:30 am">&nbsp;</td>
								<td id="a40730" title="Thursday 7:30 am">&nbsp;</td>
								<td id="a50730" title="Friday 7:30 am">&nbsp;</td>
								<td id="a60730" title="Saturday 7:30 am">&nbsp;</td>
								</tr>
								<tr>
								<td id="a00745" title="Sunday 7:45 am">&nbsp;</td>
								<td id="a10745" title="Monday 7:45 am">&nbsp;</td>
								<td id="a20745" title="Tuesday 7:45 am">&nbsp;</td>
								<td id="a30745" title="Wednesday 7:45 am">&nbsp;</td>
								<td id="a40745" title="Thursday 7:45 am">&nbsp;</td>
								<td id="a50745" title="Friday 7:45 am">&nbsp;</td>
								<td id="a60745" title="Saturday 7:45 am">&nbsp;</td>
								</tr>
							</tbody>
						</table>
						<table summary="List of available times">
							<caption>Availability Times</caption>
							<thead>
							<tr>
							<th>Day</th>
							<th>Start</th>
							<th>End</th>
							</tr>
							</thead>
							<tfoot>
							<tr>
							<th>Day</th>
							<th>Start</th>
							<th>End</th>
							</tr>
							</tfoot>
							<tbody>
							<tr>
							<td>Monday</td>
							<td>1:00 am</td>
							<td>7:22 am</td>
							</tr>
							<tr>
							<td>Friday</td>
							<td>1:00 am</td>
							<td>7:22 am</td>
							</tr>
							</tbody>
						</table>
					</div>
				</div>
				<hr />
				<div id="application-display">
					<p id="application-id">
						<span class="field">Application ID</span>
						<span class="value">3kGlDiVf&nbsp;</span>
					</p>
					<p id="expiration">
						<span class="field">Expires</span>
						<span class="value">April 5, 2009&nbsp;</span>
					</p>
					<h3><a href="#" title="">Personal Information</a></h3>
					<p id="name">
						<span class="field">Name</span>
						<span class="value">Wilcox, Jonavon&nbsp;</span>
					</p>
					<p id="vtid">
						<span class="field">Student ID Number</span>
						<span class="value">904604620&nbsp;</span>
					</p>
				</div>
				<hr />
				<div id="form-samples">
					<form method="post" action="./">
						<fieldset>
							<legend>Legend</legend>
							<div>
								<button class="large awesome red">Complete and Verify this Application</button>
							</div>
							<p>
								<label for="text-input">Text Input</label>
								<input id="text-input" name="text-input" type="text" value="Text Input" />
							</p>
							<p>
								<label for="error-input">Error Input</label>
								<input id="error-input" name="error-input" type="text" value="Text Input" class="error" />
								<label class="error" for="error-input">This is a generated error label for this input field.</label>
							</p>
							<p>
								<label for="password-input">Password Input</label>
								<input id="password-input" name="password-input" type="password" value="Password Input" />
							</p>
							<p>
								<label for="inline-text-input" class="inline">Inline Text</label>
								<input id="inline-text-input" name="inline-text-input" type="text" />
								<label for="inline-text-input1" class="inline">Inline Text</label>
								<input id="inline-text-input1" name="inline-text-input1" type="text" />
							</p>
							<p>
								<label for="inline-radio1-input" class="inline">Inline Radio</label>
								<input id="inline-radio1-input" name="radio-input" type="radio" value="1" />
								<label for="inline-radio2-input" class="inline">Inline Radio</label>
								<input id="inline-radio2-input" name="radio-input" type="radio" value="2" />
							</p>
							<p>
								<label for="radio1-input">Radio 1 Input</label>
								<input id="radio1-input" name="radio-input" type="radio" value="1" />
								<label for="radio2-input">Radio 2 Input</label>
								<input id="radio2-input" name="radio-input" type="radio" value="2" />
							</p>
							<p>
								<label for="checkbox1-input">checkbox 1 Input</label>
								<input id="checkbox1-input" name="checkbox1-input" type="checkbox" value="1" />
								<label for="checkbox2-input">checkbox 2 Input</label>
								<input id="checkbox2-input" name="checkbox2-input" type="checkbox" value="2" />
							</p>
							<p>
								<label for="select-input">Select Box</label>
								<select id="select-input" name="select-input">
									<optgroup label="1 thru 3">
										<option value="1">Option 1</option>
										<option value="2">Option 2</option>
										<option value="3">Option 3</option>
									</optgroup>
									<option value="4">Option 4</option>
									<option value="5">Option 5</option>
									<option value="6">Option 6</option>
								</select>
							</p>
							<p>
								<label for="textarea-input">Textarea Input</label>
								<textarea id="textarea-input" name="textarea-input" rows="5" cols="50">This is a textarea.</textarea>
							</p>
							<input type="submit" />
							<input type="reset" />
						</fieldset>
					</form>
				</div>
			</div>
