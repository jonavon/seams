<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
$jobcount = count($jobs);
?>
				<div id="job-table">
					<h4>Pool Information</h4>
					<p><a href="./job/add" title="Add Additional Jobs">Create a New Pool</a></p>
<?php if($jobcount > 0): ?>
					<table class="searchable" summary="Job Information">
						<thead>
							<tr>
								<th scope="col">Title</th>
								<th scope="col"># Employees</th>
								<th scope="col"># Candidates</th>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th scope="col">Title</th>
								<th scope="col"># Employees</th>
								<th scope="col"># Candidates</th>
							</tr>
						</tfoot>
						<tbody>
<?php foreach($jobs as $j): ?>
							<tr>
								<td scope="col"><a href="./job/view/<?php echo $j['id']; ?>" title="view this job"><?php echo $j['label']; ?></a></td>
								<td scope="col"><?php echo count($j['positions']); ?></td>
								<td scope="col"><?php echo count($j['pool']); ?></td>
							</tr>
<?php endforeach; ?>
						</tbody>
					</table>
<?php endif; ?>
				</div>
