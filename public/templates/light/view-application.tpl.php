<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
/*
$profile['pid'] = '{profile pid}';
$profile['firstname'] = '{profile firstname}';
$profile['lastname'] = '{profile lastname}';
$profile['vtid'] = '{profile virginia tech id}';
$profile['contact']['local']['address'] = '{profile local address}';
$profile['contact']['local']['phone'] = '{profile local phone}';
$profile['birthdate'] = '{profile day of birth}';
$profile['major'] = '{profile academic major}';
$profile['class'] = '{profile academic class}';
*/
/*
$application['id'] = '{application id}';
$application['expire'] = '{application expiration date}';
$application['content']['workstudy'] = '{application workstudy}';
$application['content']['citizen'] = false;
$application['content']['library_experience'] = '{application answer library experience}';
$application['content']['other_experience'] = '{application answer other experience}';
$application['content']['work_reference_1_name'] = '{application answer work reference name}';
$application['content']['work_reference_1_phone'] = '{application answer work reference phone}';
$application['content']['work_reference_1_notes'] = '{application answer work reference notes}';
$application['content']['work_reference_2_name'] = '{application answer work reference name}';
$application['content']['work_reference_2_phone'] = '{application answer work reference phone}';
$application['content']['work_reference_2_notes'] = '{application answer work reference notes}';
$application['content']['work_reference_3_name'] = '{application answer work reference name}';
$application['content']['work_reference_3_phone'] = '{application answer work reference phone}';
$application['content']['work_reference_3_notes'] = '{application answer work reference notes}';
$application['content']['another_department'] = '{application answer other department}';
$application['content']['citizen'] = true;
$application['content']['break_worker'] = true;
$application['content']['lift50'] = false;
$application['content']['transportation'] = true;
*/
?>
<?php if((count($jobs) > 0) && !isset($_REQUEST['hireid']) && (SEAMS::isPermitted(array(Profile::SUPERVISOR_LEVEL,Profile::PLUS_LEVEL, Profile::ADMIN_LEVEL),$permission))): ?>
			<form method="post" action="./job/submit">
				<fieldset>
					<legend>Pool Candidate</legend>
					<p><a href="./job/add" title="Add a Pool">Create a New Pool</a></p>
					<fieldset>
						<legend>Which Pool?</legend>
						<input type="hidden" name="applications&#91;&#93;" value="<?php echo $application['id']; ?>" />
						<label for="job">Your Pool</label>
						<select id="job" name="job">
<?php if(count($jobs) === 0):?>
							<option value="">New Pool</option>
<?php endif;?>
<?php foreach($jobs as $job): ?>
							<option value="<?php echo $job['id']; ?>"><?php echo $job['label']; ?></option>
<?php endforeach; ?>
						</select>
						<input type="submit" name="submit" value="submit" />
					</fieldset>
<?php $applications=(isset($applications))?$applications:null; $_apptotal = count($applications); if($_apptotal > 0):	?>				
					<table class="searchable" summary="List of user applications">
						<caption>
							<?php echo $_apptotal; ?> Applications
<?php if(isset($scheduleToHuman)): ?>
								<br/>
								<span> Schedule <?php echo $scheduleToHuman['text']; ?> Duration: <?php echo Duration::toString(((int)$durationInSeconds)); ?></span>
<?php endif; ?>
<?php if(isset($searchtext)): ?>
								<br/>
								<span> Search Text: <?php echo $searchtext; ?> </span>
<?php endif; ?>
						</caption>
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Name</th>
								<th>Application Status</th>
								<th>In another pool</th>
<?php if(isset($applications[0]['overlap'])): ?>
								<th>Overlap</th>
<?php endif; ?>
<?php if(isset($applications[0]['snippit'])):?>
								<th>Snippet</th>
<?php endif; ?>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>&nbsp;</th>
								<th>Name</th>
								<th>Application Status</th>
								<th>In another pool</th>
<?php if(isset($applications[0]['overlap'])): ?>
								<th>Overlap</th>
<?php endif; ?>
<?php if(isset($applications[0]['snippit'])):?>
								<th>Snippet</th>
<?php endif; ?>
							</tr>
						</tfoot>
						<tbody>
<?php foreach($applications as $application): ?>
							<tr>
								<td><input type="checkbox" name="applications&#91;&#93;" value="<?php echo $application['id']; ?>" /></td>
								<td><a href="./application/view/<?php echo $application['id']; ?>" title="Application of <?php echo $application['name']; ?>" rel="external"><?php echo $application['name']; ?></a></td>
								<td><?php echo ($application['expire'] > $lastperiod)?'current':'last'; ?></td>
								<td><?php echo ((int)$application['pooled'] > 1)?'yes':'no'; ?></td>
<?php if(isset($applications[0]['overlap'])): ?>
								<td><?php echo round(((int)$application['overlap'] / (int)$totalselected) * 100) . '%'; ?></td>
<?php endif; ?>
<?php if(isset($applications[0]['snippit'])):?>
								<td><?php echo preg_replace('/(\w+):/','<br /><em>$1</em>: ',htmlentities(stripslashes($application['snippit']))); ?></td>
<?php endif; ?>
							</tr>
<?php endforeach; ?>
						</tbody>
					</table>
<?php endif; ?>
				</fieldset>
			</form>
<?php endif; ?>

		<div id="application-display">
			<div class="clear">
				<a class="button" href="./application/pdf/<?php echo $application['id']; ?>" title="Print this Application" rel="external"><span>Print</span></a>
<?php if(SEAMS::isPermitted(array(Profile::SUPERVISOR_LEVEL, Profile::PLUS_LEVEL, Profile::ADMIN_LEVEL),$permission)): ?>
				<a class="button" href="./application/suppress/<?php echo $application['id']; ?>" title="Suppress this Application From Result set"><span>Suppress</span></a>
<?php endif; ?>
<?php if(isset($_REQUEST['hireid'])):?>
				<a class="button" href="./profile/hire/<?php echo $profile['pid']; ?>/<?php echo $_REQUEST['hireid'];?>" title="Hire applicant to Job"><span>Hire</span></a>
<?php endif; ?>
<?php if($_SESSION['pid'] === $profile['pid']):?>
				<a class="button" href="./application/delete/<?php echo $application['id']; ?>" title="Delete this Application"><span>Delete</span></a>
<?php endif; ?>
			</div>
<!--
			<p id="application-id">
				<span class="field">Application ID</span>
				<span class="value"><?php echo $application['id']; ?>&nbsp;</span>
			</p>
			<p id="expiration">
				<span class="field">Expires</span>
				<span class="value"><?php echo date('F j, Y',$application['expire']); ?>&nbsp;</span>
			</p>
-->
			<p id="modified">
				<span class="field">Last Modified</span>
				<span class="value"><?php echo date('F j, Y',$application['modified']); ?>&nbsp;</span>
			</p>
			<h3>
<?php if((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission))||($_SESSION['pid'] === $profile['pid'])): ?>
			<a href="./profile/edit/<?php echo $profile['pid']; ?>" title="Email for this application">
<?php endif; ?>
			Personal Information
<?php if((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission))||($_SESSION['pid'] === $profile['pid'])): ?>
			<span>[Edit]</span>
			</a> 
<?php endif; ?>
			</h3>
			<p id="name">
				<span class="field">Name</span>
				<span class="value"><?php echo "{$profile['lastname']}, {$profile['firstname']}"; ?>&nbsp;</span>
			</p>
			<p id="vtid">
				<span class="field">Student ID Number</span>
				<span class="value"><?php echo $profile['vtid']; ?>&nbsp;</span>
			</p>
			<p id="email">
				<span class="field">Email Address</span>
				<span class="value"><a href="mailto:<?php echo $profile['pid']; ?>@vt.edu" title="Email for this application"><?php echo $profile['pid']; ?>@vt.edu</a></span>
			</p>
			<p id="phone">
				<span class="field">Phone Number</span>
				<span class="value"><?php echo $profile['contact']['SCHOOL']['PHONE']; ?>&nbsp;</span>
			</p>
			<p id="academic-class">
				<span class="field">Academic Class</span>
				<span class="value"><?php echo $profile['class']; ?>&nbsp;</span>
			</p>
			<p id="academic-major">
				<span class="field">Academic Major</span>
				<span class="value"><?php echo $profile['major']; ?>&nbsp;</span>
			</p>
			<h3>
<?php if((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission))||($_SESSION['pid'] === $profile['pid'])): ?>
			<a href="./application/edit/<?php echo $application['id']; ?>" title="Edit this application">
<?php endif; ?>
				Employment Information
<?php if((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission))||($_SESSION['pid'] === $profile['pid'])): ?>
			<span>[Edit]</span>
			</a>
<?php endif; ?>
			</h3>
			<p id="workstudy">
				<span class="field">Workstudy</span>
				<span class="value">$<?php echo (empty($application['content']['workstudy']['answer']))?0:$application['content']['workstudy']['answer']; ?></span>
			</p>
			<p id="hours">
				<span class="field">Hours per week</span>
				<span class="value"><?php echo $application['content']['hours']['answer']; ?>&nbsp;</span>
			</p>
			<p id="another-department">
				<span class="field">Other department Employed</span>
				<span class="value"><?php echo $application['content']['another_department']['answer']; ?>&nbsp;</span>
			</p>
			<p id="library-experience">
				<span class="field">Library Experience</span>
				<span class="value"><?php echo $application['content']['library_experience']['answer']; ?>&nbsp;</span>
			</p>
			<p id="other-experience">
				<span class="field">Other Relevant Experience</span>
				<span class="value"><?php echo $application['content']['other_experience']['answer']; ?>&nbsp;</span>
			</p>
			<div id="skillset">
				<h4>Skills</h4>
				<ul>
<?php $skills = explode(", ",$application['content']['skills']['answer']); foreach($skills as $skill): ?>
					<li><?php echo $skill; ?></li>
<?php endforeach; ?>
				</ul>
			<div class="clear">&nbsp;</div>
			</div>
			<p id="break" class="check-box">
				<span class="field">Work During University Breaks</span>
				<span class="value"><?php echo ($application['content']['break_worker']['answer'])?"X":"&nbsp;"; ?>&nbsp;</span>
			</p>
			<p id="lift50" class="check-box">
				<span class="field">Able to lift 50 pounds</span>
				<span class="value"><?php echo ($application['content']['lift50']['answer'])?"X":"&nbsp;"; ?>&nbsp;</span>
			</p>
			<p id="transportation" class="check-box">
				<span class="field">Transportation</span>
				<span class="value"><?php echo ($application['content']['transportation']['answer'])?"X":"&nbsp;"; ?>&nbsp;</span>
			</p>
			<div id="references">
			<h4>References</h4>
			<p id="reference-1" class="work-reference">
				<span class="field name">Name</span>
				<span class="value name"><?php echo $application['content']['work_reference_0_name']['answer']; ?>&nbsp;</span>
				<span class="field phone">Phone</span>
				<span class="value phone"><?php echo $application['content']['work_reference_0_phone']['answer']; ?>&nbsp;</span>
				<span class="field notes">Contact Information</span>
				<span class="value notes"><?php echo $application['content']['work_reference_0_notes']['answer']; ?>&nbsp;</span>
			</p>
			<p id="reference-2" class="work-reference">
				<span class="field name">Name</span>
				<span class="value name"><?php echo $application['content']['work_reference_1_name']['answer']; ?>&nbsp;</span>
				<span class="field phone">Phone</span>
				<span class="value phone"><?php echo $application['content']['work_reference_1_phone']['answer']; ?>&nbsp;</span>
				<span class="field notes">Contact Information</span>
				<span class="value notes"><?php echo $application['content']['work_reference_1_notes']['answer']; ?>&nbsp;</span>
			</p>
			<p id="reference-3" class="work-reference">
				<span class="field name">Name</span>
				<span class="value name"><?php echo $application['content']['work_reference_2_name']['answer']; ?>&nbsp;</span>
				<span class="field phone">Phone</span>
				<span class="value phone"><?php echo $application['content']['work_reference_2_phone']['answer']; ?>&nbsp;</span>
				<span class="field notes">Contact Information</span>
				<span class="value notes"><?php echo $application['content']['work_reference_2_notes']['answer']; ?>&nbsp;</span>
			</p>
			<div class="clear">&nbsp;</div>
			</div>
<?php if(isset($ranges)): ?>
			<div id="availability">
			<h3>
			
<?php if((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission))||($_SESSION['pid'] === $profile['pid'])): ?>
				<a href="./schedule/edit/<?php echo $application['id']; ?>" title="Edit this application schedule">
<?php endif; ?>
					Availability
<?php if((SEAMS::isPermitted(array(Profile::ADMIN_LEVEL),$permission))||($_SESSION['pid'] === $profile['pid'])): ?>
			<span>[Edit]</span>
				</a>
<?php endif; ?>
			</h3>
<?php include 'view-schedule.tpl.php'; ?>
			</div>
<?php endif; ?>
		</div>
