<?php
$vevent = (isset($vevent))?$vevent:null;
?>
		<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
		</div>
	<div id="calendar-event-admin" class="concol">
		<div id="event-edit">
			<form id="event-form" method="post" action="./admin/calendar">
				<fieldset>
					<legend>Event Form</legend>
<?php if(isset($objid) && !isset($_POST['submit'])): ?>					
					<input type="submit" name="delete" value="delete" />
<?php endif; ?>
					<input name="uid" type="hidden" value="<?php echo (isset($_POST['delete']))?null:$vevent['uid']; ?>" />
					<p>
						<label for="summary" class="required">Summary</label>
						<select id="summary" class="required" name="summary">
							<option value="">-- Please Select --</option>
							<option value="Application Day"<?php if($vevent['summary']==='Application Day'): ?> selected="selected"<?php endif;?>>Application Day</option>
							<option value="Hiring Period Begins"<?php if($vevent['summary']==='Hiring Period Begins'): ?> selected="selected"<?php endif;?>>Hiring Period Begins</option>
							<option value="Last Day to Drop Classes"<?php if($vevent['summary']==='Last Day to Drop Classes'): ?> selected="selected"<?php endif;?>>Last Day to Drop Classes</option>
						</select>
					</p>
					<fieldset id="datetime">
					<legend>Date and Time</legend>
						<p>
							<label for="start-date" class="required inlind">Start</label>
							<input id="start-date" class="required date inline" name="start-date" type="text" value="<?php echo (isset($vevent['start']))?date('F j, Y', (int)$vevent['start']):null; ?>" />
							<input id="start-time" class="required time inline" name="start-time" type="text" value="<?php echo (isset($vevent['start']))?date('h:i A', (int)$vevent['start']):null; ?>" />
							<input id="all-day" class="inline" name="all-day" type="checkbox" value="1" <?php if($vevent['allday'] || !isset($vevent)): ?>checked="checked"<?php endif; ?>/>
							<label for="all-day" class="inline">All Day Event</label>
						</p>
						<p>
							<label for="end-date" class="inlind">End</label>
							<input id="end-date" class="date inline" name="end-date" type="text" value="<?php echo (isset($vevent['end']))?date('F j, Y', (int)$vevent['end']):null; ?>" />
							<input id="end-time" class="time inline" name="end-time" type="text" value="<?php echo (isset($vevent['end']))?date('h:i A', (int)$vevent['end']):null; ?>" />
						</p>
					</fieldset>
					<fieldset>
						<legend>Recurrence</legend>
						<p>
							<input id="recurrence" name="recurrence" type="checkbox" value="1" <?php if((bool)$vevent['rrule']): ?>checked="checked"<?php endif; ?>/>
							<label>Enable Recurrence</label>
						</p>
						<fieldset id="rrule">
							<legend>Recurrence Rule</legend>
							<p>
								<label for="freq">Frequency</label>
								<select id="freq" name="freq">
									<option value="">-- Please Select --</option>
									<option value="DAILY" <?php if($vevent['rrule']['FREQ'] === 'DAILY'): ?>selected="selected"<?php endif; ?>>DAILY</option>
									<option value="WEEKLY" <?php if($vevent['rrule']['FREQ'] === 'WEEKLY'): ?>selected="selected"<?php endif; ?>>WEEKLY</option>
									<option value="MONTHLY" <?php if($vevent['rrule']['FREQ'] === 'MONTHLY'): ?>selected="selected"<?php endif; ?>>MONTHLY</option>
									<option value="YEARLY" <?php if($vevent['rrule']['FREQ'] === 'YEARLY'): ?>selected="selected"<?php endif; ?>>YEARLY</option>
								</select>
							</p>
							<div id="recur-daily">
								<p>
									<label for="daily" class="inline">Recur every</label>
									<input id="daily" name="recur-daily&#91;interval&#93;" class="number inline" type="text" value="<?php echo $vevent['rrule']['INTERVAL']; ?>" />
									<label for="daily" class="inline">day(s)</label>
								</p>
							</div>
							<div id="recur-weekly">
<?php
$weeklyBydayArray = array();
if($vevent['rrule']['FREQ'] === 'WEEKLY') {
	foreach($vevent['rrule']['BYDAY'] AS $wd) {
		$weeklyBydayArray[] = $wd['DAY'];
	}
}
?>
								<p>
									<label for="weekly" class="inline">Recur every</label>
									<input id="weekly" name="recur-weekly&#91;interval&#93;" class="number inline" type="text" value="<?php echo $vevent['rrule']['INTERVAL']; ?>" />
									<label for="weekly" class="inline">week(s) on:</label>
								</p>
								
								<p>
									<input id="MONDAY" class="inline" name="recur-weekly&#91;byday&#93;&#91;&#93;&#91;DAY&#93;" type="checkbox" value="MO" <?php if(in_array('MO',$weeklyBydayArray)):?>checked="checked"<?php endif; ?>/>
									<label for="MONDAY" class="inline">Monday</label>

									<input id="TUESDAY" class="inline" name="recur-weekly&#91;byday&#93;&#91;&#93;&#91;DAY&#93;" type="checkbox" value="TU"  <?php if(in_array('TU',$weeklyBydayArray)):?>checked="checked"<?php endif; ?>/>
									<label for="TUESDAY" class="inline">Tuesday</label>

									<input id="WEDNESDAY" class="inline" name="recur-weekly&#91;byday&#93;&#91;&#93;&#91;DAY&#93;" type="checkbox" value="WE"  <?php if(in_array('WE',$weeklyBydayArray)):?>checked="checked"<?php endif; ?>/>
									<label for="WEDNESDAY" class="inline">Wednesday</label>

									<input id="THURSDAY" class="inline"name="recur-weekly&#91;byday&#93;&#91;&#93;&#91;DAY&#93;" type="checkbox" value="TH"  <?php if(in_array('TH',$weeklyBydayArray)):?>checked="checked"<?php endif; ?>/>
									<label for="THURSDAY" class="inline">Thursday</label>

									<input id="FRIDAY" class="inline" name="recur-weekly&#91;byday&#93;&#91;&#93;&#91;DAY&#93;" type="checkbox" value="FR"  <?php if(in_array('FR',$weeklyBydayArray)):?>checked="checked"<?php endif; ?>/>
									<label for="FRIDAY" class="inline">Friday</label>

									<input id="SATURDAY" class="inline" name="recur-weekly&#91;byday&#93;&#91;&#93;&#91;DAY&#93;" type="checkbox" value="SA"  <?php if(in_array('SA',$weeklyBydayArray)):?>checked="checked"<?php endif; ?>/>
									<label for="SATURDAY" class="inline">Saturday</label>

									<input id="SUNDAY" class="inline" name="recur-weekly&#91;byday&#93;&#91;&#93;&#91;DAY&#93;" type="checkbox" value="SU"  <?php if(in_array('SU',$weeklyBydayArray)):?>checked="checked"<?php endif; ?>/>
									<label for="SUNDAY" class="inline">Sunday</label>
								</p>
							</div>
							<div id="recur-monthly">
								<p>
									<label for="monthly" class="inline">Recur every</label>
									<input id="monthly" name="recur-monthly&#91;interval&#93;" class="number inline" type="text" value="<?php echo $vevent['rrule']['INTERVAL']; ?>" />
									<label for="monthly" class="inline">month(s)</label>
								</p>
								<p>
									<input id="recur-monthly-day" name="recur-monthly&#91;choice&#93;" class="inline" type="radio" value="bymonthday" <?php if($vevent['rrule']['FREQ'] === 'MONTHLY' && isset($vevent['rrule']['BYMONTHDAY'])):?>checked="checked"<?php endif; ?>/>
									<label for="recur-monthly-day" class="inline">Recur on the</label>
									<select id="bymonthday" name="recur-monthly&#91;bymonthday&#93;" class="inline">
										<option value="">-- Please Select --</option>
<?php for($i=1;$i<=31;$i++): ?>
										<option value="<?php echo $i; ?>" <?php if($vevent['rrule']['BYMONTHDAY'] == $i): ?>selected="selected"<?php endif; ?>><?php echo date('jS',strtotime("January $i, 2000"));?></option>
<?php endfor; ?>
										<option value="-1" <?php if($vevent['rrule']['BYMONTHDAY'] == -1): ?>selected="selected"<?php endif; ?>>Last</option>
										<option value="-2" <?php if($vevent['rrule']['BYMONTHDAY'] == -2): ?>selected="selected"<?php endif; ?>>2nd to Last</option>
										<option value="-3" <?php if($vevent['rrule']['BYMONTHDAY'] == -3): ?>selected="selected"<?php endif; ?>>3rd to Last</option>
										<option value="-4" <?php if($vevent['rrule']['BYMONTHDAY'] == -4): ?>selected="selected"<?php endif; ?>>4th to Last</option>
										<option value="-5" <?php if($vevent['rrule']['BYMONTHDAY'] == -5): ?>selected="selected"<?php endif; ?>>5th to Last</option>
									</select>
									<label for="bymonthday" class="inline">Day</label>
								</p>
								<p>
									<input id="recur-monthly-byday" name="recur-monthly&#91;choice&#93;" class="inline" type="radio" value="byday"  <?php if($vevent['rrule']['FREQ'] === 'MONTHLY' && isset($vevent['rrule']['BYDAY']['DAY'])): ?>checked="checked"<?php endif; ?>/>
									<label for="recur-monthly-byday" class="inline">Recur on the</label>
									<select id="recur-monthly-byday-select" name="recur-monthly&#91;byday&#93;" class="inline">
										<option value="">-- Please Select --</option>
<?php for($i=1;$i<=5;$i++): ?>
										<option value="<?php echo $i; ?>" <?php if($vevent['rrule']['BYDAY'][0] == $i): ?>selected="selected"<?php endif; ?>><?php echo date('jS',strtotime("January $i, 2000"));?></option>
<?php endfor; ?>
										<option value="-1" <?php if($vevent['rrule']['BYDAY'][0] == -1): ?>selected="selected"<?php endif; ?>>Last</option>
										<option value="-2" <?php if($vevent['rrule']['BYDAY'][0] == -2): ?>selected="selected"<?php endif; ?>>2nd to Last</option>
										<option value="-3" <?php if($vevent['rrule']['BYDAY'][0] == -3): ?>selected="selected"<?php endif; ?>>3rd to Last</option>
										<option value="-4" <?php if($vevent['rrule']['BYDAY'][0] == -4): ?>selected="selected"<?php endif; ?>>4th to Last</option>
									</select>
									<select id="recur-monthly-byday-suffix" name="recur-monthly&#91;byday-suffix&#93;" class="inline">
										<option value="">-- Please Select --</option>
										<option value="MO" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'MO'): ?>selected="selected"<?php endif; ?>>Monday</option>
										<option value="TU" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'TU'): ?>selected="selected"<?php endif; ?>>Tuesday</option>
										<option value="WE" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'WE'): ?>selected="selected"<?php endif; ?>>Wednesday</option>
										<option value="TH" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'TH'): ?>selected="selected"<?php endif; ?>>Thursday</option>
										<option value="FR" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'FR'): ?>selected="selected"<?php endif; ?>>Friday</option>
										<option value="SA" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'SA'): ?>selected="selected"<?php endif; ?>>Saturday</option>
										<option value="SU" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'SU'): ?>selected="selected"<?php endif; ?>>Sunday</option>
									</select>
								</p>
							</div>
							<div id="recur-yearly">
								<p>
									<label for="yearly" class="inline">Recur every</label>
									<input id="yearly" name="recur-yearly&#91;interval&#93;" class="number inline" type="text" value="<?php echo $vevent['rrule']['INTERVAL']; ?>" />
									<label for="yearly" class="inline">year(s)</label>
								</p>
								<p>
									<input id="recur-yearly-bymonthday"	name="recur-yearly&#91;choice&#93;" class="inline" type="radio" value="bymonthday" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && isset($vevent['rrule']['BYMONTHDAY'])):?>checked="checked"<?php endif; ?>/>
									<label for="recur-yearly-bymonthday" class="inline">Recur on day</label>
									<input id="recur-yearly-bymonthday-input" name="recur-yearly&#91;bymonthday&#93;" class="inline number" type="text" value="<?php if($vevent['rrule']['FREQ'] === 'YEARLY'){ echo $vevent['rrule']['BYMONTHDAY'];} ?>" />
									<label for="recur-yearly-bymonthday-input" class="inline">of</label>
									<select id="recur-yearly-bymonthday-month" name="recur-yearly&#91;bymonth&#93;" class="inline">
										<option value="">-- Please Select --</option>
										<option value="1" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '1')):?>selected="selected"<?php endif;?>>January</option>
										<option value="2" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '2')):?>selected="selected"<?php endif;?>>February</option>
										<option value="3" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '3')):?>selected="selected"<?php endif;?>>March</option>
										<option value="4" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '4')):?>selected="selected"<?php endif;?>>April</option>
										<option value="5" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '5')):?>selected="selected"<?php endif;?>>May</option>
										<option value="6" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '6')):?>selected="selected"<?php endif;?>>June</option>
										<option value="7" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '7')):?>selected="selected"<?php endif;?>>July</option>
										<option value="8" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '8')):?>selected="selected"<?php endif;?>>August</option>
										<option value="9" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '9')):?>selected="selected"<?php endif;?>>September</option>
										<option value="10" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '10')):?>selected="selected"<?php endif;?>>October</option>
										<option value="11" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '11')):?>selected="selected"<?php endif;?>>November</option>
										<option value="12" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '12')):?>selected="selected"<?php endif;?>>December</option>
									</select>
								</p>
								<p>
									<input id="recur-yearly-byday" name="recur-yearly&#91;choice&#93;" class="inline" type="radio" value="byday"  <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && isset($vevent['rrule']['BYDAY'])):?>checked="checked"<?php endif;?>/>
									<label for="recur-yearly-byday" class="inline">Recur on the</label>
									<select id="recur-yearly-byday-select" name="recur-yearly&#91;byday&#93;" class="inline">
										<option value="">-- Please Select --</option>
<?php for($i=1;$i<=5;$i++): ?>
										<option value="<?php echo $i; ?>" <?php if($vevent['rrule']['BYDAY'][0] == $i): ?>selected="selected"<?php endif; ?>><?php echo date('jS',strtotime("January $i, 2000"));?></option>
<?php endfor; ?>
										<option value="-1" <?php if($vevent['rrule']['BYDAY'][0] == -1): ?>selected="selected"<?php endif; ?>>Last</option>
										<option value="-2" <?php if($vevent['rrule']['BYDAY'][0] == -2): ?>selected="selected"<?php endif; ?>>2nd to Last</option>
										<option value="-3" <?php if($vevent['rrule']['BYDAY'][0] == -3): ?>selected="selected"<?php endif; ?>>3rd to Last</option>
										<option value="-4" <?php if($vevent['rrule']['BYDAY'][0] == -4): ?>selected="selected"<?php endif; ?>>4th to Last</option>
									</select>
									<select id="recur-yearly-byday-suffix" name="recur-yearly&#91;byday-suffix&#93;"  class="inline">
										<option value="">-- Please Select --</option>
										<option value="MO" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'MO'): ?>selected="selected"<?php endif; ?>>Monday</option>
										<option value="TU" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'TU'): ?>selected="selected"<?php endif; ?>>Tuesday</option>
										<option value="WE" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'WE'): ?>selected="selected"<?php endif; ?>>Wednesday</option>
										<option value="TH" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'TH'): ?>selected="selected"<?php endif; ?>>Thursday</option>
										<option value="FR" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'FR'): ?>selected="selected"<?php endif; ?>>Friday</option>
										<option value="SA" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'SA'): ?>selected="selected"<?php endif; ?>>Saturday</option>
										<option value="SU" <?php if($vevent['rrule']['BYDAY']['DAY'] == 'SU'): ?>selected="selected"<?php endif; ?>>Sunday</option>
									</select>
									<label for="recur-yearly-byday-month" class="inline">of</label>
									<select id="recur-yearly-byday-month" name="recur-yearly&#91;bymonth&#93;" class="inline">
										<option value="">-- Please Select --</option>
										<option value="1" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '1')):?>selected="selected"<?php endif;?>>January</option>
										<option value="2" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '2')):?>selected="selected"<?php endif;?>>February</option>
										<option value="3" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '3')):?>selected="selected"<?php endif;?>>March</option>
										<option value="4" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '4')):?>selected="selected"<?php endif;?>>April</option>
										<option value="5" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '5')):?>selected="selected"<?php endif;?>>May</option>
										<option value="6" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '6')):?>selected="selected"<?php endif;?>>June</option>
										<option value="7" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '7')):?>selected="selected"<?php endif;?>>July</option>
										<option value="8" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '8')):?>selected="selected"<?php endif;?>>August</option>
										<option value="9" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '9')):?>selected="selected"<?php endif;?>>September</option>
										<option value="10" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '10')):?>selected="selected"<?php endif;?>>October</option>
										<option value="11" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '11')):?>selected="selected"<?php endif;?>>November</option>
										<option value="12" <?php if($vevent['rrule']['FREQ'] === 'YEARLY' && ($vevent['rrule']['BYMONTH'] == '12')):?>selected="selected"<?php endif;?>>December</option>
									</select>
								</p>
								<p>
									<input id="recur-yearly-byyearday" name="recur-yearly&#91;choice&#93;" class="inline" type="radio" value="byyearday" <?php if(isset($vevent['rrule']['BYYEARDAY'])):?>checked="checked"<?php endif; ?>/>
									<label for="recur-yearly-byyearday" class="inline">Recur on day #</label>
									<input id="recur-yearly-byyearday-input" name="recur-yearly&#91;byyearday&#93;" class="number inline" type="text" value="<?php echo $vevent['rrule']['BYYEARDAY']; ?>" />
									<label for="recur-yearly-byyearday-input" class="inline">of the year</label>
								</p>
							</div>
						</fieldset>
						<fieldset id="rrange">
							<legend>Range</legend>
							<p>
								<input id="no-ending" class="inline" name="recurrence-range" type="radio" value="" />
								<label for="no-ending" class="inline">No ending date</label>
							</p>
							<p>
								<input id="recurrence-range-count" class="inline" name="recurrence-range" type="radio" value="count" <?php if(isset($vevent['rrule']['COUNT'])): ?>checked="checked"<?php endif; ?>/>
								<label for="recurrence-range-count" class="inline">End After</label>
								<input id="recurrence-range-count-input" name="count" class="inline number" type="text"  value="<?php echo $vevent['rrule']['COUNT']; ?>" />
								<label for="recurrence-range-count-input" class="inline">occurrence(s)</label>
							</p>
							<p>
								<input id="recurrence-range-until" class="inline" name="recurrence-range" type="radio" value="until"  <?php if(isset($vevent['rrule']['UNTIL'])): ?>checked="checked"<?php endif; ?>/>
								<label for="recurrence-range-until" class="inline">End on</label>
								<input id="recurrence-range-until-input" class="inline date" name="until" type="text" value="<?php echo (isset($vevent['rrule']['UNTIL']))?date('F j, Y',strtotime("{$vevent['rrule']['UNTIL']['year']}-{$vevent['rrule']['UNTIL']['month']}-{$vevent['rrule']['UNTIL']['day']}")):null; ?>" />
							</p>
						</fieldset>
					</fieldset>
					<input type="submit" name="submit" value="submit" />
				</fieldset>
			</form>
		</div>
		<div id="event-view">
			<table summary="">
				<caption>Calendar Events</caption>
				<thead>
					<tr>
						<th>Summary</th>
						<th>Recurs</th>
						<th>Start</th>
						<th>End</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Summary</th>
						<th>Recurs</th>
						<th>Start</th>
						<th>End</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($events AS $event):?>
					<tr>
						<td><a href="./admin/calendar/<?php echo $event['uid']; ?>" title="Edit This Event"><?php echo $event['summary'];?></a></td>
						<td><?php echo ($event['recurs'])?'yes':'no';?></td>
						<td><?php echo date('r',$event['start']);?></td>
						<td><?php echo date('r',$event['end']);?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
