<?php
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.color.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.metadata.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.validate.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/additional-methods.js"></script>
	<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function(){
			$("form").validate();
			$("#nopenoworkstudy").change(function (){
				$("#workstudy").val("").animate({backgroundColor:"yellow"}).animate({backgroundColor:'transparent'},1000);
				});
			$("#workstudy").change(function (){
				if(!$("#workstudy").val()){
				$("#nopenoworkstudy").attr("checked","checked");
				}
				else {
				$("#yesihaveworkstudy").attr("checked","checked");
				}
				});
			});
		function tagSkills(){
				set = $(this).text();
				current = jQuery.trim($("#skills").val());
				if(current == ''){
					$("#skills").val(set);
					$(this).addClass("selected");
				}
				else {
					var wordidx = $("#skills").val().split(",");
					var itsthere = false;
					var position;
					for(var i = 0, n = wordidx.length; i < n; ++i) {
						wordidx[i] = jQuery.trim(wordidx[i]);
						if(wordidx[i] == $(this).text()) {
							itsthere = true;
							position = i;
						}
					}
					if(itsthere) {
						wordidx.splice(position,1);
						$(this).removeClass("chosen");
					}
					else {
						wordidx.push(set);
						$(this).addClass("chosen");
					}
					$("#skills").val((wordidx.join(", ")));
				}
		}
		$(document).ready(function(){
			$("#skills-tags span").click(tagSkills);
			$("#skills-tags span").click();
			$("#skills-tags span").click();
		});
	/*]]>*/
	</script>
