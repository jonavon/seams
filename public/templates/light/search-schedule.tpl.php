<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
		<div id="aside" class="concol">
<?php include '_search-menu.tpl.php'; ?>
		</div>
		<div id="schedule-searching" class="concol">
<?php if(isset($_SESSION['schedulesearch'])): ?>
			<form method="post" action="./application/results">
				<p><input type="submit" name="search_schedule" value="Search Application Stack" /></p>
			</form>
<?php endif; ?>
<?php include 'edit-schedule.tpl.php'; ?>
<?php if(isset($_SESSION['schedulesearch'])): ?>
			<form method="post" action="./application/results">
				<p><input type="submit" name="search_schedule" value="Search Application Stack" /></p>
			</form>
<?php endif; ?>
		</div>
