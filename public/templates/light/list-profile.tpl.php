<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */

$cnt = count($profiles);

?>
		<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
		</div>
		<div id="<?php echo $objid; ?>-list" class="concol">
			<table class="searchable" summary="List of profiles">	
				<caption><?php echo "($cnt) $objid"; ?> List</caption>
				<thead>
					<tr>
						<th>PID</th>
						<th>First Name</th>
						<th>Last Name</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>PID</th>
						<th>First Name</th>
						<th>Last Name</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($profiles as $profile): ?>				
					<tr>
						<td><a href="./profile/edit/<?php echo $profile['pid'];?>" title="Edit this Profile"><?php echo $profile['pid']; ?></a></td>
						<td><?php echo $profile['firstname']; ?></td>
						<td><?php echo $profile['lastname']; ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>
