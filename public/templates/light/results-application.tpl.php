<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
/*
$jobs[] = array(
	'id' => '0',
	'label' => 'STUDENT WORKER'
);
$jobs[] = array(
	'id' => '0',
	'label' => 'Just a job'
);
$applications = array();
$applications[] = array(
	'id' => '143glhjk',
	'pid' => 'bugbunny',
	'firstname' => 'Bugs',
	'lastname' => 'Bunny',
	'expire' => 1238137112,
	'pooled' => 2
);
*/
$firstapp = current($applications);
$_apptotal = count($applications);
?>
		<div id="aside" class="concol">
<?php include '_search-menu.tpl.php'; ?>
		</div>
<?php  if($_apptotal > 0):	?>				
		<div id="application-search-results" class="concol">
			<form method="post" action="./job/submit">
				<fieldset>
					<legend>Pool Candidates</legend>
					<p><a href="./job/add" title="Add a Pool">Create a New Pool</a></p>
					<fieldset>
						<legend>Which Pool?</legend>
						<label for="job">Your Pool</label>
						<select id="job" name="job">
<?php if(count($jobs) === 0):?>
							<option value="">New Pool</option>
<?php endif;?>
<?php foreach($jobs as $job): ?>
							<option value="<?php echo $job['id']; ?>"><?php echo $job['label']; ?></option>
<?php endforeach; ?>
						</select>
						<input type="submit" name="submit" value="submit" />
					</fieldset>
					<table class="searchable" summary="List of user applications">
						<caption>
							<?php echo $_apptotal; ?> Applications
<?php if(isset($scheduleToHuman)): ?>
								<br/>
								<span> Schedule: <?php echo $scheduleToHuman['text']; ?> Duration: <?php echo Duration::toString(((int)$durationInSeconds)); ?></span>
<?php endif; ?>
<?php if(isset($_GET['skills'])): ?>
								<br />
								<span> Skills: <?php echo htmlspecialchars($_GET['skills']); ?>
<?php endif; ?>
<?php if(isset($searchtext)): ?>
								<br/>
								<span> Search Text: <?php echo $searchtext; ?> </span>
<?php endif; ?>
						</caption>
						<thead>
							<tr>
								<th>&nbsp;</th>
								<th>Name</th>
								<th>Application Status</th>
								<th>In another pool</th>
<?php if(isset($firstapp['overlap'])): ?>
								<th>Overlap</th>
<?php endif; ?>
<?php if(isset($firstapp['snippit'])):?>
								<th>Snippet</th>
<?php endif; ?>
							</tr>
						</thead>
						<tfoot>
							<tr>
								<th>&nbsp;</th>
								<th>Name</th>
								<th>Application Status</th>
								<th>In another pool</th>
<?php if(isset($firstapp['overlap'])): ?>
								<th>Overlap</th>
<?php endif; ?>
<?php if(isset($firstapp['snippit'])):?>
								<th>Snippet</th>
<?php endif; ?>
							</tr>
						</tfoot>
						<tbody>
<?php foreach($applications as $application): ?>
							<tr>
								<td><input type="checkbox" name="applications&#91;&#93;" value="<?php echo $application['id']; ?>" /></td>
								<td><a href="./application/view/<?php echo $application['id']; ?>" title="Application of <?php echo $application['name']; ?>" rel="external"><?php echo $application['name']; ?></a></td>
								<td><?php echo ($application['modified'] > $lastperiod)?'current':'last'; ?></td>
								<td><?php echo ((int)$application['pooled'] > 1)?'yes':'no'; ?></td>
<?php if(isset($firstapp['overlap'])): ?>
								<td><?php echo round(min(1,((int)$application['overlap'] / (int)$totalselected)) * 100) . '%'; ?></td>
<?php endif; ?>
<?php if(isset($firstapp['snippit'])):?>
								<td><?php echo preg_replace('/(\w+):/','<br /><em>$1</em>: ',htmlentities(stripslashes($application['snippit']))); ?></td>
<?php endif; ?>
							</tr>
<?php endforeach; ?>
						</tbody>
					</table>
				</fieldset>
			</form>
		</div>
<?php else: ?>
				<div id="search-quick" class="concol">
<?php include '_simple-search.tpl.php'; ?>
				</div>
<?php endif; ?>
