<?php
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
?>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.color.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.metadata.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/jquery.validate.js"></script>
	<script type="text/javascript" src="./<?php echo $template_dir; ?>/assets/libs/js/additional-methods.js"></script>
	<script type="text/javascript">
	//<![CDATA[
	$(document).ready(function(){
			$("form").validate();
			_showRecurrenceSection();
			$("#rrule,#rrange").toggle($("#recurrence").attr("checked") == true);
			toggleElements("#all-day","#datetime input.time");

			$("#all-day").change(function(){
				toggleElements($(this),"#datetime input.time");
			});
			$("#recurrence").change(function(){
				$("#rrule,#rrange").toggle($("#recurrence").attr("checked") == true);
			});
			$("#freq").change(function(){
				_showRecurrenceSection();
			});
	});

	function _showRecurrenceSection() {
		$("#rrule div input,#rrule div select").attr("disabled",true);
		$("#rrule div").hide();
		switch($("#freq").val()) {
			case "DAILY":
				$("#recur-daily input, #recur-daily select").attr("disabled",false);
				$("#recur-daily").slideDown(600);
				break;
			case "WEEKLY":
				$("#recur-weekly input, #recur-weekly select").attr("disabled",false);
				$("#recur-weekly").slideDown(600);
				break;
			case "MONTHLY":
				$("#recur-monthly input, #recur-monthly select").attr("disabled",false);
				$("#recur-monthly").slideDown(600);
				break;
			case "YEARLY":
				$("#recur-yearly input, #recur-yearly select").attr("disabled",false);
				$("#recur-yearly").slideDown(600);
				break;
			default:
		}
	}
	function toggleElements(selector,elements) {
		if($(selector).attr("checked")) {
			$(elements).attr("disabled",true);
		}
		else {
			$(elements).attr("disabled",false);
		}
	}
	//]]>
	</script>
