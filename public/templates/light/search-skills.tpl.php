<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
 /*
$tags[] = array(
	'tag' => 'ground',
	'count' => 8
);
$tags[] = array(
	'tag' => 'border',
	'count' => 15
);
$tags[] = array(
	'tag' => 'streets',
	'count' => 5
);
$tags[] = array(
	'tag' => 'cairo',
	'count' => 11
);
$tags[] = array(
	'tag' => 'aid',
	'count' => 33
);
$tags[] = array(
	'tag' => 'how deep',
	'count' => 17
);
$tags[] = array(
	'tag' => 'governments',
	'count' => 22
);
$tags[] = array(
	'tag' => 'rogue',
	'count' => 6
);
$tags[] = array(
	'tag' => 'ronin',
	'count' => 6
);
$tags[] = array(
	'tag' => 'broadcast',
	'count' => 6
);
$tags[] = array(
	'tag' => 'permission',
	'count' => 76
);
$tags[] = array(
	'tag' => 'authority',
	'count' => 9
);
$tags[] = array(
	'tag' => 'attack',
	'count' => 8
);
$tags[] = array(
	'tag' => 'ubuntu',
	'count' => 15
);
$tags[] = array(
	'tag' => 'writer',
	'count' => 5
);
$tags[] = array(
	'tag' => 'thanks',
	'count' => 11
);
$tags[] = array(
	'tag' => 'honolulu',
	'count' => 33
);
$tags[] = array(
	'tag' => 'evening',
	'count' => 17
);
$tags[] = array(
	'tag' => 'public statement',
	'count' => 22
);
$tags[] = array(
	'tag' => 'blonde',
	'count' => 6
);
$tags[] = array(
	'tag' => 'durable',
	'count' => 6
);
$tags[] = array(
	'tag' => 'king of jordon',
	'count' => 6
);
$tags[] = array(
	'tag' => 'coca-cola',
	'count' => 76
);
$tags[] = array(
	'tag' => 'everything',
	'count' => 9
);
$tags[] = array(
	'tag' => 'word processing',
	'count' => 8
);
$tags[] = array(
	'tag' => 'farming',
	'count' => 15
);
$tags[] = array(
	'tag' => 'computer',
	'count' => 5
);
$tags[] = array(
	'tag' => 'music',
	'count' => 11
);
$tags[] = array(
	'tag' => 'running',
	'count' => 33
);
$tags[] = array(
	'tag' => 'peanut butter',
	'count' => 17
);
$tags[] = array(
	'tag' => 'recycle',
	'count' => 22
);
$tags[] = array(
	'tag' => 'economy',
	'count' => 6
);
$tags[] = array(
	'tag' => 'water',
	'count' => 6
);
$tags[] = array(
	'tag' => 'sunflower',
	'count' => 6
);
$tags[] = array(
	'tag' => 'mr. pibb',
	'count' => 76
);
$tags[] = array(
	'tag' => 'php',
	'count' => 9
);
*/
$_GET['skills'] = (isset($_GET['skills']))?$_GET['skills']:null;
?>
		<div id="aside" class="concol">
<?php include '_search-menu.tpl.php'; ?>
		</div>
		<div id="skillset-search" class="concol">
			<h3>Skill List</h3>
			<ul>
<?php 
			$min_size = 80; // Minimum percentage of the font size.
			$max_size = 185; // Maximum percentage font size.
			// Determine the highest count from the tags array.
			$highest_value = array_reduce($tags,create_function('$i,$j','return (int)max($i,(int)$j["count"]);'));
			// Determine the lowest count from the tags array.
			$lowest_value = array_reduce($tags,create_function('$i,$j','return (int)min($i,(int)$j["count"]);'),$highest_value);
			foreach($tags as $tag):
				$size = $min_size + (((int)$tag['count'] - $lowest_value) * (($max_size - $min_size)/($highest_value - $lowest_value)));
				$size = ceil($size);
?>
				
				<li>
					<a href="./application/results?skills=<?php echo urlencode(((bool)($_GET['skills']))?$_GET['skills'] . ',' . $tag['tag']:$tag['tag']); ?>" class="tag" style="font-size: <?php echo $size; ?>%"><?php echo $tag['tag']; ?></a>
<?php
/*
					<span class="tagcount"><?php echo $tag['count']; ?></span>

*/
?>
				</li>
<?php endforeach; ?>
			</ul>
		</div>
