<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * [Filename.php]
 *
 * [Description for this file]
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version $Revision:$ $Date:$
 * @copyright Copyright (c) 2007 University Libraries, Virginia Tech
 * @package [package name]
 */
include_once('markdown.php');
$poolcount = count($pool);
?>
<?php if(isset($objid)):?>
		<div id="job-candidate-list">
			<div>
				<a class="button" href="./job/delete/<?php echo $job['id']; ?>" title="Delete this Pool"><span>Delete Pool</span></a>
<?php if($poolcount > 0): ?>
				<a class="button" href="./pool/print/<?php echo $job['id']; ?>" title="print the applications in this pool"><span>Contact Pool</span></a>
<?php endif; ?>
			</div>
			<div class="jobs">
				<h2><a href="./job/edit/<?php echo $job['id']; ?>" title="Edit This Pool">Pool ID #<?php echo $job['id']; ?> View</a></h2>
				<div>
					<h3 id="job-label"><?php echo $job['label']; ?></h3>
					<div id="job-description"><?php echo Markdown($job['description']); ?></div>
				</div>
			</div>
<?php if($poolcount > 0): ?>
			<table summary="List of candidates for Pool <?php echo $job['id']; ?>">
				<caption><?php echo $poolcount; ?> Candidates</caption>
				<thead>
					<tr>
						<th>Application</th>
						<th>Email</th>
						<th>Expires</th>
						<th>Pooled</th>
						<th>Remove</th>
					</tr>
				</thead>
				<tfoot>

					<tr>
						<th>Application</th>
						<th>Email</th>
						<th>Expires</th>
						<th>Pooled</th>
						<th>Remove</th>
					</tr>
				</tfoot>

				<tbody>
<?php foreach($pool as $candidate): ?>
					<tr>
						<td><a href="./application/view/<?php echo $candidate['id']; ?>?hireid=<?php echo $job['id']; ?>" title="View this application" rel="external"><?php echo $candidate['name']; ?></a></td>
						<td><a href="mailto:<?php echo $candidate['pid']; ?>@vt.edu" title="E-mail"><?php echo $candidate['pid']; ?>@vt.edu</a></td>
						<td><?php echo date('F j, Y',$candidate['expire']); ?></td>
						<td><?php echo $candidate['pooled']; ?></td>
						<td><a href="./job/unpool/<?php echo"{$job['id']}/{$candidate['id']}"; ?>">remove</a></td>

					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
<?php endif; ?>
<?php if(!empty($positions)):?>
			<table summary="Employees">
				<caption><?php echo count($positions); ?> Positions</caption>
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Status</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Status</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($positions as $position): ?>
					<tr>
						<td><a href="./application/view/<?php echo current($position->applications); ?>" rel="external"><?php echo $position->employee_name; ?></a></td>
						<td><a href="mailto:<?php echo $position->employee; ?>@vt.edu" title="E-mail"><?php echo $position->employee; ?>@vt.edu</a></td>
						<td><?php echo "{$position->employee_type}"; ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
<?php endif; ?>
		</div>
<?php endif; ?>
