<?php
if(!defined('SEAS_ROOT')){
	exit('This script is not meant for direct access!');
}
/**
 * %FILENAME%
 *
 * %DESCRIPTION%
 *
 * @author Jonavon Wilcox <jowilcox@vt.edu>
 * @version %REVISION% %DATE%
 * @copyright Copyright (c) 2009 University Libraries, Virginia Tech
 * @package %PACKAGE%
 */

?>
		<div id="aside" class="concol">
<?php include '_admin-menu.tpl.php'; ?>
		</div>
		<div id="manage-jobs" class="concol">
			<table class="searchable" summary="List of profiles">	
				<caption>Jobs List (<?php echo count($jobs); ?>)</caption>
				<thead>
					<tr>
						<th>Job</th>
						<th>Label</th>
						<th>Description</th>
						<th>Supervisor</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th>Job</th>
						<th>Label</th>
						<th>Description</th>
						<th>Supervisor</th>
					</tr>
				</tfoot>
				<tbody>
<?php foreach($jobs as $job): ?>				
					<tr>
						<td><?php echo $job['id']; ?></td>
						<td><a href="./job/view/<?php echo $job['id']; ?>"><?php echo $job['label']; ?></a></td>
						<td><?php echo $job['description']; ?></td>
						<td><?php echo $job['supervisor']; ?></td>
					</tr>
<?php endforeach; ?>
				</tbody>
			</table>
		</div>
