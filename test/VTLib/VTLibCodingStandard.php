<?php
/**
 * PHP_CodeSniffer Coding Standard.
 *
 * PHP version 5
 *
 * @category  PHP
 * @package   PHP_CodeSniffer
 * @author    Greg Sherwood <gsherwood@squiz.net>
 * @author    Marc McIntyre <mmcintyre@squiz.net>
 * @copyright 2009 Copyright (c) Virginia Tech Library, Library Systems
 * @license   http://matrix.squiz.net/developer/tools/php_cs/licence BSD Licence
 * @link      http://pear.php.net/package/PHP_CodeSniffer
 */

if (class_exists('PHP_CodeSniffer_Standards_CodingStandard', true) === false) {
	throw new PHP_CodeSniffer_Exception('Class PHP_CodeSniffer_Standards_CodingStandard not found');
}

/**
 * PHP_CodeSniffer Coding Standard.
 *
 * @category  PHP
 * @package   PHP_CodeSniffer
 * @author    Greg Sherwood <gsherwood@squiz.net>
 * @author    Marc McIntyre <mmcintyre@squiz.net>
 * @copyright 2006 Squiz Pty Ltd (ABN 77 084 670 600)
 * @license   http://matrix.squiz.net/developer/tools/php_cs/licence BSD Licence
 * @version   Release: 1.2.1
 * @link      http://pear.php.net/package/PHP_CodeSniffer
 */
class PHP_CodeSniffer_Standards_VTLib_VTLibCodingStandard extends PHP_CodeSniffer_Standards_CodingStandard
{


	/**
	 * Return a list of external sniffs to include with this standard.
	 *
	 * The PHP_CodeSniffer standard combines the PEAR and Squiz standards
	 * but removes some sniffs from the Squiz standard that clash with
	 * those in the PEAR standard.
	 *
	 * @return array
	 */
	public function getIncludedSniffs() {
		return array(
				'Zend',
				'Generic',
				'Generic/Sniffs/Functions/OpeningFunctionBraceKernighanRitchieSniff.php',
				'Generic/Sniffs/PHP/DisallowShortOpenTagSniff.php',
				'PEAR/Sniffs/Files/LineEndingsSniff.php',
				'PEAR/Sniffs/Functions/FunctionCallArgumentSpacingSniff.php',
				'PEAR/Sniffs/Functions/FunctionCallSignatureSniff.php',
				'PEAR/Sniffs/Functions/ValidDefaultValueSniff.php',
				);

	}//end getIncludedSniffs()


	/**
	 * Return a list of external sniffs to exclude from this standard.
	 *
	 * The PHP_CodeSniffer standard combines the PEAR and Squiz standards
	 * but removes some sniffs from the Squiz standard that clash with
	 * those in the PEAR standard.
	 *
	 * @return array
	 */
	public function getExcludedSniffs() {
		return array(
				'Zend/Sniffs/NamingConventions/ValidVariableNameSniff.php',
				'Generic/Sniffs/Functions/OpeningFunctionBraceBsdAllmanSniff.php',
				'Generic/Sniffs/WhiteSpace/DisallowTabIndentSniff.php',
				'PEAR/Sniffs/Classes/ClassDeclarationSniff.php',
				'PEAR/Sniffs/WhiteSpace/ScopeClosingBraceSniff.php',
				'Zend/Sniffs/Files/LineLengthSniff.php',
				'Generic/Sniffs/Files/LineLengthSniff.php',
				'Generic/Sniffs/VersionControl/SubversionPropertiesSniff.php',
				'Generic/Sniffs/WhiteSpace/DisallowTabIndentSniff.php',
				'Generic/Sniffs/WhiteSpace/ScopeIndentSniff.php',
				'Generic/Sniffs/NamingConventions/UpperCaseConstantNameSniff.php',
				'Generic/Sniffs/PHP/LowerCaseConstantSniff.php',
				'Generic/Sniffs/PHP/UpperCaseConstantSniff.php',
				'Generic/Sniffs/Formatting/MultipleStatementAlignmentSniff.php',
				'Generic/Sniffs/Formatting/SpaceAfterCastSniff.php',
				'Generic/Sniffs/ControlStructures/InlineControlStructureSniff.php',
				'PEAR/Sniffs/ControlStructures/ControlSignatureSniff.php',
				);

	}//end getExcludedSniffs()


}//end class
