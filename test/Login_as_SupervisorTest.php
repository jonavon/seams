<?php

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

class Example extends PHPUnit_Extensions_SeleniumTestCase
{
  function setUp()
  {
    $this->setBrowser("*firefox");
    $this->setBrowserUrl("http://nebuchadnezza.tower.lib.vt.edu/");
  }

  function testMyTestCase()
  {
    $this->open("/projects/seams/public/");
    $this->click("link=Login");
    $this->waitForPageToLoad("30000");
    $this->type("username", "x");
    $this->click("//input[@name='login']");
    $this->waitForPageToLoad("30000");
    $this->click("link=Logout");
    $this->waitForPageToLoad("30000");
  }
}
?>
